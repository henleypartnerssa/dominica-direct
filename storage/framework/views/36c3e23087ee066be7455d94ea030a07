<?php $__env->startSection('title', 'Medical Details'); ?>

<?php $__env->startSection('stepper'); ?>
    <?php echo $__env->make('modules._stepping', $data = App\Util\AppHelpers::stepperLinks($profile->profile_id, $profile->application->application_id), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<h2 class="heading admin"><?php echo e($programName); ?> - <?php echo e($profile->first_name); ?> <?php echo e($profile->last_name); ?></h2>

<div class="content">
    <p id="admin">This Medical Certificate Form is to be completed in English by a registered medical practitioner. Please supply additional details on a separate sheet if necessary. This must include HIV (AIDS) test results for each adult and child (see Section 7 below). The Medical Certificate confirmation must not be older than three months. It must be properly completed, signed, dated and stamped by the doctor.One form for each person (including children) is to be completed. Note that the medical practitioner must ask for evidence of photographic identification (such as a passport or ID card).</p>

    <?php echo $__env->make('modules._data_forms_guide', $data = ['page' => 'medical'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <input type="hidden" id="profile_id" value="<?php echo e($profile->profile_id); ?>" />

    <div class="ui family-member form">
    <?php echo Form::open(); ?>

        <input type="hidden" name="save" id="save" value="0" />
        <div class="field">
            <label>Name of Physician or Doctor</label>
            <input type="text" name="doctor_name" placeholder="Name of you Physician or Doctor" value="<?php echo e(isset($doc->doctor_name) ? $doc->doctor_name : ''); ?>">
        </div>

        <div class="field">
            <label>Address of main Physician or Doctor</label>

            <div class="two fields">
                <div class="field">
                    <input type="text" name="doc_street1" placeholder="address line 1" value="<?php echo e(isset($address->street1) ? $address->street1 : ''); ?>">
                </div>
                <div class="field">
                    <input type="text" name="doc_street2" placeholder="address line 2 (optional)" value="<?php echo e(isset($address->street2) ? $address->street2 : ''); ?>">
                </div>
            </div>

            <div class="three fields">
                <div class="field">
                    <?php if(!empty($address)): ?>
                        <?php echo $__env->make('modules._countries_dropdown', $data = ['name' => 'doc_country', 'title' => 'country', 'value' => $address->country], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php else: ?>
                        <?php echo $__env->make('modules._countries_dropdown', $data = ['name' => 'doc_country', 'title' => 'country'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>
                <div class="field">
                    <input type="text" name="doc_town" placeholder="town/city" value="<?php echo e(isset($address->town) ? $address->town : ''); ?>">
                </div>
                <div class="field">
                    <input type="text" name="doc_post_code" placeholder="postal code" value="<?php echo e(isset($address->postal_code) ? $address->postal_code : ''); ?>">
                </div>
            </div>
        </div>

        <div class="field">
            <label>The medical examiner are required to ask the following questions or to review them if they have already been answered previously.</label>
            <div class="ui toggle tb checkbox">
                <input type="checkbox" name="tb" <?php echo e((!empty($doc) && $doc->tb === true) ? 'checked' : ''); ?>>
                <label>Tuberculosis</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle hp checkbox" <?php echo e((!empty($doc) && $doc->hp === true) ? 'checked' : ''); ?>>
                <input type="checkbox" name="hp">
                <label>Hepatitis</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle tp checkbox">
                <input type="checkbox" name="tp" <?php echo e((!empty($doc) && $doc->tp === true) ? 'checked' : ''); ?>>
                <label>Typhoid</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle other_comm checkbox">
                <input type="checkbox" name="other_comm" <?php echo e((!empty($doc) && $doc->other_comm === true) ? 'checked' : ''); ?>>
                <label>Any Other Communicable Disease</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle other_heart checkbox">
                <input type="checkbox" name="other_heart" <?php echo e((!empty($doc) && $doc->other_heart === true) ? 'checked' : ''); ?>>
                <label>Any Other Heart conditions (including congenital defects)</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle stroke checkbox" <?php echo e((!empty($doc) && $doc->stroke === true) ? 'checked' : ''); ?>>
                <input type="checkbox" name="stroke">
                <label>Stroke</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle idd checkbox">
                <input type="checkbox" name="idd" <?php echo e((!empty($doc) && $doc->idd === true) ? 'checked' : ''); ?>>
                <label>Any Immune Deficiency Disease</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle cancer checkbox">
                <input type="checkbox" name="cancer" <?php echo e((!empty($doc) && $doc->cancer === true) ? 'checked' : ''); ?>>
                <label>Cancer</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle aids_hiv checkbox">
                <input type="checkbox" name="aids_hiv" <?php echo e((!empty($doc) && $doc->aids_hiv === true) ? 'checked' : ''); ?>>
                <label>AIDS / HIV</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle b2 checkbox">
                <input type="checkbox" name="b2" <?php echo e((!empty($doc) && $doc->b2 === true) ? 'checked' : ''); ?>>
                <label>Are you currently taking any prescribed medication?</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle b3 checkbox">
                <input type="checkbox" name="b3" <?php echo e((!empty($doc) && $doc->b3 === true) ? 'checked' : ''); ?>>
                <label>Do you currently have any other serious health problems?</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle b4 checkbox">
                <input type="checkbox" name="b4" <?php echo e((!empty($doc) && $doc->b4 === true) ? 'checked' : ''); ?>>
                <label>Have you been hospitalized in the last 5 years?</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle b5 checkbox">
                <input type="checkbox" name="b5" <?php echo e((!empty($doc) && $doc->b5 === true) ? 'checked' : ''); ?>>
                <label>Have you visited a doctor in the last three years for anything other than a routine check-up</label>
            </div>
        </div>

        <div class="field">
            <div class="ui toggle b6 checkbox">
                <input type="checkbox" name="b6" <?php echo e((!empty($doc) && $doc->b6 === true) ? 'checked' : ''); ?>>
                <label>For female applicants - Are you pregnant ?</label>
            </div>
        </div>

        <div class="field" style="display:none" id="b6_date">
            <label>What is the expected date of birth ?</label>
                <?php echo $__env->make('modules._inline_datebox', $data = ['b6_date', (!empty($doc)) ? $doc->b6_date : ''], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>

        <div class="field">
            <div class="ui toggle b7 checkbox">
                <input type="checkbox" name="b7" <?php echo e((!empty($doc) && $doc->b7 === true) ? 'checked' : ''); ?>>
                <label>Are you dependant on alcohol or drugs (including narcotics)?</label>
            </div>
        </div>

        <div class="field">
            <label>Any further details which may be medically relevant</label>
            <textarea name="b8"><?php echo e(isset($doc->b8) ? $doc->b8 : ''); ?></textarea>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="<?php echo e($backLink); ?>" class="ui next fluid blue left labeled icon button">
                    Back to Personal Data
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
                <button class="ui save-data fluid grey left labeled icon button">
                    Save Data
                    <i class="upload icon"></i>
                </button>
            </div>
            <div class="field">
                <button class="ui next fluid blue right labeled icon button">
                    Next Form
                    <i class="large angle right icon"></i>
                </button>
            </div>
        </div>

    <?php echo Form::close(); ?>

    </div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('modal'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(URL::asset('js/antigua_medical.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app_panel_no_right', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>