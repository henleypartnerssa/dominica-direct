<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageType extends Model
{

    protected $table        = 'language_types';
    protected $primaryKey   = 'language_type_id';

    protected $fillable     = ['type', 'code'];

    /**
     * Has many languages
     *
     * @return App\Language Language Model
     */
    public function languages() {
        return $this->hasMany('App\Languages');
    }

}
