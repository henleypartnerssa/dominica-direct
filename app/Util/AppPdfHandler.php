<?php

namespace App\Util;

use Storage;
use Auth;
use App\Programs\Antigua\AntiguaProgramCosts;
use App\Programs\Antigua\AntiguaAb1Fields;
use App\Programs\Antigua\AntiguaAb2Fields;
use App\Programs\Antigua\AntiguaAb3Fields;
use App\Programs\Antigua\AntiguaAb4Fields;
use App\Programs\Antigua\AntiguaAb5Fields;
use App\Programs\Antigua\AntiguaAblFields;
use App\Programs\Antigua\AntiguaAbmFields;
use App\Application;
use Carbon\Carbon;

class AppPdfHandler {

    private $baseDir;
    private $templateBaseDir;
    private $userDir;

    private $type;
    private $programType;
    private $template;
    private $output;
    private $caseId;
    private $profileID;

    public function __construct($type, $template = null, $case_id = null, $profile_id = null, $hasApp = false, $appType = null) {
        // Init Properties
        $this->baseDir          = storage_path('app');
        $this->templateBaseDir  = $this->baseDir.'/'.'templates/';
        $this->caseId           = $case_id;
        $this->programType      = $appType;
        $this->profileID        = $profile_id;

        // Call Setters
        $this->setTemplate($type, $template);
        $this->setType($type);
        $this->setUserDir();

    }

    private function setTemplate($type, $template = null) {
        if ($type === "quotations") {
            $this->template = $this->templateBaseDir.'quotation_v1.0.pdf';
        } else {
            $this->template = $this->templateBaseDir.$template.'.pdf';
        }
    }

    private function setType($type) {
        $this->type = $type;
    }

    private function setUserDir() {
        $authUser           = Auth::user();
        $this->userDir      = $this->baseDir.'/users/'.$authUser->user_id.'/';
    }

    public function setOutputFile($fileName, $case = null) {
        if ($this->type === 'quotations') {
            $this->output = $this->userDir.'quotations/'.$fileName;
        } else {
            $this->output = $this->userDir.'profiles/'.$this->profileID.'/downloads/'.$fileName;
        }
    }

    private function jsonTicket() {

        $ticket = null;

        if ($this->type === "quotations") {
            $ticket = [
                'type'          => $this->type,
                'template'      => $this->template,
                'dataFilePath'  => $this->output,
                'fields'        => $this->quotaionResults()
            ];
        } else {
            $ticket = [
                'type'          => $this->type,
                'template'      => $this->template,
                'dataFilePath'  => $this->output,
                'fields'        => $this->getProgramFields()
            ];
        }

        return json_encode(json_encode($ticket, JSON_UNESCAPED_SLASHES), JSON_UNESCAPED_SLASHES);
    }

    private function quotaionResults() {
        $quotaion   = new AntiguaProgramCosts($this->programType);
        $user       = Auth::user();

        return [
            Carbon::now()->format("jS F Y"),
            "Antigua and Barbuda Citizenship by ".$this->programType." Program",
            $user->name.' '.$user->surname,
            ($quotaion->spouse > 0) ? "Yes" : "No",
            strval($quotaion->child_0_11),
            strval($quotaion->child_12 + $quotaion->child_13_15 + $quotaion->child_16_17),
            strval($quotaion->child_16_17),
            strval($quotaion->child_18_25),
            strval($quotaion->parent_65),
            "USD ".str_replace('.', ',', strval($quotaion->nonRefContributions())),
            "USD ".str_replace('.', ',', strval($quotaion->RealEstatePurchase())),
            "USD ".str_replace('.', ',', strval($quotaion->finInstruments())),
            "USD ".str_replace('.', ',', $quotaion->qRequirementsTotal()),
            "USD ".str_replace('.', ',', strval($quotaion->GovernmentFees())),
            "USD ".str_replace('.', ',', strval($quotaion->professionalFees())),
            "USD ".str_replace('.', ',', strval("40.000")),
            "USD ".str_replace('.', ',', strval($quotaion->govDueDilFees())),
            "USD ".str_replace('.', ',', AppHelpers::normQuoteTotal(strval($quotaion->cardFees()))),
            "USD ".str_replace('.', ',', strval($quotaion->misc())),
            "USD ".str_replace('.', ',', strval("10.000")),
            "USD ".str_replace('.', ',', $quotaion->otherAppCostTotal()),
            "USD ".str_replace('.', ',', $quotaion->finalTotal()),
            $quotaion->generalInfo()." ".$quotaion->optCosts()
        ];
    }

    private function getProgramFields() {
        if ($this->type === 'AB1') {
            $program = new AntiguaAb1Fields($this->type, $this->caseId, $this->profileID);
            return $program->fieldset;
        } else if ($this->type === 'AB2') {
            $program = new AntiguaAb2Fields($this->type, $this->caseId, $this->profileID);
            return $program->fieldset;
        } else if ($this->type === 'AB3') {
            $program = new AntiguaAb3Fields($this->type, $this->caseId, $this->profileID);
            return $program->fieldset;
        } else if ($this->type === 'AB4') {
            $program = new AntiguaAb4Fields($this->type, $this->caseId, $this->profileID);
            return $program->fieldset;
        } else if ($this->type === 'AB5') {
            $program = new AntiguaAb5Fields($this->type, $this->caseId, $this->profileID);
            return $program->fieldset;
        } else if ($this->type === 'ABL') {
            $program = new AntiguaAblFields($this->type, $this->caseId, $this->profileID);
            return $program->fieldset;
        } else if ($this->type === 'ABM') {
            $program = new AntiguaAbmFields($this->type, $this->caseId, $this->profileID);
            return $program->fieldset;
        }
    }

    public function generatePdf() {
        $jarFile    = $this->baseDir.'/libs/'.'pdf_builder/'.'HD-PDFLib.jar';
        $cmd        = 'java -jar '.$jarFile.' '. $this->jsonTicket();
        //dd($cmd);
        $execute    = shell_exec($cmd);
        return $execute;
    }


}
