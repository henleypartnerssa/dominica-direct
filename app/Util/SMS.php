<?php

namespace App\Util;

use App\User;
use App\Util\SoapGateway as SoapGateway;

/**
 * Description of SMS
 *
 * @author gershon Koks
 */
class SMS {

    private $authUser   = null;
    private $genPin     = null;

    public function __construct(User $user) {

        $this->authUser = $user;
        $this->genPin   = $this->generateNewPin();

    }

    public function sendPin() {

        $soap = new SoapGateway($this->authUser->cell_no, $this->genPin);
        return $soap->sendMessage();

    }

    private function generateNewPin() {
        $pin = mt_rand(1000, 9999);

        $user = $this->authUser;
        $user->pin = $pin;
        $user->save();

        return "Henley Direct Authentication OTP: ".$pin;
    }

}
