<?php

namespace App\Util;

use Artisaninweb\SoapWrapper\Facades\SoapWrapper as SoapWrapper;

class SoapGateway {
    
    private $wsdl = 'https://api.clickatell.com/soap/webservice.php?wsdl';
    
    private $cell;
    private $msg;
    
    public  $session_id;
    private $api_id;
    private $api_user;
    private $api_pass;
    public  $params;
    
    /**
     * Class Constructor
     * 
     * @param string $cell_no
     * @param string $message
     */
    public function __construct($cell_no, $message) {
        
        $this->cell     = $cell_no;
        $this->msg      = $message;
        $this->api_id   = env('API_KEY');
        $this->api_user = env('API_USER');
        $this->api_pass = env('API_PASS');
        $this->params   = $this->setParams();
        
        $this->addServices();
        $this->setParams();
        $this->setSessionId();
        
    }
    
    /*
     * Adds Service Object to SoapWrapper
     */
    private function addServices() {
        
        SoapWrapper::add(function($service) {
            $service->name('sms')->wsdl($this->wsdl)->trace(true)->cache(WSDL_CACHE_NONE);
        });
        
    }
    
    /*
     * Sets general users varaibles
     */
    private function setParams() {
        
        return ['api_id'        => $this->api_id,
                'user'          => $this->api_user,
                'password'      => $this->api_pass,
                'to'            => $this->cell,
                'text'          => $this->msg
        ];
        
    }
    
    /*
     * Does SOAP Call to ClickaTell and Sets 
     * Session ID.
     */
    private function setSessionId() {
        
        $data = ['api_id'   => $this->params['api_id'],
                 'user'     => $this->params['user'],
                 'password' => $this->params['password']
                ];
        
        SoapWrapper::service('sms', function ($service) use ($data) {
            $this->session_id = $service->call('auth', $data);
        });
        
    }
    
    /**
     * Send User one-time pine via sms using SOAP
     * call to ClickaTell API.
     * 
     * @return string
     */
    public function sendMessage() {
        
        $ticket = "null";
        
        $data = ['session_id'   => $this->normaliseSessionId(),
                 'api_id'       => $this->params['api_id'],
                 'user'         => $this->params['user'],
                 'password'     => $this->params['password'],
                 'to'           => [$this->cell],
                 'from'         => '+27761814891',
                 'text'         => $this->msg
                ];
        
        SoapWrapper::service('sms', function ($service) use ($data, &$ticket) {
           $ticket = $service->call('sendMsg', $data);
        });
        
        return $this->normaliseSmsReturnMsg($ticket);
        
    }
    
    /*
     * Normalize the session id to resturn only
     * the encrypted session id.
     */
    private function normaliseSessionId() {
        
       $split = explode(" ", $this->session_id);
       return trim($split[1]);
    }
    
    private function normaliseSmsReturnMsg($message) {
        
        $split = explode(":", $message[0]);
        
        return $split[0];
        
    }
    
}
