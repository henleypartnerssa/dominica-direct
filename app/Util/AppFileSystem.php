<?php

namespace App\Util;

use Storage;
use Auth;
use App\User;
use App\Profile;

class AppFileSystem {

    private $userId     = null;
    private $profileId  = null;
    private $usersDir   = null;

    public function __construct() {
        $this->validateUsersDir();
    }

    /**
     * Validates if the User Dir Exists.
     * If not then it creates the dir.
     */
    private function validateUsersDir() {
        $usersDirs = Storage::directories();

        if (count($usersDirs) === 0) {
            Storage::makeDirectory('users');
        }
    }

    public function createUserBaseDirs($userId) {
        $this->usersDir = '/users/'.$userId . '/';
        Storage::makeDirectory($this->usersDir);
        $this->createBaseDirs();
    }

    /**
     * Create Quotations Directory.
     */
    private function createBaseDirs() {
        Storage::makeDirectory($this->usersDir.'quotations');
        Storage::makeDirectory($this->usersDir.'payments');
        Storage::makeDirectory($this->usersDir.'receipts');
        Storage::makeDirectory($this->usersDir.'profiles');
    }

    /**
     * Create Profile + subdirs for given Profile.
     *
     * @param  [int] $userId    [Auth User ID]
     * @param  [int] $profileID [Profile ID would dirs should be created for.]
     */
    public function createProfileDir($profileID) {
        $profilesDir = '/users/'.Auth::id(). '/profiles/'.$profileID;
        Storage::makeDirectory($profilesDir);
        Storage::makeDirectory($profilesDir.'/uploads');
        Storage::makeDirectory($profilesDir.'/downloads');
    }

    /**
     * Static Function which Deletes the Given Profile ID
     * from Disk.
     *
     * @param  [type] $profileID [description]
     * @return [type]            [description]
     */
    public static function delProfileDir($profileID) {
        $profilesDir = storage_path().'/users/'.Auth::id(). '/profiles/'.$profileID;
        exec('rm -r -f '.$profilesDir);
    }

    public static function getUpperFilesByDir($dir) {
        $path       = storage_path().'/app/users/'.Auth::id().'/'.$dir.'/';
        $dirFiles   = scandir($path);
        $files      = [];
        foreach($dirFiles as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            array_push($files, $file);
        }
        return $files;
    }

}
