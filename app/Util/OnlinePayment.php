<?php

namespace App\Util;

use Stripe\Stripe as stripe;
use Stripe\Customer as customer;
use Stripe\Collection;
use Stripe\Charge as charge;
use App\AntiguaProgram;
use App\Application;
use Auth;

class OnlinePayment {

    private $_token;
    private $_POST;
    private $_webUser;
    private $_listOfCustomers;
    private $_customer;
    private $_payment;

    public function __construct($request) {
        $this->_POST            = $request;
        if (env('STRIPE_STATE') === 'TEST') {
            $this->_token = stripe::setApiKey(env('STRIPE_TEST_KEY'));
        } else {
            $this->_token = stripe::setApiKey(env('STRIPE_PROD_KEY'));
        }

        $this->_webUser         = Auth::user();
        $this->_listOfCustomers = customer::all();
        $this->setCustomer();
    }

    private function setCustomer() {
        if (count($this->_listOfCustomers->__toArray(true)['data']) === 0) {
            $this->_customer = customer::create([
                'email'     => $this->_webUser->username,
                'source'    => $this->_POST['stripe_token']
            ])->__toArray(true);
        } else {
            foreach($this->_listOfCustomers->__toArray(true)['data'] as $cust) {
                if ($cust['email'] === $this->_webUser->username) {
                    $this->_customer = Customer::retrieve($cust['id'])->__toArray(true);
                    break;
                } else {
                    $this->_customer = customer::create([
                        'email'     => $this->_webUser->username,
                        'source'    => $this->_POST['stripe_token']
                    ])->__toArray(true);
                }
            }
        }
    }

    public function makePayment() {
        $this->_payment = charge::create([
            'amount'        => 50000,
            'currency'      => 'usd',
            'description'   => $this->_POST['program'].' Program',
            'customer'      => $this->_customer['id'],
            'receipt_email' => $this->_webUser->username
        ])->__toArray(true);

        if ($this->_payment['status'] === 'succeeded') {
            $app        = Application::find($this->_POST['appId']);
            $app->paid  = true;
            $app->step  = 7;
            $app->save();
            return '1';
        } else {
            return '0';
        }
    }

}
