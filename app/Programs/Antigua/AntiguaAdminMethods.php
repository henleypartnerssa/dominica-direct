<?php

namespace App\Programs\Antigua;

use App\Application;
use App\Profile;
use App\Country;
use App\Language;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\Step;
use App\Address;
use App\FamilyMember;
use App\AntiguaDeclaration;
use App\AntiguaMedical;
use AppHelpers;
use App\Util\AppFileSystem;
use Carbon\Carbon;
use App\Traits\AntiguaWorkBusinessHelper;
use App\AntiguaProgram;
use Auth;
use Input;
use Validator;
use Storage;

trait AntiguaAdminMethods {

    public function adminPersonalDetails() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $profile        = $this->PROFILE;
        $programName    = "Antigua and Barbuda";
        $code           = Country::where('name', $programName)->first()->code;
        $languages      = $this->getAllLangLevels();
        $passports      = $profile->passports;
        $profileLangs   = $this->getProfileLanguages($profile);
        return view('cases.admin_data', compact('app','profile','code','languages','passports','profileLangs','appId'));
    }

    public function adminPersonalDetailsPost($post) {
        $updateProfile  = $this->saveProfileData($post);

        $this->saveAddress($post, 'res');

        if ($post['alt_country'] !== '') {
            $this->saveAddress($post, 'alt');
        }

        $this->saveProfileLanguage($post);
        return redirect($post->getrequestUri());
    }

    public function adminMedicalDetails() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $programName    = "Antigua And Barbuda";
        $profile        = $this->PROFILE;
        $doc            = $profile->antigua_medical;
        $address        = (!empty($doc)) ? Address::find((int)$doc->address_id) : "";

        return view('cases.admin_medical', compact('case', 'appId', 'profile', 'address','doc'));
    }

    public function medicalDetailsPost($post) {
        $app            = $this->APP;
        $profile        = $this->APP;
        $this->saveMedical($post);
        return redirect($post->getrequestUri());
    }

    public function adminFamilyDetails() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $programName    = "Antigua And Barbuda";
        $profile        = $this->PROFILE;
        $depTypes       = DependantType::whereIn('type', ['Brother', 'Sister'])->get()->toArray();
        $family         = $this->getFamily($profile);

        return view('cases.admin_family', compact('appId', 'profile', 'depTypes', 'family'));
    }

    public function familyDetailsPost($post) {

        $didsaveFather = $this->saveFamMember($post, 'Father');
        $didsaveMother = $this->saveFamMember($post, 'Mother');
        if ($this->PROFILE->belongs_to !== "Child") {
            $didsaveSpouse = $this->saveFamMember($post, 'Spouse');
        }

        return redirect($post->getrequestUri());
    }

    public function adminBusinessDetails() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $profile        = $this->PROFILE;
        $bank           = $profile->bank()->get()->first();
        $work           = $profile->Work_or_businesses()->get()->first();
        $references     = $profile->employment_references()->get();
        $totalIncome    = $this->businessTotalForView($work);

        return view('cases.admin_business', compact('profile', 'appId', 'bank', 'work', 'references', 'totalIncome'));
    }

    public function adminBusinessDetailsPost($post) {
        $this->saveWorkBank($post);
        return redirect($post->getrequestUri());
    }

    public function adminDeclarations() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $profile        = $this->PROFILE;
        $programName    = "Antigua And Barbuda";
        $decl           = $this->getDeclarForView();
        return view('cases.admin_declarations', compact('appId','decl','program','profile'));
    }

    public function adminDeclarationsPost($post) {
        $this->saveDeclarations($this->APP, $post);
        return redirect($post->getrequestUri());
    }

    public function adminApprovals() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $profile        = $this->PROFILE;
        $programName    = "Antigua And Barbuda";
        $approvals      = $this->getAppStates($appId);
        return view('cases.admin_approvals', compact('appId','profile','approvals'));
    }

    public function adminDependants() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $profile        = $this->PROFILE;
        $dependants     = $app->profiles()->where("belongs_to", "!=", $profile->belongs_to)->get();
        $programName    = "Antigua And Barbuda";
        return view('cases.admin_dependants', compact('appId','dependants','profile'));
    }

    public function adminDocuments() {
        $app            = $this->APP;
        $appId          = $app->application_id;
        $profile        = $this->PROFILE;
        $files          = $this->getByProgram($app->case_no, $this->USER->user_id);
        $programName    = "Antigua And Barbuda";
        return view('cases.admin_documents', compact('appId','files','profile'));
    }

    public function getAppStates($appId) {
        $app = Application::findOrFail($appId);
        $return['dd_check']                 = $app->dd_check;
        $return['data_review']              = $app->data_review;
        $return['payment_for_submission']   = $app->payment_for_submission;
        $return['received_package']         = $app->received_package;
        $return['package_dd_check']         = $app->package_dd_check;
        $return['ciu_approval']             = $app->ciu_approval;
        $return['payment_for_investment']   = $app->payment_for_investment;
        $return['citizenship_docs']         = $app->citizenship_docs;
        $return['visa']                     = $app->visa;
        return $return;
    }

    private function getByProgram($program, $userId) {
        $storage    = storage_path().'/app/users/'.$userId.'/cases/';
        $array      = [];
        $downloads  = scandir($storage.$program.'/downloads');
        $uploads    = scandir($storage.$program.'/uploads');

        foreach ($downloads as $i => $file) {

            if ($file === '.' || $file === '..') {
                continue;
            }

            $split  = explode('.',$file);
            $split2 = explode('-',$split[0]);

            array_push($array, [
                'ref'       => $program,
                'program'   => 'Antigua and Barbuda',
                'file'      => $file,
                'type'      => 'applicaiton form',
            ]);
        }

        foreach ($uploads as $i => $file) {

            if ($file === '.' || $file === '..') {
                continue;
            }

            $split  = explode('.',$file);
            $split2 = explode('-',$split[0]);

            array_push($array, [
                'ref'       => $program,
                'program'   => 'Antigua and Barbuda',
                'file'      => $file,
                'type'      => $this->getType($split2),
            ]);
        }
        return $array;
    }

    private function getType($array) {
        $str = '';

        foreach ($array as $key => $value) {
            if ($key === count($array) - 1) {
                continue;
            }
            $str = $str.$value.' ';
        }
        return trim($str);
    }

}
