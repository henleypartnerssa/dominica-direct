<?php

namespace App\Programs\Antigua;

use Carbon\Carbon;
use App\Application;
use App\Country;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\Profile;
use AppHelpers;
use Auth;

class AntiguaAb5Fields {

    private $profile        = null;
    private $case           = null;

    public function __construct($pdf,$case_id,$profileId) {
        $this->case             = Application::findOrFail($case_id);
        $this->profile          = Profile::find($profileId);
        $this->mapAb5Fieds();
    }

    private function mapAb5Fieds() {

        $passport_1 = $this->profile->passports()->orderBy('created_at', 'asc')->first();
        $passport_2 = $this->profile->passports()->orderBy('created_at', 'desc')->first();

        $this->fieldset = [
            'A' => [
                'surname'           => $this->profile->last_name,
                'name'              => $this->profile->first_name,
                'place_of_birth'    => $this->profile->place_of_birth,
                'country'           => $this->profile->country_of_birth,
                'address'           => $this->addrByTypeHelper('res'),
                'passport_1'        => $passport_1->issuing_country.' | '.$passport_1->passport_no,
                'passport_2'        => $passport_2->issuing_country.' | '.$passport_2->passport_no,
                'd1'                => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[0],
                'd2'                => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[1],
                'm1'                => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[0],
                'm2'                => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[1],
                'y1'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[0],
                'y2'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[1],
                'y3'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[2],
                'y4'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[3],
                $this->profile->gender => $this->profile->gender
            ]
        ];
    }

    private function addrByTypeHelper($type) {
        $str        = "";
        $address    = $this->profile->addresses()->where('address_type', $type)->get();

        foreach ($address as $addr) {
            $country = $addr->country;
            $str1 = $addr->street1.' ';
            $str2 = isset($addr->street2) ? $addr->street2.' ' : "";
            $str = $str1.$str2.$country.$addr->town.' '.$addr->postal_code;
        }
        return $str;
    }

    private function dateHlper($date, $stamp) {
        $dt = Carbon::parse($date);

        if ($stamp === 'day') {
            return (strlen($dt->day) > 1) ? $dt->day : '0'.$dt->day;
        } else if ($stamp === 'month') {
            return (strlen($dt->month) > 1) ? $dt->month : '0'.$dt->month;
        } else {
            return null;
        }
    }

}
