<?php

namespace App\Programs\Antigua;

use Carbon\Carbon;
use App\Application;
use App\Country;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\Profile;
use AppHelpers;
use Auth;

class AntiguaAblFields {

    private $profile    = null;
    private $case       = null;
    private $user       = null;

    public function __construct($pdf,$case_id,$profileId) {
        $this->case     = Application::findOrFail($case_id);
        $this->user     = Auth::user();
        $this->profile  = Profile::find($profileId);
        $this->mapAblFieds();
    }

    private function mapAblFieds() {

        $father = $this->profile->family_members()->where('relationship', 'Father')->first();
        $mother = $this->profile->family_members()->where('relationship', 'Mother')->first();

        $ageYrs = Carbon::parse($this->profile->date_of_birth)->diff(Carbon::now())->y;
        if ($ageYrs === 0) {
            $age = strval(Carbon::parse($this->profile->date_of_birth)->diff(Carbon::now())->m).' months';
        } else {
            $age = strval(Carbon::parse($this->profile->date_of_birth)->diff(Carbon::now())->y).' years';
        }

        $this->fieldset = [
            '1' => [
                'surname'           => $this->profile->last_name,
                'name'              => $this->profile->first_name,
                'middle_names'      => $this->profile->legal_passport_middle_names,
                $this->profile->marital_status => $this->profile->marital_status,
                'place_of_birth'    => $this->profile->place_of_birth,
                'country'           => $this->profile->country_of_birth,
                'address'           => $this->addrByTypeHelper('res'),
                'day'               => strval(Carbon::parse($this->profile->date_of_birth)->day),
                'month'             => strval(Carbon::parse($this->profile->date_of_birth)->month),
                'year'              => strval(Carbon::parse($this->profile->date_of_birth)->year),
                $this->profile->gender => $this->profile->gender,
                'age'               => $age,
                'height'            => $this->profile->height,
                'marks'             => $this->profile->body_marks,
                'tel'               => $this->profile->permanent_telephone_no,

            ],
            '2' => [
                'father_name'               => $father->first_name.' '.$father->last_name,
                'father_place_of_birth'     => $father->place_of_birth,
                'father_dob_day'            => strval(Carbon::parse($father->date_of_birth)->day),
                'father_dob_month'          => strval(Carbon::parse($father->date_of_birth)->month),
                'father_dob_year'           => strval(Carbon::parse($father->date_of_birth)->year),
                'mother_name'               => $mother->first_name.' '.$mother->last_name,
                'mother_place_of_birth'     => $mother->place_of_birth,
                'mother_dob_day'            => strval(Carbon::parse($mother->date_of_birth)->day),
                'mother_dob_month'          => strval(Carbon::parse($mother->date_of_birth)->month),
                'mother_dob_year'           => strval(Carbon::parse($mother->date_of_birth)->year),
            ]
        ];
    }

    private function addrByTypeHelper($type) {
        $str        = "";
        $address    = $this->profile->addresses()->where('address_type', $type)->get();
        $array      = [];
        foreach ($address as $addr) {
            $country = $addr->country;
            $array['address_line_1'] = (!empty($addr->street2)) ? $addr->street1.', '.$addr->street2 : $addr->street1;
            $array['address_line_2'] = $country.', '.$addr->town;
            $array['address_line_3'] = $addr->postal_code;
        }

        return $array;
    }

    private function dateHlper($date, $stamp) {
        $dt = Carbon::parse($date);

        if ($stamp === 'day') {
            return (strlen($dt->day) > 1) ? $dt->day : '0'.$dt->day;
        } else if ($stamp === 'month') {
            return (strlen($dt->month) > 1) ? $dt->month : '0'.$dt->month;
        } else {
            return null;
        }
    }

}
