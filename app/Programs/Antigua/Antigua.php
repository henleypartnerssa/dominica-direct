<?php

namespace App\Programs\Antigua;

use App\Application;
use App\Profile;
use App\Country;
use App\Language;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\Step;
use App\Address;
use App\FamilyMember;
use App\AntiguaDeclaration;
use App\AntiguaMedical;
use AppHelpers;
use App\Util\AppFileSystem;
use Carbon\Carbon;
use App\Traits\AntiguaWorkBusinessHelper;
use App\Programs\Antigua\AntiguaBaseClassHelpers;
use App\Programs\Antigua\AntiguaAdminMethods;
use App\Programs\Antigua\AntiguaImpInfoMethods;
use App\Programs\Antigua\AntiguaProgramCosts;
use App\AntiguaProgram;
use Auth;
use Input;
use Validator;
use Storage;

class Antigua {

    // Triats
    use AntiguaWorkBusinessHelper;
    use AntiguaBaseClassHelpers;
    use AntiguaAdminMethods;

    protected $PROFILE;
    protected $APP;
    protected $USER;

    /**
     * Class Constructor. Allows empty initialization as well.
     *
     * @param [string] $progID [Antigua program ID]
     */
    public function __construct($profileId = null, $appID = null, $admin = false) {
        if ($appID !== null || $profileId !== null) {
            $this->PROFILE  = Profile::find((int)$profileId);
            $this->APP      = Application::find((int)$appID);
        }
        if ($admin) {
            $this->USER = $this->PROFILE->user;
        } else {
            $this->USER = Auth::user();
        }

    }

    public function declarationsView() {
        $app            = $this->APP;
        $steps          = AppHelpers::buildStepper(3);
        $user           = $this->USER;
        $profile        = $this->PROFILE;
        $programName    = "Antigua And Barbuda";
        $decl           = $this->getDeclarForView();
        $all            = AppHelpers::valDataPages($this->USER);
        $backLink       = '/users/profile/'.$this->PROFILE->profile_id.'/application/'.$this->PROFILE->application_id.'/work-and-busines';
        return view('cases.declarations', compact('user','profile','programName', 'steps', 'decl', 'backLink', 'all'));
    }

    public function declarationsRedirect($post) {
        $app = $this->APP;
        $this->saveDeclarations($app,$post);
        $this->PROFILE->state = 4;
        $this->PROFILE->save();
        $all = AppHelpers::valDataPages($this->USER);
        if ($all) {
            return redirect('/users/profile/'.$this->PROFILE->profile_id.'/application/'.$this->PROFILE->application_id.'/upload-documents');
        } else {
            return redirect($post->getrequestUri());
        }

    }

    public function uploadDocumentsView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 4)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(4);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->PROFILE;
        $backLink       = '/users/profile/'.$this->PROFILE->profile_id.'/application/'.$this->PROFILE->application_id.'/personal-data';
        $fwdLink        = '/users/application/'.$this->PROFILE->application_id.'/dd-check';
        $files          = $this->getUploadedFiles($app->application_id);

        return view('cases.upload_documents', compact('profile','programName','programID', 'steps', 'files', 'backLink', 'fwdLink'));
    }

    public function dataReviewView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 7)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(7);
        $programName    = "Antigua and Barbuda";
        $profile        = $app->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $state          = ($app->data_review === false) ? "disabled" : "";
        $backLink       = '/users/profile/'.$profile->profile_id.'/application/upload-documents';
        $fwdLink        = '/users/profile/'.$profile->profile_id.'/application/gov-documents';
        return view('cases.data_view', compact('profile','state', 'backLink', 'programName', 'appId', 'steps', 'fwdLink'));
    }

    public function govDocumentsView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 8)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(8);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->PROFILE;
        $user           = $this->USER;
        $id             = $app->application_id;
        $backLink       = '/users/profile/'.$profile->profile_id.'/application/'.$app->application_id.'/upload-documents';
        $fwdLink        = '/users/profile/'.$profile->profile_id.'/application/'.$app->application_id.'/upload-notary';
        $age            = Carbon::parse($this->PROFILE->date_of_birth)->diff(Carbon::now())->y;

        return view('cases.download_documents', compact('id','user', 'profile', 'programName','programID', 'steps', 'backLink', 'fwdLink', 'age'));
    }

    public function uploadNotaryView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 9)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(9);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->PROFILE;
        $user           = $this->USER;
        $backLink       = '/users/profile/'.$profile->profile_id.'/application/'.$app->application_id.'/gov-documents';
        $fwdLink        = '/users/application/'.$app->application_id.'/courier';
        $files          = $this->getUploadedFiles($app->application_id);
        return view('cases.upload_notary', compact('user', 'profile','programName', 'steps', 'files', 'backLink', 'fwdLink'));
    }

    public function courierView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 10)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(10);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->USER->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $state          = ($app->received_package === false) ? "disabled" : "";
        $backLink       = '/users/profile/'.$profile->profile_id.'/application/'.$app->application_id.'/upload-notary';
        $fwdLink        = '/users/application/'.$app->application_id.'/submission-payment';
        $split          = explode('-', $app->courier_no);
        $courier_type   = (!empty($split[0])) ? $split[0] : '';
        $courier_no     = (!empty($split[1])) ? $split[1] : '';
        $listOfCouriers = ['DHL','FedEx', 'UPS', 'TNT', 'DPD', 'DTDC', 'A1Express', 'Allied Express', 'NAPAREX', 'Swift Couriers', 'ParcelForce', 'Interlink Direct', 'Bring', 'Australia Post', 'Middle East Express', 'Aramex', 'Royale International Group', 'South African Courier Systems', 'Purolator', 'Ship to Anywhere', 'Antron Express', 'TCS Express & Logistics'];
        return view('cases.courier', compact('id', 'profile', 'programName', 'steps', 'state', 'courier_no', 'backLink', 'fwdLink', 'listOfCouriers','courier_type'));
    }

    public function couerierRedirect($post) {
        $this->APP->courier_no = $post['type'].'-'.$post['courier_no'];
        $this->APP->save();
        return '1';
    }

    public function submissionPaymentView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 11)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(11);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->USER->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $user_id        = $this->USER->user_id;
        $state          = ($app->payment_for_submission === false) ? "disabled" : "";
        $backLink       = '/users/application/'.$app->application_id.'/courier';
        $fwdLink        = '/users/application/'.$app->application_id.'/principal-approval';

        return view('cases.submission_payment', compact('id', 'profile', 'programName','programID', 'steps', 'state','user_id','program_id','backLink', 'fwdLink'));


    }

    public function principalApprovalView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 12)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(12);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->USER->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $state          = ($app->ciu_approval === false) ? "disabled" : "";
        $backLink       = '/users/application/'.$app->application_id.'/submission-payment';
        $fwdLink        = '/users/application/'.$app->application_id.'/government-investment';

        return view('cases.principal-approval', compact('id', 'profile', 'programName', 'steps', 'state', 'backLink', 'fwdLink'));
    }

    public function governmentInvestmentView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 13)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(13);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->USER->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $user_id        = $this->USER;
        $state          = ($app->payment_for_investment === false) ? "disabled" : "";
        $backLink       = '/users/application/'.$app->application_id.'/principal-approval';
        $fwdLink        = '/users/application/'.$app->application_id.'/issue-docs';

        return view('cases.government-investment', compact('id', 'profile', 'programName', 'steps', 'state','user_id','program_id', 'backLink', 'fwdLink'));

    }

    public function issueDocsView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 14)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(14);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->USER->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $state          = ($app->citizenship_docs === false) ? "disabled" : "";
        $backLink       = '/users/application/'.$app->application_id.'/government-investment';
        $fwdLink        = '/users/application/'.$app->application_id.'/collect-passport';
        return view('cases.issue-docs', compact('id', 'profile', 'programName', 'programID', 'steps', 'state', 'backLink', 'fwdLink'));
    }

    public function collectPassportView() {
        $app            = $this->APP;
        $app->step      = Step::where('step_no', 15)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(15);
        $programName    = "Antigua and Barbuda";
        $profile        = $this->USER->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $state          = ($app->visa === false) ? "disabled" : "";
        $backLink       = '/users/application/'.$app->application_id.'/issue-docs';

        return view('cases.collect-passport', compact('id', 'profile', 'programName', 'programID', 'steps', 'state', 'backLink', 'fwdLink'));
    }

    public function uploadDocumentsRedirect($progrmaName, $programID, $post) {
        $program    = AntiguaProgram::findOrFail($programID);
        $storage    = storage_path().'/app/users/'.Auth::id().'/cases/';
        $fullPath   = $storage.$program->application->case_no.'/uploads/';

        foreach ($post->all() as $key => $value) {
            if (Input::hasFile($key)) {
                $file   = Input::file($key);
                $ext    = $file->getClientOriginalExtension();
                if ($file->isValid() && $ext === 'pdf') {
                    $name   = $key.'.'.$ext;
                    $moved  = $file->move($fullPath, $name);
                    if (!$moved) {
                        return 'failed';
                    } else {
                        return $name;
                    }
                } else {
                    return 'failed';
                }
            }
        }
        return 'failed';
    }

    public function deleteDocumentsRedirect($progrmaName, $programID, $post) {
        $program = AntiguaProgram::findOrFail($programID);
        $storage = storage_path().'/app/users/'.Auth::id().'/cases/';
        $fullPath = $storage.$program->application->case_no.'/uploads/';
        $filesArr = scandir($fullPath);

        foreach($filesArr as $file) {
            if (strpos($file, $post['code']) !== false) {
                if (unlink($fullPath.$file)) {
                    return $post['code'];
                } else {
                    return 'failed';
                }
            }
        }
    }

    public function deleteNotaryRedirect($progrmaName, $programID, $post) {
        $program = AntiguaProgram::findOrFail($programID);
        $storage = storage_path().'/app/users/'.Auth::id().'/cases/';
        $fullPath = $storage.$program->application->case_no.'/uploads/';
        $filesArr = scandir($fullPath);

        foreach($filesArr as $file) {
            if (strpos($file, $post['code']) !== false) {
                if (unlink($fullPath.$file)) {
                    $split = explode('-',$post['code']);
                    return $split[1];
                } else {
                    return 'failed';
                }
            }
        }
    }

    public function uploadNotaryRedirect($progrmaName, $programID, $post) {
        $program    = AntiguaProgram::findOrFail($programID);
        $storage    = storage_path().'/app/users/'.Auth::id().'/cases/';
        $fullPath   = $storage.$program->application->case_no.'/uploads/';

        foreach ($post->all() as $key => $value) {
            if (Input::hasFile($key)) {
                $file = Input::file($key);
                if ($file->isValid()) {
                    $name   = 'notary-'.$key.'-'.$file->getClientOriginalName();
                    $moved  = $file->move($fullPath, $name);
                    if (!$moved) {
                        return 'failed';
                    } else {
                        return 'ok';
                    }
                } else {
                    return 'failed';
                }
            }
        }
        return 'failed';
    }

    public function ddCheckPackageView($progrmaName, $programID) {
        $app            = $this->AntigueProgram->application;
        $app->step      = Step::where('step_no', 10)->first()->step_id;
        $app->save();
        $steps          = AppHelpers::buildStepper(10);
        $programName    = "Antigua and Barbuda";
        $profile        = $app->profile;
        $state          = ($app->package_dd_check === false) ? "disabled" : "";
        $backLink       = '/users/cases/'.$progrmaName.'/'.$programID.'/courier';
        $fwdLink        = '/users/cases/'.$progrmaName.'/'.$programID.'/principal-approval';
        return view('cases.antigua-and-barbuda.ddc-package', compact('id', 'profile', 'programName','programID', 'steps', 'state', 'backLink', 'fwdLink'));
    }

}
