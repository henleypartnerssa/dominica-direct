<?php

namespace App\Programs\Antigua;

use App\Application;
use App\Profile;
use App\Country;
use App\Language;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\Step;
use App\Address;
use App\FamilyMember;
use App\AntiguaDeclaration;
use App\AntiguaMedical;
use AppHelpers;
use App\Util\AppFileSystem;
use App\Util\AppPdfHandler;
use App\Util\SendEmail;
use Carbon\Carbon;
use App\Traits\AntiguaWorkBusinessHelper;
use App\AntiguaProgram;
use Auth;
use Input;
use Validator;
use Storage;

trait AntiguaBaseClassHelpers {

    /**
     * Map and save POST data to Profile Model
     *
     * @param  [POST] $post [Form Post Data]
     * @return [bool]       [data saved to db]
     */
    public function saveProfileData($post) {
        $this->PROFILE->title                                   = $post['title'];
        $this->PROFILE->gender                                  = $post['gender'];
        $this->PROFILE->date_of_birth                           = Carbon::parse(AppHelpers::normalizeDate($post['dob_year'],$post['dob_month'],$post['dob_day']));
        $this->PROFILE->place_of_birth                          = $post['place_of_birth'];
        $this->PROFILE->country_of_birth                        = $post['cob'];
        $this->PROFILE->citizenship_at_birth                    = $post['cab'];
        $this->PROFILE->identity_card_no                        = $post['id_no'];
        $this->PROFILE->identity_card_no_issuing_country        = $post['id_issuing_coutry'];
        $this->PROFILE->body_marks                              = $post['body_marks'];
        $this->PROFILE->height                                  = $post['height'];
        $this->PROFILE->title_other                             = $post['title_other'];
        $this->PROFILE->first_name                              = $post['legal_first_names'];
        $this->PROFILE->legal_passport_middle_names             = $post['legal_mid_names'];
        $this->PROFILE->last_name                               = $post['legal_surname'];
        //$this->PROFILE->legal_bc_first_name                     = $post['legal_bc_first_names'];
        $this->PROFILE->legal_bc_middle_names                   = $post['legal_bc_mid_names'];
        $this->PROFILE->legal_es_first_name                     = $post['legal_es_first_names'];
        $this->PROFILE->legal_es_surname                        = $post['legal_es_surnames'];
        $this->PROFILE->legal_former_maiden_name                = $post['other_maiden_name'];
        $this->PROFILE->former_other_names                      = $post['other_names'];
        $this->PROFILE->legal_name_change_date                  = ($post['name_change_year'] !== "") ? Carbon::parse(AppHelpers::normalizeDate($post['name_change_year'],$post['name_change_month'],$post['name_change_day'])) : null;
        $this->PROFILE->legal_name_change_reason                = $post['reason_name_change'];
        $this->PROFILE->permanent_telephone_no                  = $post['permanent_telephone_no'];
        $this->PROFILE->permanent_mobile_no                     = $post['mobile_telephone_no'];
        $this->PROFILE->personal_email_address                  = $post['personal_email_address'];
        $this->PROFILE->citizen_other                           = $post['other_citizenship'];
        $this->PROFILE->reason_for_citizenship                  = $post['reason_for_citizenship'];
        $this->PROFILE->social_security_or_insurance_no         = $post['security_no'];
        $this->PROFILE->social_security_or_insurance_country    = $post['sn_issuing_coutry'];
        return $this->PROFILE->save();
    }

    private function saveMedical($post) {
        $counter    = $this->PROFILE->antigua_medical()->count();
        $address    = $this->saveAddress($post, 'doc');
        $med        = null;
        if ($counter > 0) {
            $med = $this->PROFILE->antigua_medical;
        } else {
            $med = new AntiguaMedical();
        }

        $med->application_id        = $this->PROFILE->application_id;
        $med->profile_id            = $this->PROFILE->profile_id;
        $med->user_id               = $this->PROFILE->user_id;
        $med->group_id              = $this->PROFILE->group_id;
        $med->address_id            = $address->address_id;
        $med->doctor_name           = $post['doctor_name'];
        $med->tb                    = ($post['tb'] === 'on') ? true : false;
        $med->hp                    = ($post['hp'] === 'on') ? true : false;
        $med->tp                    = ($post['tp'] === 'on') ? true : false;
        $med->other_comm            = ($post['other_comm'] === 'on') ? true : false;
        $med->other_heart           = ($post['other_heart'] === 'on') ? true : false;
        $med->stroke                = ($post['stroke'] === 'on') ? true : false;
        $med->idd                   = ($post['idd'] === 'on') ? true : false;
        $med->cancer                = ($post['cancer'] === 'on') ? true : false;
        $med->aids_hiv              = ($post['aids_hiv'] === 'on') ? true : false;
        $med->b2                    = ($post['b2'] === 'on') ? true : false;
        $med->b3                    = ($post['b3'] === 'on') ? true : false;
        $med->b4                    = ($post['b4'] === 'on') ? true : false;
        $med->b5                    = ($post['b5'] === 'on') ? true : false;
        $med->b6                    = ($post['b6'] === 'on') ? true : false;
        $med->b6_date               = (!empty($post['b6_date_year'])) ? Carbon::parse(AppHelpers::normalizeDate($post['b6_date_year'],$post['b6_date_month'],$post['b6_date_day'])) : null;
        $med->b7                    = ($post['b7'] === 'on') ? true : false;
        $med->b8                    = $post['b8'];
        $med->save();
        return $med;
    }

    public function saveFamMember($request, $type) {
        $member     = null;
        $typeLower  = strtolower($type);
        $depType    = DependantType::where('type', $type)->first();

        if ($type === "Father" || $type === "Mother" || $type === "Spouse") {
            $inFamTbl = $this->PROFILE->family_members()->where('relationship', $type)->count();
            if ($inFamTbl > 0) {
                $member = $this->PROFILE->family_members()->where('relationship', $type)->first();
                $member->dependant_type_id  = $depType->dependant_type_id;
                $member->relationship       = $type;
            } else if ($this->PROFILE->belongs_to === 'Child') {
                $pa = $this->USER->profiles()->where('belongs_to', 'Principal Applicant')->first();
                $sp = $this->USER->profiles()->where('belongs_to', 'Spouse')->first();
                if ($type === "Mother") {
                    $member = $sp;
                } else {
                    $member = $pa;
                }
            } else {
                $member = New FamilyMember();
                $member->dependant_type_id  = $depType->dependant_type_id;
                $member->relationship       = $type;
            }
        }

        $member->first_name         = $request[$typeLower.'_first_name'];
        $member->last_name          = $request[$typeLower.'_fam_name'];
        $member->gender             = ($request[$typeLower.'_first_name'] !== '') ? $request[$type.'_first_name'] : null;
        $member->date_of_birth      = Carbon::parse(AppHelpers::normalizeDate($request[$typeLower.'_dob_year'],$request[$typeLower.'_dob_month'],$request[$typeLower.'_dob_day']));
        $member->place_of_birth     = $request[$typeLower.'_pob'];
        $member->country_of_birth       = $request[$typeLower.'_citizenship'];
        $member->citizenship_at_birth   = $request[$typeLower.'_residence'];
        $member->save();

        if ($this->PROFILE->belongs_to === 'Child') {
            return;
        }

        if ($member->getTable() === "family_members" && $inFamTbl === 0) {
            $this->PROFILE->family_members()->attach($member, [
                'user_id'           => $this->PROFILE->user_id,
                'group_id'          => $this->PROFILE->group_id,
                'application_id'    => $this->PROFILE->application_id,
                'dependant_type_id' => $member->dependant_type_id
            ]);
        } else if ($type === "Spouse") {
            $s = $this->USER->profiles()->where('belongs_to', 'Spouse')->first();
            $s->first_name              = $request[$typeLower.'_first_name'];
            $s->last_name               = $request[$typeLower.'_fam_name'];
            $s->date_of_birth           = Carbon::parse(AppHelpers::normalizeDate($request[$typeLower.'_dob_year'],$request[$typeLower.'_dob_month'],$request[$typeLower.'_dob_day']));;
            $s->place_of_birth          = $request[$typeLower.'_pob'];
            $s->country_of_birth        = $request[$typeLower.'_citizenship'];
            $s->citizenship_at_birth    = $request[$typeLower.'_residence'];
            $s->save();
        }

        if ($request['father_deseased'] == 'No' || $request['mother_deseased'] == 'No') {
            if ($type === "Spouse") {
                return;
            }
            $this->saveParentAddress($member, $request, $type);
        }

    }

    public function saveParentAddress($parentObj, $post, $type) {

        $hasAddr    = $parentObj->addresses()->count();
        $addr       = null;
        $lcType     = strtolower($type);
        $post       = $post->all();
        $tbl        = $parentObj->getTable();
        $return     = false;

        if ($hasAddr > 0) {
            $addr = $parentObj->addresses()->get()->first();
        } else {
            $addr = new Address();
        }

        $addr->address_type     = ($tbl === "family_members") ? "ab-parent" : $parentObj->addresses()->where('address_type', 'res')->first()->address_type;
        $addr->street1          = $post[$lcType."_street1"];
        $addr->street2          = $post[$lcType."_street2"];
        $addr->country          = $post[$lcType."_residence"];
        $addr->town             = $post[$lcType."_town"];
        $addr->postal_code      = $post[$lcType."_postal_code"];
        $saved = $addr->save();

        if ($hasAddr === 0) {
            if ($tbl === "family_members") {
                $addr->family_members()->attach($parentObj->family_member_id, [
                    'dependant_type_id' => $parentObj->dependant_type_id
                ]);
            }
        }

        if ($tbl === "family_members") {
            if (!empty($parentObj->addresses()->get()->first()->pivot)) {
                $return = true;
            }
        } else {
            $return = $saved;
        }

        return $return;

    }

    public function saveDeclarations($app,$post) {

        $user       = $this->USER;
        $declArr    = ['c1','c2','c3','c4','c5','c6','c7','c8','c9','c10','c11','c12','c13','d1','d2','d3','d4','d5'];
        $declAddArr = [];

        foreach ($post->all() as $key => $value) {
            if ($key === '_token' || strpos($key,'message') !== false) {
                continue;
            }
            array_push($declAddArr, $key);
        }

        foreach($declArr as $item) {
            $count  = $this->PROFILE->antigua_declarations()->where('code', $item)->count();

            if ($count > 0) {
                $decl = $this->PROFILE->antigua_declarations()->where('code', $item)->get()->first();
            } else {
                $decl = new AntiguaDeclaration();
            }

            if (array_search($item, $declAddArr) !== false) {
                $decl->application_id       = $app->application_id;
                $decl->profile_id           = $this->PROFILE->profile_id;
                $decl->user_id              = $user->user_id;
                $decl->group_id             = $user->group_id;
                $decl->code                 = $item;
                $decl->answer               = true;
                $decl->message              = (strpos($item,'d') !== false) ? "" : $post[$item.'_message'];
            } else {
                $decl->application_id       = $app->application_id;
                $decl->profile_id           = $this->PROFILE->profile_id;
                $decl->user_id              = $user->user_id;
                $decl->group_id             = $user->group_id;
                $decl->code                 = $item;
                $decl->answer               = false;
                $decl->message              = (strpos($item,'d') !== false) ? "" : $post[$item.'_message'];
            }
            $decl->save();
        }
    }

    public function getDeclarForView() {
        $decl   = $this->PROFILE->antigua_declarations()->get()->toArray();
        $array  = [];

        foreach ($decl as $value) {
            $array[$value['code']] = [
                'answer'    => $value['answer'],
                'message'   => $value['message']
            ];
        }
        return $array;
    }

    private function manageProfileForCase($request) {

        $user       =   Auth::user();
        $profiles   =   $user->profiles()->get();
        $list = $user->profiles()->lists('belongs_to')->toArray();

        if ($profiles->count() > 0) {
            foreach ($profiles as $p) {

                if (in_array($request['PA'], $list) !== false) {
                    $profile = $p->where('belongs_to', $request['PA'])->get()->first();
                    $profile->belongs_to = $request['PA'];
                    $profile->save();
                    break;
                } else if(empty($p->belongs_to)) {
                    $profile = $p;
                    $profile->belongs_to = $request['PA'];
                    $profile->save();
                    break;
                } else {
                    $profile = new Profile();
                    $profile->belongs_to    = $request['PA'];
                    $profile->user_id       = $user->user_id;
                    $profile->group_id      = $user->group_id;
                    if ($request['PA'] === 'Spouse of PA') {
                        $spouse = $user->profiles()->get()->first()->family_members()->where('relationship', 'Spouse')->get()->first();
                        $profile->first_name        = $spouse->first_name;
                        $profile->last_name         = $spouse->surname;
                        $profile->gender            = 'female';
                        $profile->date_of_birth     = $spouse->date_of_birth;
                        $profile->place_of_birth    = $spouse->place_of_birth;
                        $profile->citizenship_at_birth = $spouse->citizenship;
                        $profile->country_of_birth  = $spouse->country_of_birth;
                    } else if ($request['PA'] === 'Dependant of PA') {
                        $split = explode(' ', $request['DEP']);
                        $child = $user->profiles()->get()->first()->family_members()->where('first_name', $split[0])->where('surname', $split[1])->get()->first();
                        $profile->first_name        = $child->first_name;
                        $profile->last_name         = $child->surname;
                        $profile->gender            = $child->gender;
                        $profile->date_of_birth     = $child->date_of_birth;
                        $profile->place_of_birth    = $child->place_of_birth;
                        $profile->citizenship_at_birth = $child->citizenship;
                        $profile->country_of_birth  = $child->country_of_birth;
                    }
                    $profile->save();
                    break;
                }
            }
        } else {
            $profile = new Profile();
            $profile->belongs_to    = $request['PA'];
            $profile->user_id       = $user->user_id;
            $profile->group_id      = $user->group_id;
            $profile->belongs_to    = $applying_as;
            $profile->save();
        }
        return $profile;

    }

    public function genAndEmailQuotation($programType) {
        $user           = Auth::user();
        $pdfName        = env('COMPANY')." - Antugua and Barbuda - Quotaiton.pdf";
        $pdf_builder    = new AppPdfHandler('quotations', null, null, false, $programType);
        $pdf_builder->setOutputFile($pdfName);
        $pdf = (string)$pdf_builder->generatePdf();
        if (strpos($pdf, '1') !== false) {
            $mail = new SendEmail("quotation", ['name' => $user->name, 'lastname' => $user->name, 'email' => $user->username], [
                "name"          => $user->name,
                "surname"       => $user->surname,
                "attachment"    => storage_path('app').'/users/'.$user->user_id.'/quotations/'.$pdfName
            ]);
        } else {
            return false;
        }
        return true;
    }

    public function getUploadedFiles($programID) {
        $storage    = storage_path().'/app/users/'.Auth::id().'/profiles/'.$this->PROFILE->profile_id.'/uploads';
        $filesArr   = scandir($storage);
        $array          = [];
        $passArr        = [];
        $idArr          = [];
        $ntArray        = [];
        $birthArr       = [];
        $policeArr      = [];
        $marriageArr    = [];
        $divorceArr     = [];
        $militaryArr    = [];
        $nameChangeArr  = [];
        $reArray        = [];
        $refArr         = [];
        $bankArr        = [];
        $proofArr       = [];
        $confArr        = [];
        $affArr         = [];
        $hivArr         = [];
        $reArray        = [];
        foreach($filesArr as $file) {
            if (strpos($file,'passport-') !== false) {
                $split = explode('.',$file);
                array_push($passArr, [
                    'code' => $split[0],
                    'file' => $file
                ]);
            } else if (strpos($file,'identity-') !== false) {
                $split = explode('.',$file);
                array_push($idArr, [
                    'code' => $split[0],
                    'file' => $file
                ]);
            } else if (strpos($file,'notary-') !== false) {
                $split = explode('-',$file);
                $st1 = str_replace($split[0].'-', '', $file);
                $st2 = str_replace($split[1].'-', '', $st1);
                array_push($ntArray, [
                    'code' => $split[1],
                    'file' => $st2
                ]);
            } else if (strpos($file,'birth-') !== false) {
                $split = explode('.',$file);
                array_push($birthArr, [
                    'code' => $split[0],
                    'file' => $file
                ]);
            } else if (strpos($file,'police-') !== false) {
                $split = explode('.',$file);
                array_push($policeArr, [
                    'code' => $split[0],
                    'file' => $file
                ]);
            } else if (strpos($file,'marriage-') !== false) {
                $split = explode('.',$file);
                array_push($marriageArr, [
                    'code' => $split[0],
                    'file' => $file
                ]);
            } else if (strpos($file,'divorce-') !== false) {
                $split = explode('.',$file);
                array_push($divorceArr, [
                    'code' => $split[0],
                    'file' => $file
                ]);
            } else if (strpos($file,'military-') !== false) {
                $split = explode('.',$file);
                array_push($militaryArr, [
                    'code' => $split[0],
                    'file' => $file
                ]);
            } else if (strpos($file,'name-change-') !== false) {
               $split = explode('.',$file);
               array_push($nameChangeArr, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           } else if (strpos($file,'reference-letter-') !== false) {
               $split = explode('.',$file);
               array_push($refArr, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           } else if (strpos($file,'bank-') !== false) {
               $split = explode('.',$file);
               array_push($bankArr, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           } else if (strpos($file,'proof-') !== false) {
               $split = explode('.',$file);
               array_push($proofArr, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           } else if (strpos($file,'higher-learning-') !== false) {
               $split = explode('.',$file);
               array_push($confArr, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           } else if (strpos($file,'affidavit-') !== false) {
               $split = explode('.',$file);
               array_push($affArr, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           } else if (strpos($file,'HIV-') !== false) {
               $split = explode('.',$file);
               array_push($hivArr, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           } else if (strpos($file,'real-estate-') !== false) {
               $split = explode('.',$file);
               array_push($reArray, [
                   'code' => $split[0],
                   'file' => $file
               ]);
           }
        }

        $array['passports']     = $passArr;
        $array['pass_count']    = count($passArr);
        $array['identities']    = $idArr;
        $array['id_count']      = count($idArr);
        $array['notary']        = $ntArray;
        $array['birth_count']   = count($birthArr);
        $array['birth']         = $birthArr;
        $array['police']        = $policeArr;
        $array['police_count']      = count($policeArr);
        $array['marriage']          = $marriageArr;
        $array['marriage_count']    = count($marriageArr);
        $array['divorce']           = $divorceArr;
        $array['divorce_count']     = count($divorceArr);
        $array['military']          = $militaryArr;
        $array['military_count']    = count($militaryArr);
        $array['name']              = $nameChangeArr;
        $array['name_count']        = count($nameChangeArr);
        $array['ref']               = $refArr;
        $array['ref_count']         = count($refArr);
        $array['bank']              = $bankArr;
        $array['bank_count']        = count($bankArr);
        $array['proof']             = $proofArr;
        $array['proof_count']       = count($proofArr);
        $array['conf']              = $confArr;
        $array['conf_count']        = count($confArr);
        $array['affidavit']         = $affArr;
        $array['affidavit_count']   = count($affArr);
        $array['hiv']               = $hivArr;
        $array['hiv_count']         = count($hivArr);
        $array['re']                = $reArray;
        $array['re_count']          = count($reArray);
        return $array;
    }

}
