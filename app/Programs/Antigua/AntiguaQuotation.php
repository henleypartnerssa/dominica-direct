<?php

namespace App\Programs\Antigua;

use App\Programs\FormulaFields;
use Auth;
use Carbon\Carbon;

class AntiguaQuotation extends FormulaFields {

    private $type;

    public function __construct($type) {
        $this->type = $type;
        $this->setBaseTotals($type);
        $this->setTotDependants();
    }

    private function setBaseTotals($type) {
        $this->minimum_investment       = ($type === 'Donation') ? number_format(200.000, 3) : 0.0;
        $this->real_estate_investment   = ($type === 'Real Estate') ? number_format(400.000, 3) : 0.0;
        $this->cash_investment          = ($type === 'Business') ? number_format(1500.000, 3) : 0.0;

        $this->ddf_main_applicant       = number_format(7.500, 3);
        $this->ddf_spouse               = number_format(7.500, 3);
        $this->ddf_dependant_18_plus_or_parent_65_plus = number_format(4.000, 3);
        $this->ddf_dependant_12         = number_format(2.000, 3);
        $this->ddf_dependant_13_15      = number_format(2.000, 3);
        $this->ddf_dependant_16_17      = number_format(2.000, 3);

        $this->gf_single_applicant      = number_format(50.000, 3);
        $this->gf_spouse                = number_format(50.000, 3);
        $this->gf_child_18_plus         = number_format(50.000, 3);
        $this->gf_parent_65_plus        = number_format(50.000, 3);
        $this->gf_childern_below_18yrs  = number_format(25.000, 3);
        $this->gf_passport_card_fee_per_person_over_16   = floatval(0.3);
        $this->gf_passport_card_fee_per_person_under_16  = floatval(0.3);

        $this->pf_single_application    = number_format(35.000,3);
        $this->pf_up_to_3_dependents    = number_format(45.000,3);
        $this->pf_4_6_dependents        = number_format(60.000,3);
        $this->pf_7_plus_dependents     = number_format(70.000,3);
        $this->pf_additional_applicant  = number_format(5.000,3);
    }

    private function setTotDependants() {

        $usersMembers = Auth::user()->profiles()->first()->family_members()->where('is_included', true)->get();

        foreach($usersMembers as $familyMember) {

            $dependantType = $familyMember->dependant_type->type;

            if ($dependantType === 'Spouse') {
                $this->spouse++;
                continue;
            }

            $this->setDependant($familyMember->date_of_birth);

        }

    }

    private function setDependant($dateOfBirth) {
        $dob    = Carbon::parse($dateOfBirth);
        $now    = Carbon::now();
        $diff   = $dob->diffInYears($now);

        switch ($diff) {
            case ($diff < 11):
                $this->child_0_11++;
                break;
            case ($diff === 12):
                $this->child_12++;
                break;
            case ($diff > 12 && $diff <= 15):
                $this->child_13_15++;
                break;
            case ($diff > 15 && $diff <= 17):
                $this->child_16_17++;
                break;
            case ($diff > 17 && $diff <= 25):
                $this->child_18_25++;
                break;
            case ($diff === 26):
                $this->child_26++;
                break;
            case ($diff > 55 && $diff <= 64):
                $this->parent_55_64++;
                break;
            case ($diff >= 65):
                $this->parent_65++;
                break;
            default:
                break;
        }

    }

    public function nonRefContributions() {
        return ($this->type == "Donation" || $this->type == "Real Estate") ? number_format($this->minimum_investment, 3) : number_format(15.000, 3);
    }

    public function finInstruments() {
        return floatval(0.0);
    }

    public function RealEstatePurchase() {
        return ($this->type == "Real Estate") ? number_format($this->real_estate_investment,3) : floatval(0.0);
    }

    public function GovernmentFees() {

        $totUnder   = $this->child_0_11 + $this->child_12 + $this->child_13_15 + $this->child_16_17;
        $totOver    = $this->child_18_25 + $this->parent_65;

        $main   = ($this->mainApplicant + $this->spouse) * number_format(50.000, 3);
        $under  = $totUnder * $this->gf_childern_below_18yrs;
        $over   = $totOver * $this->gf_child_18_plus;

        $total  = 0.0;

        if($this->type == "Donation") {
            if ($totUnder == 0 && $totOver == 0) {
                $total = $main;
            } else if (($totUnder + $totOver) <= 2) {
                $total = 100.000;
            } else if (($totUnder + $totOver) >= 3) {

                if ($totUnder == 1) {
                    $total = ($main + $under + $over) - number_format(75.000, 3);
                } else if ($totUnder >= 2) {
                    $total = ($main + $under + $over) - number_format(50.000, 3);
                } else {
                    $total = ($main + $under + $over) - number_format(100.000, 3);
                }
            }
        } else {
            $total = $main + $under + $over;
        }

        return number_format($total, 3);
    }

    public function professionalFees() {
        $main   = $this->mainApplicant + $this->spouse;
        $dep    = $this->child_0_11 + $this->child_12 + $this->child_13_15 + $this->child_16_17 + $this->child_18_25;
        $par    = $this->parent_65;
        $tot    = $main + $dep + $par;
        $final  = 0.0;

        switch ($tot) {
            case 1:
                $final = $this->pf_single_application;
                break;
            case 2: case 3: case 4:
                $final = $this->pf_up_to_3_dependents;
                break;
            case 5: case 6:
                $final = $this->pf_4_6_dependents;
                break;
            case 7:
                $final = $this->pf_7_plus_dependents;
                break;
            default:
                $final = ($tot >= 8)    ? ($tot - 7) * $this->pf_additional_applicant + $this->pf_7_plus_dependents
                                        : $this->pf_additional_applicant + $this->pf_7_plus_dependents;

        }

        return number_format($final, 3);

    }

    public function govDueDilFees() {

        $ddf_1  = ($this->mainApplicant * $this->ddf_main_applicant) + ($this->spouse * $this->ddf_spouse);
        $ddf_2  = ($this->child_0_11 * $this->ddf_dependant_0_11) + ($this->child_12 * $this->ddf_dependant_12);
        $ddf_3  = ($this->child_13_15 * $this->ddf_dependant_13_15) + ($this->child_16_17 + $this->ddf_dependant_13_15);
        $ddf_4  = ($this->child_18_25 * $this->ddf_dependant_18_plus_or_parent_65_plus) + ($this->parent_65 * $this->ddf_dependant_18_plus_or_parent_65_plus);

        return number_format($ddf_1 + $ddf_2 + $ddf_3 + $ddf_4, 3);

    }

    public function cardFees() {
        $main   = $this->mainApplicant + $this->spouse;
        $dep    = $this->child_0_11 + $this->child_12 + $this->child_13_15 + $this->child_16_17 + $this->child_18_25;
        $par    = $this->parent_65;
        $tot    = $main + $dep + $par;
        $final  = $tot * 0.3;

        return number_format($final, 3);
    }

    public function misc() {
        return floatval(0.0);
    }

    public function qRequirementsTotal() {
        return number_format($this->nonRefContributions() + $this->finInstruments() + $this->RealEstatePurchase(), 3);
    }

    public function otherAppCostTotal() {
        return number_format($this->GovernmentFees() + $this->professionalFees() + $this->govDueDilFees() + $this->cardFees() + $this->misc(), 3);
    }

    public function finalTotal() {
        return number_format($this->qRequirementsTotal() + $this->otherAppCostTotal(), 3);
    }

    public function generalInfo() {
        return "May incur a 2.5% stamp duty if brought from certain developers, 1.0% property closing costs, and escrow fees if buying off plan.";
    }

    public function optCosts() {
        return "VIP service is offered at 50% of professional fees";
    }

}
