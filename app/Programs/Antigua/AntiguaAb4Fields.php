<?php

namespace App\Programs\Antigua;

use Carbon\Carbon;
use App\Application;
use App\Country;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\Profile;
use AppHelpers;
use Auth;

class AntiguaAb4Fields {

    private $profile        = null;
    private $case           = null;
    public $fieldset        = null;
    private $index  = 1;
    private $data   = [];

    public function __construct($pdf,$case_id,$profileId) {
        $this->case     = Application::findOrFail($case_id);
        $this->profile  = Profile::find($profileId);
        $this->mapAb4Fieds();
    }

    private function mapAb4Fieds() {

        $this->fieldset = [
            'A' => [
                'surname'           => $this->profile->last_name,
                'name'              => $this->profile->first_name,
                'place_of_birth'    => $this->profile->place_of_birth.', '.$this->profile->country_of_birth,
                'd1'                => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[0],
                'd2'                => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[1],
                'm1'                => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[0],
                'm2'                => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[1],
                'y1'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[0],
                'y2'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[1],
                'y3'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[2],
                'y4'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[3],
                $this->profile->gender => $this->profile->gender
            ],
            'B' => $this->getIncludedDep()
        ];
    }

    private function getIncludedDep() {
        $dep = $this->profile->family_members()->where('is_included', true)->get();
        $index = 1;
        $array = [];
        foreach ($dep as $key => $value) {
            $dep_type   = DependantType::findOrfail($value->dependant_type_id)->type;
            $rel        = $value->relationship;
            $array['b_names_'.$index]           = $value->first_name.' '.$value->surname;
            $array['b_dob_'.$index]             = $value->date_of_birth;
            $array['b_dob_relationship_'.$index]    = ($dep_type === $rel) ? $rel : $rel.' '.$dep_type;
            ++$index;
        }
        return $array;
    }

    private function addrByTypeHelper($type) {
        $str        = "";
        $address    = $this->profile->addresses()->where('address_type', $type)->get();

        foreach ($address as $addr) {
            $country = $addr->country;
            $str1 = $addr->street1.' ';
            $str2 = isset($addr->street2) ? $addr->street2.' ' : "";
            $str = $str1.$str2.$country.$addr->town.' '.$addr->postal_code;
        }
        return $str;
    }

    private function dateHlper($date, $stamp) {
        $dt = Carbon::parse($date);

        if ($stamp === 'day') {
            return (strlen($dt->day) > 1) ? $dt->day : '0'.$dt->day;
        } else if ($stamp === 'month') {
            return (strlen($dt->month) > 1) ? $dt->month : '0'.$dt->month;
        } else {
            return null;
        }
    }

}
