<?php

namespace App\Programs\Antigua;

use Carbon\Carbon;
use App\Application;
use App\Country;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\Profile;
use AppHelpers;
use Auth;

class AntiguaAb3Fields {

    private $profile        = null;
    private $case           = null;
    public $fieldset        = null;
    private $index  = 1;
    private $data   = [];

    public function __construct($pdf,$case_id,$profileId) {
        $this->case             = Application::findOrFail($case_id);
        $this->profile          = Profile::find($profileId);
        $this->mapAb3Fieds();
    }

    private function mapAb3Fieds() {

        $passport_1 = $this->profile->passports()->orderBy('created_at', 'asc')->first();
        $passport_2 = $this->profile->passports()->orderBy('created_at', 'desc')->first();
        $this->fieldset = [
            'A' => [
                'surname'           => $this->profile->last_name,
                'name'              => $this->profile->first_name,
                'place_of_birth'    => $this->profile->place_of_birth,
                'country'           => $this->profile->country_of_birth,
                'address'           => $this->addrByTypeHelper('res'),
                'passport_1'        => $passport_1->issuing_country.' | '.$passport_1->passport_no,
                'passport_2'        => $passport_2->issuing_country.' | '.$passport_2->passport_no,
                'd1'                => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[0],
                'd2'                => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[1],
                'm1'                => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[0],
                'm2'                => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[1],
                'y1'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[0],
                'y2'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[1],
                'y3'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[2],
                'y4'                => strval(Carbon::parse($this->profile->date_of_birth)->year)[3],
                $this->profile->gender => $this->profile->gender,
                'doctor_name'       => $this->profile->antigua_medical->doctor_name,
                'doctor_address'    => $this->addrByTypeHelper('doc'),
            ],
            'B' => [
                $this->boolHelper('tb', $this->profile->antigua_medical->tb) => $this->boolHelper('tb', $this->profile->antigua_medical->tb),
                $this->boolHelper('hp', $this->profile->antigua_medical->hp) => $this->boolHelper('hp', $this->profile->antigua_medical->hp),
                $this->boolHelper('tp', $this->profile->antigua_medical->tp) => $this->boolHelper('tp', $this->profile->antigua_medical->tp),
                $this->boolHelper('other_comm', $this->profile->antigua_medical->other_comm) => $this->boolHelper('other_comm', $this->profile->antigua_medical->other_comm),
                $this->boolHelper('other_heart', $this->profile->antigua_medical->other_heart) => $this->boolHelper('other_heart', $this->profile->antigua_medical->other_heart),
                $this->boolHelper('stroke', $this->profile->antigua_medical->stroke) => $this->boolHelper('stroke', $this->profile->antigua_medical->stroke),
                $this->boolHelper('idd', $this->profile->antigua_medical->idd) => $this->boolHelper('idd', $this->profile->antigua_medical->idd),
                $this->boolHelper('cancer', $this->profile->antigua_medical->cancer) => $this->boolHelper('cancer', $this->profile->antigua_medical->cancer),
                $this->boolHelper('aids_hiv', $this->profile->antigua_medical->aids_hiv) => $this->boolHelper('aids_hiv', $this->profile->antigua_medical->aids_hiv),
                $this->boolHelper('b2', $this->profile->antigua_medical->b2) => $this->boolHelper('b2', $this->profile->antigua_medical->b2),
                $this->boolHelper('b3', $this->profile->antigua_medical->b3) => $this->boolHelper('b3', $this->profile->antigua_medical->b3),
                $this->boolHelper('b4', $this->profile->antigua_medical->b4) => $this->boolHelper('b4', $this->profile->antigua_medical->b4),
                $this->boolHelper('b5', $this->profile->antigua_medical->b5) => $this->boolHelper('b5', $this->profile->antigua_medical->b5),
                $this->boolHelper('b6', $this->profile->antigua_medical->b6) => $this->boolHelper('b6', $this->profile->antigua_medical->b6),
                'b6_date' => (!empty($this->profile->antigua_medical->b6_date)) ? $this->profile->antigua_medical->b6_date : '',
                $this->boolHelper('b7', $this->profile->antigua_medical->b7) => $this->boolHelper('b7', $this->profile->antigua_medical->b7),
                'b8' => $this->multiline("b8_line", $this->profile->reason_for_citizenship, 119, 13)
            ]
        ];
    }

    private function boolHelper($fieldName, $value) {
        if ($value === true) {
            return $fieldName.'_'.'yes';
        } else {
            return $fieldName.'_'.'no';
        }
    }

    private function addrByTypeHelper($type) {
        $str        = "";
        $address    = $this->profile->addresses()->where('address_type', $type)->get();

        foreach ($address as $addr) {
            $country = $addr->country;
            $str1 = $addr->street1.' ';
            $str2 = isset($addr->street2) ? $addr->street2.' ' : "";
            $str = $str1.$str2.$country.$addr->town.' '.$addr->postal_code;
        }
        return $str;
    }

    private function dateHlper($date, $stamp) {
        $dt = Carbon::parse($date);

        if ($stamp === 'day') {
            return (strlen($dt->day) > 1) ? $dt->day : '0'.$dt->day;
        } else if ($stamp === 'month') {
            return (strlen($dt->month) > 1) ? $dt->month : '0'.$dt->month;
        } else {
            return null;
        }
    }

    private function addrByAddrHelper($address) {
        $str        = "";
        $country    = $address->country;
        $str1       = $address->street1.' ,';
        $str2       = isset($address->street2) ? $address->street2.' ,' : "";
        $str        = $str1.$str2.$address->town.', '.$country.', '.$address->postal_code;
        return $str;
    }

    private function multiline($field, $text, $maxChar, $maxSentances) {
        $char_coount    = $maxChar;
        $sentance_count = 1;
        $max_sentance   = $maxSentances;
        $split          = explode(' ', $text);
        $array          = [];
        $sentance       = "";
        foreach ($split as $key => $value) {
            $tmp = $sentance.' '.$value;
            if (strlen($tmp) > $char_coount) {
                if($sentance_count !== $max_sentance) {
                    $array[$field.'_'.$sentance_count] = $sentance;
                    $sentance = $value;
                    ++$sentance_count;
                } else if ($sentance_count > $max_sentance) {
                    return $array;
                }
            } else {
                $sentance = $tmp;
            }
            if ($key === (count($split) - 1)) {
                $array[$field.'_'.$sentance_count] = $sentance;
            }
        }
        return $array;
    }

}
