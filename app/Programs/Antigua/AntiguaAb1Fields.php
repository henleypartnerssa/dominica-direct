<?php

namespace App\Programs\Antigua;

use Carbon\Carbon;
use App\Application;
use App\Country;
use App\Profile;
use App\LanguageType;
use App\LanguageLevel;
use App\DependantType;
use App\AntiguaProgram;
use AppHelpers;
use Auth;

class AntiguaAb1Fields {

    private $pdf            = null;
    private $user           = null;
    private $profile        = null;
    private $case           = null;
    private $Family         = null;
    private $qualifications = null;
    private $bankAccount    = null;
    private $business       = null;
    private $spouse         = null;

    public $fieldset        = null;

    private $index  = 1;
    private $data   = [];

    public function __construct($pdf,$case_id,$profileId) {
        $this->pdf              = $pdf;
        $this->case             = Application::findOrFail($case_id);
        $this->user             = Auth::user();
        $this->profile          = Profile::find($profileId);
        $this->Family           = $this->profile->family_members()->get();
        $this->spouse           = $this->determineSpouse();
        $this->qualifications   = $this->profile->qualifications()->get();
        $this->bankAccount      = $this->profile->bank()->get()->first();
        $this->business         = $this->profile->Work_or_businesses()->get()->first();
        $this->mapAb1Fieds();
    }

    private function mapAb1Fieds() {

        $this->fieldset = [
            'statement'     => $this->multiline("stat_line", $this->profile->reason_for_citizenship, 119, 10),
            'completing'    => [$this->case->comp_forms_as => $this->case->comp_forms_as],
            'A'             => $this->sectionA(),
            'B'             => $this->sectionB(),
            'C'             => $this->getDeclarations(),
            'D'             => $this->getComments()
        ];
    }

    private function sectionA() {

        return [
            'A1'    => [
                $this->profile->title   => $this->profile->title,
                'title_other_text'      => $this->profile->title_other,
                'surname'               => $this->profile->last_name

            ],
            'A2'    => ['legal_passport_names'      => $this->profile->first_name.' '.$this->profile->legal_passport_middle_names],
            'A3'    => ['former_names'              => $this->profile->legal_former_maiden_name.' '.$this->profile->former_other_names],
            'A4'    => ['name_change'               => $this->profile->legal_name_change_date.' '.$this->profile->legal_name_change_reason],
            'A5-1'  => ['birth_certificte_names'    => $this->profile->first_name.' '.$this->profile->legal_bc_middle_names],
            'A5-2'  => ['place_of_birth'            => $this->profile->place_of_birth],
            'A6-1'  => ['ethnic_names'              => $this->profile->legal_es_first_name.' '.$this->profile->legal_es_surname],
            'A6-2'  => ['country_of_birth'          => $this->profile->country_of_birth],
            'A7'    => [
                'D1' => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[0],
                'D2' => strval($this->dateHlper($this->profile->date_of_birth, 'day'))[1],
                'M1' => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[0],
                'M2' => strval($this->dateHlper($this->profile->date_of_birth, 'month'))[1],
                'Y1' => strval(Carbon::parse($this->profile->date_of_birth)->year)[0],
                'Y2' => strval(Carbon::parse($this->profile->date_of_birth)->year)[1],
                'Y3' => strval(Carbon::parse($this->profile->date_of_birth)->year)[2],
                'Y4' => strval(Carbon::parse($this->profile->date_of_birth)->year)[3],
            ],
            'A8'    => [$this->profile->gender          => $this->profile->gender],
            'A10'   => [$this->profile->marital_status  => $this->profile->marital_status],
            'A12'   => ['citizenship_at_birth'          => $this->profile->citizenship_at_birth],
            'A11'   => ['details_marriage_law_status'   => $this->A11Helper()],
            'A13'   => ['other_citizenship'             => $this->profile->citizen_other],
            'A14'   => [
                'passport_1' => $this->passportsHelper('asc', 'passport_1'),
                'passport_2' => $this->passportsHelper('desc', 'passport_2'),
            ],
            'A15'   => [
                'id_no'         => $this->profile->identity_card_no,
                'id_country'    => $this->profile->identity_card_no_issuing_country
            ],
            'A16'   => [
                'social_sec_no'         => $this->profile->social_security_or_insurance_no,
                'social_sec_country'    => $this->profile->social_security_or_insurance_country
            ],
            'A17'   => ['languages' => $this->langHelper()],
            'A18'   => [
                'primary_address'       => $this->addrByTypeHelper("res"),
                'alternative_address'   => $this->addrByTypeHelper("alt"),
                'tel'                   => $this->profile->permanent_telephone_no,
                'mobile'                => $this->profile->permanent_mobile_no,
                'email'                 => $this->profile->personal_email_address
            ],
            'A19'   => [
                'occupation'                    => $this->business->primary_occupation,
                $this->selfEmpoyedField($this->business->self_employed)  => $this->selfEmpoyedField($this->business->self_employed),
                'nature_of_business'            => $this->business->nature_of_business,
                'important'                     => $this->business->mi_persons_companies,
                'business_name'                 => $this->business->name_of_business,
                'business_web'                  => $this->business->business_web_url,
                'business_tel'                  => $this->business->business_tel_no,
                'business_email'                => $this->business->business_email,
                'business_address'              => $this->addrByAddrHelper($this->business->address()->get()->first()),
            ],
            'A20'   => [
                'net_income'        => AppHelpers::stripCurrency($this->business->gross_net_income),
                'gross_'.AppHelpers::getCurrencyFromTotal($this->business->gross_net_income) => 'O',
                'source_of_income'  => $this->business->source_of_income,
                'net_'.AppHelpers::getCurrencyFromTotal($this->business->gross_net_income) => 'O',
                'geo_activities'    => $this->business->business_activities_geo,
                'net_worth'         => AppHelpers::stripCurrency($this->business->total_net_worth),
                'total_net_held_as' => $this->buildNetWorth(),
                'geo_net_worth'     => $this->business->business_net_worth_geo,
            ],
            'A21' => $this->multiline("sum_line", $this->business->total_net_worht_summary, 119, 10),
            'A26' => [
                'account_owner'     => $this->bankAccount->account_name_of,
                'account_no'        => $this->bankAccount->account_no,
                'account_bic'       => $this->bankAccount->account_iban_bic,
                'account_address'   => $this->addrByAddrHelper($this->bankAccount->address()->get()->first()),
            ],
        ];
    }

    private function sectionB() {

        $this->setB3("Brother");
        $this->setB3("Sister");

        return [
            'B1'    => $this->setParent('Father'),
            'B2'    => $this->setParent('Mother'),
            'B3'    => $this->data,
            'B4'    => ($this->profile->belongs_to === "Child") ? [] : [
                'spouse_surname'        => $this->spouse->last_name,
                'spouse_name'           => $this->spouse->first_name,
                'spouse_place_of_birth' => $this->spouse->place_of_birth,
                'spouse_citizenship'    => $this->spouse->country_of_birth,
                'spouse_country'        => $this->spouse->citizenship_at_birth,
                'spouse_gender_f'       => 'spouse_gender_f',
                'spouse_included_'.$this->spouseHelper($this->spouse->is_included) => 'spouse_included_'.$this->spouseHelper($this->spouse->is_included),
                'spouse_d1'             => strval($this->dateHlper($this->spouse->date_of_birth, 'day'))[0],
                'spouse_d2'             => strval($this->dateHlper($this->spouse->date_of_birth, 'day'))[1],
                'spouse_m1'             => strval($this->dateHlper($this->spouse->date_of_birth, 'month'))[0],
                'spouse_m2'             => strval($this->dateHlper($this->spouse->date_of_birth, 'month'))[1],
                'spouse_y1'             => strval(Carbon::parse($this->spouse->date_of_birth)->year)[0],
                'spouse_y2'             => strval(Carbon::parse($this->spouse->date_of_birth)->year)[1],
                'spouse_y3'             => strval(Carbon::parse($this->spouse->date_of_birth)->year)[2],
                'spouse_y4'             => strval(Carbon::parse($this->spouse->date_of_birth)->year)[3],
            ],
            'B5'    => $this->getChildren(),
            'B6'    => $this->getProfileAddresses(),
            'B7'    => $this->getProfileEducation(),
            'B8'    => $this->getProfileReferences()
        ];
    }

    private function getDeclarations() {
        $declarations = $this->profile->antigua_declarations()->get();
        $array = [];
        foreach($declarations as $declaration) {
            $array[$declaration->code.'_'.$this->spouseHelper($declaration->answer)] = $declaration->code.'_'.$this->spouseHelper($declaration->answer);
        }
        return $array;
    }

    private function getComments() {
        $declarations = $this->profile->antigua_declarations()->where("message", "!=", "")->get();
        $array = [];
        $index = 1;
        foreach($declarations as $declaration) {
            $array['q'.$index.'_num']       = $declaration->code;
            $array['q'.$index.'_message']   = $declaration->message;
            ++$index;
        }
        return $array;
    }

    private function getProfileAddresses() {
        $addresses  = $this->profile->addresses()->where('address_type', 'ten')->get();
        $index      = 1;
        $array      = [];

        foreach($addresses as $address) {
            $array['add_from_date_'.$index] = $address->pivot->from_date;
            $array['add_to_date_'.$index]   = $address->pivot->to_date;
            $array['add_to_add_'.$index]    = $this->addrByAddrHelper($address);
            ++$index;
        }

        return $array;

    }

    private function getProfileEducation() {
        $qualifications = $this->profile->qualifications()->get();
        $index = 1;
        $array = [];

        foreach($qualifications as $qualification) {
            $array['qual_'.$index.'_from_date'] = $qualification->from_date;
            $array['qual_'.$index.'_to_date']   = $qualification->to_date;
            $array['qual_'.$index.'_school']    = $qualification->qualification_name;
            $array['qual_'.$index.'_location']  = $qualification->location;
            $array['qual_'.$index]              = $qualification->qualification_type;
            ++$index;
        }

        return $array;
    }

    private function getProfileReferences() {
        $references = $this->profile->employment_references()->get();
        $index      = 1;
        $array      = [];

        foreach($references as $reference) {
            $array['emp_'.$index.'_from_date']  = $reference->from_date;
            $array['emp_'.$index.'_to_date']    = $reference->to_date;
            $array['emp_'.$index.'_occ']        = $reference->occupation;
            $array['emp_'.$index.'_emp']        = $reference->employer;
            $array['emp_'.$index.'_location']   = $reference->location;
            $array['emp_'.$index.'_type']       = $reference->type_of_business;
            ++$index;
        }

        return $array;
    }

    private function spouseHelper($answer) {
        if ($answer === true) {
            return 'true';
        } else {
            return 'false';
        }
    }

    private function multiline($field, $text, $maxChar, $maxSentances) {
        $char_coount    = $maxChar;
        $sentance_count = 1;
        $max_sentance   = $maxSentances;
        $split          = explode(' ', $text);
        $array          = [];
        $sentance       = "";
        foreach ($split as $key => $value) {
            $tmp = $sentance.' '.$value;
            if (strlen($tmp) > $char_coount) {
                if($sentance_count !== $max_sentance) {
                    $array[$field.'_'.$sentance_count] = $sentance;
                    $sentance = $value;
                    ++$sentance_count;
                } else if ($sentance_count > $max_sentance) {
                    return $array;
                }
            } else {
                $sentance = $tmp;
            }
            if ($key === (count($split) - 1)) {
                $array[$field.'_'.$sentance_count] = $sentance;
            }
        }
        return $array;
    }

    private function dateHlper($date, $stamp) {
        $dt = Carbon::parse($date);

        if ($stamp === 'day') {
            return (strlen($dt->day) > 1) ? $dt->day : '0'.$dt->day;
        } else if ($stamp === 'month') {
            return (strlen($dt->month) > 1) ? $dt->month : '0'.$dt->month;
        } else {
            return null;
        }
    }

    private function A11Helper() {
        if (empty($spouse)) {
            return "";
        }
        $spouse     = ($this->profile->marital_status === 'Divorced') ? 'Ex-Spouse' : 'Spouse';
        $str        = $spouse.' '.
                        $this->spouse->first_name.' '.$this->spouse->last_name.' '.
                        'married '.$this->profile->data_of_marriage.' in '.$this->profile->place_of_marriage;

        if ($this->profile->marital_status === 'Divorced') {
            $str = $str.', Divorse granted in '.$this->profile->place_divorce.' on '.$this->profile->date_divorce;
        }

        if ($this->profile->marital_status === "Never Married") {
            $str = 'Never Married';
        }

        return  $str;

    }

    private function passportsHelper($condition, $position) {

        $count      = $this->profile->passports()->count();
        $passports  = $this->profile->passports()->orderBy('created_at', $condition)->first();
        $pos        = $position;

        if ($position === 'passport_2' && $count === 1 ) {
            $data = [
                $pos.'_country' => "",
                $pos.'_no'      => "",
                $pos.'_issue'   => "",
                $pos.'_date'    => "",
                $pos.'_exp'     => "",
            ];
            return $data;
        }

        $data = [
            $pos.'_country' => $passports->issuing_country,
            $pos.'_no'      => $passports->passport_no,
            $pos.'_issue'   => $passports->place_of_issue,
            $pos.'_date'    => $passports->date_of_issue,
            $pos.'_exp'     => $passports->date_of_expiration,
        ];

        return $data;

    }

    private function langHelper() {
        $str    = "";
        $langs  = $this->profile->languages()->get()->toArray();

        for($i = 0; $i < count($langs); $i++) {

            $lkpLang    = LanguageType::find((int)$langs[$i]['language_type_id'])->type;
            $lkpLevel   = LanguageLevel::find((int)$langs[$i]['language_level_id'])->level;

            if ($i !== count($langs) - 1) {
                $str = $str.$langs[$i]['action'].' '.$lkpLang.' '.$lkpLevel.',';
            } else {
                $str = $str.$langs[$i]['action'].' '.$lkpLang.' '.$lkpLevel;
            }

        }
        return $str;
    }

    private function addrByTypeHelper($type) {
        $str        = "";
        $address    = $this->profile->addresses()->where('address_type', $type)->get();

        foreach ($address as $addr) {
            $country    = $addr->country;
            $str1       = $addr->street1.' ';
            $str2       = isset($addr->street2) ? $addr->street2.' ' : "";
            $str        = $str1.$str2.$country.$addr->town.' '.$addr->postal_code;
        }
        return $str;
    }

    private function addrByAddrHelper($address) {
        $str        = "";
        if (!empty($address)) {
            $country    = $address->country;
            $str1       = $address->street1.' ,';
            $str2       = isset($address->street2) ? $address->street2.' ,' : "";
            $str        = $str1.$str2.$address->town.', '.$country.', '.$address->postal_code;
        }
        return $str;
    }

    private function selfEmpoyedField($value) {
        return ($value === true) ? "self_yes" : "self_no";
    }

    private function setParent($type) {

        if ($this->profile->belongs_to === "Child" && $type === "Father") {
            $parent = $this->user->profiles()->where('belongs_to', 'Principal Applicant')->first();
        } else if ($this->profile->belongs_to === "Child" && $type === "Mother") {
            $parent = $this->user->profiles()->where('belongs_to', 'Spouse')->first();
        } else {
            $parent = $this->Family->where('relationship', $type)->first();
        }

        $t      = strtolower($type);
        $gender = ($t === 'father') ? $t.'_male' : $t.'_female';
        $addr   = $parent->addresses()->where('address_type', 'res')->first();
        return [
            $t.'_surname'           => $parent->last_name,
            $t.'_name'              => $parent->first_name,
            $gender                 => $gender,
            $t.'_d1'                => strval($this->dateHlper($parent->date_of_birth, 'day'))[0],
            $t.'_d2'                => strval($this->dateHlper($parent->date_of_birth, 'day'))[1],
            $t.'_m1'                => strval($this->dateHlper($parent->date_of_birth, 'month'))[0],
            $t.'_m2'                => strval($this->dateHlper($parent->date_of_birth, 'month'))[1],
            $t.'_y1'                => strval(Carbon::parse($parent->date_of_birth)->year)[0],
            $t.'_y2'                => strval(Carbon::parse($parent->date_of_birth)->year)[1],
            $t.'_y3'                => strval(Carbon::parse($parent->date_of_birth)->year)[2],
            $t.'_y4'                => strval(Carbon::parse($parent->date_of_birth)->year)[3],
            $t.'_place_of_birth'    => $parent->place_of_birth,
            $t.'_citizenship'       => $parent->country_of_birth,
            $t.'_address'           => ($parent->addresses()->count() > 0) ? $this->addrByAddrHelper($addr) : "deceased",
        ];
    }

    private function setB3($type) {

        $typeId     = DependantType::all()->where('type', $type)->first()->dependant_type_id;
        $sibling    = $this->Family->where('dependant_type_id', $typeId);

        foreach($sibling as $b) {
            $this->data['sibling_'.$this->index.'_name']            = $b->first_name;
            $this->data['sibling_'.$this->index.'_surname']         = $b->last_name;
            $this->data['sibling_'.$this->index.'_palce_of_birth']  = $b->place_of_birth;
            $this->data['sibling_'.$this->index.'_citizenship']     = $b->country_of_birth;
            $this->data['sibling_'.$this->index.'_residence']       = (empty($b->citizenship_at_birth)) ? $b->country_of_birth : $b->citizenship_at_birth;
            $this->data['sibling_'.$this->index.'_'.strtolower($b->gender)] = 'sibling_'.$this->index.'_'.strtolower($b->gender);
            $this->data['sibling_'.$this->index.'_d1'] = strval($this->dateHlper($b->date_of_birth, 'day'))[0];
            $this->data['sibling_'.$this->index.'_d2'] = strval($this->dateHlper($b->date_of_birth, 'day'))[1];
            $this->data['sibling_'.$this->index.'_m1'] = strval($this->dateHlper($b->date_of_birth, 'month'))[0];
            $this->data['sibling_'.$this->index.'_m2'] = strval($this->dateHlper($b->date_of_birth, 'month'))[1];
            $this->data['sibling_'.$this->index.'_y1'] = strval(Carbon::parse($b->date_of_birth)->year)[0];
            $this->data['sibling_'.$this->index.'_y2'] = strval(Carbon::parse($b->date_of_birth)->year)[1];
            $this->data['sibling_'.$this->index.'_y3'] = strval(Carbon::parse($b->date_of_birth)->year)[2];
            $this->data['sibling_'.$this->index.'_y4'] = strval(Carbon::parse($b->date_of_birth)->year)[3];
            $this->index++;
        }
    }

    private function getChildren() {
        $typeId     = DependantType::all()->where('type', 'Child')->first()->dependant_type_id;
        $sibling    = $this->Family->where('dependant_type_id', $typeId);
        $profiles   = $this->user->profiles()->where('belongs_to', 'Child')->get();
        $array      = [];
        $index      = 1;
        $sibName    = '';
        $profiles->merge($sibling);

        foreach($profiles as $b) {
            if ($b->getTable() === "profiles") {
                $included = "true";
            } else {
                $included = "false";
            }
            if ($b->first_name === $sibName) {
                continue;
            } else {
                $sibName = $b->first_name;
            }
            $array['child_'.$index.'_name']             = $b->first_name;
            $array['child_'.$index.'_surname']          = $b->last_name;
            $array['child_'.$index.'_place_of_birth']   = $b->place_of_birth;
            $array['child_'.$index.'_citizenship']      = $b->country_of_birth;
            $array['child_'.$index.'_country']          = $b->citizenship_at_birth;
            $array['child_'.$index.'_included_'.$included]      = 'child_'.$index.'_included_'.$included;
            $array['child_'.$index.'_'.strtolower($b->gender)]  = 'child_'.$index.'_'.strtolower($b->gender);
            $array['child_'.$index.'_d1'] = strval($this->dateHlper($b->date_of_birth, 'day'))[0];
            $array['child_'.$index.'_d2'] = strval($this->dateHlper($b->date_of_birth, 'day'))[1];
            $array['child_'.$index.'_m1'] = strval($this->dateHlper($b->date_of_birth, 'month'))[0];
            $array['child_'.$index.'_m2'] = strval($this->dateHlper($b->date_of_birth, 'month'))[1];
            $array['child_'.$index.'_y1'] = strval(Carbon::parse($b->date_of_birth)->year)[0];
            $array['child_'.$index.'_y2'] = strval(Carbon::parse($b->date_of_birth)->year)[1];
            $array['child_'.$index.'_y3'] = strval(Carbon::parse($b->date_of_birth)->year)[2];
            $array['child_'.$index.'_y4'] = strval(Carbon::parse($b->date_of_birth)->year)[3];
            $index++;
        }
        return $array;
    }

    private function buildNetWorth() {
        $totalWorths    = $this->business->total_net_worths()->get();
        $array          = [];
        foreach ($totalWorths as $worth) {
            $type           = $worth->type;
            $amount         = AppHelpers::stripCurrency($worth->amount);
            $currency       = AppHelpers::getCurrencyFromTotal($worth->amount);
            $array[$type]   = $type;
            $array[$type.'_amount'] = $amount;
            $array[$type.'_'.$currency] = "O";
        }
        return $array;
    }

    private function determineSpouse() {
        $spouse = null;
        if ($this->profile->belongs_to === "Spouse") {
            $spouse = $this->user->profiles()->where('belongs_to', 'Principal Applicant')->first();
        } else if ($this->profile->belongs_to === "Principal Applicant") {
            $spouse = $this->user->profiles()->where('belongs_to', 'Spouse')->first();
        }

        return $spouse;
    }

}
