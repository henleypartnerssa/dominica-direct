<?php

namespace App\Programs;

class FormulaFields {
    
    public $mainApplicant = 1;
        
    // Dependants
    public $spouse          = 0;
    public $child_0_11      = 0;
    public $child_12        = 0;
    public $child_13_15     = 0;
    public $child_16_17     = 0;
    public $child_18_25     = 0;
    public $child_26        = 0;
    public $parent_55_64    = 0;
    public $parent_65       = 0;
    
    // Investment Totals
    public $minimum_investment      = 0.0;
    public $real_estate_investment  = 0.0;
    public $cash_investment         = 0.0;
    
    // Due Diligence Fees
    public $ddf_main_applicant      = 0.0;
    public $ddf_spouse              = 0.0;
    public $ddf_dependant_18_plus_or_parent_65_plus = 0.0;
    public $ddf_dependant_18_plus_or_parent_55_plus = 0.0;
    public $ddf_dependant_0_11      = 0.0;
    public $ddf_dependant_12        = 0.0;
    public $ddf_dependant_13_15     = 0.0;
    public $ddf_dependant_16_17     = 0.0;
    
    // Government Fees
    public $gf_single_applicant                = 0.0;
    public $gf_spouse                          = 0.0;
    public $gf_child_18_plus                   = 0.0;
    public $gf_parent_65_plus                  = 0.0;
    public $gf_childern_below_18yrs            = 0.0;
    public $gf_spouse_and_first_two_dependents = 0.0;
    public $gf_additional_after_first_two_child_18_plus_or_parent_65_plus  = 0.0;
    public $gf_additional_after_first_two_childern_below_18yrs             = 0.0;
    public $gf_main_applicant_and_spouse           = 0.0;
    public $gf_application_with_up_to_3_dependents = 0.0;
    public $gf_4_6_dependents                      = 0.0;
    public $gf_7_plus_dependents                   = 0.0;
    public $gf_any_dependent_aged_18_plus          = 0.0;
    public $gf_any_dependent_aged_0_17             = 0.0;
    public $gf_citizenship_certificate_fees_adults             = 0.0;
    public $gf_citizenship_certificate_fees_children           = 0.0;
    public $gf_additional_residence_cards_not_inc_initial_card = 0.0;
    public $gf_passport_card_fee_per_person_under_16           = 0.0;
    public $gf_passport_card_fee_per_person_over_16            = 0.0;
    
    // Professional Fees
    public $pf_single_application   = 0.0;
    public $pf_up_to_3_dependents   = 0.0;
    public $pf_4_6_dependents       = 0.0;
    public $pf_7_plus_dependents    = 0.0;
    public $pf_additional_applicant = 0.0;
    public $pf_spouse               = 0.0;
    public $pf_child_18_plus        = 0.0;
    public $pf_parent_65_plus       = 0.0;
    public $pf_parent_55_plus       = 0.0;
    public $pf_child_below_18yo     = 0.0;
    public $pf_pep_surcharge        = 0.0;
    
    // Other Fees
    public $other_bank_charge                           = 0.0;
    public $other_other_variable_fees_not_referenced    = 0.0;
    public $other_vip_professional_services             = 0.0;
    public $other_real_estate_lease_search_mandate      = 0.0;
    public $other_annual_property_management_fee        = 0.0;
}