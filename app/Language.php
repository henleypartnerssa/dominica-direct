<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{

    protected $table        = 'languages';
    protected $primaryKey   = 'language_id';
    protected $fillable     = [
        'language_type_id',
        'profile_id',
        'user_id',
        'group_id',
        'language_level_id',
        'action'
    ];

    public function language_levels() {
        return $this->belongsTo('App\LanguageLevel');
    }

    public function language_types() {
        return $this->belongsTo('App\LanguageType');
    }

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

}
