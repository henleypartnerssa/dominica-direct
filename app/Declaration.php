<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Declaration extends Model
{
    protected $table        = 'declarations';
    protected $primaryKey   = 'declaration_id';
    protected $fillable     = ['application_id','profile_id','group_id','user_id','code', 'answer', 'message'];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

}
