<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Crypt;
use Hash;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'group_id',
                            'name',
                            'surname',
                            'username',
                            'password',
                            'pin',
                            'token',
                            'doc_pass',
                            'cell_no',
                            'is_active',
                            'valid_pin',
                            'attempts'
                          ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['token', 'doc_pass', 'password'];

    /**
     * Encrypts the Password
     *
     * @param mixed $password
     */
    public function setPasswordAttribute($password) {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Encrypts the Token Attribute
     *
     * @param mixed $token
     */
    public function setTokenAttribute($token) {
        $this->attributes['token'] = Hash::make($token);
    }

    /**
     * User belongs to indavidual group.
     *
     * @return App\Group
     */
    public function group() {
        return $this->belongsTo('App\Group');
    }

    /**
     * Has many Applications.
     *
     * @return App\Application
     */
    public function profiles() {
        return $this->hasMany('App\Profile');
    }

    /**
     * User has zero or many quotaions.
     *
     * @return App\Quotaion
     */
    public function quotations() {
        return $this->hasMany('App\Quotation');
    }

    public function messages() {
        return $this->hasMany('App\Message');
    }
}
