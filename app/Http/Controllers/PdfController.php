<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Util\AppPdfHandler;
use App\Util\AppFileSystem;
use App\Application;
use App\Profile;
use App\Address;
use App\FamilyMember;
use Auth;
use Input;

class PdfController extends Controller
{

    public function __construct() {
        $this->middleware('nopin');
    }


    public function generatePdf(Request $request) {
        $case = Application::findOrfail((int)$request['case']);
        $pdfBuilder = new AppPdfHandler($request['form'], $request['form'], $case->application_id, $request['profile_id']);
        $pdfBuilder->setOutputFile($request['form'].".pdf",$case->case_no);
        $generated = (string)$pdfBuilder->generatePdf();
        if ($generated == 1) {
            return '1';
        } else {
            return '0';
        }
    }

    public function uploadPdf(Request $request) {
        $app        = Application::find($request['appId']);
        $storage    = storage_path().'/app/users/'.Auth::id().'/cases/';
        $fullPath   = $storage.$app->case_no.'/uploads/';
        $fullPathDD = $storage.$app->case_no.'/dd/';
        foreach ($request->all() as $key => $value) {
            if (Input::hasFile($key)) {
                $file   = Input::file($key);
                $ext    = $file->getClientOriginalExtension();
                if ($file->isValid() && $ext === 'pdf') {
                    $name   = $key.'.'.$ext;
                    $split  = explode('-',$key);
                    $path   = (in_array('dd', $split)) ? $fullPathDD : $fullPath;
                    $moved  = $file->move($path, $name);
                    if (!$moved) {
                        return 'failed';
                    } else {
                        return $key;
                    }
                } else {
                    return 'failed';
                }
            } else {
                continue;
            }
        }
        return 'failed';
    }

    public function downloadDocument($fileName) {
        $split      = explode('_', $fileName);
        $user_id    = explode('-',$split[0])[1];
        $upload     = storage_path('app').'/users/'.$user_id.'/cases/'.$split[0].'/uploads/';
        $download   = storage_path('app').'/users/'.$user_id.'/cases/'.$split[0].'/downloads/';
        $uFile      = $upload.$split[1];
        $dFile      = $download.$split[1];
        if (file_exists($uFile)) {
            return response()->download($uFile, $split[1], array('Content-Type: application/pdf'));
        } else if (file_exists($dFile)) {
            return response()->download($dFile, $split[1], array('Content-Type: application/pdf'));
        } else {
            return abort(404, 'Could not Find File');
        }
    }

    public function downloadQuote($fileName) {
        $filePath = storage_path('app').'/users/'.Auth::id().'/quotations/'.$fileName;
        if (file_exists($filePath)) {
            return response()->download($filePath, $fileName, array('Content-Type: application/pdf'));
        } else {
            return abort(404, 'Could not Find File');
        }
    }

    public function deleteDocument(Request $request) {
        $split      = explode('_', $request['file']);
        $user_id    = explode('-',$split[0])[1];
        $upload     = storage_path('app').'/users/'.$user_id.'/cases/'.$split[0].'/uploads/';
        $download   = storage_path('app').'/users/'.$user_id.'/cases/'.$split[0].'/downloads/';
        $uFile      = $upload.$split[1];
        $dFile      = $download.$split[1];
        if (file_exists($uFile)) {
            unlink($uFile);
            return 'ok';
        } else if (file_exists($dFile)) {
            unlink($dFile);
            return 'ok';
        } else {
            return 'error';
        }
    }

    public function deletePdf(Request $request) {
        $app        = Application::find($request['appId']);
        $profile    = $app->profiles()->first();
        $dd         = storage_path('app').'/users/'.$profile->user_id.'/cases/'.$app->case_no.'/dd/';
        $upload     = storage_path('app').'/users/'.$profile->user_id.'/cases/'.$app->case_no.'/uploads/';
        $download   = storage_path('app').'/users/'.$profile->user_id.'/cases/'.$app->case_no.'/downloads/';
        $ddFile     = $dd.$request['file'].'.pdf';
        $uFile      = $upload.$request['file'].'.pdf';
        $dFile      = $download.$request['file'].'.pdf';

        if (file_exists($uFile)) {
            unlink($uFile);
            return $request['file'];
        } else if (file_exists($dFile)) {
            unlink($dFile);
            return $request['file'];
        } else if (file_exists($ddFile)) {
            unlink($ddFile);
            return $request['file'];
        } else {
            return 'Error';
        }
    }

    public function documents() {
        $files = $this->getAllFiles();
        return view('documents', compact('files'));
    }

    public function documentsByType($id) {
        if ($id === 'uploads') {
            $files = $this->getUploads();
        } else if ($id === 'downloads') {
            $files = $this->getDownloads();
        } else {
            $files = $this->getByProgram($id);
        }
        return view('documents', compact('files'));
    }

    private function getAllFiles() {
        $storage    = storage_path().'/app/users/'.Auth::id().'/cases/';
        $dirs       = scandir($storage);
        $array      = [];
        foreach ($dirs as $dir) {
            if (strpos($dir,'.') !== false) {
                continue;
            }
            $files = scandir($storage.$dir.'/uploads');

            foreach ($files as $i => $file) {

                if (strpos($file,'.') !== false) {
                    continue;
                }

                $split  = explode('.',$file);
                $split2 = explode('-',$split[0]);

                array_push($array, [
                    'ref'       => $dir,
                    'program'   => 'Antigua and Barbuda',
                    'file'      => $file,
                    'type'      => $this->getType($split2),
                ]);
            }
        }
        return $array;
    }

    private function getByProgram($program) {
        $storage    = storage_path().'/app/users/'.Auth::id().'/cases/';
        $array      = [];
        $downloads  = scandir($storage.$program.'/downloads');
        $uploads    = scandir($storage.$program.'/uploads');

        foreach ($downloads as $i => $file) {

            if ($file === '.' || $file === '..') {
                continue;
            }

            $split  = explode('.',$file);
            $split2 = explode('-',$split[0]);

            array_push($array, [
                'ref'       => $program,
                'program'   => 'Antigua and Barbuda',
                'file'      => $file,
                'type'      => 'applicaiton form',
            ]);
        }

        foreach ($uploads as $i => $file) {

            if ($file === '.' || $file === '..') {
                continue;
            }

            $split  = explode('.',$file);
            $split2 = explode('-',$split[0]);

            array_push($array, [
                'ref'       => $program,
                'program'   => 'Antigua and Barbuda',
                'file'      => $file,
                'type'      => $this->getType($split2),
            ]);
        }
        return $array;
    }

    private function getUploads() {
        $storage    = storage_path().'/app/users/'.Auth::id().'/cases/';
        $dirs       = scandir($storage);
        $array      = [];
        foreach ($dirs as $dir) {
            if (strpos($dir,'.') !== false) {
                continue;
            }
            $files = scandir($storage.$dir.'/uploads');

            foreach ($files as $i => $file) {

                if ($i === 0 || $i === 1) {
                    continue;
                }

                $split  = explode('.',$file);
                $split2 = explode('-',$split[0]);

                array_push($array, [
                    'ref'       => $dir,
                    'program'   => 'Antigua and Barbuda',
                    'file'      => $file,
                    'type'      => $this->getType($split2),
                ]);
            }
        }
        return $array;
    }

    private function getDownloads() {
        $storage    = storage_path().'/app/users/'.Auth::id().'/cases/';
        $dirs       = scandir($storage);
        $array      = [];
        foreach ($dirs as $dir) {
            if (strpos($dir,'.') !== false) {
                continue;
            }
            $files = scandir($storage.$dir.'/downloads');

            foreach ($files as $i => $file) {

                if ($i === 0 || $i === 1) {
                    continue;
                }

                $split  = explode('.',$file);
                $split2 = explode('-',$split[0]);

                array_push($array, [
                    'ref'       => $dir,
                    'program'   => 'Antigua and Barbuda',
                    'file'      => $file,
                    'type'      => 'application form',
                ]);
            }
        }
        return $array;
    }

    private function getType($array) {
        $str = '';

        foreach ($array as $key => $value) {
            if ($key === count($array) - 1) {
                continue;
            }
            $str = $str.$value.' ';
        }
        return trim($str);
    }

}
