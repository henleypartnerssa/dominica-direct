<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Profile;
use App\FamilyMember;
use App\DependantType;
use App\Application;
use App\Step;
use AppHelpers;
use App\Util\AppFileSystem;
use Auth;
use Carbon\Carbon;
use hpsadev\ProgramsFileManager\DirectoryManager;

/**
 * Controller linked to Profile Model
 * as well as Profile specific Views.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.0
 */

class ProfilesController extends Controller
{

    /**
     * Manages AJAX POST request when attempting to save Users
     * Key Information to database.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return string   '2' = [created new Profile with Key Data]
     *                  '0' = [Failed creating Profile]
     */
    public function newKeyInfo(Request $request) {

        if (Auth::user()->profiles()->count() > 0) {
            return '2';
        }

        $user       = Auth::user();
        $profile    = new Profile();

        $profile->user_id       = $user->user_id;
        $profile->group_id      = $user->group_id;
        $profile->application_id = $this->createApp()->application_id;
        $profile->first_name    = $request['name'];
        $profile->last_name     = $request['surname'];
        $profile->date_of_birth = $request['dob'];
        $profile->gender        = $request['gender'];
        $profile->belongs_to    = "Principal Applicant";

        $success = $user->profiles()->create($profile->toArray());
        $fs = new DirectoryManager();
        $fs->createProfileDir($profile->profile_id);

        if ($success) {
            return '2';
        } else {
            return '0';
        }
    }

    /**
     * Manages AJAX POST request when attempting to save Users
     * Key information changes to already existing Profile.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return string   '1' = Successfully saved data.
     *                  '0' = Error saving data.
     */
    public function updateKeyInfo(Request $request) {

        if (Auth::user()->profiles()->count() > 0) {
            return '1';
        }

        $user       = Auth::user();
        $profile    = new Profile();

        $profile->user_id           = $user->user_id;
        $profile->group_id          = $user->group_id;
        $profile->application_id    = $this->createApp()->application_id;
        $profile->first_name        = $request['name'];
        $profile->last_name         = $request['surname'];
        $profile->date_of_birth     = $request['dob'];
        $profile->gender            = $request['gender'];
        $profile->belongs_to        = "Principal Applicant";

        $success    = $user->profiles()->create($profile->toArray());
        $fs = new DirectoryManager();
        $fs->createProfileDir($success->profile_id);

        if ($success) {
            return '1';
        } else {
            return '0';
        }
    }

    /**
     * POST method for adding dependant that is
     * not Main Applicant nor Spouse.
     *
     * @since 1.0 Function Init
     * @param Request $request [description]
     * @return mixed array if created and '0' if not.
     */
    public function addDependant(Request $request) {
        $profile = $this->saveToProfile($request);
        if ($profile) {
            $reponse = [
                'id'        => $profile->profile_id,
                'fullname'  => $profile->first_name . " " . $profile->last_name,
                'type'      => $profile->belongs_to
            ];
            return json_encode($reponse);
        } else {
            return "0";
        }

    }

    /**
     * POST Method used delete dependant by casdade
     * deleting Profile record.
     *
     * @since 1.1 Added new delete functionality where depandants are
     *        deleted from Fam/Fam_Mem/Fam_Pro tables.
     * @since 1.0 Function Init
     * @param  Request $request POST Request Data
     * @return string           action indicator
     */
    public function deleteDependant(Request $request) {
        $pa         = Profile::where('user_id', Auth::id())->first();
        $member     = Profile::find((int)$request['id']);
        $memDel     = $member->delete();
        // HACK: remove file using depreciated method.
        // TODO: this remove functionality should be execute from File Manager Package
        AppFileSystem::delProfileDir($request['id']);
        if ($memDel) {
            return '1';
        } else {
            return '0';
        }
    }

    /**
     * Internal helper function that create
     * new Application which links to Profile.
     *
     * @since 1.0 Function Init
     * @return [Application] [created application]
     */
    private function createApp() {
        $app            = new Application();
        $app->case_no   = AppHelpers::genCaseNo();
        $app->step      = Step::where('step_no', 1)->first()->step_id;
        $app->save();
        return $app;
    }

    /**
     * Internal helper method that Create Profiles
     * other than that of the Main Applicant or Spouse.
     *
     * @since 1.0 Function Init
     * @param  [Request] $request [POST Data]
     * @return [Profile]          [Created Profile.]
     */
    private function saveToProfile($request) {
        $user       = Auth::user();
        $profile    = new Profile();
        $depType    = DependantType::find((int)$request['type']);
        $dupRec     = $user->profiles()->where('belongs_to', $depType->type)->first();

        if (!empty($dupRec) && $dupRec->belongs_to === 'Spouse') {
            return false;
        }

        $profile->user_id           = $user->user_id;
        $profile->group_id          = $user->group_id;
        $profile->application_id    = $user->profiles()->first()->application_id;
        $profile->first_name        = $request['name'];
        $profile->last_name         = $request['surname'];
        $profile->date_of_birth     = $request['dob'];
        $profile->gender            = $request['gender'];
        $profile->belongs_to        = $depType->type;
        $profile->save();
        $fs = new DirectoryManager();
        $fs->createProfileDir($profile->profile_id);
        return $profile;
    }

}
