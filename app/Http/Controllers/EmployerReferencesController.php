<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EmploymentReference;
use Auth;
use App\Profile;

class EmployerReferencesController extends Controller
{
    public function store(Request $request) {

        $profile = Profile::findOrFail((int)$request['profile_id']);
        $user   = $profile->user;
        $exists = false;
        $ref    = null;
        $request['user_id']     = $user->user_id;
        $request['group_id']    = $user->group_id;

        if ($request['ref_id'] !== '') {
            $ref    = EmploymentReference::findOrFail((int)$request['ref_id']);
            $exists = true;
        } else {
            $ref = new EmploymentReference();
        }

        $ref->profile_id            = $request['profile_id'];
        $ref->application_id        = $profile->application_id;
        $ref->user_id               = $request['user_id'];
        $ref->group_id              = $request['group_id'];
        $ref->occupation            = $request['occupation'];
        $ref->employer              = $request['employer_name'];
        $ref->type_of_business      = $request['type_of_business'];
        $ref->from_date             = $request['from_month'].'/'.$request['from_year'];
        $ref->to_date               = $request['to_month'].'/'.$request['to_year'];
        $ref->location              = $request['employer_location'];
        $ref->reason_for_leaving    = $request['reason_for_leaving'];
        $ref->save();

        $ticket = [
            'state' => ($exists) ? 'update' : 'new',
            'id'    => $ref->employment_reference_id,
            'ref'   => $ref->employer.' (From: '.$ref->from_date.' To:'.$ref->to_date.')'
        ];

        return json_encode($ticket);
    }

    public function destroy(Request $request) {
        $ref = EmploymentReference::findOrFail($request['id']);
        if ($ref->delete()) {
            return '1';
        } else {
            return '0';
        }
    }

    public function find(Request $request) {
        return json_encode(EmploymentReference::findOrFail($request['id']));
    }

}
