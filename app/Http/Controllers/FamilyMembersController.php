<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FamilyMember;
use Auth;
use App\Application;
use App\Profile;
use App\DependantType;
use Carbon\Carbon;
use AppHelpers;
use App\Address;

class FamilyMembersController extends Controller
{

    public function saveFamilyMember(Request $request) {
        $profile    = Profile::findOrFail($request['profile_id']);
        $user       = $profile->user;
        $member     = null;
        $depType    = ($request['gender'] === 'male') ? 'Brother' : 'Sister';
        $is_new     = false;

        if (!empty($request['family_member_id'])) {
            $member = FamilyMember::findOrFail($request['family_member_id']);
        } else {
            $member = new FamilyMember();
            $is_new = true;
        }
        if ($request['is_inlaw']) {
            $depType    = ($request['gender'] === 'male') ? 'Father in-law' : 'Mother in-law';
        } else if ($request['child']) {
            $depType    = 'Child';
        }

        $member->dependant_type_id          = DependantType::where('type', $depType)->first()->dependant_type_id;
        $member->first_name                 = $request['first_name'];
        $member->last_name                  = $request['last_name'];
        $member->gender                     = $request['gender'];
        $member->occupation                 = $request['occupation'];
        $member->date_of_birth              = (!empty($request['date_of_birth_year'])) ? Carbon::parse(AppHelpers::normalizeDate($request['date_of_birth_year'],$request['date_of_birth_month'],$request['date_of_birth_day'])) : null;
        $member->place_of_birth             = $request['place_of_birth'];
        $member->citizenship                = $request['citizenship'];
        $member->relationship               = DependantType::where('type', $depType)->first()->type;
        $member->is_included                = false;
        $member->save();

        if ($is_new) {
            $profile->family_members()->attach($member, [
                'user_id'           => $profile->user_id,
                'group_id'          => $profile->group_id,
                'application_id'    => $profile->application_id,
                'dependant_type_id' => $member->dependant_type_id
            ]);
        }

        $this->saveFamilyMemberAddress($member, $request);

        $data = [
            'id'    => $member['family_member_id'],
            'name'  => $member['first_name'].' '.$member['surname'].' ('.$member['relationship'].' '.$request['member_type'].')'
        ];
        return json_encode($data);

    }

    private function saveFamilyMemberAddress($member, $request) {
        $address    = null;
        $is_new     = false;
        if ($member->addresses()->count() > 0) {
            $address    = $member->addresses()->first();
        } else {
            $address    = new Address();
            $is_new     = true;
        }
        $address->country       = $request['country'];
        $address->address_type  = 'cur';
        $address->street1       = $request['street1'];
        $address->street2       = $request['street2'];
        $address->town          = $request['town'];
        $address->postal_code   = $request['post_code'];
        $saved                  = $address->save();
        if ($saved) {
            if ($is_new) {
                $address->family_members()->attach($address->address_id, [
                    'family_member_id'  => $member->family_member_id,
                    'dependant_type_id' => $member->dependant_type_id
                ]);
            }
        }
        return $address;
    }

    public function find(Request $request) {
        $member     = FamilyMember::find((int)$request['id']);
        $address    = $member->addresses()->first();
        $array      = array_merge($member->toArray(), $address->toArray());
        return json_encode($array);
    }

    public function destroy(Request $request) {
        $member     = FamilyMember::find((int)$request['id']);
        $profile    = Profile::find($request['profile_id']);
        $addr       = $member->addresses()->first();
        if ($addr) {
            $member->addresses()->detach([
                'address_id'        => $addr->address_id,
                'family_member_id'  => $member->family_member_id,
                'dependant_type_id' => $member->dependant_type_id
            ]);
        }
        $profile->family_members()->detach($member, [
            'user_id'           => $profile->user_id,
            'group_id'          => $profile->group_id,
            'application_id'    => $profile->application_id,
            'dependant_type_id' => $member->dependant_type_id
        ]);
        $addr->delete();
        $didDelete = $member->delete();

        if ($didDelete) {
            return '1';
        } else {
            return '0';
        }
    }

}
