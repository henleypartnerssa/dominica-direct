<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use App\IdentityDocument;
use App\Profile;
use AppHelpers;

class IdentityDocumentsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $flag;

        if (!empty($request['pass_id'])) {
            $passport                       = IdentityDocument::findOrFail((int)$request['pass_id']);
            $passport->document_type        = 'passport';
            $passport->issuing_country      = $request['issueing_country'];
            $passport->document_number      = $request['document_number'];
            $passport->place_of_issue       = $request['place_of_issue'];
            $passport->date_of_issue        = Carbon::parse($request['doi']);
            $passport->date_of_expiration   = Carbon::parse($request['dox']);
            $passport->save();
            $flag                           = "exist";
        } else {
            $request['group_id']            = $user->group_id;
            $request['user_id']             = $user->user_id;
            $request['application_id']      = Profile::find($request['profile_id'])->application->application_id;
            $request['document_type']       = 'passport';
            $request['issuing_country']     = $request['issueing_country'];
            $request['date_of_issue']       = Carbon::parse($request['doi']);
            $request['date_of_expiration']  = Carbon::parse($request['dox']);
            $passport                       = IdentityDocument::create($request->all());
            $flag                           = "new";
        }

        return json_encode([
            'flag'      => $flag,
            'id'        => $passport->identity_document_id,
            'country'   => $passport->issuing_country
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $passport = IdentityDocument::findOrFail($request['id']);
        $deleted = $passport->delete();

        if ($deleted) {
            return '1';
        } else {
            return '0';
        }
    }

    public function find(Request $request) {
        $passport = IdentityDocument::findOrFail((int)$request['id']);
        return json_encode($passport);
    }

}
