<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PreviousSpouse;
use App\Profile;
use Auth;
use AppHelpers;
use Carbon\Carbon;

class PreviousSpousesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * POST method for adding/Updating Previous Spouse.
     *
     * @param  Request $request Form POST data
     * @return JSON             JSON object
     */
    public function postAddPrevSpouse(Request $request) {
        $count      = ($request['previous_spouse_id']) ? PreviousSpouse::find($request['previous_spouse_id'])->count() : null;
        $profile    = Profile::find($request['profile_id']);
        $prevSpouse = null;
        if ($count) {
            $prevSpouse = PreviousSpouse::find($request['previous_spouse_id']);
        } else {
            $prevSpouse = new PreviousSpouse();
        }
        $prevSpouse->profile_id         = $profile->profile_id;
        $prevSpouse->user_id            = $profile->user_id;
        $prevSpouse->group_id           = $profile->group_id;
        $prevSpouse->application_id     = $profile->application_id;
        $prevSpouse->full_name          = $request['full_name'];
        $prevSpouse->place_of_birth     = $request['place_of_birth'];
        $prevSpouse->date_of_birth      = (!empty($request['date_of_birth_year'])) ? Carbon::parse(AppHelpers::normalizeDate($request['date_of_birth_year'],$request['date_of_birth_month'],$request['date_of_birth_day'])) : null;
        $prevSpouse->nationality        = $request['nationality'];
        $prevSpouse->divorse_dt         = (!empty($request['divorse_dt_year'])) ? Carbon::parse(AppHelpers::normalizeDate($request['divorse_dt_year'],$request['divorse_dt_month'],$request['divorse_dt_day'])) : null;
        $prevSpouse->marriage_from_dt   = (!empty($request['marriage_from_dt_year'])) ? Carbon::parse(AppHelpers::normalizeDate($request['marriage_from_dt_year'],$request['marriage_from_dt_month'],$request['marriage_from_dt_day'])) : null;
        $prevSpouse->marriage_to_dt     = (!empty($request['marriage_to_dt_year'])) ? Carbon::parse(AppHelpers::normalizeDate($request['marriage_to_dt_year'],$request['marriage_to_dt_month'],$request['marriage_to_dt_day'])) : null;
        $saved                          = $prevSpouse->save();
        return json_encode([
            'id'    => $prevSpouse->previous_spouse_id,
            'name'  => $prevSpouse->full_name
        ]);
    }

    /**
     * POST method of fiding Previous Spouse.
     *
     * @param  integer $id Previous Spouse ID
     * @return JSON     JSON encoded Prev Spouse Object
     */
    public function postFindPrevSpouse(Request $request) {
        $prevSpouse = PreviousSpouse::findOrFail($request['previous_spouse_id']);
        return json_encode($prevSpouse);
    }

    /**
     * POST method for deleting Previous Spouse.
     *
     * @param  integer $id Prev Spouse ID
     * @return string     indicator 1/0
     */
    public function postDeletePrevSpouse(Request $request) {
        $delete = PreviousSpouse::findOrFail($request['previous_spouse_id'])->delete();
        return ($delete) ? '1' : '0';
    }

}
