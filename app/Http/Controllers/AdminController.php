<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Group;
use App\User;
use App\Util\SMS;
use App\Application;
use App\Profile;
use App\Programs\Antigua\Antigua;
use App\Util\SendEmail;
use Hash;

class AdminController extends Controller
{

    public function personalDetails($profileId, $appId) {
        $p = new Antigua($profileId, $appId, true);
        return $p->adminPersonalDetails();
    }

    public function personalDetailsPost($profileId, $appId, Request $request) {
        $p = new Antigua($profileId, $appId, true);
        return $p->adminPersonalDetailsPost($request);
    }

    public function medicalDetails($profileId, $appId) {
        $p = new Antigua($profileId, $appId, true);
        return $p->adminMedicalDetails();
    }

    public function medicalDetailsPost($profileId, $appId, Request $request) {
        $p = new Antigua($profileId, $appId, true);
        return $p->medicalDetailsPost($request);
    }

    public function familyDetails($profileId, $appId) {
        $p = new Antigua($profileId, $appId, true);
        return $p->adminFamilyDetails();
    }

    public function familyDetailsPost($profileId, $appId, Request $request) {
        $p = new Antigua($profileId, $appId, true);
        return $p->familyDetailsPost($request);
    }

    public function businessDetails($profileId, $appId) {
        $p = new Antigua($profileId, $appId, true);
        return $p->adminBusinessDetails();
    }

    public function businessDetailsPost($profileId, $appId,Request $request) {
        $p = new Antigua($profileId, $appId, true);
        return $p->adminBusinessDetailsPost($request);
    }

    public function declarations($profileId, $appId) {
            $p = new Antigua($profileId, $appId, true);
            return $p->adminDeclarations();
    }

    public function declarationsPost($profileId, $appId,Request $request) {
            $p = new Antigua($profileId, $appId, true);
            return $p->adminDeclarationsPost($request);
    }

    public function dependants($profileId, $appId) {
            $p = new Antigua($profileId, $appId, true);
            return $p->adminDependants();
    }

    public function documents($profileId, $appId) {
            $p = new Antigua($profileId, $appId, true);
            return $p->adminDocuments();
    }

    public function approvals($profileId, $appId) {
        $p = new Antigua($profileId, $appId, true);
        return $p->adminApprovals();
    }

    public function updateProfileState(Request $request) {
        $apps = Application::findOrFail((int)$request['id']);
        $user = User::where('user_id', $apps->user_id)->first();
        $apps->$request['type'] = $request['state'];
        $saved = $apps->save();
        // Send Mail
        $recipient  = ['name' => $user->name, 'lastname' => $user->surname, 'email' => $user->username];
        $data       = ['name' => $user->name, 'surname' => $user->surname, 'app_no' => $apps->case_no, 'checkName' => $this->lkpCheck($request['type'])];
        //$mail = new SendEmail('ddCheck',$recipient,$data);
        if ($saved) {
            return '1';
        } else {
            return '0';
        }
    }

    private function lkpCheck($chek) {
        $list['dd_check']                   = 'Due Diligance Check';
        $list['payment_for_submission']     = 'Payment For Submission Verification Check';
        $list['received_package']           = 'Received Courier Package Check';
        $list['package_dd_check']           = 'Package Due Diligance Check';
        $list['ciu_approval']               = 'CIU Approval Check';
        $list['payment_for_investment']     = 'Payment For Investment Check';
        $list['citizenship_docs']           = 'Recieved Governemnt Documentation Check';
        $list['visa']                       = 'Recieved New Passports From Government Check';
        return $list[$chek];
    }

}
