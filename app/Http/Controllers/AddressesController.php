<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Address;
use App\Country;
use App\Profile;
use DB;

class AddressesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $addr       = null;
        $exists     = false;

        if ($request['addr_id'] !== '') {
            $addr   = Address::FindOrFail((int)$request['addr_id']);
            $exists = true;
        } else {
            $addr = new Address();
        }

        $addr->country          = $request['country'];
        $addr->address_type     = $request['type'];
        $addr->street1          = $request['line1'];
        $addr->street2          = $request['line2'];
        $addr->town             = $request['city'];
        $addr->postal_code      = $request['code'];

        if (!$exists) {

            if ($addr->save()) {

                $profile = Profile::findOrFail($request['profile_id']);

                $data = [
                    'profile_id'    => $profile->profile_id,
                    'application_id' => $profile->application_id,
                    'user_id'       => $profile->user_id,
                    'group_id'      => $profile->group_id,
                    'from_date'     => $request['from_month'].'/'.$request['from_year'],
                    'to_date'       => $request['to_month'].'/'.$request['to_year']
                ];

                $addr->profiles()->attach($addr->address_id, $data);

                $ticket = [
                    'saved' => '1',
                    'id' => $addr->address_id,
                    'addr' => $addr->street1.' - '.$addr->town.' - '.$addr->country
                ];

                return json_encode($ticket);

            } else {
                $ticket = [
                    'saved' => '0'
                ];
                return json_encode($ticket);
            }
        } else if ($exists) {
            $pivot = Profile::findOrFail($request['profile_id'])->addresses()->get()->where('address_id', (int)$request['addr_id'])->first()->pivot;
            $pivot->from_date   = $request['from_month'].'/'.$request['from_year'];
            $pivot->to_date     = $request['to_month'].'/'.$request['to_year'];
            $saved              = $addr->save();
            $pivSave            = $pivot->save();

            if ($saved || $pivSave) {
                
                $ticket = [
                    'saved' => '2',
                    'id'    => $addr->address_id,
                    'addr'  => $addr->street1.' - '.$addr->town.' - '.$addr->country
                ];
                return json_encode($ticket);
            } else {
                $ticket = [
                    'saved' => '0'
                ];
                return json_encode($ticket);
            }

        }

    }

    public function find(Request $request) {

        $address = Address::FindOrFail($request['id']);
        $profileAddr = Profile::findOrFail($request['profile_id'])->addresses()->get()->where('address_id', (int)$request['id'])->first()->toArray();
        return json_encode($profileAddr);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $addr = Address::findOrFail($request['id']);

        if ($addr->profiles()->detach()) {
            if ($addr->delete()) {
                return '1';
            } else {
                return '0';
            }
        }
    }

}
