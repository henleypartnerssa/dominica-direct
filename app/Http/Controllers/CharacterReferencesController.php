<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use App\CharacterReference;
use App\Profile;
use App\Address;
use AppHelpers;

class CharacterReferencesController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $charID     = $request['char_id'];
        $char       = new CharacterReference();
        $profile    = Profile::find($request['profile_id']);
        $address    = NULL;

        if ($charID) {
            $char       = CharacterReference::find($charID);
            $address    = $char->address;
        }

        $address = $this->saveCharAddress($request, $char, 'char_ref', 'cur');

        $char->profile_id       = $profile->profile_id;
        $char->user_id          = $profile->user_id;
        $char->group_id         = $profile->group_id;
        $char->application_id   = $profile->application_id;
        $char->address_id       = $address->address_id;
        $char->full_name        = $request['full_name'];
        $char->years_known      = $request['years_known'];
        $char->occupation       = $request['occupation'];
        $char->employer         = $request['employer'];
        $char->email            = $request['email'];
        $char->landline_no      = $request['landline_no'];
        $char->mobile_no        = $request['mobile_no'];
        $char->work_no          = $request['work_no'];
        $char->save();

        return json_encode([
            'id'    => $char->character_ref_id,
            'name'  => $char->full_name
        ]);
    }

    private function saveCharAddress($post, $charRef, $type, $ext = null) {
        $address    = $charRef->address;
        $is_new     = false;
        $imuteType  = $type;
        if(!$address) {
            $address    = new Address();
            $is_new     = true;
        }
        $type = ($ext) ? $ext : $type;
        // Update Address with POST data.
        $address->country       = $post[$type.'_country'];
        $address->address_type  = $imuteType;
        $address->street1       = $post[$type.'_street1'];
        $address->street2       = $post[$type.'_street2'];
        $address->town          = $post[$type.'_town'];
        $address->postal_code   = $post[$type.'_post_code'];
        $address->date_since    = (!empty($post[$type.'_since_month'])) ? $post[$type.'_since_month'].'/'.$post[$type.'_since_year'] : NULL;
        $saved                  = $address->save();
        return $address;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $charRef        = CharacterReference::findOrFail($request['id']);
        $charRefAddr    = $charRef->address;
        $delChar        = $charRef->delete();
        $delCharAddr    = $charRefAddr->delete();
        return ($delChar && $delCharAddr) ? '1' : '0';
    }

    public function find(Request $request) {
        $charRef        = CharacterReference::findOrFail((int)$request['id']);
        $charRefAddr    = $charRef->address;
        $results        = [
            'char'      => $charRef->toArray(),
            'charAddr'  => $charRefAddr
        ];
        return json_encode($results);
    }

}
