<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Message;
use App\Application;
use App\Group;
use App\Profile;
use App\Util\AppHelpers;

class MessagesController extends Controller
{

    public function __construct() {
        $this->middleware('nopin');
    }

    /**
     * Reutns View wich Lists all users Messages.
     *
     * @return [View] [User Messages Page]
     */
    public function userMessages() {
        $user           = Auth::user();
        $listMessages   = $this->getAppMessages($user);
        $data           = AppHelpers::appProcessUrl();
        return view('messages', compact('listMessages','data'));
    }

    public function  userMessagesSent() {

    }

    /**
     * Displayes Creante New Message Page Form,
     *
     * @return [View] [Create Message Page]
     */
    public function userCreateMessage() {
        $app    = Auth::user()->profiles()->first()->application_id;
        $data   = AppHelpers::appProcessUrl();
        return view('message_create', compact('app','data'));
    }

    /**
     * This function Saves a newly created message to DB.
     *
     * @param  Request $request [Form POST data]
     * @return [Redirect]       [description]
     */
    public function saveNewMessage(Request $request) {
        $user       = Auth::user()->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $admin      = Group::where('roles', 'admin')->get()->first();
        $newMessage = new Message();
        $newMessage->application_id = $request['app'];
        $newMessage->user_id        = $user->user_id;
        $newMessage->group_id       = $user->group_id;
        $newMessage->subject        = $request['subject'];
        $newMessage->sent_to        = User::where('group_id', $admin->group_id)->get()->first()->user_id;
        $newMessage->message        = $request['message'];
        $newMessage->save();
        return redirect('/users/messages');
    }

    public function openMessage($id) {
        $user_id        = Auth::user()->user_id;
        $message        = Message::findOrFail($id);
        if ($message->sent_to === $user_id) {
            $message->read  = true;
            $message->save();
        }
        $prevMessages   = Message::where('subject', $message->subject)->orderBy('created_at', 'asc')->get();
        $data           = AppHelpers::appProcessUrl();
        return view('message_open', compact('message', 'prevMessages','data'));
    }

    public function adminOpenMessage($id) {
        $user_id        = Auth::user()->user_id;
        $message        = Message::findOrFail($id);
        if ($message->sent_to === $user_id) {
            $message->read  = true;
            $message->save();
        }
        $prevMessages = Message::where('subject', $message->subject)->orderBy('created_at', 'asc')->get();
        return view('admin.message_open', compact('message', 'prevMessages'));
    }

    public function replyMessage($id, Request $request) {
        $message    = Message::findOrFail($id);
        $newMessage = new Message();
        $newMessage->application_id = $message->application_id;
        $newMessage->user_id        = $message->sent_to;
        $newMessage->group_id       = Group::where('roles', 'user')->first()->group_id;
        $newMessage->subject        = $message->subject;
        $newMessage->sent_to        = $message->user_id;
        $newMessage->message        = $request['message'];
        $newMessage->save();
        return redirect('/users/messages');
    }

    public function adminReplyMessage($id, Request $request) {
        $message    = Message::findOrFail($id);
        $newMessage = new Message();
        $newMessage->application_id = $message->application_id;
        $newMessage->user_id        = $message->sent_to;
        $newMessage->group_id       = Group::where('roles', 'admin')->first()->group_id;
        $newMessage->subject        = $message->subject;
        $newMessage->sent_to        = $message->user_id;
        $newMessage->message        = $request['message'];
        $newMessage->save();
        return redirect('/admin/messages');
    }

    public function caseMessages($id) {
        $app = Application::find((int)$id);
        $listMessages = $this->getallAppMessages($app);
        return view('messages', compact('listMessages'));
    }

    public function adminMessages() {
        $listMessages   = $this->getAllAdminMessages();
        return view('admin.all_messages', compact('listMessages'));
    }

    public function adminCaseMessages($profileId, $id) {
        $app            = Application::find((int)$id);
        $appId          = $app->application_id;
        $listMessages   = $this->getUserMsgsForAdmin($id);
        $profile        = $app->profiles()->where('belongs_to', 'Principal Applicant')->first();
        return view('admin.messages', compact('listMessages','appId','profile'));
    }

    private function getallAppMessages($app) {
        $messages   = $app->messages()->where('application_id', $app->application_id)->orderBy('created_at', 'desc')->get();
        $array      = [];
        $subjects   = [];
        foreach ($messages as $i => $msg) {

            if ($i !== 0) {
                if (array_search($msg->subject, $subjects) !== false) {
                    continue;
                }
            }
            array_push($subjects, $msg->subject);

            $from   = User::find($msg->sent_from);
            $to     = User::find($msg->sent_to);

            array_push($array, [
                'id'        => $msg->message_id,
                'case'      => $app->case_no,
                'subject'   => $msg->subject,
                'from'      => $from->name.' '.$from->surname,
                'to'        => $to->name.' '.$to->surname,
                'read'      => $msg->read,
                'timestamp' => $msg->created_at->toDateTimeString()
            ]);
        }

        return $array;
    }

    private function getAppMessages($user) {
        $array          = [];
        $subjects       = [];
        $messages       = Message::where('sent_to', Auth::id())->orderBy('created_at', 'desc')->get();
        foreach ($messages as $i => $msg) {
            if ($i !== 0) {
                if (array_search($msg->subject, $subjects) !== false) {
                    continue;
                }
            }
            array_push($subjects, $msg->subject);
            $from   = User::find($msg->user_id);
            $to     = User::find($msg->sent_to);
            array_push($array,[
                'id'        => $msg->message_id,
                'subject'   => $msg->subject,
                'from'      => $from->name.' '.$from->surname,
                'to'        => $to->name.' '.$to->surname,
                'read'      => $msg->read,
                'timestamp' => $msg->created_at->toDateTimeString()
            ]);
        }
        return $array;
    }

    private function getAllAdminMessages() {
        $messages = Message::where('sent_to', Auth::id())->orderBy('created_at', 'desc')->get();
        $array      = [];
        $subjects   = [];
        foreach ($messages as $i => $msg) {
            if ($i !== 0) {
                if (array_search($msg->subject, $subjects) !== false) {
                    continue;
                }
            }
            array_push($subjects, $msg->subject);
            $from   = User::find($msg->user_id);
            $to     = User::find($msg->sent_to);
            array_push($array, [
                'id'        => $msg->message_id,
                'case'      => $msg->application->case_no,
                'subject'   => $msg->subject,
                'from'      => $from->name.' '.$from->surname,
                'to'        => $to->name.' '.$to->surname,
                'read'      => $msg->read,
                'timestamp' => $msg->created_at->toDateTimeString()
            ]);
        }
        return $array;
    }

    private function getUserMsgsForAdmin($appId) {
        $profile    = Application::find($appId)->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $user       = $profile->user;
        $messages   = $user->messages;
        $array      = [];
        $subjects   = [];
        foreach ($messages as $i => $msg) {
            if ($i !== 0) {
                if (array_search($msg->subject, $subjects) !== false) {
                    continue;
                }
            }
            array_push($subjects, $msg->subject);
            $from   = User::find($msg->user_id);
            $to     = User::find($msg->sent_to);
            array_push($array, [
                'id'        => $msg->message_id,
                'case'      => $msg->application->case_no,
                'subject'   => $msg->subject,
                'from'      => $from->name.' '.$from->surname,
                'to'        => $to->name.' '.$to->surname,
                'read'      => $msg->read,
                'timestamp' => $msg->created_at->toDateTimeString()
            ]);
        }
        return $array;
    }
}
