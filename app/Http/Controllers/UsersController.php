<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Route as Route;
use Illuminate\Http\Request as Request;
use Illuminate\Http\Response as Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Util\SMS as Sms;
use App\Traits\UsersControllerHelper;
use Session;
use Auth;
use Hash;
use Crypt;
use Cookie;
use App\User;
use App\Group;
use App\Country;
use App\DependantType;
use App\LanguageLevel;
use AppHelpers;
use DB;
use hpsadev\Mailer\Mailer;

/**
 * Controller linked to User Model
 * as well as User specific Views and functionalities such as
 * quthentication, locking accounts etc.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     1.0.1
 */
class UsersController extends Controller
{

    use UsersControllerHelper;

    /**
     * Execute array or middleware
     * @since 1.0 Function Init
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login', 'register', 'verify', 'verifyMobile', 'adminLogin', 'adminUser', 'checkEmail', 'changeMobile']]);
        $this->middleware('nopin', ['except' => ['logout', 'pin', 'checkPin', 'resendPin', 'changeMobile', 'mobile', 'setMobile']]);
    }

    /**
     * Users dashboard Page.
     *
     * @since 1.0 Function Init
     * @return View
     */
    public function dashboard() {

        $user           = Auth::user();
        $profiles       = $user->profiles;
        $pinFlg         = $user->valid_pin;
        $statusFlg      = $user->is_active;

        // if user has no document password
        if (empty($user->doc_pass)) {
            return redirect('/users/doc-password');
        }

        if ($pinFlg === false) {
            return redirect('users/pin');
        } else if ($pinFlg === true && $statusFlg === true && $profiles->count() === 0) {
            return redirect('/users/key-info');
        }

        $data       = $this->buildDashData($profiles);
        $counter    = $user->profiles()->count() + 1;
        return view('users.dashboard', compact('data', 'counter'));
    }

    /**
     * Users Account Page.
     *
     * @since 1.0 Function Init
     * @return [type] [description]
     */
    public function account() {
        $authUser   = Auth::user();
        $depTypes   = DependantType::all()->toArray();
        $data       = AppHelpers::appProcessUrl();
        return view('users.account', compact('authUser', 'depTypes', 'data'));
    }

    /**
     * Updates Users data.
     *
     * @since 1.0 Function Init
     * @param  Request $request [POST data]
     * @return [mixed]           [Redirect or abort]
     */
    public function updateAccount(Request $request) {
        $authUser = Auth::user();
        $authUser->name     = $request['name'];
        $authUser->surname  = $request['surname'];
        $authUser->username = $request['username'];
        $authUser->cell_no  = $request['cell_no'];

        if (!empty($request['password'])) {
            $authUser->password = $request['password'];
        }

        if ($request['doc_password'] !== '123456789') {
            $authUser->doc_pass = Crypt::encrypt($request['doc_password']);
        }

        if ($authUser->save()) {
            return redirect('/users/dashboard');
        } else {
            return abort(403, 'Failed Updating account Details.');
        }
    }

    /**
     * Users Key Information page
     *
     * @since 1.0 Function Init
     * @return [View] [users.key_info view]
     */
    public function keyInfo() {
        $userInfo   = $this->getUserInfo();
        $depTypes   = DependantType::where('type', 'Spouse')->orWhere('type', 'Child')->orWhere('type', 'Father')->orWhere('type', 'Mother')->get()->toArray();
        $pa         = AUTH::user()->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $dependants = (!empty($pa)) ? $pa->family_members()->get() : [];
        return view('users.key_info', compact('userInfo', 'depTypes', 'dependants'));
    }

    /**
     * This method attempts to register a User by saving the
     * POST data to the User Model. If email provided already exists
     * d not save details else save new user details to DB.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request) {

        if ($request->ajax()) {
            // Call to Internal Helper Method to create User.
            $user = $this->addUser($request);
            // If user is saved then send verification email
            if ($user) {
                return Mailer::verificationMail([
                    'user'  => $user,
                    'data'  => [
                        'name'      => $user->name,
                        'lastname'  => $user->surname
                    ]
                ]);
            } else {
                return 0;
            }
        } else {
            // redirect User back to Home if no Ajax
            return redirect('/');
        }

    }

    /**
     * Ajax POST Request to Login User.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return string
     */
    public function login(Request $request) {
        $auth = Auth::attempt(['username' => $request['username'], 'password' => $request['login_password']]);

        if ($auth) {
            $user = Auth::user();
            if ($user->is_active === false) {
                Auth::logout();
                return redirect('/')->withErrors("Not Registered");
            }
            if ($user->is_active === true && $user->valid_pin === true) {
                $user->valid_pin = false;
                $user->pin = null;
                $user->save();
            }
            return redirect('/users/pin');
        } else {
            return redirect('/')->withErrors("Invalid Login Details");
        }

    }

    /**
     * Logout User
     *
     * @since 1.0 Function Init
     * @return [redirect] [redirect user to welcome page]
     */
    public function logout() {
        $user = Auth::user();
        $user->valid_pin = false;
        $user->pin = null;
        $user->save();
        Auth::logout();
        return redirect('/');
    }

    /**
     * Verfiy Route - Determines if User attempting to
     * register have recieved the verification email.
     *
     * @since 1.0 Function Init
     * @param string $secret
     * @return Redirect
     */
    public function verify($secret) {

        $user = User::find(Crypt::decrypt($secret));

        if (isset($user)) {

            Auth::login($user);

            if (Auth::check()) {
                return redirect('/users/mobile');
            } else {
                return redirect('/');
            }

        } else {
            return redirect('/');
        }

    }

    /**
     * Users Mobile number change verification link.
     *
     * @since 1.0 Function Init
     * @param  string $secret users hashed email address
     * @return redirect         redirect user to mobile or home page
     */
    public function verifyMobile($secret) {

        $user = User::where('username', Crypt::decrypt($secret))->first();

        if (isset($user)) {

            Auth::login($user);

            if (Auth::check()) {
                return redirect('/users/mobile');
            } else {
                return redirect('/');
            }

        } else {
            return redirect('/');
        }

    }

    /**
     * Redirectd Dsplay View which request User to insert
     * Mobile number to recieve pin.
     *
     * @since 1.0 Function Init
     * @return mixed
     */
    public function mobile() {

        $authUser = Auth::user();

        if ($authUser->is_active === true && $authUser->valid_pin == true) {
            return redirect('users/dashboard');
        } else if ($authUser->is_active === false && $authUser->valid_pin === false) {
            $countries = Country::all();
            return view('users.mobile', compact('countries'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Update Users Mobile Number.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return int
     */
    public function setMobile(Request $request) {

        if ($request->ajax()) {

            $user            = User::findOrFail(Auth::user()->user_id);
            $user->cell_no   = $request['mobile'];

            $sms        = new Sms($user);
            $smsFlag    = $sms->sendPin();

            if ($smsFlag === "ERR") {
                return '2';
            } else {
                $user->is_active = true;
                $user->save();
                return '1';
            }
        } else {
            return '0';
        }

    }

    /**
     * Returns Change Mobile number page
     *
     * @since 1.0 Function Init
     * @return View
     */
    public function changeMobile() {

        $user = Auth::user();
        $user->is_active    = false;
        $user->valid_pin    = false;
        $user->cell_no      = null;
        $user->save();

        Mailer::mobileEmail([
            'user'  => $user,
            'data'  => [
                'name'      => $user->name,
                'lastname'  => $user->surname
            ]
        ]);
        Auth::logout();

        return view('users.change_mobile');
    }

    public function docPassword() {
        return view('users.doc_password');
    }

    public function saveDocPassword(Request $request) {
        $user = Auth::user();
        $user->doc_pass = trim(Crypt::encrypt($request['password']));
        $user->save();
        return redirect('users/dashboard');
    }

    /**
     * Validates if the User Pin exists and if
     * pin exists then login user.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return string $indicator
     */
    public function checkPin(Request $request) {

        $user = Auth::user();

        if ($request->ajax()) {

            if ($user->pin === (int)$request['pin']) {

                if ($user->is_active === false && $user->attempts === 0) {
                    return 'locked';
                }

                $user->valid_pin = true;
                $user->attempts = 3;
                $user->save();

                return 'valid';

            } else {

                return $this->getLogOnAttempts($user);
            }
        }

    }

    /**
     * Display Pin Page.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return mixed
     */
    public function pin(Request $request) {

        $authUser = Auth::user();

        if ($authUser->is_active === true && $authUser->valid_pin == true) {
            return redirect('users/dashboard');
        }

        if ($authUser->is_active !== false && $authUser->pin === null) {
            if (!env('APP_DEBUG')) {
                $sms = new Sms(Auth::user());
                $sms->sendPin();
            } else {
                $sms = new Sms(Auth::user());
            }
        }

        return view('users.pin');

    }

    /**
     * AJAX POST request handeler the generates
     * and resend users pin number.
     *
     * @since 1.0 Function Init
     * @return string indicator 1/0
     */
    public function resendPin() {
        if (!env('APP_DEBUG')) {
            $sms    = new Sms(Auth::user());
            $sent   = $sms->sendPin();
        } else {
            $sms    = new Sms(Auth::user());
            $sent   = true;
        }
        if ($sent) {
            return '1';
        } else {
            return '0';
        }
    }

    /**
     * AJAX Post method handler that validates if users
     * is attempting to register duplicate email address
     * or not.
     *
     * @since 1.0 Function Init
     * @param  Request $request POST request data
     * @return string           indicator Dup/OK
     */
    public function checkEmail(Request $request) {
        if ($request->ajax()) {
            $exists = User::where('username', $request['email'])->count();
            if ($exists > 0) {
                return 'DUP';
            } else {
                return 'OK';
            }
        } else {
            return abort(403, 'Sorry page cannot be found!!!');
        }
    }

    /**
     * Logout User and Display Locked Account Page.
     *
     * @since 1.0 Function Init
     * @return View
     */
    public function locked() {
        Auth::logout();
        return view('users.locked_account');
    }
}
