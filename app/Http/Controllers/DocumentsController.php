<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Util\AppPdfHandler;
use App\Util\AppFileSystem;
use App\Application;
use App\Profile;
use App\Address;
use App\FamilyMember;
use Auth;
use Input;

class DocumentsController extends Controller
{

    public function __construct() {
        $this->middleware('nopin');
    }

    public function documents() {
        $fileTree['quotaions']  = AppFileSystem::getUpperFilesByDir("quotations");
        $fileTree['payments']   = AppFileSystem::getUpperFilesByDir("payments");
        $fileTree['receipts']   = AppFileSystem::getUpperFilesByDir("receipts");
        return view('documents', compact('fileTree'));
    }

    /**
     * This Ajax Method recieves POST data and Maps
     * Program data to PDF file.
     *
     * @param  Request $request [POST data]
     * @return [string]         [indocator]
     */
    public function generatePdf(Request $request) {
        $case       = Application::findOrfail((int)$request['case']);
        $pdfBuilder = new AppPdfHandler($request['form'], $request['form'], $case->application_id, $request['profile_id']);
        $pdfBuilder->setOutputFile($request['form'].".pdf",$case->case_no);
        $generated  = (string)$pdfBuilder->generatePdf();
        if ($generated == 1) {
            return '1';
        } else {
            return '0';
        }
    }

    /**
     * This function Reosense with Download File.
     *
     * @param  [string] $fileRequest [delimited file download request]
     * @return [Response]           [File to download]
     */
    public function downloadGeneratePdf($fileRequest) {
        $split      = explode('-', $fileRequest);
        $case       = Application::find((int)$split[1]);
        $user       = Auth::user();
        $baseDir    = storage_path('app').'/users/'.$user->user_id.'/profiles/'.$split[2].'/downloads/';
        $file       = $baseDir.strtoupper($split[0]).'.pdf';
        $type       = $this->lkpFileName($split[0],$split[2]); // <-- Internal to this Class
        return response()->download($file, $type, array('Content-Type: application/pdf'));
    }


    /**
     * Class internal Helper function used to lookup download file name.
     *
     * @param  [string] $type       [Download PDF Type]
     * @param  [int]    $profileId  [Profile ID]
     * @return [string]             [File Name]
     */
    private function lkpFileName($type,$profileId) {
        $profile    = Profile::find($profileId);
        $name       = $profile->first_name.' '.$profile->last_name;
        $data = [
            'ab1' => $name." - Citizenship by Investment Application.pdf",
            'ab2' => $name." - Photograph and Signature Certificate.pdf",
            'ab3' => $name." - Medical Certificate.pdf",
            'ab4' => $name." - Investment Confirmation Form.pdf",
            'ab5' => $name." - Agent Form.pdf",
            'abl' => $name." - Form L.pdf",
            'abm' => $name." - Form M.pdf",
            'invoice-2' => 'Governement Application Fees - Invoice 1.pdf',
            'invoice-3' => 'Governement Application Fees - Invoice 2.pdf',
        ];

        return $data[$type];
    }

}
