<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Application;
use App\Traits\ApplicationsControllerHelper;
use App\Util\OnlinePayment;
use Auth;
use App\Step;
use App\Country;
use App\DependantType;
use App\Profile;
use App\AntiguaProgram;
use App\FamilyMember;
use App\Programs\Antigua\Antigua;
use hpsadev\dominicadirect\Dominica as Dominica;
use AppHelpers;
use hpsadev\Mailer\Mailer;

class ApplicationsController extends Controller
{

    use ApplicationsControllerHelper;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Returns Select Program View.
     *
     * @return [View]
     */
    public function selectProgram() {
        $domStep = new Dominica();
        return $domStep->selectProgramView('cases.select_program');
    }

    /**
     * Select Program Form POST Request Method. Makes use of
     * Controller Helper Method.
     *
     * @param redirect  redirect to Program Costs Page
     */
    public function selectProgramPost(Request $request) {
        $domStep = new Dominica();
        return $domStep->selectProgramRedirect($request);
    }

    public function importnantInfoView($profileId) {
        $domStep = new Dominica();
        return $domStep->importnantInfoView($profileId, 'cases.important_info');
    }

    public function importantInfoPost($profileID, Request $request) {
        $domStep = new Dominica($profileID);
        return $domStep->importnantInfoRedirect($request);
    }

    public function ddCheck() {
        $domStep = new Dominica();
        return $domStep->ddCheckView('cases.dd_check');
    }

    public function paymentForServices() {
        $domStep = new Dominica();
        return $domStep->paymentForServicesView('cases.payment_for_services');
    }

    public function personalData($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->personalDataView('cases.personal_data');
    }

    public function personalDataPost($profileId, Request $request) {
        $domStep = new Dominica($profileId);
        return $domStep->personalDataRedirect($request);
    }

    public function medicalDetails($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->medicalDetailsView('cases.medical');
    }

    public function medicalDetailsPost($profileId, Request $request) {
        $domStep = new Dominica($profileId);
        return $domStep->medicalDetailsRedirect($request);
    }

    public function familyMembers($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->familyMembersView('cases.family_members');
    }

    public function familyMembersPost($profileId, Request $request) {
        $domStep = new Dominica($profileId);
        return $domStep->familyMembersRedirect($request);
    }

    public function business($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->businessView('cases.business');
    }

    public function businessPost($profileId, Request $request) {
        $domStep = new Dominica($profileId);
        return $domStep->businessRedirect($request);
    }

    public function declarations($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->declarationsView('cases.declarations');
    }

    public function declarationsPost($profileId, Request $request) {
        $domStep = new Dominica($profileId);
        return $domStep->declarationsRedirect($request);
    }

    public function uploadDocuments($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->uploadDocumentsView('cases.upload_documents');
    }

    public function dataReview() {
        $domStep = new Dominica();
        return $domStep->dataReviewView('cases.data_view');
    }

    public function govDocuments($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->govDocumentsView('cases.download_documents');
    }

    public function uploadNotary($profileId) {
        $domStep = new Dominica($profileId);
        return $domStep->uploadNotaryView('cases.upload_notary');
    }

    public function balanceOfFees() {
        $domStep = new Dominica();
        return $domStep->balanceOfFeesView('cases.balance_of_fees');
    }

    public function courier() {
        $domStep = new Dominica();
        return $domStep->courierView('cases.courier');
    }

    public function postCourier(Request $request) {
        $profile    = Auth::user()->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $app        = $profile->application;
        $app->courier_no = $request['type'].'-'.$request['courier_no'];
        $app->save();
        Mailer::adminCourier([
            'data' => [
                'first_name'    => $profile->first_name,
                'last_name'     => $profile->last_name,
                'courier'       => $request['type'],
                'track_no'      => $request['courier_no'],
                'appName'       => $app->case_no,
            ]
        ]);
        return '1';
    }

    public function  packageCheck() {
        $domStep = new Dominica();
        return $domStep->packageCheckView('cases.package_check');
    }

    public function submission() {
        $domStep = new Dominica();
        return $domStep->submissionView('cases.ciu-submission');
    }

    public function investmentStatus() {
        $domStep = new Dominica();
        return $domStep->investmentStatusView('cases.investment_status');
    }

    public function oath() {
        $domStep = new Dominica();
        return $domStep->oathView('cases.oath');
    }

    public function passportCollection() {
        $domStep = new Dominica();
        return $domStep->passportCollectionView('cases.collect-passport');
    }

}
