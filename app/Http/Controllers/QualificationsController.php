<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Qualification;
use App\Profile;
use App\Address;
use Auth;

class QualificationsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $profile = Profile::findOrFail($request['profile_id']);
        if (!empty($request['id'])) {
            $existing = Qualification::findOrFail((int)$request['id']);
            $qual = $existing;
        } else {
            $qual = new Qualification();
        }

        $qual->profile_id           = $profile->profile_id;
        $qual->application_id       = $profile->application_id;
        $qual->group_id             = $profile->group_id;
        $qual->user_id              = $profile->user_id;
        $qual->qualification_name   = $request['school'];
        $qual->qualification_type   = $request['type'];
        $qual->from_date            = $request['from_month'].'/'.$request['from_year'];
        $qual->to_date              = $request['to_month'].'/'.$request['to_year'];
        $qual->location             = $request['location'];
        $qual->save();

        $data = [
            'id'    => $qual->qualification_id,
            'name'  => $qual->qualification_name,
            'type'  => $qual->qualification_type.' - From: '.$qual->from_date.' To: '.$qual->to_date
        ];

        return json_encode($data);
    }

    /**
     * Find Qualification.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function postFind(Request $request)
    {
        $return = Qualification::findOrFail((int)$request['id']);
        return json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $qual = Qualification::findOrFail((int)$request['id']);
        $addrId = $qual->address_id;

        if ($qual->delete()) {
            return '1';
        } else {
            return '0';
        }

    }
}
