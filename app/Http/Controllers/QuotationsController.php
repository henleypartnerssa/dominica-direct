<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\ApplicationType;
use App\Quotation;
use AppHelpers;
use App\Country;
use App\Util\AppPdfHandler;
use Carbon\Carbon;
use Storage;
use Response;

class QuotationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $authUser   = Auth::user();
        $quotes     = $this->listQuotes($authUser);
        return view("quotations.index", compact('quotes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $authUser       = Auth::user();
        $appType        = ApplicationType::findOrFail($request['programs']);
        $type           = AppHelpers::normalizeProgramName($appType->type);
        $totQuotes      = $authUser->quotations->count();
        $counter        = ($totQuotes === 0) ? 1 : $totQuotes + 1;
        $q_name         = $type.'-'.$counter;

        $request['group_id']            = $authUser->group_id;
        $request['user_id']             = $authUser->user_id;
        $request['name']                = $q_name;

        $quotaion = Quotation::create($request->all());

        if ($quotaion) {
            $pdf_builder = new AppPdfHandler('quotations');
            $pdf_builder->setOutputFile($q_name.".pdf");
            $pdf = $pdf_builder->generatePdf();
        }

        $quotes = $this->listQuotes($authUser);
        return view("quotations.index", compact('quotes'));
    }

    private function listQuotes($user) {

        $quotes = $user->quotations()->orderBy('quotation_id', 'DESC')->get();
        $array  = [];

        foreach($quotes as $quote) {

            $countryCode    = Country::where('name', 'Antigua and Barbuda')->first()->code;

            array_push($array, [
                'country' => 'Antigua and Barbuda',
                'code' => $countryCode,
                'date' => Carbon::parse($quote->created_at),
                'link' => url('/quotations/download/'.$quote->name.'.pdf'),
            ]);

        }

        return $array;
    }

    /**
     * Download File Link
     *
     * @param  [string] $fileName [Filename of the file to be downloaded]
     * @return [string]           [download file]
     */
    public function download($fileName) {
        $user       = Auth::user();
        $baseDir    = storage_path('app').'/users/'.$user->user_id.'/quotations/';
        $file       = $baseDir.$fileName;
        return response()->download($file, $fileName, array('Content-Type: application/pdf'));
    }
}
