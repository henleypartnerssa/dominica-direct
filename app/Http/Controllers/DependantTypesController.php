<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DependantType;

class DependantTypesController extends Controller
{

    public function FindById(Request $request) {
        $type = null;
        if (!empty($id)) {
            $type = DependantType::findOrFail((int)$id);
        } else {
            $type = DependantType::findOrFail((int)$request['id']);
        }
        return $type->type;
    }

}
