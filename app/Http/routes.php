<?php

use App\Util\AppPdfHandler;
/*************************
           AUTH
/*************************/
Route::controllers(['auth' => 'Auth\AuthController']);

/*************************
      PUBLIC PAGES
/*************************/
Route::get('/', ['middleware' => 'publicRedirect', function () { return view('pages.welcome'); }]);             // Home Page

/*************************
      USERS POST
/*************************/
Route::post('/users/register', 'UsersController@register');                     // Recieves data from POST Requests for Registration and Reponds 1/0
Route::post('/users/login', 'UsersController@login');                           // Recieves data from POST Requests for Login and Responds 1/0
Route::post('/users/mobile/number', 'UsersController@setMobile');               // Recieves data from POST Requests to update Users mobile number
Route::post('/users/mobile/pin', 'UsersController@checkPin');                   // Recieves data from POST Requests to valide Pin
Route::post('/users/account', 'UsersController@updateAccount');                 // Recieves data from POST Requests to upadate User account.
Route::post('/users/password', 'UsersController@password');                     // Returns Auth Users Password.
Route::post('/users/mail-check', 'UsersController@checkEmail');                 // Validates if POST email exists or not.
Route::post('/users/key-info/create', 'ProfilesController@newKeyInfo');         // Recieves data from POST Requst to create new Profile record.
Route::post('/users/key-info/update', 'ProfilesController@updateKeyInfo');      // Recieves data from POST Requst to update existing Profile Data.
Route::post('/users/messages/create', 'MessagesController@saveNewMessage');
Route::post('/users/messages/{id}', 'MessagesController@replyMessage');
Route::post('/users/doc-password', 'UsersController@saveDocPassword');

/*************************
      USERS GET
/*************************/
Route::get('/users/dashboard', 'UsersController@dashboard');                    // Displays User Dashboard view
Route::get('/users/logout', 'UsersController@logout');                          // Logout User
Route::get('/users/account', 'UsersController@account');                        // Displays User Account view
Route::get('/users/mobile/change', 'UsersController@changeMobile');             // Display change mobile view
Route::get('/users/verify/{id}', 'UsersController@verify');                     // Mail Authentication verification
Route::get('/users/mobile/verify/{id}', 'UsersController@verifyMobile');        // Mobile Authentication verification
Route::get('/users/mobile', 'UsersController@mobile');                          // Display Moble page view
Route::get('/users/pin', 'UsersController@pin');                                // Display OTP page view
Route::get('/users/resend-pin', 'UsersController@resendPin');                   // Resend OTP
Route::get('/users/locked-account', 'UsersController@locked');                  // Diaplay User account locked view
Route::get('/users/key-info', 'UsersController@keyInfo');                       // Display User Key Information page view
Route::get('/users/doc-password', 'UsersController@docPassword');               // Display User Document Password View

Route::get('/users/messages', 'MessagesController@userMessages');
Route::get('/users/messages/create', 'MessagesController@userCreateMessage');
Route::get('/users/messages/{id}', 'MessagesController@openMessage');
Route::get('/users/messages/case/{id}', 'MessagesController@caseMessages');

/*************************
     Quotaions Route
/*************************/
Route::resource('quotations', 'QuotationsController');
Route::get('/quotations/download/{fileName}', 'QuotationsController@download');

/*************************
     Cases GET
/*************************/
Route::get('/users/application/select-program', 'ApplicationsController@selectProgram');
Route::get('/users/application/dd-check', 'ApplicationsController@ddCheck');
Route::get('/users/application/payment-for-services', 'ApplicationsController@paymentForServices');
Route::get('/users/application/balance-of-fees', 'ApplicationsController@balanceOfFees');
Route::get('/users/application/data-review', 'ApplicationsController@dataReview');
Route::get('/users/application/courier', 'ApplicationsController@courier');
Route::get('/users/application/package-check', 'ApplicationsController@packageCheck');
Route::get('/users/application/submission', 'ApplicationsController@submission');
Route::get('/users/application/investment-status', 'ApplicationsController@investmentStatus');
Route::get('/users/application/oath', 'ApplicationsController@oath');
Route::get('/users/application/collect-passport', 'ApplicationsController@passportCollection');
Route::get('/users/profile/{profileId}/application/important-information', 'ApplicationsController@importnantInfoView');
Route::get('/users/profile/{profileId}/application/personal-data', 'ApplicationsController@personalData');
Route::get('/users/profile/{profileId}/application/medical-details', 'ApplicationsController@medicalDetails');
Route::get('/users/profile/{profileId}/application/family-members', 'ApplicationsController@familyMembers');
Route::get('/users/profile/{profileId}/application/business', 'ApplicationsController@business');
Route::get('/users/profile/{profileId}/application/declarations', 'ApplicationsController@declarations');
Route::get('/users/profile/{profileId}/application/upload-documents', 'ApplicationsController@uploadDocuments');
Route::get('/users/profile/{profileId}/application/gov-documents', 'ApplicationsController@govDocuments');
Route::get('/users/profile/{profileId}/application/upload-notary', 'ApplicationsController@uploadNotary');

/*************************
     Cases POST
/*************************/
Route::post('/users/application/select-program', 'ApplicationsController@selectProgramPost');
Route::post('/users/profile/{profileId}/application/important-information', 'ApplicationsController@importantInfoPost');
Route::post('/users/profile/{profileId}/application/personal-data', 'ApplicationsController@personalDataPost');
Route::post('/users/profile/{profileId}/application/medical-details', 'ApplicationsController@medicalDetailsPost');
Route::post('/users/profile/{profileId}/application/family-members', 'ApplicationsController@familyMembersPost');
Route::post('/users/profile/{profileId}/application/business', 'ApplicationsController@businessPost');
Route::post('/users/profile/{profileId}/application/declarations', 'ApplicationsController@declarationsPost');
Route::post('/users/cases/{program}/{id}/upload-documents', 'ApplicationsController@casePostUploadDocuments');
Route::post('/users/cases/{program}/{id}/delete-documents', 'ApplicationsController@casePostDeleteDocuments');
Route::post('/users/cases/{program}/{id}/upload-notary', 'ApplicationsController@casePostUploadNotary');
Route::post('/users/cases/{program}/{id}/delete-notary', 'ApplicationsController@casePostDeleteNotary');
Route::post('/users/application/courier', 'ApplicationsController@postCourier');

/*************************
    POST Profile Actions
/*************************/
//Profile
Route::post('/users/profile/add-dependant', 'ProfilesController@addDependant');
Route::post('/users/profile/delete-dependant', 'ProfilesController@deleteDependant');
//Address
Route::post('/users/profile/add-address', 'AddressesController@store');
Route::post('/users/profile/delete-address', 'AddressesController@destroy');
Route::post('/users/profile/find-address', 'AddressesController@find');
//Identiy Document
Route::post('/users/profile/add-identity-doc', 'IdentityDocumentsController@store');
Route::post('/users/profile/delete-identity-doc', 'IdentityDocumentsController@destroy');
Route::post('/users/profile/identity-doc', 'IdentityDocumentsController@find');
//FamilyMember
Route::post('/users/profile/save-family-member', 'FamilyMembersController@saveFamilyMember');
Route::post('/users/profile/delete-family-member', 'FamilyMembersController@destroy');
Route::post('/users/profile/find-family-member', 'FamilyMembersController@find');
//Qualifications
Route::post('/users/profile/add-qualification', 'QualificationsController@store');
Route::post('/users/profile/delete-qualification', 'QualificationsController@destroy');
Route::post('/users/profile/post-find-qualification', 'QualificationsController@postFind');
//Employment Reference
Route::post('/users/profile/add-employer-reference', 'EmployerReferencesController@store');
Route::post('/users/profile/delete-employer-reference', 'EmployerReferencesController@destroy');
Route::post('/users/profile/find-employer-reference', 'EmployerReferencesController@find');
//Previous Spouse
Route::post('/users/profile/add-previous-spouse', 'PreviousSpousesController@postAddPrevSpouse');
Route::post('/users/profile/find-previous-spouse', 'PreviousSpousesController@postFindPrevSpouse');
Route::post('/users/profile/delete-previous-spouse', 'PreviousSpousesController@postDeletePrevSpouse');
//Character Reference
Route::post('/users/profile/add-character-reference', 'CharacterReferencesController@store');
Route::post('/users/profile/find-character-reference', 'CharacterReferencesController@find');
Route::post('/users/profile/delete-character-reference', 'CharacterReferencesController@destroy');

Route::post('/users/family/type', 'DependantTypesController@FindById');

/*************************
        Documents
/*************************/
Route::get('/users/documents', 'DocumentsController@documents');                                            // Display All Documents
Route::get('/users/documents/{id}', 'PdfController@documentsByType');
Route::get('/users/documents/download/{fileName}', 'PdfController@downloadDocument');
Route::post('/documents/generate-pdf', 'DocumentsController@generatePdf');                                  // Generate and Download Generated PDF's
Route::post('/cases/pdf/upload', 'PdfController@uploadPdf');
Route::post('/cases/pdf/delete', 'PdfController@deletePdf');
Route::post('/users/documents/delete', 'PdfController@deleteDocument');

/*Route::group(['prefix' => 'api/v1'], function() {
    Route::get('/testing', function() {
        dd("This is the API");
    });
});*/
