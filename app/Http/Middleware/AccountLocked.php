<?php

namespace App\Http\Middleware;

use Closure;

/**
 * This Middleware is resposible to redirect to
 * Redirect users back to home page if the users
 * account has been locked.
 */
class AccountLocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()) {
            if ($request->user()->is_active === false && $request->user()->valid_pin === false && $request->user()->attemps === 3) {
                return redirect('/')->withErrors("Not Registered");
            }
        }
        return $next($request);
    }
}
