<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Group;

class PublicRedirectAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->user()) {
            $groupAdmin = Group::where('roles', 'admin')->first()->group_id;
            $groupUser  = Group::where('roles', 'user')->first()->group_id;
            $user       = Auth::user();
            if ($user->group_id === $groupUser) {
                return redirect('users/dashboard');
            } else if ($user->group_id === $groupAdmin) {
                return redirect('admin/dashboard');
            }

        }

        return $next($request);
    }
}
