<?php

namespace App\Http\Middleware;

use Closure;

/**
 * This Middleware is resposible to redirect
 * users to pin page if user has not entered a pin.
 */
class NoPin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->user()) {
            if ($request->user()->valid_pin === false) {
                return redirect('/users/pin');
            }
        }

        return $next($request);
    }
}
