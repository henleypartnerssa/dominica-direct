<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table        = 'messages';
    protected $primaryKey   = 'message_id';
    protected $fillable     = [
        'application_id',
        'profile_id',
        'user_id',
        'group_id',
        'subject',
        'sent_from',
        'read',
        'message',
        'created_at'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function application() {
        return $this->belongsTo('App\Application');
    }

}
