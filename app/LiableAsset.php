<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiableAsset extends Model
{
    protected $table        = 'liable_assets';
    protected $primaryKey   = 'liable_asset_id';
    protected $fillable     = [
        'profile_id',
        'user_id',
        'group_id',
        'applicaiton_id',
        'item',
        'amount',
        'indicator'
    ];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

}
