<?php

namespace App\Traits;

use App\Http\Requests;
use App\_Case;
use App\LanguageType;
use App\User;
use App\Step;
use Carbon\Carbon;
use App\Util\AppHelpers;
use App\Address;
use App\LanguageLevel;
use App\Language;
use App\Profile;
use App\DependantType;
use App\FamilyMember;

/**
 * This Trait containts additional functions for CasesController
 * specifically for Personal Data Form.
 */
trait CasesPerosnalDataFormHelper
{

    /**
     * Saves POST data from Main Personal Detail form to DB.
     * Also saves users selected Languages to DB.
     *
     * @param  [int]        $id         [Case_ID]
     * @param  [Request]    $request    [POST data]
     * @return [bool] [returns true if all data save, false if and save attempt failed]
     */
    public function savePersonalDataToDb($id,$request) {
        // Get current used User Profile from Case
        $profile = _Case::findOrFail($id)->profile;

        // Map Request fields to Profile Object
        $profile->title                                 = $request['title'];
        $profile->title_other                           = $request['title_other'];
        $profile->legal_passport_surname                = $request['legal_surname'];
        $profile->legal_passport_first_name             = $request['legal_first_names'];
        $profile->legal_passport_middle_names           = $request['legal_mid_names'];
        $profile->gender                                = $request['gender'];
        $profile->marital_status                        = $request['marital_status'];
        $profile->data_of_marriage                      = ($request['mar_year'] !== "") ? Carbon::parse(AppHelpers::normalizeDate($request['mar_year'],$request['mar_month'],$request['mar_day'])) : null;
        $profile->place_of_marriage                     = $request['marriage_place'];
        $profile->date_divorce                          = ($request['div_year'] !== "") ? Carbon::parse(AppHelpers::normalizeDate($request['div_year'],$request['div_month'],$request['div_day'])) : null;
        $profile->place_divorce                         = $request['div__place'];
        $profile->legal_bc_first_name                   = $request['legal_bc_first_names'];
        $profile->legal_bc_middle_names                 = $request['legal_bc_mid_names'];
        $profile->legal_es_first_name                   = $request['legal_es_first_names'];
        $profile->legal_es_surname                      = $request['legal_es_surnames'];
        $profile->legal_former_maiden_name              = $request['other_maiden_name'];
        $profile->former_other_names                    = $request['other_names'];
        $profile->legal_name_change_date                = ($request['name_change_year'] !== "") ? Carbon::parse(AppHelpers::normalizeDate($request['name_change_year'],$request['name_change_month'],$request['name_change_day'])) : null;
        $profile->legal_name_change_reason              = $request['reason_name_change'];
        $profile->identity_card_no                      = $request['id_no'];
        $profile->identity_card_issuing_country         = $request['id_issuing_coutry'];
        $profile->social_security_or_insurance_no       = $request['security_no'];
        $profile->social_security_or_insurance_country  = $request['sn_issuing_coutry'];
        $profile->date_of_birth                         = Carbon::parse(AppHelpers::normalizeDate($request['dob_year'],$request['dob_month'],$request['dob_day']));
        $profile->place_of_birth                        = $request['place_of_birth'];
        $profile->country_of_birth                      = $request['cob'];
        $profile->citizenship_at_birth                  = $request['cab'];
        $profile->permanent_telephone_no                = $request['permanent_telephone_no'];
        $profile->permanent_mobile_no                   = $request['mobile_telephone_no'];
        $profile->personal_email_address                = $request['personal_email_address'];
        $profile->citizen_other                         = $request['other_citizenship'];
        $saved =  $profile->save();

        // If Profile Saved successfully then Save Request languages
        // to relational language table.
        if ($saved) {
            // Call Trait internal function to save Residential Address to
            // DB table.
            $savedResAddr = $this->savePersonalDataAddress($profile, $request, 'res');

            // If Alternative Address are populate then call internalt Trait
            // function to save to Address to DB.
            if ($request['alt_country'] !== '' && $savedResAddr) {
                $savedAltAddr = $this->savePersonalDataAddress($profile, $request, 'alt');
            } else if (!$savedResAddr) {
                return false;
            }

            // Call internal Trait function multiple times to save/delete POST
            // Languages to/from DB.
            $lang_1 = $this->saveOrDelPersonalDataLanguages($profile,$request,'fluent', 'speak');
            $lang_2 = $this->saveOrDelPersonalDataLanguages($profile,$request,'fluent', 'read');
            $lang_3 = $this->saveOrDelPersonalDataLanguages($profile,$request,'fluent', 'write');
            $lang_4 = $this->saveOrDelPersonalDataLanguages($profile,$request,'basic', 'speak');
            $lang_5 = $this->saveOrDelPersonalDataLanguages($profile,$request,'basic', 'read');
            $lang_6 = $this->saveOrDelPersonalDataLanguages($profile,$request,'basic', 'write');
            $lang_7 = $this->saveOrDelPersonalDataLanguages($profile,$request,'weak', 'speak');
            $lang_8 = $this->saveOrDelPersonalDataLanguages($profile,$request,'weak', 'read');
            $lang_9 = $this->saveOrDelPersonalDataLanguages($profile,$request,'weak', 'write');

            if ($savedResAddr && $lang_1 && $lang_2 && $lang_3 && $lang_4 && $lang_5 && $lang_6 && $lang_7 && $lang_8 && $lang_9) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    /**
     * Save Address POST data to database.
     *
     * @param  [Profile]    $profile [Users Profile to which relational address needs to be saved to.]
     * @param  [Request]    $request [Form POST data]
     * @param  [string]     $type    [Used to distinguish between Res and Alt form POST data field values]
     * @return [bool]       [description]
     */
    public function savePersonalDataAddress($profile,$request,$type) {

        $is_new     = false;
        $address    = null;

        // Check if Has Address relation already. If so then prepare for Address record Update else
        // prepare for new Address DB insert.
        if ($profile->addresses()->where('address_type', $type)->get()->count() > 0) {
            $address = $profile->addresses()->where('address_type', $type)->get()->first();
        } else {
            $address    = new Address();
            $is_new     = true;
        }

        // Map POST data to Address Object and Save to DB
        $address->country_id    = $request[$type.'_country'];
        $address->address_type  = $type;
        $address->street1       = $request[$type.'_street1'];
        $address->street2       = $request[$type.'_street2'];
        $address->town          = $request[$type.'_town'];
        $address->postal_code   = $request[$type.'_post_code'];;
        $saved = $address->save();

        // If is new Address then Insert record to Many-Many table [Address_Profile] as well.
        if ($saved && $is_new) {
            $data = [
                'profile_id'    => $profile->profile_id,
                'user_id'       => $profile->user_id,
                'group_id'      => $profile->group_id,
                'country_id'    => $address->country_id
            ];

            $savedManyToMany = $address->profiles()->attach($address->address_id, $data);

            if ($savedManyToMany) {
                return true;
            } else {
                return false;
            }
        // if existing Address has been updated or not.
        } else if (!$saved || $saved) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Each User->Profile are allowed to save/delete multiple languages. This function
     * saves/deletes POST specified languages to/from DB based in paresed paramenters.
     *
     * @param  [Profile]    $profile    [Users profile to which relational languages needs to be saved to.]
     * @param  [Request]    $request    [Form POST data]
     * @param  [string]     $type       [Specifier used to determine DB query-by records or selective POST inputs.]
     * @param  [string]     $action     [Specifier used to determine DB query-by records or selective POST inputs.]
     * @return [bool]       [Returns true of Language are successfully saved/deleted to/from DB.]
     */
    public function saveOrDelPersonalDataLanguages($profile,$request,$type, $action) {

        // Assign POST input value .i.e [speak_fluent] multi-selection box value.
        $inputs  = $request[$action.'_'.$type];

        // If Form input has value then save to DB else delete existing language
        // from DB.
        if ($inputs !== "") {

            $splitInputs = explode(',', $inputs);
            $level      = LanguageLevel::all()->where('level', $type)->first();

            // Loop through values of FORM input
            foreach($splitInputs as $langTypeId) {

                // if blank input value iterate to next.
                if (empty($langTypeId)) {
                    continue;
                }

                // Query DB to check if language exists.
                $profLangs = $profile->languages()
                ->where('language_type_id', (int)$langTypeId)
                ->where('action',$action)
                ->where('language_level_id',$level->language_level_id)->get();

                // If DB record exists then iterate to next else insert new
                // language to DB.
                if ($profLangs->count() > 0) {
                    continue;
                } else {
                    $ltypeId                        = LanguageType::find((int)$langTypeId)->language_type_id;
                    $language                       = new Language();
                    $language->language_type_id     = $ltypeId;
                    $language->profile_id           = $profile->profile_id;
                    $language->user_id              = $profile->user_id;
                    $language->group_id             = $profile->group_id;
                    $language->language_level_id    = $level->language_level_id;
                    $language->action               = $action;
                    $saved = $language->save();
                }
            }
            // Delete existing language from DB if not in form input.
            return $this->delLanguages($profile,$splitInputs,$action,$type);
        } else {
            // Delete existing language from DB if not in form input.
            return $this->delLanguages($profile,explode(',', $inputs),$action, $type);
        }

    }

    /**
     * Delete existing Language Records from DB if not specified in FORM input but
     * exists within DB.
     *
     * @param  [Profile]    $profile [Users Profile from which relational delete will occur.]
     * @param  [Array]      $array   [Array of FORM input language values]
     * @param  [string]     $action  [Specifier used to determine DB query-by records or selective POST inputs.]
     * @param  [string]     $level   [Specifier used to determine DB query-by records or selective POST inputs.]
     * @return [bool]       [Return false if DB delete failed else return true]
     */
    private function delLanguages($profile, $array, $action, $level) {
        $langType   = LanguageLevel::all()->where('level', $level)->first()->language_level_id;
        $langs      = $profile->languages()->where('action',$action)->where('language_level_id',$langType)->get()->lists('language_type_id')->toArray();
        $return     = true;

        // Loop through Array of existing DB languages identifiers
        foreach($langs as $l) {

            // Remove blank elements from array paramenter
            $blank = array_search('',$array);
            unset($array[$blank]);

            // check if language exists within array parameter
            $val = array_search(strval($l),$array);

            // If language is not within array parameter then delete language from DB.
            if ($val === false) {
                $profLangs  = $profile->languages()->where('language_type_id', $l)->where('action',$action)->get()->first();
                $deleted    = $profLangs->delete();

                if (!$deleted) {
                    $return = false;
                    break;
                }
            }
        }

        return $return;

    }

    /**
     * Builds array of Languages derived from DB.
     *
     * @return [Array] [array of languages]
     */
    public function getAllLangLevels() {
        return [
            [
                $this->getLanguages('speak', 'fluent'),
                $this->getLanguages('speak', 'basic'),
                $this->getLanguages('speak', 'weak'),
            ],
            [
                $this->getLanguages('read', 'fluent'),
                $this->getLanguages('read', 'basic'),
                $this->getLanguages('read', 'weak'),
            ],
            [
                $this->getLanguages('write', 'fluent'),
                $this->getLanguages('write', 'basic'),
                $this->getLanguages('write', 'weak'),
            ]
        ];
    }

    /**
     * Get Lanuages from DB and returns custom built custom array
     * which will be used for FORM Languages DropDown.
     *
     * @param  [string] $type  [action = read/write/speak]
     * @param  [string] $level [level = fluent/basic/weak]
     * @return [array]
     */
    public function getLanguages($type,$level) {

        $langs = LanguageType::all()->toArray();
        $data =  [
            'name'      => $type.'_'.$level,
            'title'     => $type." ".$level,
            'languages' => $langs
        ];

        return $data;

    }

    /**
     * Gets Users->Profile existing languages from DB. Which will be
     * used to pupulate the Form languages input values.
     *
     * @param  [Profile] $profile [Users Profile used to query relational languages from]
     * @return [Array]   [array of Profile existing languages]
     */
    private function getProfileLanguages($profile) {
        $languages = $profile->languages()->get();
        $speakFluent    = '';
        $speakBasic     = '';
        $speakWeak      = '';
        $readFluent     = '';
        $readBasic      = '';
        $readWeak       = '';
        $writeFluent    = '';
        $writeBasic     = '';
        $writeWeak      = '';

        foreach($languages as $lang) {
            $level = LanguageLevel::find((int)$lang->language_level_id)->level;
            if ($lang->action === "speak" && $level === "fluent") {
                $speakFluent = $speakFluent.$lang->language_type_id.',';
            } else if ($lang->action === "speak" && $level === "basic") {
                $speakBasic = $speakBasic.$lang->language_type_id.',';
            } else if ($lang->action === "speak" && $level === "weak") {
                $speakWeak = $speakWeak.$lang->language_type_id.',';
            }else if ($lang->action === "read" && $level === "fluent") {
                $readFluent = $readFluent.$lang->language_type_id.',';
            }else if ($lang->action === "read" && $level === "basic") {
                $readBasic = $readBasic.$lang->language_type_id.',';
            }else if ($lang->action === "read" && $level === "weak") {
                $readWeak = $readWeak.$lang->language_type_id.',';
            }else if ($lang->action === "write" && $level === "fluent") {
                $writeFluent = $writeFluent.$lang->language_type_id.',';
            }else if ($lang->action === "write" && $level === "basic") {
                $writeBasic = $writeBasic.$lang->language_type_id.',';
            }else if ($lang->action === "write" && $level === "weak") {
                $writeWeak = $writeWeak.$lang->language_type_id.',';
            }
        }

        return [
            'speak_fluent'   => $speakFluent,
            'speak_basic'    => $speakBasic,
            'speak_weak'     => $speakWeak,
            'read_fluent'    => $readFluent,
            'read_basic'     => $readBasic,
            'read_weak'      => $readWeak,
            'write_fluent'   => $writeFluent,
            'write_basic'    => $writeBasic,
            'write_weak'     => $writeWeak,
        ];
    }

    /**
     * Determines if a User requires new Profile for
     * users newly created Case.
     *
     * @param User $auth
     * @param string $applying_as
     * @return Profile
     */
    private function manageProfileForCase(User $auth, $applying_as) {

        $profile            = null;
        $totalUserProfiles  = $auth->profiles()->count();


        if ($totalUserProfiles > 0) {

            $specificUserProfiles   = $auth->profiles()->where('completing_form_as', $applying_as);
            $keyinfoUserProfiles    = $auth->profiles()->where('completing_form_as', null);
            if ($specificUserProfiles->count() > 0) {
                $profile = $specificUserProfiles->first();
                $profile->completing_form_as = $applying_as;
                $profile->save();
            } else if ($keyinfoUserProfiles->count() > 0) {
                $profile = $keyinfoUserProfiles->first();
                $profile->completing_form_as = $applying_as;
                $profile->save();
            } else {
                $newProfile = new Profile();
                $newProfile->user_id            = $auth->user_id;
                $newProfile->group_id           = $auth->group_id;
                $newProfile->completing_form_as = $applying_as;
                $profile                        = $auth->profiles()->create($newProfile->toArray());
            }

        }

        return $profile;

    }


}
