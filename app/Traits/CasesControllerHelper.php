<?php

namespace App\Traits;

use App\Http\Requests;
use App\_Case;
use App\LanguageType;
use App\User;
use App\Step;
use Carbon\Carbon;
use App\Util\AppHelpers;
use App\Address;
use App\LanguageLevel;
use App\Language;
use App\Profile;
use App\DependantType;
use App\FamilyMember;
use App\Programs\Antigua;
use App\AntiguaProgram;

trait ApplicationsControllerHelper
{

    public function lkpPersonalDataView($program, $programId) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programId);
            return $p->personalDataView();
        }
    }

    public function termsConditionsRedirect($program, $case = null) {
        if ($program === "antigua-and-barbuda") {
            $tc = new Antigua();
            return $tc->termsConditionsView($case);
        }
    }

    public function personalDetailsRedirect($program, $programID, Request $request) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programID);
            return $p->personalDataView($program, $programID, $request);
        }
    }

    public function familyMembersView($program, $programId) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programId);
            return $p->familyMembersView($program, $programId);
        }
    }

    public function saveFamMember($profileId, $request, $type) {

        $hasMember  = FamilyMember::where('relationship', $type)->count();
        $typeLower  = strtolower($type);
        $profile    = Profile::where('profile_id', $profileId)->get()->first();
        $member     = (count($hasMember) > 0) ? FamilyMember::where('relationship', $type)->first() : null;
        $depType    = DependantType::where('type', $type)->first();

        if ($member === null) {
            $member = new FamilyMember();
        }

        $member->profile_id         = $profile->profile_id;
        $member->user_id            = $profile->user_id;
        $member->group_id           = $profile->group_id;
        $member->dependant_type_id  = $depType->dependant_type_id;
        $member->first_name         = $request[$typeLower.'_first_name'];
        $member->surname            = $request[$typeLower.'_fam_name'];
        $member->gender             = ($request[$typeLower.'_first_name'] !== '') ? $request[$type.'_first_name'] : null;
        $member->date_of_birth      = Carbon::parse(AppHelpers::normalizeDate($request[$typeLower.'_dob_year'],$request[$typeLower.'_dob_month'],$request[$typeLower.'_dob_day']));
        $member->place_of_birth     = $request[$typeLower.'_pob'];
        $member->citizenship            = $request[$typeLower.'_citizenship'];
        $member->country_of_residence   = $request[$typeLower.'_residence'];
        $member->relationship           = $type;

        if ($type === 'Spouse') {
            $val = ($request['is_included'] === '1') ? true : false;
            $member->is_included = $val;
        }

        if ($member->save()) {
            return true;
        } else {
            false;
        }

    }

    private function getFamily($profile) {
        $brotherID  = DependantType::where('type', 'Brother')->first()->dependant_type_id;
        $sisID      = DependantType::where('type', 'Sister')->first()->dependant_type_id;
        $childID    = DependantType::where('type', 'Child')->first()->dependant_type_id;

        $data = [
            'father'    => $profile->family_members()->where('relationship', 'Father')->first(),
            'mother'    => $profile->family_members()->where('relationship', 'Mother')->first(),
            'spouse'    => $profile->family_members()->where('relationship', 'Spouse')->first(),
            'siblings'  => $profile->family_members()->whereIn('dependant_type_id', array($brotherID, $sisID))->get(),
            'children'  => $profile->family_members()->where('dependant_type_id', $childID)->get(),
        ];

        return $data;
    }

}
