<?php

namespace App\Traits;

use App\Profile;
use App\FamilyMember;
use App\DependantType;
use App\Util\AppHelpers;
use Carbon\Carbon;

trait CasesFamilyDataFormHelper
{

    /**
     * Funtion used to save/update Parent/Spouse Family Memeber to/from DB.
     *
     * @param  [int]        $profileId [description]
     * @param  [Request]    $request   [description]
     * @param  [string]     $type      [description]
     * @return [bool]       [returns true in db insert or update]
     */
    public function saveSpouseOrParent($profileId, $request, $type) {

        $hasMember  = FamilyMember::where('relationship', $type)->count();
        $typeLower  = strtolower($type);
        $profile    = Profile::where('profile_id', $profileId)->get()->first();
        $member     = (count($hasMember) > 0) ? FamilyMember::where('relationship', $type)->first() : null;
        $depType    = DependantType::where('type', $type)->first();

        if ($member === null) {
            $member = new FamilyMember();
        }

        $member->profile_id         = $profile->profile_id;
        $member->user_id            = $profile->user_id;
        $member->group_id           = $profile->group_id;
        $member->dependant_type_id  = $depType->dependant_type_id;
        $member->first_name         = $request[$typeLower.'_first_name'];
        $member->surname            = $request[$typeLower.'_fam_name'];
        $member->gender             = ($request[$typeLower.'_first_name'] !== '') ? $request[$type.'_first_name'] : null;
        $member->date_of_birth      = Carbon::parse(AppHelpers::normalizeDate($request[$typeLower.'_dob_year'],$request[$typeLower.'_dob_month'],$request[$typeLower.'_dob_day']));
        $member->place_of_birth     = $request[$typeLower.'_pob'];
        $member->citizenship        = $request[$typeLower.'_citizenship'];
        $member->relationship       = $type;

        if ($type === 'Spouse') {
            $val = ($request['is_included'] === '1') ? true : false;
            $member->is_included = $val;
        }

        if ($member->save()) {
            return true;
        } else {
            false;
        }

    }

    /**
     * Build custom array of existing User->Profile family members.
     *
     * @param  [Profile] $profile [Users-Profile to do relational query from]
     * @return [array]
     */
    private function getFamilyForForm($profile) {
        $brotherID  = DependantType::where('type', 'Brother')->first()->dependant_type_id;
        $sisID      = DependantType::where('type', 'Sister')->first()->dependant_type_id;
        $childID    = DependantType::where('type', 'Child')->first()->dependant_type_id;

        $data = [
            'father'    => $profile->family_members()->where('relationship', 'Father')->first(),
            'mother'    => $profile->family_members()->where('relationship', 'Mother')->first(),
            'spouse'    => $profile->family_members()->where('relationship', 'Spouse')->first(),
            'siblings'  => $profile->family_members()->whereIn('dependant_type_id', array($brotherID, $sisID))->get(),
            'children'  => $profile->family_members()->where('dependant_type_id', $childID)->get(),
        ];

        return $data;
    }

}
