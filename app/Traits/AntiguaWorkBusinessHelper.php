<?php

namespace App\Traits;

use App\Profile;
use App\Address;
use App\WorkOrBusiness;
use App\Bank;
use App\TotalNetWorth;
use App\Util\AppHelpers;

trait AntiguaWorkBusinessHelper {

    public function saveWorkBank($request) {

        $saveBankAddr = $this->saveWorkBankAddress($this->PROFILE, $request, 'bank');
        $saveWorkAddr = $this->saveWorkBankAddress($this->PROFILE, $request, 'work');

        if ($saveBankAddr && $saveWorkAddr) {
            $saveWork = $this->saveWork($this->PROFILE, $saveWorkAddr, $request);
            $saveBank = $this->saveBank($this->PROFILE, $saveBankAddr, $request);

            if ($saveWork && $saveBank) {
                $this->saveNetWorths($saveWork, $request, 'bank_acc');
                $this->saveNetWorths($saveWork, $request, 'investment_acc');
                $this->saveNetWorths($saveWork, $request, 're_holdings');
                $this->saveNetWorths($saveWork, $request, 'business_assets');
                $this->saveNetWorths($saveWork, $request, 'other');
                return true;
            } else {
                return false;
            }
        }

    }

    private function saveWorkBankAddress($profile, $request, $type) {

        if ($type === 'bank') {
            $count = (isset($profile->bank->account_id)) ? $profile->bank()->get()->first()->address()->count(): 0;
        } else {
            $count = (isset($profile->Work_or_businesses()->get()->first()->business_id)) ? $profile->Work_or_businesses()->get()->first()->address()->count() : 0;
        }

        $address = null;

        if ($count > 0) {
            if ($type === 'bank') {
                $address = $profile->bank()->get()->first()->address()->first();
            } else {
                $address = $profile->Work_or_businesses()->get()->first()->address()->first();
            }
        } else {
            $address = new Address();
        }

        $address->address_type  = $type;
        $address->country       = $request[$type.'_country'];
        $address->street1       = $request[$type.'_street1'];
        $address->street2       = $request[$type.'_street2'];
        $address->town          = $request[$type.'_town'];
        $address->postal_code   = $request[$type.'_postal_code'];
        $address->save();

        return $address;
    }

    private function saveWork($profile, $address, $request) {

        $work = null;

        if ($profile->Work_or_businesses()->count() > 0) {
            $work = $profile->Work_or_businesses()->get()->first();
        } else {
            $work = new WorkOrBusiness();
        }

        $work->profile_id               = $request['profile_id'];
        $work->application_id           = $profile->application_id;
        $work->group_id                 = $profile->group_id;
        $work->user_id                  = $profile->user_id;
        $work->address_id               = $address->address_id;
        $work->name_of_business         = $request['name_of_business'];
        $work->business_tel_no          = $request['business_tel_no'];
        $work->business_email           = $request['business_email'];
        $work->business_web_url         = $request['business_web_url'];
        $work->primary_occupation       = $request['primary_occupation'];
        $work->self_employed            = ($request['employed'] ==='yes') ? true : false;
        $work->nature_of_business       = $request['nature_of_business'];
        $work->mi_persons_companies     = $request['mi_persons_companies'];
        $work->gross_net_income         = AppHelpers::normalizeMoneyTotal($request['gross_net_income'],$request['gross_currency']);
        $work->source_of_income         = $request['source_of_income'];
        $work->total_net_worth          = AppHelpers::normalizeMoneyTotal($request['total_net_worth'],$request['net_currency']);
        $work->total_net_worht_summary  = $request['total_net_worht_summary'];
        $work->business_activities_geo  = $request['business_activities_geo'];
        $work->business_net_worth_geo   = $request['business_net_worth_geo'];
        $work->save();

        return $work;
    }

    private function  saveBank($profile, $address, $request) {

        $bank = null;

        if ($profile->bank()->count() > 0) {
            $bank = $profile->bank()->get()->first();
        } else {
            $bank = new Bank();
        }

        $bank->profile_id       = $request['profile_id'];
        $bank->application_id   = $profile->application_id;
        $bank->group_id         = $profile->group_id;
        $bank->user_id          = $profile->user_id;
        $bank->address_id       = $address->address_id;
        $bank->account_name_of  = $request['account_name_of'];
        $bank->bank_name        = $request['bank_name'];
        $bank->account_no       = $request['account_number'];
        $bank->account_iban_bic = $request['account_bic'];
        $bank->save();
        return $bank;
    }

    public function saveNetWorths($business, $post, $type) {
        $program    = $this->APP;
        $total      = $post[$type.'_total'];
        $curr       = $post[$type.'_currency'];

        if ($post[$type] === 'on') {

            $counter = $business->total_net_worths()->where('type', $type)->count();
            if ($counter > 0) {
                $net = $business->total_net_worths()->where('type', $type)->get()->first();
                $net->update(['amount' => AppHelpers::normalizeMoneyTotal($total,$curr)]);
                return;
            }

            $business->total_net_worths()->create([
                'profile_id'        => $business->profile_id,
                'user_id'           => $business->user_id,
                'group_id'          => $business->group_id,
                'address_id'        => $business->address_id,
                'application_id'    => $business->application_id,
                'type'              => $type,
                'amount'            => AppHelpers::normalizeMoneyTotal($total,$curr)
            ]);
        }
    }

    public function businessTotalForView($business) {

        $array = null;
        if (!empty($business)) {
            $array = [
                "gross_total"       => AppHelpers::stripCurrency($business->gross_net_income),
                "gross_currency"    => AppHelpers::getCurrencyFromTotal($business->gross_net_income),
                "net_total"         => AppHelpers::stripCurrency($business->total_net_worth),
                "net_currency"      => utf8_decode(AppHelpers::getCurrencyFromTotal($business->total_net_worth))
            ];

            foreach($business->total_net_worths as $net) {
                $array[$net->type."_total"]      = AppHelpers::stripCurrency($net->amount);
                $array[$net->type."_currency"]   = AppHelpers::getCurrencyFromTotal($net->amount);
            }
        }

        return $array;

    }

}
