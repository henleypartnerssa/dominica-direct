<?php

namespace App\Traits;

use App\User;
use App\Group;
use App\Profile;
use App\Step;
use App\Util\SendEmail as UtilMailer;
use App\Util\AppFileSystem;
use Crypt;
use Auth;
use Carbon\Carbon;
use AppHelpers;
use hpsadev\ProgramsFileManager\DirectoryManager;

/**
 * This Trait contains functions to assist
 * the UsersController class.
 */
trait UsersControllerHelper {

    /**
     * Add new User.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return User $user
     */
    public function addUser($request) {

        $exists = User::where('username', $request['email'])->count();
        $user   = null;

        if ($exists === 0) {
            $request['group_id']    = Group::where('roles', 'user')->first()->group_id;
            $request['username']    = $request['email'];
            $request['token']       = $request['email'];
            $request['is_active']   = false;
            $request['valid_pin']   = false;
            $request['attempts']    = 3;
            // Save POST data to DB.
            $user   = User::create($request->all());
            // Create User FileSystem Base Directories.
            $fs     = new DirectoryManager();
            $fs->createUserBaseDirs($user->user_id);
        }
        return $user;

    }

    /**
     * Validates Users LogOn Attempts.
     *
     * @since 1.0 Function Init
     * @param User $user
     * @return string
     */
    public function getLogOnAttempts(User $user) {

        if ($user->is_active === false) {
            return 'locked';
        }

        if ($user->is_active && ($user->attempts === 0 || $user->attempts === NULL)) {

            $user->is_active    = false;
            $user->attempts     = 3;
            $user->save();
            return 'locked';

        } else {

            $attempt        = $user->attempts - 1;
            $user->attempts = $attempt;
            $user->save();
            return 'invalid';

        }
    }

    /**
     * Send Verification Email to User
     * attempting to Register.
     *
     * @since 1.0 Function Init
     * @param Request $request
     * @return int
     */
    public function sendVerificationEmail($user, $request) {

        $url        = "users/verify";
        $secret     = Crypt::encrypt($user->user_id);
        $uri        = url($url, $parameters = array($secret), $secure = false);
        $recipient  = ['name' => $request['name'], 'lastname' => $request['lastname'], 'email' => $request['email']];
        $data       = ['name' => $request['name'], 'lastname' => $request['lastname'], 'url' => $uri];
        $mailer     = new UtilMailer("verification", $recipient, $data);

        return $mailer->didSend;
    }

    /**
     * Send User verification email for
     * Mobile number changed.
     *
     * @since 1.0 Function Init
     * @param User $user
     * @return int
     */
    public function sendMobileNumberChangeEmail($user) {

        $url        = "users/mobile/verify";
        $secret     = Crypt::encrypt($user->username);
        $uri        = url($url, $parameters = array($secret), $secure = false);
        $recipient  = ['email' => $user->username];
        $data       = ['url' => $uri];
        $mailer     = new UtilMailer("changeMobile", $recipient, $data);

        return $mailer->didSend;

    }

    /**
     * Build Array of data specifically for
     * the Users Dashboard page.
     *
     * @since 1.0 Function Init
     * @param \App\Application $applications
     * @return array
     */
    public function buildDashData($profiles) {
        $array      = [];
        $app        = $profiles->first()->application;
        $pa         = $profiles->where('belongs_to', 'Principal Applicant')->first();
        $members    = [];
        foreach ($profiles->sortBy('created_at') as $p) {
            array_push($members, [
                'applicant'     => $p->first_name.' '.$p->last_name,
                'fname'         => $p->first_name."'s",
                'applicant_t'   => $p->belongs_to,
                'message_url'   => '/users/messages/case/'.$p->application_id,
                'profile_id'    => $p->profile_id
            ]);
        }
        $array['members']       = $members;
        $array['ref']           = $app->case_no;
        $array['program']       = env('PROGRAM_NAME');
        $array['current_step']  = ($app->step === 1) ? 0 : $app->step;
        $array['url']           = AppHelpers::stepperLinks($pa->profile_id, $pa->application_id)[$app->step];
        $array['paid']          = ($app->paid === true) ? "Yes" : "No";

        return $array;
    }

    /**
     * Get the User's primary information from the Profile model.
     *
     * @since 1.0 Function Init
     * @return array array of profile fields information
     */
    public function getUserInfo() {

        $user       = Auth::user();
        $profile   = $user->profile;

        return [
            'name'      => $user->name,
            'surname'   => $user->surname,
            'dob'       => (!empty($profile->date_of_birth)) ? Carbon::parse($profile->date_of_birth) : null,
            'gender'    => (!empty($profile->gender)) ? $profile->gender : null
        ];

    }

    /**
     * Returns the Authenticated Users Password
     *
     * @since 1.0 Function Init
     * @return string user passord
     */
    public function password() {
        $user = Auth::user();
        return $user->password;
    }

}
