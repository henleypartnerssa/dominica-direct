<?php

namespace App\Traits;

use App\Http\Requests;
use App\LanguageType;
use App\User;
use App\Step;
use Carbon\Carbon;
use App\Util\AppHelpers;
use App\Address;
use App\LanguageLevel;
use App\Language;
use App\Profile;
use App\DependantType;
use App\FamilyMember;
use App\Programs\Antigua\Antigua;
use App\AntiguaProgram;

trait ApplicationsControllerHelper
{
    public function uploadDocumentsRedirect($program, $programID, $request) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programID);
            return $p->uploadDocumentsRedirect($program, $programID, $request);
        }
    }

    public function deleteDocumentsRedirect($program, $programID, $request) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programID);
            return $p->deleteDocumentsRedirect($program, $programID, $request);
        }
    }

    public function governmentDocumentsView($program, $programID) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programID);
            return $p->governmentDocumentsView($program, $programID);
        }
    }

    public function uploadNotaryRedirect($program, $programID, $request) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programID);
            return $p->uploadNotaryRedirect($program, $programID, $request);
        }
    }

    public function deleteNotaryRedirect($program, $programID, $request) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programID);
            return $p->deleteNotaryRedirect($program, $programID, $request);
        }
    }

    public function ddCheckPackageView($program, $programID) {
        if ($program === "antigua-and-barbuda") {
            $p = new Antigua($programID);
            return $p->ddCheckPackageView($program, $programID);
        }
    }

}
