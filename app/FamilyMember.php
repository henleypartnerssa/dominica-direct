<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class FamilyMember extends Model
{
    protected $table        = 'family_members';
    protected $primaryKey   = 'family_member_id';

    protected $fillable     =   [
                                    'profile_id',
                                    'user_id',
                                    'group_id',
                                    'application_id',
                                    'dependant_type_id',
                                    'first_name',
                                    'middle_names',
                                    'last_name',
                                    'gender',
                                    'date_of_birth',
                                    'place_of_birth',
                                    'citizenship_at_birth',
                                    'country_of_birth',
                                    'relationship',
                                    'is_included'
                                ];


    public function setDateOfBirthAttribute($date) {
        $this->attributes['date_of_birth'] = Carbon::parse($date);
    }

    public function profiles() {
        return $this->belongsToMany('App\Profile', 'family_profile');
    }

    public function addresses() {
        return $this->belongsToMany('App\Address','address_family_member')->withTimestamps();
    }

    public function dependant_type() {
        return $this->belongsTo('App\DependantType');
    }

    public function citizenship() {
        return $this->belongsTo('App\Country', 'citizenship', 'country_id');
    }

    public function residence() {
        return $this->belongsTo('App\Country', 'country_of_residence', 'country_id');
    }

}
