<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResidenceCard extends Model
{

    /**
     * Database table name
     * @var string
     */
    protected $table        = 'residence_cards';

    /**
     * table primarhy Key
     * @var string
     */
    protected $primaryKey   = 'residence_card_id';

    /**
     * fields that can be edited.
     * @var array
     */
    protected $fillable     = [
        'profile_id','user_id','group_id','application_id','country_id','card_no','place_of_issue','date_of_issue','date_of_expiration', 'issuing_country'
    ];

    /**
     * Many to One Relationship with Profile Model
     * @return Relationship Many to One Relationship
     */
    public function profile() {
        return $this->belongsTo('App\Profile');
    }

}
