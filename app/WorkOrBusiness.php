<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkOrBusiness extends Model
{
    protected $table        = 'businesses';
    protected $primaryKey   = 'business_id';
    protected $fillable     = [
        'profile_id',
        'user_id',
        'group_id',
        'address_id',
        'applicaiton_id',
        'name_of_business',
        'registration_no',
        'country_of_registration',
        'phone_no',
        'fax_no',
        'email',
        'web_url',
        'primary_occupation',
        'occupation_by_training',
        'self_employed',
        'nature_of_business',
        'mi_persons_companies',
        'gross_net_income',
        'source_of_income',
        'total_net_worth',
        'total_net_held_as',
        'total_net_held_as_other',
        'total_net_worth_summary',
        'activities_geo',
        'companies_share_dir'
    ];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

    public function address() {
        return $this->belongsTo('App\Address');
    }

    public function antigua_programs() {
        return $this->belongsToMany('App\AntiguaProgram', 'antigua_total_net_worths', 'business_id')->withPivot('type', 'amount')->withTimestamps();
    }

    public function professional_licences() {
        return $this->hasMany('App\ProfessionalLicence', 'business_id');
    }



}
