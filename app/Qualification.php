<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $table        = 'qualifications';
    protected $primaryKey   = 'qualification_id';
    protected $fillable     = [
        'profile_id',
        'user_id',
        'group_id',
        'address_id',
        'country_id',
        'qualification_name',
        'qualification_type',
        'from_date',
        'to_date'
    ];
    
    public function profile() {
        return $this->belongsTo('App\Profile');
    }
    
    public function address() {
        return $this->belongsTo('App\Address');
    }
}
