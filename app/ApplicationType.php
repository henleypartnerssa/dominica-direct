<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationType extends Model
{
    
    protected $table = 'application_types';
    protected $primaryKey = 'application_type_id';
    
    public function quotaions() {
        return $this->hasMany('App\Quotation');
    }
    
}
