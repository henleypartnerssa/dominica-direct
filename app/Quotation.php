<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{

    protected $table = 'quotations';
    protected $primaryKey = 'quotation_id';

    protected $fillable = ['user_id', 'group_id', 'name'];


    public function user() {
        return $this->belongsTo('App\User');
    }

    public function application_type() {
        return $this->belongsTo('App\ApplicationType');
    }
}
