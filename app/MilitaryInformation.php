<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class MilitaryInformation extends Model
{
    protected $table        = 'military_information';
    protected $primaryKey   = 'military_info_id';
    protected $fillable     = [
        'application_id',
        'profile_id',
        'user_id',
        'group_id',
        'branch',
        'active_service_dt',
        'seperation_dt',
        'type_of_discharge',
        'ranking_at_seperation',
        'serial_no',
        'offence'
    ];

    protected $dates        = ['active_service_dt', 'seperation_dt'];

    public function setActiveServiceDtAttribute($date) {
        $this->attributes['active_service_dt'] = Carbon::parse($date)->toDateString();
    }

    public function setSeperationDtAttribute($date) {
        $this->attributes['seperation_dt'] = Carbon::parse($date)->toDateString();
    }

    public function getActiveServiceDtAttribute($value) {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getSeperationDtAttribute($value) {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

}
