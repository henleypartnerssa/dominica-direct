<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdentityDocument extends Model
{
    protected $table        = 'identity_documents';
    protected $primaryKey   = 'identity_document_id';
    protected $fillable     = [
        'profile_id',
        'user_id',
        'group_id',
        'application_id',
        'document_type',
        'document_number',
        'issuing_country',
        'place_of_issue',
        'date_of_issue',
        'date_of_expiration'
    ];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

}
