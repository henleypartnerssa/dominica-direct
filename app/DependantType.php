<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DependantType extends Model
{
    protected $table        = 'dependant_types';
    protected $primaryKey   = 'dependant_type_id';

    protected $fillable     = ['type'];

    public function family_members() {
        return $this->hasMany('App\FamilyMember');
    }
}
