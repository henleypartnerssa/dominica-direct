<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medical extends Model
{
    protected $table        = 'medicals';
    protected $primaryKey   = 'medical_id';
    protected $fillable     = [
        'application_id',
        'profile_id',
        'user_id',
        'group_id',
        'address_id',
        'doctor_name',
        'doc_tel',
        'doc_fax',
        'doc_email',
        'a_one',
        'a_two',
        'a_three',
        'a_four'
    ];

    public function profile() {
        return $this->hasOne('App\Profile');
    }

    public function address() {
        return $this->hasOne('App\Address', 'address_id');
    }

}
