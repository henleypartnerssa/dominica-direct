<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionalLicence extends Model
{
    protected $table        = 'professional_licences';
    protected $primaryKey   = 'pro_licence_id';
    protected $fillable     = [
        'business_id',
        'address_id',
        'profile_id',
        'user_id',
        'group_id',
        'application_id',
        'licence_type',
        'licence_no',
        'authority',
        'disiplinary'
    ];

    public function Work_or_business() {
        return $this->belongsTo('App\WorkOrBusiness');
    }

}
