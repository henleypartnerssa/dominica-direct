<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table        = 'bank_accounts';
    protected $primaryKey   = 'account_id';
    protected $fillable     = [
        'profile_id',
        'user_id',
        'group_id',
        'address_id',
        'country_id',
        'account_name_of',
        'bank_name',
        'account_no',
        'account_iban_bic'
    ];
    
    public function profile() {
        return $this->belongsTo('App\Profile');
    }

    public function address() {
        return $this->belongsTo('App\Address');
    }
    
}
