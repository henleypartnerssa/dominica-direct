<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CharacterReference extends Model
{
    protected $table        = 'character_references';
    protected $primaryKey   = 'character_ref_id';
    protected $fillable     = [
        'address_id',
        'profile_id',
        'group_id',
        'user_id',
        'full_name',
        'years_known',
        'occupation',
        'employer',
        'email',
        'landline_no',
        'mobile_no',
        'work_no'
    ];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    public function address()
    {
        return $this->belongsTo('App\Address');
    }

}
