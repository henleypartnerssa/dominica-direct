<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageLevel extends Model
{
    protected $table        = 'language_levels';
    protected $primaryKey   = 'language_level_id';
    protected $fillable = [
        'level'
    ];

    public function languages() {
        return $this->hasMany('App\Language');
    }

}
