<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $table        = 'countries';
    protected $primaryKey   = 'country_id';

    protected $fillable     = ['name', 'code', 'calling_codes'];

    public function family_members_citizenship() {
        $this->hasOne('App\FamilyMember', 'citizenship', 'country_id');
    }

    public function family_members_residence() {
        return $this->belongsTo('App\FamilyMember', 'country_of_residence', 'country_id');
    }

    public function passport() {
        return $this->hasMany('App\Passport');
    }

}
