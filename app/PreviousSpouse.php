<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreviousSpouse extends Model
{
    /**
     * Database table name
     * @var string
     */
    protected $table        = 'previous_spouses';

    /**
     * table primarhy Key
     * @var string
     */
    protected $primaryKey   = 'previous_spouse_id';

    /**
     * fields that can be edited.
     * @var array
     */
    protected $fillable     = [
        'profile_id',
        'user_id',
        'group_id',
        'application_id',
        'full_name',
        'place_of_birth',
        'date_of_birth',
        'nationality',
        'divorse_dt',
        'marriage_from_dt',
        'marriage_to_dt',
    ];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }
}
