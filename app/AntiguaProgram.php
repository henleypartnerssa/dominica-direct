<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntiguaProgram extends Model
{
    protected $table        = 'antigua_programs';
    protected $primaryKey   = 'antigua_program_id';
    protected $fillable     = [
        'application_id',
        'profile_id',
        'user_id',
        'group_id',
        'completing_form_as',
        'title_other',
        'gender',
        'marital_status',
        'data_of_marriage',
        'place_of_marriage',
        'legal_passport_first_name',
        'legal_passport_middle_names',
        'legal_passport_surname',
        'legal_bc_first_name',
        'legal_bc_middle_names',
        'legal_es_first_name',
        'legal_es_middle_names',
        'legal_es_surname',
        'legal_former_maiden_name',
        'legal_former_other_names',
        'legal_name_change_date',
        'legal_name_change_reason',
        'permanent_telephone_no',
        'permanent_mobile_no',
        'personal_email_address',
        'identity_card_no',
        'social_security_or_insurance_no',
        'social_security_or_insurance_country',
        'reason_for_citizenship'
    ];

    public function application() {
        return $this->belongsTo('App\Application');
    }

    public function Work_or_businesses() {
        return $this->belongsToMany('App\WorkOrBusiness', 'antigua_total_net_worths', 'antigua_total_net_worth_id')->withPivot('type', 'amount')->withTimestamps();
    }

    public function antigua_declarations() {
        return $this->hasMany('App\AntiguaDeclaration');
    }

    public function antigua_medical() {
        return $this->hasOne('App\AntiguaMedical');
    }
}
