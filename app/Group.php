<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    protected $table        = 'groups';
    protected $primaryKey   = 'group_id';
    protected $fillable     = ['roles'];


    /**
     * Group has many users.
     *
     * @return One to Many Relation
     */
    public function users() {

        return $this->hasMany('App\User');
    }

}
