<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Profile extends Model
{
    /**
     * Database table name
     * @var string
     */
    protected $table        = 'profiles';

    /**
     * table primarhy Key
     * @var string
     */
    protected $primaryKey   = 'profile_id';

    /**
     * fields that can be edited.
     * @var array
     */
    protected $fillable     =   [
                                    'group_id',
                                    'user_id',
                                    'application_id',
                                    'state',
                                    'belongs_to',
                                    'title',
                                    'gender',
                                    'first_name',
                                    'middle_names',
                                    'last_name',
                                    'native_name',
                                    'maiden_name',
                                    'mothers_maiden_name',
                                    'changed_name',
                                    'name_change_date',
                                    'name_change_reason',
                                    'date_of_birth',
                                    'place_of_birth',
                                    'country_of_birth',
                                    'citizenship_at_birth',
                                    'other_citizenship',
                                    'languages',
                                    'criminal_offences',
                                    'criminal_offence_reason',
                                    'marital_status',
                                    'date_of_marriage',
                                    'place_of_marriage',
                                    'place_divorce',
                                    'date_divorce',
                                    'personal_mobile_no',
                                    'personal_landline_no',
                                    'personal_fax_no',
                                    'personal_email_address',
                                    'eye_color',
                                    'hair_color',
                                    'height',
                                    'weight',
                                    'distinguising_marks',
                                ];

    /**
     * Fields that will not display.
     * @var array
     */
    protected $hidden       = [''];

    protected $dates        = ['name_change_date', 'date_of_birth', 'date_of_marriage', 'date_divorce'];

    public function setDateOfBirthAttribute($date) {
        $this->attributes['date_of_birth'] = Carbon::parse($date)->toDateString();
    }

    public function setNameChangeDateAttribute($date) {
        $this->attributes['name_change_date'] = Carbon::parse($date)->toDateString();
    }

    public function setDateOfMarriageAttribute($date) {
        $this->attributes['date_of_marriage'] = Carbon::parse($date)->toDateString();
    }

    public function setDateOfDivorceAttribute($date) {
        $this->attributes['date_divorce'] = Carbon::parse($date)->toDateString();
    }

    public function getDateOfBirthAttribute($value) {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getNameChangeDateAttribute($value) {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getDateOfMarriageAttribute($value) {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function getDateOfDivorceAttribute($value) {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * Belongs to a User.
     *
     * @return App\User
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Belongs to a Specific Program
     *
     * @return App\PreogramType
     */
     public function family_members() {
         return $this->belongsToMany('App\FamilyMember', 'family_profile')->withTimestamps();
     }

    /**
     * Has Many Cases
     *
     * @return App\Case
     */
    public function application() {
        return $this->belongsTo('App\Application');
    }

    public function identity_documents() {
        return $this->hasMany('App\IdentityDocument');
    }

    public function addresses() {
        return $this->belongsToMany('App\Address','address_profile')->withPivot('from_date', 'to_date')->withTimestamps();
    }

    public function qualifications() {
        return $this->hasMany('App\Qualification');
    }

    public function work_or_businesses() {
        return $this->hasMany('App\WorkOrBusiness');
    }

    public function bank() {
        return $this->hasOne('App\Bank');
    }

    public function languages() {
        return $this->hasMany('App\Language');
    }

    public function employment_references() {
        return $this->hasMany('App\EmploymentReference');
    }

    public function medical() {
        return $this->hasOne('App\Medical');
    }

    public function declarations() {
        return $this->hasMany('App\Declaration');
    }

    public function previous_spouses() {
        return $this->hasMany('App\PreviousSpouse');
    }

    public function liable_assets() {
        return $this->hasMany('App\LiableAsset');
    }

    public function military_information() {
        return $this->hasOne('App\MilitaryInformation');
    }

    public function character_references() {
        return $this->hasMany('App\CharacterReference');
    }

}
