<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalNetWorth extends Model
{
    protected $table        = 'total_net_worths';
    protected $primaryKey   = 'total_net_worth_id';
    protected $fillable     = ['application_id', 'business_id', 'address_id', 'profile_id', 'user_id', 'group_id', 'type', 'amount'];

    public function work_or_businesse() {
        return $this->belongsTo('App\WorkOrBusiness');
    }

}
