<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table        = 'addresses';
    protected $primaryKey   = 'address_id';
    protected $fillable     = ['application_id', 'country', 'address_type', 'street_one', 'street_two', 'postal_code', 'date_since'];

    public function profiles() {
        return $this->belongsToMany('App\Profile', 'address_profile')->withPivot('from_date', 'to_date')->withTimestamps();
    }

    public function family_members() {
        return $this->belongsToMany('App\FamilyMember', 'address_family_member')->withTimestamps();
    }

    public function country() {
        return $this->belongsTo('App\Country');
    }

    public function qualification() {
        return $this->hasOne('App\Qualification');
    }

    public function medical() {
        return $this->hasOne('App\Medical');
    }

    public function character_references() {
        return $this->hasMany('App\CharacterReference');
    }

}
