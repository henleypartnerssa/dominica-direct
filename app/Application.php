<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table        = 'applications';

    protected $primaryKey   = 'application_id';

    protected $fillable     = [
        'case_no',
        'comp_forms_as',
        'type',
        'step',
        'courier_no',
        'dd_check',
        'retainer_paid',
        'data_review',
        'balance_paid',
        'docs_received',
        'application_review',
        'sent_to_dominica',
        'submitted_to_gov',
        'ciu_approval',
        'investment_completed',
        'oath',
        'passport_issued',
        'account_balance'
    ];

    public function profiles() {
        return $this->hasMany('App\Profile');
    }

    public function antigua_program() {
        return $this->hasOne('App\AntiguaProgram');
    }

    public function steps() {
        return $this->belongsTo('App\Step');
    }

    public function messages() {
        return $this->hasMany('App\Message');
    }

}
