<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentReference extends Model
{
    protected $table = 'employment_references';
    protected $primaryKey = 'employment_reference_id';
    protected $fillable = [
        'profile_id',
        'application_id',
        'user_id',
        'group_id',
        'occupation',
        'employer',
        'type_of_business',
        'from_date',
        'to_date',
        'reason_for_leaving'
    ];

    public function profile() {
        return $this->belongsTo('App\Profile');
    }

}
