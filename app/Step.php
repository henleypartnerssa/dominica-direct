<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $table        = 'steps';
    protected $primaryKey   = 'step_id';

    protected $fillable     = ['title', 'description', 'step_no'];

    /**
     * Has Many Cases
     *
     * @return App\Case
     */
    public function applications() {
        return $this->hasMany('App\Application');
    }
}
