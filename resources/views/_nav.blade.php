
    <div class="ui middle aligned center aligned grid">
        <div class="row">
            <div class="column" style="border-top: 3px solid #A9E2F3;border-bottom: 1px solid #A9E2F3;padding-top: 0;padding-bottom: 1px ">
                <div class="ui compact secondary pointing menu">
                    <a class="item @yield('l1')" href="{{ url('/') }}">Services</a>
                    <a class="item @yield('l2')" href="{{ url('/programs') }}">Programs</a>
                    <a class="item @yield('l3')" href="{{ url('/faq') }}">FAQ</a>
                    <a class="item @yield('l4')" href="#">News</a>
                    <a class="item @yield('l5')" href="#">Contact</a>
                </div>
            </div>
        </div>
    </div>
