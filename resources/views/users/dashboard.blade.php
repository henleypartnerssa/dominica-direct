@extends('app_panel')

@section('title', 'Dashboard')

@section('content')
<h2 class="heading admin">Instructions</h2>

<p id="admin">This is a Welcome to Henley Direct Application Dashbaord. Form here you can create Cases for Citizenship and Residency Programs of your choice. This platform also allows you to submit messages to our supprt team to assist you during your Citizenship or Residency application. The progress of all your applications will be listed below.</p>

<h4 class="ui horizontal divider header">
    <i class="table icon"></i>
    Overview of Your Tasks
</h4>

<table class="ui celled structured small table">
    <thead>
        <tr>
            <th>Reference No</th>
            <th>Program Type</th>
            <th>Current Status</th>
            <th>Applicant</th>
            <th>Applicant Type</th>
            <th>Docuemnts</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td rowspan="{{$counter}}"><h5 class="ui center aligned header">{{ $data['ref'] }}</h5></td>
        <td rowspan="{{$counter}}"><h4 class="ui center aligned header">{{ $data['program'] }}</h4></td>
        <td rowspan="{{$counter}}" class="ui center aligned">
          {{ ($data['current_step'] / 16) * 100 }}% complete<br>
          @if($data['current_step'] === 0)
            <a href="{{$data['url']}}">Get Started</a>
          @else
            <a href="{{$data['url']}}">Go to Step {{ $data['current_step'] }}</a>
          @endif
        </td>
    </tr>
      @for($i = 0; $i < count($data['members']); $i++)
      <tr>
        <td>{{$data['members'][$i]['applicant']}}</td>
        <td>{{$data['members'][$i]['applicant_t']}}</td>
        <td><a href="/users/filesystem/list-documents/{{ $data['members'][$i]['profile_id'] }}|uploads">{{$data['members'][$i]['fname']}} Documents</a></td>
      </tr>
      @endfor
    </tbody>
</table>

<h4 class="ui horizontal divider header">
    <i class="info icon"></i>
    Helpful Information
</h4>

@include('modules._required_docs')

@stop
