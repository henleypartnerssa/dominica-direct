@extends('app_panel_no_grid')

@section('title', 'Verified Account')

@section('content')

        <h2 class="heading admin">Verification Mobile Number</h2>

        <p id="admin-center">
           Account Verification complete. Due to the Confidentiality of the the information you will
           be submitting for your application we would like to send you a one-time pin to your cell phone.
           Please fill in your mobile phone number below:
        </p>

        <div class="ui large mobile form">

            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

            <div class="inline fields">

                <div class="seven wide field">
                    <label>Mobile Number</label>
                    @include('modules._mobile_codes_dropdown', $countries)
                </div>

                <div class="seven wide field">
                    <input type="text" name="number" id="number" placeholder="Mobile Number...">
                </div>

                <div class="field">
                    <div id="sendButton" class="ui right blue fluid labeled large icon button">
                        submit
                        <i class="small angle right icon"></i>
                    </div>
                </div>

            </div>

        </div>

        <div class="ui mobile negative hidden message">
            <div class="header">Not Valid Mobile Number</div>
            <p>Please validate if your mobile number or country code are correct.</p>
        </div>

@stop

@section('script')
    <script src="{{ URL::asset('js/users_mobile.js') }}"></script>
@stop
