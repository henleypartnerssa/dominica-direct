@extends('app')

@section('title', 'Change Mobile Number')

@section('content')

    <div class="ui grid">
        <div class="row devider">
            <div class="eleven wide slider column">
                <h2 class="heading">Change Mobile Number</h2>
                <p>
                    To change your mobile number, please click on the verification link we have sent to your email account.
                </p>
                <p>Please note that we have locked your account in the mean time for security purposes.</p>
            </div>
            
            <div class="five wide column">
                <h2 class="ui HD-heading-background header">
                    Latest News
                </h2>
                <div class="news_content content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                </div>
            </div>

        </div>
    </div>
@stop

@section('script')
    <script src="{{ URL::asset('js/general.js') }}"></script>
@stop
