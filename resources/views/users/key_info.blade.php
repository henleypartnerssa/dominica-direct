@extends('app_panel')

@section('title', 'Key Information')

@section('content')

<h2 class="heading admin">Key Information</h2>

<p id="admin">Welcome to Newlandsglobal's platform for Dominca Citizenship by Investment Program. Please enter your key information and add all depdendants you wish to include in you application.</p>


<div class="ui raised segment">

    <div class="ui key_info large form-left form">

        <div class="two fields">

            <div class="required field">
                <label>Name</label>
                <input type="text" name="name" id="name" value="{{ $userInfo['name'] }}" />
            </div>

            <div class="required field">
                <label>Surname</label>
                <input type="text" name="surname" id="surname" value="{{ $userInfo['surname'] }}" />
            </div>

        </div>

        <div class="two fields">

            <div class="required field">
                <label>Date of Birth</label>

                <div class="three fields">
                    <div class="field">
                        <input type="number" name="year" id="year" placeholder="YYYY" min="1900" max="2015" value="{{(isset($userInfo['dob']->year)) ? $userInfo['dob']->year : ""}}" />
                    </div>
                    <div class="field">
                        <input type="number" name="month" id="month" placeholder="MM" min="01" max="12" value="{{(isset($userInfo['dob']->month)) ? $userInfo['dob']->month : ""}}" />
                    </div>
                    <div class="field">
                        <input type="number" name="day" id="day" placeholder="DD" min="01" max="31" value="{{(isset($userInfo['dob']->day)) ? $userInfo['dob']->day : ""}}" />
                    </div>
                </div>

            </div>

            <div class="required field">
                <label>Gender</label>

                <div class="ui fluid search selection dropdown">
                    <input type="hidden" name="gender" id="gender" value="{{isset($userInfo['gender']) ? $userInfo['gender'] : ""}}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Gender</div>
                        <div class="menu">
                            <div class="item" data-value="male"><i class="man icon"></i>Male</div>
                            <div class="item" data-value="female"><i class="woman icon"></i>Female</div>
                        </div>
                </div>

            </div>

        </div>

        <div class="field" id="dep">
            <label>Dependants</label>

            <div class="ui middle aligned selection list">
                @foreach($dependants as $dep)
                <div class="item {{$dep->family_member_id}}">
                    <div class="right floated content">
                        <div class="ui red delete button" id="{{$dep->family_member_id}}">Delete</div>
                    </div>
                    <i class="ui avatar image user icon"></i>
                    <div class="content">{{$dep->first_name}} {{$dep->last_name}} ({{$dep->relationship}})</div>
                </div>
                @endforeach
            </div>

            <a href="#" id="add_dep"><i class="add circle icon"></i> Click here to add Dependant</a>
        </div>

        <div class="field">
            <button class="ui fluid blue submit button" id="save_dep">Save & Exit</button>
        </div>

        <div class="field">
            <div class="ui negative message" style="display:none">
                <i class="close icon"></i>
                <div class="header">
                    Duplicate Entry
                </div>
                <p>
                    For this program you are not allowed to add more than one Spouse. If you an enquiry regarding this then please contact support.
                </p>
            </div>
        </div>

    </div>
</div>

@stop

@section('modal')
    @include('modals._key_info_add_dep')
@stop

@section('script')
    <script src="{{ URL::asset('js/users_keyinfo.js') }}"></script>
@stop
