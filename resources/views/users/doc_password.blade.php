@extends('app_panel_no_grid')

@section('title', 'Document Password')

@section('content')

    <h2 class="heading admin">Enter Docuemnts Password</h2>

    <p id="admin-center">
        Please note that this password will be used to encrypt all uploaded and generated documents. This password should contain a minimun of <b>8 characters</b>.
    </p>

    {!! Form::open(['id' => 'doc_password']) !!}
    <div class="ui doc_password large form">

        <div class="two fields">
            <div class="field">
                <input type="password" name="password" placeholder="type document password here...">
            </div>
            <div class="field">
                <input type="password" name="re_passsword" placeholder="Re-type document password here...">
            </div>
        </div>
        <div class="field">
            <button class="ui fluid inverted blue button">
                Save
            </button>
        </div>

    </div>
    {!! Form::close() !!}

@stop

@section('script')
    <script src="{{ URL::asset('js/users_doc_password.js') }}"></script>
@stop
