@extends('app')

@section('title', 'Account Locked')

@section('content')

    <div class="ui grid">
        <div class="row devider">
            <div class="eleven wide slider column">
                <h2 class="heading">Account Locked!</h2>
                <p>Your account has been locked. Please contact support: <b>support@henley-direct.com</b></p>
            </div>
            
            <div class="five wide column">
                <h2 class="ui HD-heading-background header">
                    Latest News
                </h2>
                <div class="news_content content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque et nisi in erat pharetra posuere. Suspendisse potenti.</p>
                </div>
            </div>
            
        </div>
    </div>
@stop

@section('script')
    <script src="{{ URL::asset('js/general.js') }}"></script>
@stop