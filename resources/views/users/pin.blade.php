@extends('app_panel_no_grid')

@section('title', 'Pin')

@section('content')

    <h2 class="heading admin">Enter Your Pin</h2>

    <p id="admin-center">
        We have sent a 4-digit OTP number to the following mobile number: <b>{{ Auth::user()->cell_no }}</b>
    </p>

    <div class="ui pin large form">

        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

        <div class="inline fields">
            <div class="four wide field">
                <input type="text" name="num1" id="num1" maxlength="1">
            </div>
            <div class="four wide field">
                <input type="text" name="num2" id="num2" maxlength="1">
            </div>

            <div class="four wide field">
                <input type="text" name="num3" id="num3" maxlength="1">
            </div>

            <div class="four wide field">
                <input type="text" name="num4" id="num4" maxlength="1">
            </div>
        </div>
        <div class="three fields">
            <div class="field">
                <button id="checkPin" class="ui fluid blue left labeled icon submit button">
                    Submit
                    <i class="angle right icon"></i>
                </button>
            </div>
            <div class="field">
                <button id="resendPin" class="ui fluid blue left labeled icon submit button">
                    Resend Pin
                    <i class="refresh icon"></i>
                </button>
            </div>
            <div class="field">
                <a class="ui left blue fluid labeled icon submit button" href="{{ url('users/mobile/change') }}">
                    Change Number
                    <i class="mobile icon"></i>
                </a>
            </div>
        </div>

        <div class="ui pin negative hidden message">
            <i class="close icon"></i>
            <div class="header">Invalid Pin</div>
            <p>You seem to have entered an invalid pin. Please try again or change the mobile number we are attempting to send the pin to.</p>
        </div>

        <div class="ui resend-pin negative hidden message">
            <i class="close icon"></i>
            <div class="header">Error Sending Pin</div>
            <p>Unfortunately your OTP could not be sent. Please change your mobile number or cantact support for assistance.</p>
        </div>

        <div class="ui resend-pin positive message hidden">
            <i class="close icon"></i>
            <div class="header">SMS Sent</div>
            <p>Message successfully sent to your inbox.</p>
        </div>

    </div>

@stop

@section('script')
    <script src="{{ URL::asset('js/users_pin.js') }}"></script>
@stop
