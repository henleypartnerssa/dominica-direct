@extends('app_panel')

@section('title', 'Verified Account')

@section('content')

        <h2 class="heading admin">Account Details</h2>

        <p id="admin-center">
           From the data form below you may Edit and Update your profile data. Note that the form listed below will only update the Principal Applicants data and not that of the Spouse of listed depedants.
        </p>

        <div class="ui large settings form" style="text-align:left;">

            {!! Form::open(['id'=>'account']) !!}

                <div class="two fields">
                    <div class="required field">
                        <label>First Name</label>
                        <input type="text" name="name" value="{{ $authUser->name }}">
                    </div>
                    <div class="required field">
                        <label>Last Name</label>
                        <input type="text" name="surname" value="{{ $authUser->surname }}">
                    </div>
                </div>

                <div class="two fields">
                    <div class="required field">
                        <label>Username</label>
                        <input type="text" name="username" value="{{ $authUser->username }}">
                    </div>
                    <div class="required field">
                        <label>Mobile Number</label>
                        <input type="text" name="cell_no" value="{{ $authUser->cell_no }}">
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <label>New Login Password</label>
                        <input type="password" name="password" placeholder="new password here...">
                    </div>
                    <div class="field">
                        <label>Retype Login Password</label>
                        <input type="password" name="retypePassword" placeholder="retype new password...">
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <label>New Document Password</label>
                        <input type="password" name="doc_password" placeholder="new document password here..." value="123456789">
                    </div>
                    <div class="field">
                        <label>Retype Document Password</label>
                        <input type="password" name="retype_doc_password" placeholder="retype new document password..." value="123456789">
                    </div>
                </div>

                <div class="two fields">
                    <div class="field">
                        <button class="ui fluid inverted blue submit button" id="save_dep">Update Data</button>
                    </div>
                    <div class="field">
                        <a class="ui fluid inverted red button" href="/users/dashboard">Exit</a>
                    </div>
                </div>

            {!! Form::close() !!}

            </div>

        </div>

        <div class="ui mobile negative hidden message">
            <div class="header">Not Valid Mobile Number</div>
            <p>Please validate if your mobile number or country code are correct.</p>
        </div>

@stop

@section('script')
    <script src="{{ URL::asset('js/users_account.js') }}"></script>
@stop
