<!DOCTYPE html>
<html>
    <head>
        <title>Henley Direct - @yield('title')</title>

        <link href="{{ URL::asset('semantic-ui/semantic.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ URL::asset('js/jquery-1.12.0.min.js') }}"></script>
        <script src="{{ URL::asset('semantic-ui/semantic.min.js') }}"></script>
        <script src="{{ URL::asset('js/api.js') }}"></script>
        <script src="{{ URL::asset('js/panel_general.js') }}"></script>

    </head>
    <body>

        @include('_panel_header', $u = (!empty($user)) ? $user : Auth::user())

        <div class="breaker">

        </div>
        <div class="ui container">
            <div class="ui vertical stripe segment">

                <div class="ui middle aligned center grid">
                    <div class="row">
                        <div class="column">
                            <div class="ui center aligned segment">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="ui coupled modal">
            @include('modals._aquire_quote')
            @yield('modal');
        </div>

        @yield('script')

    </body>

</html>
