@if(Auth::check())
    @include('_panel_top_nav')
@else
    @include('_top_nav')
@endif

<div class="">
    <div class="row" style="padding: 4em 0;background:#f6f6f6">
        <div class="column">
            <div class="ui middle aligned center aligned grid">
                <a class="row" href="/users/dashboard" style="color:#000">
                    <div class="two wide column" style="margin:0;padding:0;text-align:right">
                        <img src="{{asset('img/dominica-flg.jpg')}}">
                    </div>
                    <div class="five wide column">
                        <h1 class="heading text-left">Newlands Dominica Direct</h1>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top:0;">
        <div class="column" style="background:#f6f6f6">
            @include('_nav_auth')
        </div>
    </div>
</div>
