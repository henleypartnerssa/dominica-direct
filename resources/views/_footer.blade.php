<div class="ui footer menu">

    <div class="ui container">

        <div class="item">
            Copyright 2016 Newlands Global Citizenship Ltd
        </div>

        <div class="item">
            Legal
        </div>

        <div class="item">
            Privacy
        </div>

        <div class="item">
            Conditions of Use
        </div>

        <div class="right menu">

            <div class="item topnav-right">
                <img src="img/imc.gif" />
            </div>

        </div>
    </div>

</div>
