<?php use App\Util\AppHelpers; ?>

@extends('app_panel')

@section('title', 'Quotations')

@section('content')

<h2 class="heading admin">Your Quotations</h2>
<p id="admin">Please see below for a record of your quotations. You can click on the 'Download Quote' tab to view and print a quote.</p>
<div class="ui middle aligned selection list">

    @foreach($quotes as $quote)
    <div class="item" style="text-align: left">

            <div class="right floated content">
                <a class="ui blue button" href="{{ $quote['link'] }}" download>Download Quote</a>
            </div>

            <i class="ui avatar image {{ $quote['code'] }} flag"></i>
            <div class="content">
                <div class="header">{{ $quote['country'] }}</div>
                <div class="description"><small><b>created on:</b>  {{ $quote['date'] }}</small></div>
            </div>

        </div>
    @endforeach

</div>

@stop
