@if(Auth::check())
    @include('_panel_top_nav')
@else
    @include('_top_nav')
@endif

<div class="ui heading middle aligned center aligned grid">
    <div class="row" style="padding: 4em 0;background:#f6f6f6">
        <div class="column">
            <h1 class="heading">Welcome to Newlands Dominica Direct</h1>
        </div>
    </div>
    <div class="row" style="border-top:1px solid #000;border-bottom:1px solid #000;background:#f6f6f6">
        <div class="column">
            <p>The online residence and citizenship program processing platform</p>
        </div>
    </div>
</div>
