@extends('app_panel')

@section('title', 'Dashboard')

@section('content')
<h2 class="heading admin">Messages</h2>

<p id="admin">Listed below are all your inbox messages.</p>

<h4 class="ui horizontal divider header">
    Your Inbox Messages
</h4>

<div class="ui middle aligned selection list">

    @foreach($listMessages as $message)
    <a class="item" style="text-align: left" id="{{$message['id']}}" href="/users/messages/{{$message['id']}}">
        <div class="right floated content">
            <small style="float:right"><b>{{$message['timestamp']}}</b></small>
        </div>
        @if($message['read'] === false)
            <i class="ui avatar image mail icon"></i>
        @else
            <i class="ui avatar image mail outline icon"></i>
        @endif
        <div class="content">
            <div class="header">From: {{$message['from']}}</div>
            <div class="description">Subject: {{$message['subject']}}</div>
        </div>
    </a>
    @endforeach
</div>

@stop
