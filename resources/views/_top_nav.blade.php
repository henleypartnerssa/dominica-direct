<div class="ui menu newlands-bg b-none s-none" style="height:50px">

    <div class="ui container">

        <div class="item topnav-left">
            <img src="img/newlands_logo_brown.png" />
            <span class="tel"><i class="big inverted phone icon"></i>+44 0 333 44-444 55</span>
        </div>

        <div class="right menu">

            <a class="item home-link" href="http://www.newlandsglobal.com">
                RETURN TO WEBSITE >
            </a>

        </div>
    </div>

</div>
