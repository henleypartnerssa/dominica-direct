@extends('app_panel_no_right')

@section('title', 'Courier Package')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Courier Application</h2>

<div class="content">
    <p id="admin">
        Please courier the following original signed documents for all Applicants to Henley & Partners, 9 Henley House, Hope St, Jersey JE2 3NS, Jersey and insert the Courier Number in the block provided and then click Submit:
        <br>
        <strong>Form D1 - Disclosure:</strong> Please Sign, date and insert Place of signature
        <br>
        <strong>Form D2 - Fingerprint & Signature:</strong> Original certified by Lawyer/ Notary
        <br>
        <strong>Form D3 - Medical Questionnaire:</strong> Completed and signed by Medical Practitioner For all applicants over 12 years
        <br>
        <strong>Form D4 - Investment Agreement:</strong> Complete and sign one copy.
        <br>
        <strong>Form 12:</strong> Complete and sign one copy.
    </p>

    <div class="ui courier form">

        <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />

        <div class="inline fields">
            <div class="field">
                <label>Courier Type:</label>
            </div>
            <div class="field">
                <div class="ui selection dropdown">
                    <input type="hidden" name="type" id="type" value="{{$courier_type}}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Select Courier Type</div>
                    <div class="menu">
                        @foreach($listOfCouriers as $item)
                            <div class="item" data-value="{{$item}}">{{$item}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Courier Number:</label>
            </div>
            <div class="field">
                <input type="text" name="courier_no" id="courier_no" placeholder="insert number here..." value="{{{$courier_no or ''}}}">
            </div>
            <div class="field">
                <button class="ui submit grey button">Submit</button>
            </div>

        </div>
        <div class="field">
            <div class="ui success message" style="display:none">
                <div class="header">Form Submitted</div>
                <p>Courier reference number saved.</p>
            </div>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Notary Uploads
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/ab_courier.js') }}"></script>
@stop
