@extends('app_panel_no_right')

@section('title', 'Declarations')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Declarations - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">Please note that if you answer “yes” to any of the questions from, you must provide a detailed explanation.</p>

    @if($user->profiles()->count() > 4)
        @include('modules._data_forms_guide_split', $data = ['page' => 'decl'])
    @else
        @include('modules._data_forms_guide', $data = ['page' => 'decl'])
    @endif

    <div class="ui antigua-declarations form">
    {!! Form::open() !!}
        <input type="hidden" id="declarations" value="{{{$declJSON or ''}}}" />
        <input type="hidden" id="profile_id" value="{{ $profile->profile_id }}" />

        <div class="inline fields">
            <div class="field">
                <div class="ui d77_no radio checkbox">
                    <input type="radio" name="d77_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d77_yes radio checkbox">
                    <input type="radio" name="d77_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been arrested, detained, charged, indicted, convicted, found guilty or been expunged of any offence(s) against the law in any country (Except Minor traffic citations.)?</label>
        </div>
        <div class="field d77 hide">
            <label>Please provide reason</label>
            <textarea rows='3' rows='3' name="d77_message">{{{$decl['d77_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d78_no radio checkbox">
                    <input type="radio" name="d78_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d78_yes radio checkbox">
                    <input type="radio" name="d78_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever testified before a grand jury or investigative hearing or probe?</label>
        </div>
        <div class="field d78 hide">
            <label>Please provide reason</label>
            <textarea rows='3' rows='3' name="d78_message">{{{$decl['d78_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d79_no radio checkbox">
                    <input type="radio" name="d79_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d79_yes radio checkbox">
                    <input type="radio" name="d79_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have any charges, or accusations of illegal activity of any nature been made against you in any country?</label>
        </div>
        <div class="field d79 hide">
            <label>Please provide reason</label>
            <textarea rows='3' rows='3' name="d78_message">{{{$decl['d79_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d80_no radio checkbox">
                    <input type="radio" name="d80_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d80_yes radio checkbox">
                    <input type="radio" name="d80_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been sentenced to serve a period of time in detention or been in probation?</label>
        </div>
        <div class="field d80 hide">
            <label>Please provide reason</label>
            <textarea rows='3' rows='3' name="d80_message">{{{$decl['d80_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d81_no radio checkbox">
                    <input type="radio" name="d81_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d81_yes radio checkbox">
                    <input type="radio" name="d81_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever received a pardon for any criminal offense? (If yes, note Date, City, County, State and Country.)</label>
        </div>
        <div class="field d81 hide">
            <label>Please provide reason</label>
            <textarea rows='3' rows='3' name="d81_message">{{{$decl['d81_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d82_no radio checkbox">
                    <input type="radio" name="d82_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d82_yes radio checkbox">
                    <input type="radio" name="d82_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever had a civil or criminal record expunged or sealed by a court order? Yes /No If yes, give details.</label>
        </div>
        <div class="field d82 hide">
            <label>Please provide reason</label>
            <textarea rows='3' rows='3' name="d82_message">{{{$decl['d82_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d83_no radio checkbox">
                    <input type="radio" name="d83_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d83_yes radio checkbox">
                    <input type="radio" name="d83_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been subpoenaed to appear to testify before a federal, state, or county grand jury, board or commission?</label>
        </div>
        <div class="field d83 hide">
            <label>Please provide reason</label>
            <textarea rows='3' rows='3' name="d83_message">{{{$decl['d83_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d84_no radio checkbox">
                    <input type="radio" name="d84_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d84_yes radio checkbox">
                    <input type="radio" name="d84_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Has a criminal indictment, information, or complaint ever been returned against you, but for which you were not arrested or in which you were named as an un-indicted co-party?</label>
        </div>
        <div class="field d84 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d84_message">{{{$decl['d84_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d85_no radio checkbox">
                    <input type="radio" name="d85_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d85_yes radio checkbox">
                    <input type="radio" name="d85_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you, as an individual, or as an owner, partner, director or officer of any partnership, corporation or other entity, ever been a party to a lawsuit as either a plaintiff or defendant? (Other than divorces).</label>
        </div>
        <div class="field d85 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d85_message">{{{$decl['d85_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d86_no radio checkbox">
                    <input type="radio" name="d86_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d86_yes radio checkbox">
                    <input type="radio" name="d86_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been involved, directly or indirectly, in the financing of terrorism or in any terrorist or criminal organization?</label>
        </div>
        <div class="field d86 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d86_message">{{{$decl['d86_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d87_no radio checkbox">
                    <input type="radio" name="d87_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d87_yes radio checkbox">
                    <input type="radio" name="d87_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been under investigation by any law enforcement agency or tax authority in any country?</label>
        </div>
        <div class="field d87 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d87_message">{{{$decl['d87_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d88_no radio checkbox">
                    <input type="radio" name="d88_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d88_yes radio checkbox">
                    <input type="radio" name="d88_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been involved personally, or as a director in any bankruptcy, insolvency or liquidation?</label>
        </div>
        <div class="field d88 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d88_message">{{{$decl['d88_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d89_no radio checkbox">
                    <input type="radio" name="d89_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d89_yes radio checkbox">
                    <input type="radio" name="d89_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been refused an entry visa to, or residency permit in any country, been unlawfully present in, been deported from any country, or sought to assist others to do the same?</label>
        </div>
        <div class="field d89 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d89_message">{{{$decl['d89_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d90_no radio checkbox">
                    <input type="radio" name="d90_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d90_yes radio checkbox">
                    <input type="radio" name="d90_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever had a visa cancelled?</label>
        </div>
        <div class="field d90 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d90_message">{{{$decl['d90_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d91_no radio checkbox">
                    <input type="radio" name="d91_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d91_yes radio checkbox">
                    <input type="radio" name="d91_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever applied for citizenship in any country and citizenship has not been granted?</label>
        </div>
        <div class="field d91 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d91_message">{{{$decl['d91_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d92_no radio checkbox">
                    <input type="radio" name="d92_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d92_yes radio checkbox">
                    <input type="radio" name="d92_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been the subject of any order, judgment or decree of any federal or state authority barring, suspending, or otherwise limiting your right to engage in any professional or business practice or activity?</label>
        </div>
        <div class="field d92 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d92_message">{{{$decl['d92_message'] or ''}}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui d93_no radio checkbox">
                    <input type="radio" name="d93_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui d93_yes radio checkbox">
                    <input type="radio" name="d93_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been a senior politician, head of state or government, official of a political party, senior judicial or military official, and/or senior executive of state-owned enterprise?</label>
        </div>
        <div class="field d93 hide">
            <label>Please provide reason</label>
            <textarea rows='3' name="d93_message">{{{$decl['d92_message'] or ''}}}</textarea>
        </div>

        <div class="field" id="charater_references">
            <label>Please provide the details of two Charater References who have known you for five (5) years or more. Do not include relatives, present employers or employees;</label>
            <div class="ui middle aligned charator_references selection list">
                @if($charRef)
                    @foreach($charRef as $char)

                    <div class="item {{$char->character_ref_id}}" data-id="{{$char->character_ref_id}}">
                        <div class="right floated content">
                            <div class="ui blue inverted charater_references edit button" id="{{$char->character_ref_id}}">Edit</div>
                            <div class="ui red inverted charater_references delete button" id="{{$char->character_ref_id}}">Delete</div>
                        </div>
                        <i class="ui avatar image user icon"></i>
                        <div class="content">{{$char->full_name}}</div>
                    </div>

                    @endforeach
                @endif
            </div>

            <a href="#" id="add_charater_reference_link"><i class="add circle icon"></i> Click here to add Character Reference</a>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Business Details
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <button class="ui next fluid blue right labeled icon button">
                    Finish
                    <i class="large angle right icon"></i>
                </button>
            </div>
        </div>
    {!! Form::close() !!}
    </div>

</div>

@stop

@section('modal')
    @include('modals._add_character_reference')
@stop

@section('script')
    <script src="{{ URL::asset('js/antigua_declarations.js') }}"></script>
@stop
