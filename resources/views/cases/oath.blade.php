@extends('app_panel_no_right')

@section('title', 'Investment Status')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Oath Of Allegiance</h2>

<div class="content">
    <p id="admin">
        You are require to take an Oath of Allegiance in the presence of an authorised notary. Please download the the form provided by the CIU buy clicking the download button below. Note that this form should be compelete by the authorised notary during the swearing of the oath.
    </p>

    <div class="ui submission form">

        <div class="field">
            <div class="ui inverted fluid green button">
                Download Oath Form
            </div>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Investment Status
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
