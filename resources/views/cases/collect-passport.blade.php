@extends('app_panel_no_right')

@section('title', 'Collect New Passport')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Passport Collection</h2>

<div class="content">
    <p id="admin">Your passport/s have been issued and sent to you by courier.  The Tracking Number is:  xxxxxxxxx</p>

    <div class="ui submission form">

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Personal Data
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
