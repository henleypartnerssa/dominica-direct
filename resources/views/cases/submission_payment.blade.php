@extends('app_panel_no_right')

@section('title', 'Payment for Submission')

@section('stepper')
    @include('modules._stepping',$data = App\Util\AppHelpers::stepperLinks($profile->profile_id, $profile->application->application_id))
@stop

@section('content')

<h2 class="heading admin">{{ $programName }} - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">The 10% Government Processing and Government Due Diligence Fees are now due for payment.  Please download the invoice to remit the payment as per the payment details captured on the invoice.</p>

    <div class="ui submission form">

        <div class="field">
            <a href="/cases/pdf/download/invoice-2" class="ui next fluid grey left labeled icon button">
                Download Invoice
                <i class="large download icon"></i>
            </a>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Courier
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
