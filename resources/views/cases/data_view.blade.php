@extends('app_panel_no_right')

@section('title', 'Data View')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Data Review Process</h2>

<div class="content">
    <p id="admin">We are currently conducting a review on all the data submitted. This process might take a few days, upon completion we will notify you via sms and email.</p>

    <div class="ui upload-documents form">

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Uploaded Documents
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
