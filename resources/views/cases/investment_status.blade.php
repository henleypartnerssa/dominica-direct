@extends('app_panel_no_right')

@section('title', 'Investment Status')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Investment Status</h2>

<div class="content">
    <p id="admin">
        We are now awating information and documentation regarding your Government Investment status. We will notify you via sms and email once we have recieved confirmation regarding your Government Investment.
    </p>

    <div class="ui submission form">

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Courier
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
