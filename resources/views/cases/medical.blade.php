@extends('app_panel_no_right')

@section('title', 'Medical Details')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Medical Details - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">
        This Medical Questionnaire is to be completed in English by a Registered Medical Practitioner.
        Any additional information can be submitted on a separate sheet of paper. The Medical Practitioner must ask for
        evidence of identification (such as a passport).
    </p>

    @if($user->profiles()->count() > 4)
        @include('modules._data_forms_guide_split', $data = ['page' => 'medical'])
    @else
        @include('modules._data_forms_guide', $data = ['page' => 'medical'])
    @endif

    <p>
        The Medical Examiner is requested to ask the following questions or to review them if they have been answered
        previously. Give details and dates if any of the questions below are answered with “Yes”
    </p>

    <input type="hidden" id="profile_id" value="{{ $profile->profile_id }}" />
    <input type="hidden" id="a1" value="{{($medical and $medical->a_one !== 'no') ? '1' : '0'}}" />
    <input type="hidden" id="a2" value="{{($medical and $medical->a_two !== 'no') ? '1' : '0'}}" />
    <input type="hidden" id="a3" value="{{($medical and $medical->a_three !== 'no') ? '1' : '0'}}" />
    <input type="hidden" id="a4_select" value="{{{$four_select or '0'}}}" />

    <div class="ui medical form">
    {!! Form::open(['id' => 'medical-form']) !!}
        <input type="hidden" name="save" id="save" value="0" />

        <div class="required field">
            <label>Full Name of Medical Examiner</label>
            <input type="text" name="doctor_name" placeholder="Name of you Physician or Doctor" value="{{{$medical->doctor_name or ''}}}">
        </div>

        <div class="required field">
            <label>Organisation Address</label>

            <div class="two fields">
                <div class="field">
                    <input type="text" name="doctor_street1" placeholder="address line 1" value="{{{$address->street1 or ''}}}">
                </div>
                <div class="field">
                    <input type="text" name="doctor_street2" placeholder="address line 2 (optional)" value="{{{$address->street2 or ''}}}">
                </div>
            </div>

            <div class="three fields">
                <div class="field">
                    @if(!empty($address))
                        @include('modules._countries_dropdown', $data = ['name' => 'doctor_country', 'title' => 'country', 'value' => $address->country])
                    @else
                        @include('modules._countries_dropdown', $data = ['name' => 'doctor_country', 'title' => 'country'])
                    @endif
                </div>
                <div class="field">
                    <input type="text" name="doctor_town" placeholder="town/city" value="{{{$address->town or ''}}}">
                </div>
                <div class="field">
                    <input type="text" name="doctor_post_code" placeholder="postal code" value="{{{$address->postal_code or ''}}}">
                </div>
            </div>
        </div>

        <div class="three fields">
            <div class="required field">
                <label>Telephone No.</label>
                <input type="text" name="tel" value="{{{$medical->doc_tel or ''}}}" />
            </div>
            <div class="field">
                <label>Fax No.</label>
                <input type="text" name="fax" value="{{{$medical->doc_fax or ''}}}" />
            </div>
            <div class="required field">
                <label>Email No.</label>
                <input type="email" name="email" value="{{{$medical->doc_email or ''}}}" />
            </div>
        </div>

        <p>
            The Medical Examiner is requested to ask the following questions or to review them if they have been answered
            previously. Give details and dates if any of the questions below are answered with “Yes”.
        </p>

        <div class="inline fields">
            <div class="field">
                <div class="ui a1-no radio checkbox">
                    <input type="radio" name="a1-no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui a1-yes radio checkbox">
                    <input type="radio" name="a1-yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Do you currently have any health problems?</label>
        </div>

        <div class="field" id="a1-message" style="display:none">
            <label>Please specify why</label>
            <textarea name="a1-message" id="a1-message-area" rows="3" placeholder="type message why here...">{{($medical and $medical->a_one !== 'no') ? trim(explode('|', $medical->a_one)[1]) : ''}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui a2-no radio checkbox">
                    <input type="radio" name="a2-no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui a2-yes radio checkbox">
                    <input type="radio" name="a2-yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever been hospitalised?</label>
        </div>

        <div class="field" id="a2-message" style="display:none">
            <label>Please specify why</label>
            <textarea name="a2-message" id="a2-message-area" rows="3" placeholder="type message why here...">{{($medical and $medical->a_two !== 'no') ? trim(explode('|', $medical->a_two)[1]) : ''}}</textarea>
        </div>

        <div class="inline fields">
            <div class="field">
                <div class="ui a3-no radio checkbox">
                    <input type="radio" name="a3-no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui a3-yes radio checkbox">
                    <input type="radio" name="a3-yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you visited a doctor in the last three (3) years?</label>
        </div>

        <div class="field" id="a3-message" style="display:none">
            <label>Please specify why</label>
            <textarea name="a3-message" id="a3-message-area" rows="3" placeholder="type message why here...">{{($medical and $medical->a_three !== 'no') ? trim(explode('|', $medical->a_three)[1]) : ''}}</textarea>
        </div>

        <div class="grouped fields">
            <label>Do you suffer from or have you ever suffered from any of the following. (Confirm by checking the checkbox)</label>
            <div class="field">
                <div class="ui 4a checkbox">
                    <input type="checkbox" name="4a">
                    <label>Tuberculosis</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4b checkbox">
                    <input type="checkbox" name="4b">
                    <label>Leprosy</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4c checkbox">
                    <input type="checkbox" name="4c">
                    <label>Hepititis (specify type)</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4d checkbox">
                    <input type="checkbox" name="4d">
                    <label>Typhoid, dysentery orany other infectious orcommunicable diseases</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4e checkbox">
                    <input type="checkbox" name="4e">
                    <label>AIDS or AIDS related conditions,any Immune Deficiency Syndrome</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4f checkbox">
                    <input type="checkbox" name="4f">
                    <label>Genetic or Familial Disorders</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4g checkbox">
                    <input type="checkbox" name="4g">
                    <label>Deafness or Chronic Ear Disease</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4h checkbox">
                    <input type="checkbox" name="4h">
                    <label>Blindness or Eye Disease</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4i checkbox">
                    <input type="checkbox" name="4i">
                    <label>Any cancerous disease: benign /malignant</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4j checkbox">
                    <input type="checkbox" name="4j">
                    <label>Headache, migraine, epilepsy or dizziness</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4k checkbox">
                    <input type="checkbox" name="4k">
                    <label>Nervous or mental illness ordisorders</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4l checkbox">
                    <input type="checkbox" name="4l">
                    <label>Any allergies, asthma or pulmonary disease</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4m checkbox">
                    <input type="checkbox" name="4m">
                    <label>Cardiovascular diseases, arterial hypertension</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4n checkbox">
                    <input type="checkbox" name="4n">
                    <label>Liver, stomach or intestinal diseases</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4o checkbox">
                    <input type="checkbox" name="4o">
                    <label>Typhoid, dysentery or any other infectious or communicable diseases</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4p checkbox">
                    <input type="checkbox" name="4p">
                    <label>Urinary tract disease</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4q checkbox">
                    <input type="checkbox" name="4q">
                    <label>Venereal diseases</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4r checkbox">
                    <input type="checkbox" name="4r">
                    <label>Rheumatism, Muscle, Joint or bone diseases</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4s checkbox">
                    <input type="checkbox" name="4s">
                    <label>Skin diseases</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4t checkbox">
                    <input type="checkbox" name="4t">
                    <label>Cosmetic operations</label>
                </div>
            </div>
            <div class="field">
                <div class="ui 4u checkbox">
                    <input type="checkbox" name="4u">
                    <label>Any other illness or disorder</label>
                </div>
            </div>
        </div>

        <div class="field" id="a4-message">
            <label>If “Yes” to any of the above, please give details and dates.</label>
            <textarea name="4-message" rows="8" placeholder="type message why here...">{{(!empty($medical->a_four)) ? trim(explode('|', $medical->a_four)[1]) : ''}}</textarea>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui back fluid blue left labeled icon button">
                    Back to Personal Data
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
                <button class="ui save-data fluid grey left labeled icon button">
                    Save Data
                    <i class="upload icon"></i>
                </button>
            </div>
            <div class="field">
                <button class="ui next fluid blue right labeled icon button">
                    Next Form
                    <i class="large angle right icon"></i>
                </button>
            </div>
        </div>

    {!! Form::close() !!}
    </div>

</div>

@stop

@section('modal')
@stop

@section('script')
    <script src="{{ URL::asset('js/antigua_medical.js') }}"></script>
@stop
