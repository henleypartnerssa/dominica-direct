@extends('app_panel_no_right')

@section('title', 'Input Data')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Personal Data - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">Complete one form for each person on the Application.  This includes the Main Applicant plus any dependants (e.g. spouses and children).In the case of sections which are not applicable, fill in "N/A", e.g. employment information for minor children.  Please note therefore that each question should be answered.Each form should be completed personally, except in the case of children who cannot yet write.  For children who are below the age of 18, both parents must sign the form on behalf of the child as their legal guardians.  If one parent has sole custody over the child, or other persons are the legal guardians, then appropriate documentation must be provided</p>

    @if($user->profiles()->count() > 4)
        @include('modules._data_forms_guide_split', $data = ['page' => 'personal'])
    @else
        @include('modules._data_forms_guide', $data = ['page' => 'personal'])
    @endif

    <input type="hidden" id="profile_id" value="{{ $profile->profile_id }}" />
    <input type="hidden" id="res_counter" value="{{$profile->addresses()->where('address_type', 'res')->count()}}" />
    <input type="hidden" id="military" value="{{($milInfo) ? '1' : '0'}}" />
    <input type="hidden" id="offence" value="{{(!empty($milInfo) and $milInfo->offence) ? '1' : '0'}}" />

    <div class="ui personal-data form">
    {!! Form::open() !!}

        <input type="hidden" name="save" id="save" value="0" />

        @include('cases.input_personal_details')

        <div class="three fields">
            <div class="field">
                <button class="ui save-data fluid grey left labeled icon button">
                    Save Data
                    <i class="upload icon"></i>
                </button>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <button class="ui next fluid blue right labeled icon button">
                    Medical Details
                    <i class="large angle right icon"></i>
                </button>
            </div>
        </div>
    {!! Form::close() !!}
    </div>

</div>

@stop

@section('modal')
    @include('modals._add_passport')
    @include('modals._add_address')
    @include('modals._add_qualification')
@stop

@section('script')
    <script src="{{ URL::asset('js/cases_personal_data.js') }}"></script>
    <script src="{{ URL::asset('js/cases_personal_data_addresses.js') }}"></script>
    <script src="{{ URL::asset('js/cases_personal_data_qualification.js') }}"></script>
    <script src="{{ URL::asset('js/cases_antigua_passport.js') }}"></script>
@stop
