@extends('app_panel_no_right')

@section('title', 'Select Program')

@section('stepper')
    @include('modules._stepping', $data = [])
@stop

@section('content')

<h2 class="heading admin">Select your Program</h2>

<div class="content">
    <p id="admin">Please select the Program you would like to apply for. Note that you are not allowed to create more than one Application per Applicant Type.</p>
</div>

<div class="ui stack segment">

    <div class="ui form">
        {!! Form::open(['id' => 'select-form']) !!}

            <div class="field">
                @include('modules._programs_dropdown')
            </div>

            <div class="required field">
                <label>
                <i class="inline info circle icon" data-title="Please Note" data-content="All and any statements and/or declarations made in this application by anyone completing this application on behalf of the Principal Applicant shall be deemed to be the statements and/or declarations made by the Principal Applicant himself or herself."></i>
                  Are you completing this application as
                </label>
                <div class="ui selection dropdown">
                    <input type="hidden" name="PA">
                    <i class="dropdown icon"></i>
                    <div class="default text">Select Principal Applicant (PA)</div>
                    <div class="menu">
                      <div class="item" data-value="Principal Applicant (PA)">Principal Applicant (PA)</div>
                      <div class="item" data-value="Spouse of PA">Spouse of PA</div>
                      <div class="item" data-value="Dependant of PA">Dependant of PA</div>
                      <div class="item" data-value="Authorised agent of the PA">Authorised agent of the PA</div>
                      <div class="item" data-value="Benefactor of the PA">Benefactor of the PA</div>
                    </div>
                </div>
            </div>

            <div class="field">
                <button class="ui logon fluid blue btn-color submit button">Select</button>
            </div>

            <div class="field">
                <div class="ui negative message" style="display:none">
                  <div class="header">
                    Duplicate Selection
                  </div>
                  <p>Unfortunately you are not allowed to submit the same program twice.</p>
                </div>
            </div>

        {!! Form::close() !!}

    </div>

</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/cases_select_program.js') }}"></script>
@stop
