@extends('app_panel_no_right')

@section('title', 'Issuing of Governemnt Documents')

@section('stepper')
    @include('modules._stepping',$data = App\Util\AppHelpers::stepperLinks($profile->profile_id, $profile->application->application_id))
@stop

@section('content')

<h2 class="heading admin">{{ $programName }} - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">We are now awaiting approval from the Prime Minister regarding your Certificate/s of Citizenship, which will accompany the passport application form/s for the issuing of your passport. Please note that this process could take 6 to 8 weeks and we will notify you via sms or email.</p>

    <div class="ui submission form">

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Wired Payment
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
