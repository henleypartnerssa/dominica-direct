@extends('app_panel_no_right')

@section('title', 'Online Payment')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Request For Payment</h2>

<div class="content">
    <p id="admin">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque egestas interdum lorem eget molestie. Suspendisse non euismod enim. Proin gravida venenatis efficitur. Morbi elementum erat ac nibh facilisis aliquam. Maecenas vitae pretium nulla, viverra molestie turpis. In eu ex ut felis gravida dignissim sed id lacus. Phasellus vehicula diam vulputate, aliquet sem id, molestie diam. Etiam id mi non arcu rhoncus porttitor. Nunc nec faucibus elit, pharetra accumsan lacus. Etiam molestie velit justo, a lacinia lectus commodo eu. Proin consectetur ornare est eu volutpat. Aliquam nibh ipsum, sollicitudin ac ullamcorper eu, efficitur quis ipsum. Maecenas fermentum tellus urna, vitae feugiat sapien pharetra a. Mauris eu enim id neque facilisis faucibus nec eget est.
    </p>

    <div class="ui online-payment form">
        <div class="three fields">
            <div class="field">
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>
</div>

@stop
