@extends('app_panel_no_right')

@section('title', 'Family Members')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Family Members - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">Give details of all family members, whether applying for citizenship with you or not, including those adopted legally. If any family member is deceased, only give their names and write “deceased” in the field “Residential Address”. If you are applying for citizenship with your spouse and any of your children, please note that in addition to submitting their details on this form, a separate form must be completed for each person who applies with you. Please note that generally only children under the age of 18 years who are the applicant’s biological children, legally adopted children or children of a previous marriage may be included in your application for citizenship. Children under the age of 25 years may be included if they are dependent, unmarried and in full time education; however, in that case separate Government fees apply.</p>

    @if($user->profiles()->count() > 4)
        @include('modules._data_forms_guide_split', $data = ['page' => 'family'])
    @else
        @include('modules._data_forms_guide', $data = ['page' => 'family'])
    @endif

    <input type="hidden" id="profile_id" value="{{ $profile->profile_id }}" />

    <div class="ui family-member form">
    {!! Form::open(['id' => 'family-member-form']) !!}

    <input type="hidden" name="f_included" id="f_included" value="{{{$f_included or ''}}}">
    <input type="hidden" name="m_included" id="m_included" value="{{{$m_included or ''}}}">
    <input type="hidden" name="save" id="save" value="0">

        <div class="field">
            <div class="father_holder">
                <div class="required field">
                    <label>Details of your Father</label>

                    <div class="two fields">

                        <div class="field">
                            <input type="text" name="father_fam_name" placeholder="Surname/family name" value="{{{ $father['father']->last_name or ''}}}">
                        </div>
                        <div class="field">
                            <input type="text" name="father_first_name" placeholder="First/Given names" value="{{{ $father['father']->first_name or ''}}}">
                        </div>

                    </div>

                    <div class="two fields">

                        <div class="required field">
                            <label>Date of birth</label>
                            @if(isset($father['father']))
                                @include('modules._inline_datebox', $data = ['father_dob', $father['father']->date_of_birth])
                             @else
                                @include('modules._inline_datebox', $data = ['father_dob'])
                             @endif
                        </div>

                        <div class="field">
                            <label>Deseased ?</label>
                            <div class="ui selection dropdown">
                                <input type="hidden" name="father_deseased" id="father_deseased" value="{{{$father['father_ds'] or ''}}}">
                                <i class="dropdown icon"></i>
                                <div class="default text">Deseased ?</div>
                                <div class="menu">
                                  <div class="item" data-value="No">No</div>
                                  <div class="item" data-value="Yes">Yes</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="three fields">
                        <div class="required field">
                            <label>Place of Birth</label>
                            <input type="text" name="father_pob" placeholder="Place of Birth" value="{{{ $father['father']->place_of_birth or ''}}}">
                        </div>
                        <div class="required field">
                            <label>citizenship</label>
                            @if(isset($father['father']))
                                @include('modules._countries_dropdown', $data = ['name'=>'father_citizenship', 'title' => 'Citizenship at birth', 'value' => $father['father_ctzs']])
                            @else
                                @include('modules._countries_dropdown', $data = ['name'=>'father_citizenship', 'title' => 'Citizenship at birth'])
                            @endif
                        </div>
                        <div class="required field">
                            <label>Occupation</label>
                            <input type="text" name="father_occupation" placeholder="Your father's occupation" value="{{{$father['father_occ'] or ''}}}" />
                        </div>
                    </div>

                    <div class="field hide" id="father_res_addr">
                        <label>Father's Current Residential Address</label>
                        <div class="two fields">
                            <div class="field">
                                <input type="text" name="father_street1" placeholder="address line 1" value="{{{$father['father_addr']->street1 or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="father_street2" placeholder="address line 2 (optional)" value="{{{$father['father_addr']->street2 or ''}}}">
                            </div>
                        </div>

                        <div class="three fields">
                            <div class="required field">
                                @if(!empty($father['father_addr']))
                                    @include('modules._countries_dropdown', $data = ['name'=>'father_country', 'title' => 'Coutnry of Residence', 'value' => $father['father_addr']->country])
                                @else
                                    @include('modules._countries_dropdown', $data = ['name'=>'father_country', 'title' => 'Coutnry of Residence'])
                                @endif
                            </div>
                            <div class="field">
                                <input type="text" name="father_town" placeholder="town/city" value="{{{$father['father_addr']->town or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="father_post_code" placeholder="postal code" value="{{{$father['father_addr']->postal_code or ''}}}">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="field">
            <div class="mother_holder">
                <div class="required field">
                    <label>Details of your Mother</label>

                    <div class="two fields">

                        <div class="field">
                            <input type="text" name="mother_fam_name" placeholder="Surname/family name" value="{{{ $mother['mother']->last_name or ''}}}">
                        </div>
                        <div class="field">
                            <input type="text" name="mother_first_name" placeholder="First/Given names" value="{{{ $mother['mother']->first_name or ''}}}">
                        </div>

                    </div>

                    <div class="two fields">

                        <div class="required field">
                            <label>Date of birth</label>
                            @if(isset($mother['mother']))
                                @include('modules._inline_datebox', $data = ['mother_dob', $mother['mother']->date_of_birth])
                             @else
                                @include('modules._inline_datebox', $data = ['mother_dob'])
                             @endif
                        </div>

                        <div class="field">
                            <label>Deseased ?</label>
                            <div class="ui selection dropdown">
                                <input type="hidden" name="mother_deseased" id="mother_deseased" value="{{{$mother['mother_ds'] or ''}}}">
                                <i class="dropdown icon"></i>
                                <div class="default text">Deseased ?</div>
                                <div class="menu">
                                  <div class="item" data-value="No">No</div>
                                  <div class="item" data-value="Yes">Yes</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="three fields">
                        <div class="required field">
                            <label>Place of Birth</label>
                            <input type="text" name="mother_pob" placeholder="Place of Birth" value="{{{ $mother['mother']->place_of_birth or ''}}}">
                        </div>
                        <div class="required field">
                            <label>citizenship</label>
                            @if(isset($mother['mother']))
                                @include('modules._countries_dropdown', $data = ['name'=>'mother_citizenship', 'title' => 'Citizenship at birth', 'value' => $mother['mother_ctzs']])
                            @else
                                @include('modules._countries_dropdown', $data = ['name'=>'mother_citizenship', 'title' => 'Citizenship at birth'])
                            @endif
                        </div>
                        <div class="field">
                            <label>Occupation</label>
                            <input type="text" name="mother_occupation" placeholder="Your mother's occupation" value="{{{$mother['mother_occ'] or ''}}}" />
                        </div>
                    </div>

                    <div class="field hide" id="mother_res_addr">
                        <label>mother's Residential Address</label>
                        <div class="two fields">
                            <div class="field">
                                <input type="text" name="mother_street1" placeholder="address line 1" value="{{{$mother['mother_addr']->street1 or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="mother_street2" placeholder="address line 2 (optional)" value="{{{$mother['mother_addr']->street2 or ''}}}">
                            </div>
                        </div>

                        <div class="three fields">
                            <div class="required field">
                                @if(!empty($mother['mother_addr']))
                                    @include('modules._countries_dropdown', $data = ['name'=>'mother_country', 'title' => 'Coutnry of Residence', 'value' => $mother['mother_addr']->country])
                                @else
                                    @include('modules._countries_dropdown', $data = ['name'=>'mother_country', 'title' => 'Coutnry of Residence'])
                                @endif
                            </div>
                            <div class="field">
                                <input type="text" name="mother_town" placeholder="town/city" value="{{{$mother['mother_addr']->town or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="mother_post_code" placeholder="postal code" value="{{{$mother['mother_addr']->postal_code or ''}}}">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="field">
            <label>Provide Details of you family in-law's not included in your application (include mother, father in-law)</label>

            <div class="ui middle aligned inlaw selection list">
                @if(!empty($inlaws))
                    @foreach($inlaws as $i)

                        <div class="item {{ $i['family_member_id'] }}">
                            @if($i['type'] !== 'Profile')
                                <div class="right floated content">
                                    <div class="ui blue inverted inlaw edit button" id="{{ $i['family_member_id'] }}">Edit</div>
                                    <div class="ui red inverted inlaw delete button" id="{{ $i['family_member_id'] }}">Delete</div>
                                </div>
                            @endif
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{$i['fullname']}} ({{$i['relationship']}})
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_inlaw_link"><i class="add circle icon"></i> Click here to add family in-law</a>
        </div>

        <div class="field">
            <label>Provide details of you brothers and sisters (include half, step and adopted)</label>

            <div class="ui middle aligned sibling selection list">
                @if(!empty($siblings))
                    @foreach($siblings as $sibling)

                        <div class="item {{ $sibling->family_member_id }}">
                            <div class="right floated content">
                                <div class="ui blue inverted sibling edit button" id="{{ $sibling->family_member_id }}">Edit</div>
                                <div class="ui red inverted sibling delete button" id="{{ $sibling->family_member_id }}">Delete</div>
                            </div>
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{$sibling->first_name}} {{$sibling->last_name}} ({{$sibling->relationship}})
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_sibling_link"><i class="add circle icon"></i> Click here to add Sibling</a>
        </div>

        <div class="field">
            <label>Provide Details of any ex-spouses</label>

            <div class="ui middle aligned previous_spouse selection list">
                @if(!empty($ex))
                    @foreach($ex as $e)

                        <div class="item {{ $e->previous_spouse_id }}">
                            <div class="right floated content">
                                <div class="ui blue inverted previous_spouse edit button" id="{{ $e->previous_spouse_id }}">Edit</div>
                                <div class="ui red inverted previous_spouse delete button" id="{{ $e->previous_spouse_id }}">Delete</div>
                            </div>
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{ $e->full_name }} (Ex-Spouse)
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_previous_spouse_link"><i class="add circle icon"></i> Click here to add family ex-spouses</a>
        </div>

        <div class="field">
            <label>Details of children not included in application (biological, adopted and step-children)</label>

            <div class="ui middle aligned child selection list">
                @if(!empty($children))
                    @foreach($children as $c)

                        <div class="item {{ $c->family_member_id }}">
                            <div class="right floated content">
                                <div class="ui blue inverted child edit button" id="{{ $c->family_member_id }}">Edit</div>
                                <div class="ui red inverted child delete button" id="{{ $c->family_member_id }}">Delete</div>
                            </div>
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{$c->first_name}} {{$c->last_name}} ({{$c->relationship}})
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_child_link"><i class="add circle icon"></i> Click here to add child</a>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Medical Details
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
                <button class="ui save-data fluid grey left labeled icon button">
                    Save Data
                    <i class="upload icon"></i>
                </button>
            </div>
            <div class="field">
                <button class="ui next fluid blue right labeled icon button">
                    Next Form
                    <i class="large angle right icon"></i>
                </button>
            </div>
        </div>

    {!! Form::close() !!}
    </div>

</div>

@stop

@section('modal')
    @include('modals._add_previous_spouse')
    @include('modals._add_sibling')
    @include('modals._add_in_law')
    @include('modals._add_child')
@stop

@section('script')
    <script src="{{ URL::asset('js/cases_family_members.js') }}"></script>
    <script src="{{ URL::asset('js/sibling.js') }}"></script>
    <script src="{{ URL::asset('js/previous_spouse.js') }}"></script>
    <script src="{{ URL::asset('js/in_laws.js') }}"></script>
    <script src="{{ URL::asset('js/child.js') }}"></script>
@stop
