@extends('app_panel_no_right')

@section('title', 'Download Application Forms')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Dominica Government Forms - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">
        Please download the documents listed below in order for you to print and complete as follows:
        <br>
        <br>
        <strong>Form D1 - Disclosure:</strong> Please Sign, date and insert Place of signature
        <br>
        <br>
        <strong>Form D2 - Fingerprint &amp; Signature:</strong> Original certified by Lawyer/ Notary
        <br>
        <br>
        <strong>Form D3 - Medical Questionnaire:</strong> Completed and signed by Medical Practitioner For all applicants over 12 years
        <br>
        <br>
        <strong>Form D4 - Investment Agreement:</strong> Complete and sign one copy.
        <br>
        <br>
        <strong>Form 12:</strong> Complete and sign one copy.
    </p>

    <div class="ui grid">
        <div class="row">
            <div class="sixteen wide column">
                <div class="ui tabular menu">
                    @foreach($user->profiles()->orderBy('created_at')->get() as $p)
                    <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/users/profile/{{$p->profile_id}}/application/gov-documents">
                        <div class="content">
                            {{$p->first_name}} {{$p->last_name}}
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="sixteen wide column">
                <table class="ui single line table">
                    <thead>
                        <tr>
                            <th class="four wide">Applicant Name</th>
                            <th class="six wide">File Name</th>
                            <th class="two wide">File Type</th>
                            <th class="two wide">Created Date</th>
                            <th class="two wide"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($files) > 0)
                        @foreach($files as $file)
                        <tr class="item {{$file['key']}}">
                            <td>{{$file['applicant']}}</td>
                            <td><i class="red large file pdf outline icon"></i>{{$file['file']}}</td>
                            <td><b>{{$file['type']}}</b></td>
                            <td><i>{{$file['date']}}</i></td>
                            <td>
                                <a class="ui positive download button" href="/users/filesystem/download-profile-{{$folder}}/{{$profile->profile_id}}-{{$file['file']}}">Download</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td>
                                <div class="heading">
                                    <h2>Folder Empty</h2>
                                </div>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br>
    <div class="ui form">
        <div class="three fields">
            <div class="field">

            </div>
            <div class="field">

            </div>
            <div class="field">
                <a class="ui fluid inverted blue button" href="{{$fwdLink}}">Next Form</a>
            </div>
        </div>
    </div>

</div>

<input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />
<input type="hidden" id="profile_id" style="display: none" value="{{ $profile->profile_id }}" />

@stop

@section('script')
    <script src="{{ URL::asset('js/file_manager.js') }}"></script>
    <script src="{{ URL::asset('js/documents.js') }}"></script>
@stop
