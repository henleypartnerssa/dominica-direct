@extends('app_panel_no_right')

@section('title', 'Upload Docuemnts')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Personal Documentation - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">"The following is a checklist of all documents and forms required and specific instructions regarding each item.  This may help you to complete everything efficiently.

        <h3 class="ui horizontal divider header">
            Upload Personal Documents
        </h3>

    <div class="ui grid">
        <div class="row">
            <div class="sixteen wide column">
                <div class="ui tabular menu">
                    @foreach($user->profiles()->orderBy('created_at')->get() as $p)
                    <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/users/profile/{{$p->profile_id}}/application/upload-documents">
                        <div class="content">
                            {{$p->first_name}} {{$p->last_name}}
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <br />

    <div class="ui upload-documents form">
        <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />
        <input type="hidden" id="profile_id" style="display: none" value="{{ $profile->profile_id }}" />

        <div id="file_inputs">
        </div>

        <!--PASSPORTS-->
        <div class="field">
            <input type="hidden" id="total_passport" style="display: none" name="total_passport" value="{{ count($files['passport']) }}" />
            <label id="passport">
                <i class="inline info circle icon" data-title="Passport Copy" data-content="You must submit a certified copy of the passport of each person included in the Application. Copy all pages showing your name, nationality, place of birth, date and place of issue, expiry date, passport number, and issuing country. Where children are included in the passport of a parent, these pages must also be copied. If a passport is not available, please supply a certified copy of that person’s identity card or other equivalent document."></i>
                Upload certified and legalized copy of applicants Passport. includes all pages.
            </label>
            <div class="ui middle aligned passport selection list">
                @if($files)
                    @foreach($files['passport'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_passport">
                <i class="add upload icon"></i>Click here to upload Passport
            </a>
        </div>
        <!--NATIONAL ID-->
        <div class="field">
            <input type="hidden" id="total_national_id" style="display: none" name="total_national_id" value="{{{count($files['national_id']) or '0'}}}" />
            <label id="national_id">
                Upload certified and legalized copy of applicants National ID document
            </label>
            <div class="ui middle aligned national_id selection list">
                @if($files)
                    @foreach($files['national_id'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_national_id">
                <i class="add upload icon"></i>Click here to upload National ID
            </a>
        </div>

        <!--BIRTH CERTIFICATE-->
        <div class="field">
            <input type="hidden" id="total_birth_certificate" style="display: none" name="total_birth_certificate" value="{{{count($files['birth_certificate']) or '0'}}}" />
            <label id="birth_certificate">
                Upload certified and legalized copy of applicants Birth Certificate
            </label>
            <div class="ui middle aligned birth_certificate selection list">
                @if($files)
                    @foreach($files['birth_certificate'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_birth_certificate">
                <i class="add upload icon"></i>Click here to upload Birth Certificate
            </a>
        </div>

        @if($profile->belongs_to === 'Principal Applicant')
            <!--PROOF OF RESIDENTIAL ADDRESS-->
            <div class="field">
                <input type="hidden" id="total_proof_of_residential_address" style="display: none" name="total_proof_of_residential_address" value="{{{count($files['proof_of_residential_address']) or '0'}}}" />
                <label id="proof_of_residential_address">
                    Upload certified or legalized proof of Residential Address not older than 3 months e.g utility bill, bank statement ect.
                </label>
                <div class="ui middle aligned proof_of_residential_address selection list">
                    @if($files)
                        @foreach($files['proof_of_residential_address'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_proof_of_residential_address">
                    <i class="add upload icon"></i>Click here to upload Proof of Residential Address
                </a>
            </div>
        @endif

        <!--MARRIAGE CERTIFICATE-->
        <div class="field">
            <input type="hidden" id="total_marriage_certificate" style="display: none" name="total_marriage_certificate" value="{{{count($files['marriage_certificate']) or '0'}}}" />
            <label id="marriage_certificate">
                Upload certified and legalized copy of Marriage Certificate
            </label>
            <div class="ui middle aligned marriage_certificate selection list">
                @if($files)
                    @foreach($files['marriage_certificate'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_marriage_certificate">
                <i class="add upload icon"></i>Click here to upload Marriage Certificate
            </a>
        </div>

        <!--DIVORCE DOCUMENT-->
        <div class="field">
            <input type="hidden" id="total_divorse_document" style="display: none" name="total_divorse_document" value="{{{count($files['divorse_certificate']) or '0'}}}" />
            <label id="divorse_document">
                Upload certified and legalized copy of Divorse Document. (if applicable)
            </label>
            <div class="ui middle aligned divorse_document selection list">
                @if($files)
                    @foreach($files['divorse_certificate'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_divorse_document">
                <i class="add upload icon"></i>Click here to upload Divorse Document
            </a>
        </div>

        @if($profile->belongs_to === 'Principal Applicant')
            <!--LETTER OF APPLICATION-->
            <div class="field">
                <input type="hidden" id="total_letter_of_application" style="display: none" name="total_letter_of_application" value="{{{count($files['divorse_certificate']) or '0'}}}" />
                <label id="letter_of_application">
                    Upload signed copy of Letter of Application. (to be addressed to Prime Minister of Dominica)
                </label>
                <div class="ui middle aligned letter_of_application selection list">
                    @if($files)
                        @foreach($files['divorse_certificate'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_letter_of_application">
                    <i class="add upload icon"></i>Click here to upload Letter of Application
                </a>
            </div>

            <!--PROFESSIONAL REFERENCE-->
            <div class="field">
                <input type="hidden" id="total_professional_reference" style="display: none" name="total_professional_reference" value="{{{count($files['professional_reference']) or '0'}}}" />
                <label id="professional_reference">
                    Upload signed copy of Professional Reference. (connected to work for a minimum of 3 years)
                </label>
                <div class="ui middle aligned professional_reference selection list">
                    @if($files)
                        @foreach($files['professional_reference'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_professional_reference">
                    <i class="add upload icon"></i>Click here to upload Professional Reference
                </a>
            </div>


            <!--PERSONAL REFERENCE-->
            <div class="field">
                <input type="hidden" id="total_personal_reference" style="display: none" name="total_personal_reference" value="{{{count($files['personal_reference']) or '0'}}}" />
                <label id="personal_reference">
                    Upload copy of Personal Reference. (must have known applicant for minimum of 5 years)
                </label>
                <div class="ui middle aligned personal_reference selection list">
                    @if($files)
                        @foreach($files['personal_reference'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_personal_reference">
                    <i class="add upload icon"></i>Click here to upload Personal Reference
                </a>
            </div>

            <!--AFFIDAVIT TO SOURCE OF FUNDS-->
            <div class="field">
                <input type="hidden" id="total_affidavit_funds" style="display: none" name="total_affidavit_funds" value="{{{count($files['affidavit_of_funds']) or '0'}}}" />
                <label id="affidavit_funds">
                    Upload copy of Affidavit attesting to Source of Funds
                </label>
                <div class="ui middle aligned affidavit_funds selection list">
                    @if($files)
                        @foreach($files['affidavit_of_funds'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_affidavit_funds">
                    <i class="add upload icon"></i>Click here to upload Affidavit
                </a>
            </div>

            <!--LETTER OF EMPLOYMENT-->
            <div class="field">
                <input type="hidden" id="total_letter_of_employment" style="display: none" name="total_letter_of_employment" value="{{{count($files['letter_of_employment']) or '0'}}}" />
                <label id="letter_of_employment">
                    Upload copy of Letter of employment/ Financial statements/ Letter of Incorporation
                </label>
                <div class="ui middle aligned letter_of_employment selection list">
                    @if($files)
                        @foreach($files['letter_of_employment'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_letter_of_employment">
                    <i class="add upload icon"></i>Click here to upload Letter of Employment
                </a>
            </div>
        @endif

        @if($age > 18 and $age < 25)
            <!--LETTER OF RECOMMENDATION-->
            <div class="field">
                <input type="hidden" id="total_letter_of_recommendation" style="display: none" name="total_letter_of_recommendation" value="{{{count($files['letter_of_recommendation']) or '0'}}}" />
                <label id="letter_of_recommendation">
                    Upload copy of Letter of Recommendation from School/University
                </label>
                <div class="ui middle aligned letter_of_recommendation selection list">
                    @if($files)
                        @foreach($files['letter_of_recommendation'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_letter_of_recommendation">
                    <i class="add upload icon"></i>Click here to upload Letter of Recommendation
                </a>
            </div>
        @endif

        @if($age > 16)
            <!--POLICE CETIFICATE-->
            <div class="field">
                <input type="hidden" id="total_police_certificate" style="display: none" name="total_police_certificaten" value="{{{count($files['police_certificate']) or '0'}}}" />
                <label id="police_certificate">
                    Upload Police Certificate
                </label>
                <div class="ui middle aligned police_certificate selection list">
                    @if($files)
                        @foreach($files['police_certificate'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_police_certificate">
                    <i class="add upload icon"></i>Click here to upload Police Certificate
                </a>
            </div>
        @endif

        @if($age > 12 and $age < 15)
            <!--AFFIDAVIT FROM PARENT-->
            <div class="field">
                <input type="hidden" id="total_affidavit_from_parent" style="display: none" name="total_affidavit_from_parent" value="{{{count($files['affidavit_from_parent']) or '0'}}}" />
                <label id="affidavit_from_parent">
                    Upload copy of Affidavit from Parent of No Criminal Record
                </label>
                <div class="ui middle aligned affidavit_from_parent selection list">
                    @if($files)
                        @foreach($files['affidavit_from_parent'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_affidavit_from_parent">
                    <i class="add upload icon"></i>Click here to upload Affidavit
                </a>
            </div>
        @endif

        @if($profile->belongs_to === 'Principal Applicant')
            <!--RECOMMENDATIONS FROM BANKS-->
            <div class="field">
                <input type="hidden" id="total_banks_recommendations" style="display: none" name="total_banks_recommendations" value="{{{count($files['banks_recommendations']) or '0'}}}" />
                <label id="banks_recommendations">
                    Upload copy of Recommendation from bankers (must show that account has been held for a minimum of 2 years) plus latest 12 months bank statements.
                </label>
                <div class="ui middle aligned banks_recommendations selection list">
                    @if($files)
                        @foreach($files['banks_recommendations'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_banks_recommendations">
                    <i class="add upload icon"></i>Click here to upload Recommendations
                </a>
            </div>

            <!--BUSINESS PLAN-->
            <div class="field">
                <input type="hidden" id="total_business_plan" style="display: none" name="total_business_plan" value="{{{count($files['business_plan']) or '0'}}}" />
                <label id="business_plan">
                    Upload copy of Business Plan/C.V.
                </label>
                <div class="ui middle aligned business_plan selection list">
                    @if($files)
                        @foreach($files['business_plan'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_business_plan">
                    <i class="add upload icon"></i>Click here to upload Plan
                </a>
            </div>
        @endif

        <!--MILLITARY SERVICE DOCUMENT-->
        <div class="field">
            <input type="hidden" id="total_millitary_service" style="display: none" name="total_millitary_service" value="{{{count($files['millitary_service']) or '0'}}}" />
            <label id="millitary_service">
                Upload copy of Military Service documents (if applicable)
            </label>
            <div class="ui middle aligned millitary_service selection list">
                @if($files)
                    @foreach($files['millitary_service'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_millitary_service">
                <i class="add upload icon"></i>Click here to upload Military Service documents
            </a>
        </div>

        @if($profile->belongs_to === 'Principal Applicant')
            <!--POWER OF ATTORNEY-->
            <div class="field">
                <input type="hidden" id="total_power_of_attorney" style="display: none" name="total_power_of_attorney" value="{{{count($files['power_of_attorney']) or '0'}}}" />
                <label id="power_of_attorney">
                    Upload copy of Power of Attorney – Local agent
                </label>
                <div class="ui middle aligned power_of_attorney selection list">
                    @if($files)
                        @foreach($files['power_of_attorney'] as $file)
                        <div class="item {{$file['key']}}">
                            <div class="right floated content">
                                <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">File Name: {{$file['file']}}</div>
                        </div>
                        @endforeach
                    @endif
                </div>
                <a href="#" id="add_power_of_attorney">
                    <i class="add upload icon"></i>Click here to upload Power of Attorney
                </a>
            </div>
        @endif

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Input Data
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/file_upload.js') }}"></script>
    <script src="{{ URL::asset('js/file_manager.js') }}"></script>
    <script src="{{ URL::asset('js/antigua_upload_documents.js') }}"></script>
@stop
