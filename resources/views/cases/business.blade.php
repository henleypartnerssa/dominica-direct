@extends('app_panel_no_right')

@section('title', 'Work and Business')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Work, business and Source of Wealth - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">Please provide details regarding your work, business and bank information. In the case of sections which are not applicable, write “N/A”, e.g. employment information for minor children. Please note therefore that each question should be answered. Each form should be completed personally, except in the case of children who cannot yet write. For children who are below the age of 18, both parents must sign the form on behalf of the child as their legal guardians. If one parent has sole custody over the child, or other persons are the legal guardians, then appropriate documentation must be provided.</p>

    @if($user->profiles()->count() > 4)
        @include('modules._data_forms_guide_split', $data = ['page' => 'business'])
    @else
        @include('modules._data_forms_guide', $data = ['page' => 'business'])
    @endif

    <div class="ui work-business form">
    {!! Form::open() !!}

        <input type="hidden" name="profile_id" id="profile_id" value="{{ $profile->profile_id }}">
        <input type="hidden" name='employed' id='employed' value="{{(!empty($business->self_employed) && $business->self_employed === true) ? 'yes' : 'no'}}">
        <input type="hidden" name="save" id="save" value="0" />

        <div class="four fields">
            <div class="required field">
                <label>Self Employed ?</label>
                <div class="ui fluid search selection dropdown">
                    <input type="hidden" name="employment" id="employment" value="{{(!$business->self_employed) ? '0' : '1'}}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Select Level</div>
                        <div class="menu">
                            <div class="item" data-value="1">Yes</div>
                            <div class="item" data-value="0">No</div>
                        </div>
                </div>
            </div>
            <div class="required field">
                <label>Name of business or employer</label>
                <input type="text" name="name_of_business" placeholder="business or employer name" value="{{{$business->name_of_business or ''}}}">
            </div>
            <div class="field">
                <label>Occupation by training</label>
                <input type="text" name="occupation_by_training" placeholder="occupation by training" value="{{{$business->occupation_by_training or ''}}}">
            </div>
            <div class="required field">
                <label>Current Primary occupation</label>
                <input type="text" name="primary_occupation" placeholder="current occupation" value="{{{$business->primary_occupation or ''}}}">
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                <label>Business, Registration Country</label>
                @if(isset($business->country_of_registration))
                    @include('modules._countries_dropdown', $data = ['name' => 'registration_country', 'title' => 'registration country', 'value' => $business->country_of_registration])
                @else
                    @include('modules._countries_dropdown', $data = ['name' => 'registration_country', 'title' => 'registration country'])
                @endif
            </div>
            <div class="field">
                <label>Business, Registration Number</label>
                <input type="text" name="registration_no" placeholder="registration country" value="{{{$business->registration_no or ''}}}" />
            </div>
        </div>
        <div class="four fields">
            <div class="field">
                <label>Business telephone number</label>
                <input type="text" name="phone_no" placeholder="telephone number" value='{{{$business->phone_no or ''}}}'>
            </div>
            <div class="field">
                <label>Business Fax number</label>
                <input type="text" name="fax_no" placeholder="fax number" value='{{{$business->fax_no or ''}}}'>
            </div>
            <div class="field">
                <label>Business emaill address</label>
                <input type="text" name="email" placeholder="email address" value='{{{$business->email or ''}}}'>
            </div>
            <div class="field">
                <label>Busines website address/url</label>
                <input type="text" name="web_url" placeholder="website URL" value='{{{$business->web_url or ''}}}'>
            </div>
        </div>

        <div class="required field">
            <label>Address of you business or employer</label>

            <div class="two fields">
                <div class="field">
                    <input type="text" name="business_street1" placeholder="address line 1" value='{{{$business->address->street1 or ''}}}'>
                </div>
                <div class="field">
                    <input type="text" name="business_street2" placeholder="address line 2 (optional)" value='{{{$business->address->street2 or ''}}}'>
                </div>
            </div>

            <div class="three fields">
                <div class="field">
                    @if(isset($business->address))
                        @include('modules._countries_dropdown', $data = ['name' => 'business_country', 'title' => 'country', 'value' => $business->address->country])
                    @else
                        @include('modules._countries_dropdown', $data = ['name' => 'business_country', 'title' => 'country'])
                    @endif
                </div>
                <div class="field">
                    <input type="text" name="business_town" placeholder="town/city" value='{{{$business->address->town or ''}}}'>
                </div>
                <div class="field">
                    <input type="text" name="business_postal_code" placeholder="postal code" value='{{{$business->address->postal_code or ''}}}' >
                </div>
            </div>

        </div>

        <div class="field">
            <label>Nature of your business or employer's Businesses</label>
            <textarea rows="4" name="nature_of_business" placeholder="details here...">{{{$business->nature_of_business or ''}}}</textarea>
        </div>

        <div class="field">
            <label>Most important person/companies with whom you or your employer does business</label>
            <textarea rows="4" name="mi_persons_companies" placeholder="details here...">{{{$business->mi_persons_companies or ''}}}</textarea>
        </div>

        <div class="field">
            <label>Main sources and business activities from which you generate your main source of income</label>
            <textarea rows="4" name="source_of_income" placeholder="details here...">{{{$business->source_of_income or ''}}}</textarea>
        </div>

        <div class="field">
            <label>List all companies of which you are currently a shareholder or director</label>
            <textarea rows="4" name="companies_share_dir" placeholder="list companies here..">{{{$business->companies_share_dir or ''}}}</textarea>
        </div>

        <div class="field">
            <label>
                Geographical juristiction(s) in which you conduct your business
            </label>
            <textarea rows="4" name="activities_geo" placeholder="details here...">{{{$business->activities_geo or ''}}}</textarea>
        </div>

        <div class="field" {{($profile->belongs_to === "Spouse" || $profile->belongs_to === "Child") ? 'style=display:none' : ''}}>

            <div class="two fields">
                <div class="required field">
                    <label>Your gross anual income</label>
                    <div class="ui left icon input">
                        <input type="text" name="gross_net_income" placeholder="9999999.99" value='{{{$business->gross_net_income or ''}}}' >
                        <i class="dollar icon"></i>
                    </div>
                </div>

                <div class="required field">
                    <label>Your Total Net Worth</label>
                    <div class="ui left icon input">
                        <input type="text" name="total_net_worth" placeholder="USD - 9999999.99" value="{{{$business->total_net_worth or ''}}}">
                        <i class="dollar icon"></i>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Provide the estimated value of your Assets (Please provide documentary uspport for these estimations)</label>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="fixed_asset" placeholder=" Fixed Assets (eg. property vehicle, etc)" value="{{{$assets['fixed_assets'] or ''}}}" />
                            <i class="dollar icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="savings_deposit_asset" placeholder="Savings / Deposits" value="{{{$assets['savings_deposit_asset'] or ''}}}" />
                            <i class="dollar icon"></i>
                        </div>
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="investment_asset" placeholder="Investments (eg. stocks, shares, bonds, etc)" value="{{{$assets['investment_asset'] or ''}}}" />
                            <i class="dollar icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="other_asset" placeholder="other (eg. Other Asset Name - 99999.99)" value="{{{$assets['other_asset'] or ''}}}" />
                            <i class="dollar icon"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label>Provide the estimated value of your Liabilities (Please provide documentary uspport for these estimations)</label>

                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="outstanding_long_term_loans_liability" placeholder=" Outstanding long term loans" value="{{{$liabilities['outstanding_long_term_loans_liability'] or ''}}}" />
                            <i class="dollar icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="outstanding_short_term_loans_liability" placeholder="Outstanding short term loans" value="{{{$liabilities['outstanding_short_term_loans_liability'] or ''}}}" />
                            <i class="dollar icon"></i>
                        </div>
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <div class="ui left icon input">
                            <input type="text" name="other_liability" placeholder="other (eg. Other Liability Name - 99999.99)" value="{{{$liabilities['other_liability'] or ''}}}" />
                            <i class="dollar icon"></i>
                        </div>
                    </div>
                    <div class="field">

                    </div>
                </div>
            </div>

            <div class="required field">
                <label>
                    Please provide a summarized statement of how you have accumulated your Total Net Worth by listing the main acquisitions / dispositions and events
                </label>
                <textarea rows="15" name="total_net_worth_summary" placeholder="details here...">{{{$business->total_net_worth_summary or ''}}}</textarea>
            </div>

            @include('modules._professional_licences')

            <div class="required field">
                <label>Give details of your employment history during the last 10 years, including any periods of unemployment and self-employment</label>
                <div class="ui middle aligned references selection list">
                    @foreach($references as $ref)
                        <div class="item {{$ref->employment_reference_id}}">
                            <div class="right floated content">
                                <div class="ui blue inverted edit_reference button" id="{{$ref->employment_reference_id}}">Edit</div>
                                <div class="ui red inverted del_reference button" id="{{$ref->employment_reference_id}}">Delete</div>
                            </div>
                            <i class="ui avatar image bookmark icon"></i>
                            <div class="content">{{$ref->employer}} (From: {{$ref->from_date}} To: {{$ref->to_date}})</div>
                        </div>
                    @endforeach
                </div>
                <a href="#" id="add_reference_link"><i class="add circle icon"></i> Click here to add Employer Reference</a>
            </div>

            <div class="field" {{($profile->belongs_to === "Spouse of PA" || $profile->belongs_to === "Dependant of PA") ? 'style=display:none' : ''}}>
                <label>Personal bank account details from which you will be sending the funds to the Government of Dominica</label>

                <div class="four fields">

                    <div class="field">
                        <input type="text" name="account_name_of" placeholder="account in the name of" value="{{{$bank->account_name_of or ''}}}" >
                    </div>
                    <div class="field">
                        <input type="text" name="bank_name" placeholder="bank name" value="{{{$bank->bank_name or ''}}}">
                    </div>
                    <div class="field">
                        <input type="text" name="account_number" placeholder="account number" value="{{{$bank->account_no or ''}}}">
                    </div>
                    <div class="field">
                        <input type="text" name="account_bic" placeholder="IBAN / BIC code" value="{{{$bank->account_iban_bic or ''}}}">
                    </div>
                </div>

                <div class="field">
                    <label>Bank Account Address</label>

                    <div class="two fields">
                        <div class="field">
                            <input type="text" name="bank_street1" placeholder="address line 1" value="{{{$bank->address->street1 or ''}}}">
                        </div>
                        <div class="field">
                            <input type="text" name="bank_street2" placeholder="address line 2 (optional)" value="{{{$bank->address->street2 or ''}}}" >
                        </div>
                    </div>

                    <div class="three fields">
                        <div class="field">
                            @if(isset($bank->address))
                                @include('modules._countries_dropdown', $data = ['name' => 'bank_country', 'title' => 'country', 'value' => $bank->address->country])
                            @else
                                @include('modules._countries_dropdown', $data = ['name' => 'bank_country', 'title' => 'country'])
                            @endif
                        </div>
                        <div class="field">
                            <input type="text" name="bank_town" placeholder="town/city" value="{{{$bank->address->town or ''}}}" >
                        </div>
                        <div class="field">
                            <input type="text" name="bank_post_code" placeholder="postal code" value="{{{$bank->address->postal_code or ''}}}" >
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Family Details
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
                <button class="ui save-data fluid grey left labeled icon button">
                    Save Data
                    <i class="upload icon"></i>
                </button>
            </div>
            <div class="field">
                <button class="ui next fluid blue right labeled icon button">
                    Next Form
                    <i class="large angle right icon"></i>
                </button>
            </div>
        </div>
    {!! Form::close() !!}
    </div>

</div>

@stop

@section('modal')
    @include('modals._add_refereces')
@stop

@section('script')
    <script src="{{ URL::asset('js/cases_work_business.js') }}"></script>
@stop
