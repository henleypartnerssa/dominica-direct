@extends('app_panel_no_right')

@section('title', 'Upload Notary Forms')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Notary Uploads - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
    <p id="admin">
        Please scan, save and upload the following fully signed and completed documents listed below:
        <br>
        <br>
        <strong>Form D2 - Fingerprint &amp; Signature:</strong> Original certified by Lawyer/ Notary
        <br>
        <br>
        <strong>Form D3 - Medical Questionnaire:</strong> Completed and signed by Medical Practitioner For all applicants over 12 years
        <br>
        <br>
        <strong>Form 12:</strong> Complete and sign one copy.
    </p>

    <div class="ui grid">
        <div class="row">
            <div class="sixteen wide column">
                <div class="ui tabular menu">
                    @foreach($user->profiles()->orderBy('created_at')->get() as $p)
                    <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/users/profile/{{$p->profile_id}}/application/upload-notary">
                        <div class="content">
                            {{$p->first_name}} {{$p->last_name}}
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="ui upload-documents form">
        <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />
        <input type="hidden" id="profile_id" style="display: none" value="{{ $profile->profile_id }}" />

        <div id="file_inputs">
        </div>

        <div class="field">
            <input type="hidden" id="total_D2" style="display: none" name="total_D2" value="{{{count($files['d2_fingerprint_&_signature']) or '0'}}}" />
            <label id="D2">D2 - Fingerprint & Signature</label>
            <div class="ui middle aligned D2 selection list">
                @if($files)
                    @foreach($files['d2_fingerprint_&_signature'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_D2">
                <i class="add upload icon"></i>Click here to upload D2 Form
            </a>
        </div>

        <div class="field">
            <input type="hidden" id="total_D3" style="display: none" name="total_D3" value="{{{count($files['d3_medical_questionnaire']) or '0'}}}" />
            <label id="D2">D3 - Medical Questionnaire</label>
            <div class="ui middle aligned D3 selection list">
                @if($files)
                    @foreach($files['d3_medical_questionnaire'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_D3">
                <i class="add upload icon"></i>Click here to upload D3 Form
            </a>
        </div>

        <div class="field">
            <input type="hidden" id="total_F12" style="display: none" name="total_F12" value="{{{count($files['form_12']) or '0'}}}" />
            <label id="D2">Form 12</label>
            <div class="ui middle aligned F12 selection list">
                @if($files)
                    @foreach($files['form_12'] as $file)
                    <div class="item {{$file['key']}}">
                        <div class="right floated content">
                            <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">File Name: {{$file['file']}}</div>
                    </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_F12">
                <i class="add upload icon"></i>Click here to upload Form 12
            </a>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Government Forms
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/file_upload.js') }}"></script>
    <script src="{{ URL::asset('js/file_manager.js') }}"></script>
    <script src="{{ URL::asset('js/antigua_upload_notary.js') }}"></script>
@stop
