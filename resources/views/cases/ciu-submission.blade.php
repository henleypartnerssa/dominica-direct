@extends('app_panel_no_right')

@section('title', 'CIU Submission')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Government Submission</h2>

<div class="content">
    <p id="admin">
        Your application has been submitted to the Dominica Government for CIU Approval. We will notify you via sms and email if you have received approval.
    </p>

    <div class="ui submission form">

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Courier
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
