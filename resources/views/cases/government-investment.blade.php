@extends('app_panel_no_right')

@section('title', 'Government Investment')

@section('stepper')
    @include('modules._stepping',$data = App\Util\AppHelpers::stepperLinks($profile->profile_id, $profile->application->application_id))
@stop

@section('content')

<h2 class="heading admin">{{ $programName }} - {{$profile->first_name}} {{$profile->last_name}}</h2>

<div class="content">
<p id="admin">
    To finalize the citizenship process, you now have 30 days, within which to:
    <br>
    <br>
    - Pay the 90% balance of the government processing fees and passport fees;
    <br>
    - Pay the final balance of the purchase price and provide evidence of the wire transfer for the Title to be registered in your name.
    <br>
    <br>
    Please download the invoice to remit the payment as per the payment details captured on the invoice.
</p>

    <div class="ui submission form">

        <div class="field">
            <a href="/cases/pdf/download/invoice-3" class="ui next fluid grey left labeled icon button">
                Download Invoice
                <i class="large download icon"></i>
            </a>
        </div>

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to CIU Submission
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
