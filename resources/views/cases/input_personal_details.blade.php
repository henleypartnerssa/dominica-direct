
<div class="four fields">
    <div class="required field">
        <label>Title</label>
        <div class="ui selection dropdown">
            <input type="hidden" name="title" value="{{ $profile->title }}">
            <i class="dropdown icon"></i>
            <div class="default text">Your title</div>
            <div class="menu">
              <div class="item" data-value="Mr">Mr</div>
              <div class="item" data-value="Mrs">Mrs</div>
              <div class="item" data-value="Miss">Miss</div>
              <div class="item" data-value="Ms">Ms</div>
              <div class="item" data-value="Other">Other</div>
            </div>
        </div>
    </div>

    <div class="required field">
        <label>Last / Family Name</label>
        <input type="text" name="last_name" id="last_name" placeholder="Family Name" value="{{ $profile->last_name }}" />
    </div>

    <div class="required field">
        <label>Gender</label>

        <div class="ui fluid search selection dropdown">
            <input type="hidden" name="gender" value="{{$profile->gender}}">
            <i class="dropdown icon"></i>
            <div class="default text">Gender</div>
                <div class="menu">
                    <div class="item" data-value="male"><i class="man icon"></i>Male</div>
                    <div class="item" data-value="female"><i class="woman icon"></i>Female</div>
                </div>
        </div>
    </div>

    <div class="required field">
        <label>Marital Status</label>

        <div class="ui fluid search selection dropdown">
            <input type="hidden" name="marital_status" id="marital_status" value="{{ $profile->marital_status }}">
            <i class="dropdown icon"></i>
            <div class="default text">Marital Status</div>
                <div class="menu">
                    <div class="item" data-value="Single">Single</div>
                    <div class="item" data-value="Married">Married</div>
                    <div class="item" data-value="Divorced">Divorced</div>
                    <div class="item" data-value="Seperated">Seperated</div>
                    <div class="item" data-value="Widowed">Widowed</div>
                    <div class="item" data-value="Engaged">Engaged</div>
                </div>
        </div>
    </div>

</div>

<div class="two fields" id="marriage" style="display: none">

    <div class="required field">
        <label>Date of Marriage</label>
        @include('modules._inline_datebox', $data = ['marriage', $profile->date_of_marriage])
    </div>

    <div class="required field">
        <label>Place of Marriage</label>
        <input type="text" name="place_of_marriage" placeholder="Place of marriage" value="{{{$profile->place_of_marriage or ''}}}" />
    </div>

</div>

<div class="two fields" id="divorce" style="display: none">

    <div class="required field">
        <label>Date when Divorce was granted</label>
        @include('modules._inline_datebox', $data = ['divorve', $profile->date_divorce])
    </div>

    <div class="required field">
        <label>Place where Divorce was granted</label>
        <input type="text" name="place_divorce" placeholder="Place of marriage" value="{{{$profile->place_divorce or ''}}}" />
    </div>

</div>

<div class="two fields">
    <div class="required field">
        <label>First / Given Name</label>
        <input type="text" name="first_name" placeholder="your first name" value="{{{$profile->first_name or ''}}}" />
    </div>
    <div class="field">
        <label>Middle Name's</label>
        <input type="text" name="middle_names" placeholder="your middle name's" value="{{{$profile->middle_names or ''}}}" />
    </div>
</div>

<div class="three fields">
    <div class="field">
        <label>Maiden Name</label>
        <input type="text" name="maiden_name" placeholder="your maiden name" value="{{{$profile->maiden_name or ''}}}"  />
    </div>
    <div class="field">
        <label>Mother's Maiden Name</label>
        <input type="text" name="mothers_maiden_name" placeholder="your mothers maiden name" value="{{{$profile->mothers_maiden_name or ''}}}" />
    </div>
    <div class="field">
        <label>Native Name</label>
        <input type="text" name="native_name" placeholder="Name in local language charactors" value="{{{$profile->native_name or ''}}}" />
    </div>
</div>

<div class="field">
    <label>If you have changed your name, date and reason for change</label>

    <div class="two fields">
        <div class="field">
            <label>Name</label>
            <input type="text" name="changed_name" placeholder="Specifiy the actual changed name here" value="{{{$profile->changed_name or ''}}}" />
        </div>
        <div class="field">
            <label>Date of name change</label>
            @include('modules._inline_datebox', $data = ['name_change', $profile->name_change_date])
        </div>
    </div>

    <div class="field">
        <label>Reason for name change</label>
        <textarea rows="3" name="name_change_reason" placeholder="state reason here...">{{{$profile->name_change_reason or ''}}}</textarea>
    </div>
</div>

<div class="field">
    <div class="four fields">
        <div class="required field">
            <label>Color of Eye</label>
            <input type="text" name="eye_color" value="{{{$profile->eye_color or ''}}}">
        </div>
        <div class="required field">
            <label>Color of Hair</label>
            <input type="text" name="hair_color" value="{{{$profile->hair_color or ''}}}">
        </div>
        <div class="required field">
            <label>Height</label>
            <input type="text" name="height" placeholder="in (cm)" value="{{{$profile->height or ''}}}">
        </div>
        <div class="required field">
            <label>Weight</label>
            <input type="text" name="weight" placeholder="in (kg)" value="{{{$profile->weight or ''}}}">
        </div>
    </div>
    <div class="field">
        <label>Distinguising Marks</label>
        <textarea rows="3" name="distinguising_marks" placeholder="state body marks here...">{{{$profile->distinguising_marks or ''}}}</textarea>
    </div>
</div>

<div class="required field">
    <label>Identity card number and issuing  country</label>

    <div class="two fields">
        <div class="field">
            <input type="text" name="id_no" id="id_no" placeholder="Identity Number" value="{{{$profile->identity_documents()->where('document_type', 'id_document')->first()->document_number or ''}}}">
        </div>

        <div class="field">
            @include('modules._countries_dropdown', $data = ['name'=>'id_issuing_country', 'title' => 'Issuing Country', 'value' => ($profile->identity_documents()->where('document_type', 'id_document')->count()) ? $profile->identity_documents()->where('document_type', 'id_document')->first()->issuing_country : ''])
        </div>
    </div>
</div>

<div class="field">
    <label>Social Security number and issuing country</label>

    <div class="two fields">
        <div class="field">
            <input type="text" name="security_no" placeholder="Social Security Number" value="{{{$profile->identity_documents()->where('document_type', 'social_security')->first()->document_number or ''}}}">
        </div>

        <div class="field">
            @include('modules._countries_dropdown', $data = ['name'=>'security_issuing_country', 'title' => 'Issuing Country', 'value' => ($profile->identity_documents()->where('document_type', 'social_security')->count()) ? $profile->identity_documents()->where('document_type', 'social_security')->first()->issuing_country : ''])
        </div>
    </div>
</div>

<div class="field">
    <label>Drivers Licence</label>

    <div class="two fields">
        <div class="field">
            <input type="text" name="drivers_no" placeholder="Drivers Licence Number" value="{{{$profile->identity_documents()->where('document_type', 'drivers_licence')->first()->document_number or ''}}}">
        </div>
        <div class="field">
            @include('modules._countries_dropdown', $data = ['name'=>'drivers_issuing_country', 'title' => 'Issuing Country', 'value' => ($profile->identity_documents()->where('document_type', 'drivers_licence')->count()) ? $profile->identity_documents()->where('document_type', 'drivers_licence')->first()->issuing_country : '
            '])
        </div>
    </div>
</div>

<div class="two fields">

    <div class="required field">
        <label>Place of Birth</label>
        <input type="text" name="place_of_birth" id="place_of_birth" value="{{{$profile->place_of_birth or ''}}}" >
    </div>

    <div class="required field">
        <label>Date of Birth</label>
            @include('modules._inline_datebox', $data = ['dob', $profile->date_of_birth])
    </div>

</div>

<div class="two fields">

    <div class="required field">
        <label>Country of birth</label>
        @include('modules._countries_dropdown', $data = ['name' => 'country_of_birth', 'title' => 'Country of Birth', 'value' => $profile->country_of_birth])
    </div>

    <div class="required field">
        <label>Citizenship at birth</label>
        @include('modules._countries_dropdown', $data = ['name' => 'citizenship_at_birth', 'title' => 'Citizenship of Birth', 'value' => $profile->citizenship_at_birth])
    </div>

</div>

<div class="field">

    <label>
        <i class="inline info circle icon" data-title="Please Note" data-content="Please list the dates of any changes of citizenship, including relinquishing citizenship, and the places at which such changes where recorded"></i>
        I am also a citizen, or used to be a citizen of the following countries
    </label>

    <textarea rows="5" name="other_citizenship" placeholder="Details here...">{{{$profile->other_citizenship or ''}}}</textarea>
</div>

<div class="three fields">
    <div class="required field">
        <label>Current Address Street 1</label>
        <input type="text" name="cur_street1" placeholder="address line 1" value="{{{$curAddr->street1 or ''}}}">
    </div>
    <div class="field">
        <label>Current Address Street 2</label>
        <input type="text" name="cur_street2" placeholder="address line 2 (optional)" value="{{{$curAddr->street2 or ''}}}">
    </div>
    <div class="required field">
        <label>Date Since</label>
        @if($curDateSince)
            @include('modules._inline_datebox_noday', $data = ['cur_since', $curDateSince[0], $curDateSince[1]])
        @else
            @include('modules._inline_datebox_noday', $data = ['cur_since'])
        @endif
    </div>
</div>

<div class="three fields">
    <div class="field">
        @if(!empty($curAddr))
            @include('modules._countries_dropdown', $data = ['label' => 'Current Address country', 'name' => 'cur_country', 'title' => 'country', 'value' => $curAddr->country])
        @else
            @include('modules._countries_dropdown', $data = ['label' => 'Current Address country', 'name' => 'cur_country', 'title' => 'country'])
        @endif
    </div>
    <div class="required field">
        <label>Current Address City/Town</label>
        <input type="text" name="cur_town" placeholder="town/city" value="{{{$curAddr->town or ''}}}">
    </div>
    <div class="required field">
        <label>Current Address Postal Code</label>
        <input type="text" name="cur_post_code" placeholder="postal code" value="{{{$curAddr->postal_code or ''}}}">
    </div>
</div>

<div class="inline fields">
    <div class="inline field">
        <div class="ui same_add checkbox">
            <input type="checkbox" name="same_addr" id="same_addr" tabindex="0" class="hidden">
            <label>Permanent Residential Address is the same as Current Address</label>
        </div>
    </div>
</div>

<div class="field" id="res_addr">
    <div class="three fields">
        <div class="field">
            <label>Permanent Residential Address Street 1</label>
            <input type="text" name="res_street1" placeholder="address line 1" value="{{{$profile->addresses()->where('address_type', 'res')->get()->first()->street1 or ''}}}">
        </div>
        <div class="field">
            <label>Permanent Residential Address Street 2</label>
            <input type="text" name="res_street2" placeholder="address line 2 (optional)" value="{{{$profile->addresses()->where('address_type', 'res')->get()->first()->street2 or ''}}}">
        </div>
        <div class="field">
            <label>Date Since</label>
            @include('modules._inline_datebox_noday', $data = ['res_since'])
        </div>
    </div>

    <div class="three fields">
        <div class="field">
            @if(!empty($profile->addresses()->where('address_type', 'res')->get()->first()))
                @include('modules._countries_dropdown', $data = ['label' => 'Permanent Residential Address country', 'name' => 'res_country', 'title' => 'country', 'value' => $profile->addresses()->where('address_type', 'res')->get()->first()->country])
            @else
                @include('modules._countries_dropdown', $data = ['label' => 'Permanent Residential Address country', 'name' => 'res_country', 'title' => 'country'])
            @endif
        </div>
        <div class="field">
            <label>Permanent Residential Address City/Town</label>
            <input type="text" name="res_town" placeholder="town/city" value="{{{$profile->addresses()->where('address_type', 'res')->get()->first()->town or ''}}}">
        </div>
        <div class="field">
            <label>Permanent Residential Address Postal Code</label>
            <input type="text" name="res_post_code" placeholder="postal code" value="{{{$profile->addresses()->where('address_type', 'res')->get()->first()->postal_code or ''}}}">
        </div>
    </div>
</div>

<div class="required field">
    <label>
        <i class="inline info circle icon" data-title="Please Note" data-content="Residences should include, witout limitation, any place where they have lived for a period of 6 months or more"></i>
        List all addresses where you have lived in the past 10 years
    </label>

    <div class="ui middle aligned address selection list">
        @foreach($profile->addresses->where('address_type', 'ten') as $address)

            <div class="item {{$address->address_id}}" data-id="{{$address->address_id}}">
                <div class="right floated content">
                    <div class="ui blue inverted address edit button" id="{{$address->address_id}}">Edit</div>
                    <div class="ui red inverted address delete button" id="{{$address->address_id}}">Delete</div>
                </div>
                <i class="ui avatar image marker icon"></i>
                <div class="content">{{$address->street1}} - {{$address->town}} -  {{$address->country}}</div>
            </div>

        @endforeach
    </div>
    <a href="#" id="add_address"><i class="add circle icon"></i>Click here to add Address</a>
</div>

<div class="field">
    <label>Please list languages that you can read, understand, speak and / or write fluently</label>
    @include('modules._languages_select_dropdown')
</div>

<div class="required field">
    <label>
        Give Details of all the education you have had and qualifications you have obtained
    </label>

    <div class="ui middle aligned qual selection list" id="qual_list">
        @foreach($profile->qualifications as $qual)
            <div class="item {{$qual->qualification_id}}" data-id="{{$qual->qualification_id}}">
                <div class="right floated content">
                    <div class="ui blue inverted qual edit button" id="{{$qual->qualification_id}}">Edit</div>
                    <div class="ui red inverted qual delete button" id="{{$qual->qualification_id}}">Delete</div>
                </div>
                <i class="ui avatar image university icon"></i>
                <div class="content">{{$qual->qualification_name}} ({{$qual->qualification_type}} From: {{$qual->from_date}} To: {{$qual->to_date}})</div>
            </div>
        @endforeach
    </div>
    <a href="#" id="add_qualification"><i class="add circle icon"></i>Click here to add Qualification</a>
</div>

<div class="three fields">

    <div class="required field">
        <label>Personal Landlne number</label>
        <input type="text" name="personal_landline_no" placeholder="telephone number" value="{{{$profile->personal_landline_no or ''}}}">
    </div>

    <div class="required field">
        <label>Persoanl Mobile number</label>
        <input type="text" name="personal_mobile_no" placeholder="mobile number" value="{{{$profile->personal_mobile_no or ''}}}">
    </div>

    <div class="required field">
        <label>Personal email address</label>
        <input type="text" name="personal_email_address" placeholder="email address" value="{{{$profile->personal_email_address or ''}}}">
    </div>

</div>

<div class="field" id="passports">
    <label>Additional Passports Information</label>
    <input type="hidden" name="has_pass" id="has_pass" value="{{ (count($passports) > 0) ? count($passports) : 0 }}" />
    <div class="ui middle aligned main-passport selection list">
        @if(count($passports) > 0)
            @foreach($passports as $passport)

            <div class="item {{$passport->identity_document_id}}" data-id="{{$passport->identity_document_id}}">
                <div class="right floated content">
                    <div class="ui blue inverted edit_passport edit button" id="{{$passport->identity_document_id}}">Edit</div>
                    <div class="ui red inverted del_passport delete button" id="{{$passport->identity_document_id}}">Delete</div>
                </div>
                <i class="ui avatar image book icon"></i>
                <div class="content">{{$passport->issuing_country}} Passport </div>
            </div>

            @endforeach
        @endif
    </div>

    <a href="#" id="addPassport"><i class="add circle icon"></i> Click here to add Additional Passport Information</a>
</div>

<div class="inline fields">
    <div class="inline field">
        <div class="ui military checkbox">
            <input type="checkbox" name="military" id="military" tabindex="0" class="hidden">
            <label>Have you ever served in any armed forces ?</label>
        </div>
    </div>
</div>

<div class="field military hide">
    <div class="required field">
        <label>Branch</label>
        <input type="text" name="branch" value="{{{$milInfo->branch or ''}}}" />
    </div>
    <div class="two fields">
        <div class="required field">
            <label>Date of entry active service</label>
            @if($milInfo)
                @include('modules._inline_datebox', $data = ['active_service_dt', $milInfo->active_service_dt])
            @else
                @include('modules._inline_datebox', $data = ['active_service_dt'])
            @endif

        </div>
        <div class="required field">
            <label>Date of seperation</label>
            @if($milInfo)
                @include('modules._inline_datebox', $data = ['seperation_dt', $milInfo->seperation_dt])
            @else
                @include('modules._inline_datebox', $data = ['seperation_dt'])
            @endif
        </div>
    </div>
    <div class="two fields">
        <div class="required field">
            <label>Type of discharge</label>
            <input type="text" name="type_of_discharge" value="{{{$milInfo->type_of_discharge or ''}}}" />
        </div>
        <div class="required field">
            <label>Ranking at seperation</label>
            <input type="text" name="ranking_at_seperation" value="{{{$milInfo->ranking_at_seperation or ''}}}" />
        </div>
    </div>
    <div class="required field">
        <label>Serial Number</label>
        <input type="text" name="serial_no" value="{{{$milInfo->serial_no or ''}}}" />
    </div>
    <div class="inline fields">
        <div class="inline field">
            <div class="ui offence checkbox">
                <input type="checkbox" name="offence_check" id="offence_check" tabindex="0" class="hidden">
                <label>Where you ever arrested for an offence, which resulted in summary action, a trial, or special or general court material ?</label>
            </div>
        </div>
    </div>
    <div class="field offence hide">
        <label>Please provide message</label>
        <textarea name="offence" rows="3">{{{$milInfo->offence or ''}}}</textarea>
    </div>
</div>
