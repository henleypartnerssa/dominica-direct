@extends('app_panel_no_right')

@section('title', 'Program Costs')

@section('stepper')
    @include('modules._stepping', $data = App\Util\AppHelpers::stepperLinks($pa->profile_id, $pa->application->application_id))
@stop

@section('content')

<h2 class="heading admin">Program Costs - {{$app->type}}</h2>

<div class="content">
    <p id="admin">Please find all Costs related to this Program listed below. Note that this page only indicates the costs involved when applying for Antigua and Barbuda programs and not any form agreement between yourself and Newlandsglobal.</p>
</div>

<div class="ui stacked segment">
    <div class="ui info message" style="display:{{$message}}">
      <i class="close icon"></i>
      <div class="header">
        Sent Email
      </div>
      <p>Please check your email account. We have sent you a email with an Quotation attachment file.</p>
    </div>
    <table class="ui celled table">
        <thead>
            <tr class="newlands-bg"><th colspan="3"><h3>Total Applicants</h3></th></tr>
        </thead>
        <tbody>
            <tr>
                <td class="six wide">Main Applicant</td>
                <td>{{$pa->first_name." ".$pa->last_name}}</td>
            </tr>
            <tr>
                <td class="six wide">Spouse</td>
                <td>{{($costs->spouse > 0) ? 'Yes' : 'No'}}</td>
            </tr>
            <tr>
                <td class="six wide">Dependent 0 - 11 years</td>
                <td>{{($costs->child_0_11 + $costs->child_12)}}</td>
            </tr>
            <tr>
                <td class="six wide">Dependent 12 - 16 years</td>
                <td>{{($costs->child_12 + $costs->child_13_15 + $costs->child_16_17)}}</td>
            </tr>
            <tr>
                <td class="six wide">Dependent 17 - 18 years</td>
                <td>{{($costs->child_16_17 + $costs->child_18_25)}}</td>
            </tr>
            <tr>
                <td class="six wide">Dependent 18+</td>
                <td>{{($costs->child_18_25 + $costs->parent_55_64 + $costs->parent_65)}}</td>
            </tr>
        </tbody>
    </table>
    <div class="breaker-10"></div>
    <table class="ui celled table">
        <thead>
            <tr><th colspan="3"><h3>Total Investment</h3></th></tr>
        </thead>
        <tbody>
            <tr>
                <td class="six wide">Contribution</td>
                <td>USD {{$costs->nonRefContributions()}}</td>
            </tr>
            <tr>
                <td class="six wide">Real Estate</td>
                <td>USD {{$costs->RealEstatePurchase()}}</td>
            </tr>
            <tr>
                <td class="six wide">Other</td>
                <td>N/A</td>
            </tr>
            <tr class="active">
                <td class="six wide right aligned"><b>Sub Total</b></td>
                <td><b>USD {{$costs->qRequirementsTotal()}}</b></td>
            </tr>
        </tbody>
    </table>
    <div class="breaker-10"></div>
    <table class="ui celled table">
        <thead>
            <tr><th colspan="3"><h3>Required Fees</h3></th></tr>
        </thead>
        <tbody>
            <tr>
                <td class="six wide">Government Fees</td>
                <td>USD {{$costs->GovernmentFees()}}</td>
            </tr>
            <tr>
                <td class="six wide">Government Application Fees</td>
                <td>USD {{$costs->professionalFees()}}</td>
            </tr>
            <tr>
                <td class="six wide">Government Processing Fees</td>
                <td>USD 400.000</td>
            </tr>
            <tr>
                <td class="six wide">Government Due Diligence Fees</td>
                <td>USD {{$costs->govDueDilFees()}}</td>
            </tr>
            <tr>
                <td class="six wide">Government Passport Fees</td>
                <td>USD {{App\Util\AppHelpers::normQuoteTotal($costs->cardFees())}}</td>
            </tr>
            <tr>
                <td class="six wide">Government Certificate Issuance Fees</td>
                <td>USD 0</td>
            </tr>
            <tr>
                <td class="six wide">Newlands Professional Service Fees</td>
                <td>USD 10.000</td>
            </tr>
            <tr class="active">
                <td class="six wide right aligned"><b>Sub Total</b></td>
                <td><b>USD {{$costs->otherAppCostTotal()}}</b></td>
            </tr>
        </tbody>
    </table>
    <div class="breaker-10"></div>
    <table class="ui celled table">
    <tbody>
        <tr class="active">
            <td class="six wide right aligned"><b>Total</b></td>
            <td><b>USD {{$costs->finalTotal()}}</b></td>
        </tr>
    </tbody>
</table>

<div class="ui form">
    <div class="three fields">
        <div class="field">
            <a class="ui back fluid blue left labeled icon button" href="/users/application/select-program">
                Select Program
                <i class="large angle left icon"></i>
            </a>
        </div>
        <div class="field">
            <button class="ui fluid grey send-quote button">
                Send Quote
            </button>
        </div>
        <div class="field">
            <a class="ui next fluid blue right labeled icon button" href="/users/profile/{{$pa->profile_id}}/application/{{$pa->application->application_id}}/personal-data">
                Continue
                <i class="large angle right icon"></i>
            </a>
        </div>
    </div>
</div>

</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/file_upload.js') }}"></script>
    <script src="{{ URL::asset('js/app_file_manager.js') }}"></script>
    <script src="{{ URL::asset('js/important_info.js') }}"></script>
@stop
