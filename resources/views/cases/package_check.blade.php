@extends('app_panel_no_right')

@section('title', 'Package Due Diligance Check')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">Package Due Diligence Check</h2>

<div class="content">
    <p id="admin">Your application has been received by our office and are currently being reviewed to ensure that all the documents and requirements are done correctly.  You will be notified via sms and email once your application for citizenship  has been submitted to the CIU.  Please note that once your application has been submitted, the due diligence process takes approximately 90 days to be completed.</p>

    <div class="ui submission form">

        <div class="three fields">
            <div class="field">
                <a href="{{$backLink}}" class="ui next fluid blue left labeled icon button">
                    Back to Uploaded Documents
                    <i class="large angle left icon"></i>
                </a>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <a href="{{$fwdLink}}" class="ui next fluid blue right labeled icon {{$state}} button">
                    Next Form
                    <i class="large angle right icon"></i>
                </a>
            </div>
        </div>
    </div>

</div>

@stop

@section('script')
@stop
