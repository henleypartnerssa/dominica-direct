@extends('app_panel_no_right')

@section('title', 'Terms and Conditions')

@section('stepper')
    @include('modules._stepping')
@stop

@section('content')

<h2 class="heading admin">{{$profile->first_name}} {{$profile->last_name}} - Important Information</h2>

<div class="content">
    <p id="admin">Please fill out the data for each dependant. Information provided will be used to conclude Due Diligence check.</p>
</div>

<div class="ui stacked segment">
    @if($user->profiles()->count() > 4)
        @include('modules._key_info_guide_split')
    @else
        @include('modules._key_info_guide')
    @endif

    <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />

    <div class="ui important-info form">
        {!! Form::open(['id' => 'impInfo-form'])!!}

        <input type="hidden" name="profile_id" id="profile_id" style="display: none" value="{{{$profile->profile_id or ''}}}" />
        <input type="hidden" name="save" id="save" style="display: none" value="0" />
        <input type="hidden" id="age" style="display: none" value="{{$age}}" />

        <div id="file_inputs">
            @if(count($passFiles) > 0)
                @foreach($passFiles as $fileItem)
                    <input type="file" name="{{$fileItem['key']}}" id="{{$fileItem['key']}}">
                @endforeach
            @endif
            @if(count($resFiles) > 0)
                @foreach($resFiles as $fileItem)
                    <input type="file" name="{{$fileItem['key']}}" id="{{$fileItem['key']}}">
                @endforeach
            @endif
        </div>

        <div class="two fields">
            <div class="required field">
                <label>Surname</label>
                <input type="text" name="surname" value="{{{$profile->last_name or ''}}}">
            </div>
            <div class="field">
                <label>Former/Maiden Names</label>
                <input type="text" name="other_names" value="{{{$profile->maiden_name or ''}}}">
            </div>
        </div>

        <div class="two fields">
            <div class="required field">
                <label>Given Name(s)</label>
                <input type="text" name="given_names" value="{{{$profile->first_name or ''}}}">
            </div>
            <div class="required field">
                <label>Gender</label>

                <div class="ui fluid search selection dropdown">
                    <input type="hidden" name="gender" id="gender" value="{{{$profile->gender or ''}}}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Gender</div>
                        <div class="menu">
                            <div class="item" data-value="male"><i class="man icon"></i>Male</div>
                            <div class="item" data-value="female"><i class="woman icon"></i>Female</div>
                        </div>
                </div>
            </div>
        </div>

        <div class="two fields">
            <div class="required field">
                <label>Date of Birth</label>
                    @include('modules._inline_datebox', $data = ['dob', $profile->date_of_birth])
            </div>
            <div class="required field">
                <label>Nationality</label>

                @include('modules._countries_dropdown', $data = ['name' => 'nationality', 'title' => 'Your Nationality', 'value' => (!empty($profile->country_of_birth)) ? $profile->country_of_birth : ''])
            </div>
        </div>

        <div class="field">
            <div class="personalHolder"  style="{{($age <= 17) ? 'display:none' : 'display:block'}}">
                <div class="two fields">
                    <div class="required field">
                        <label>Personal Email Address</label>
                        <input type="text" name="email" value="{{{$profile->personal_email_address or ''}}}"/>
                    </div>
                    <div class="required field">
                        <label>Personal Contact Number</label>
                        <input type="text" name="contact_no" value="{{{$profile->personal_mobile_no or ''}}}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="two fields">
            <div class="required field">
                <label>Passport No</label>
                <input type="text" name="pass_no" placeholder="Passport Number" value="{{{$passport->document_number or ''}}}">
            </div>
            <div class="required field">
                <label>Passport Country of Issue</label>
                @include('modules._countries_dropdown', $data = ['name' => 'pass_country', 'title' => 'Country of Issue', 'value' => (!empty($passport->issuing_country)) ? $passport->issuing_country : ''])
            </div>
        </div>

        <div class="field">
            <label>Passport Place of Issue</label>
            <input type="text" name="pass_place" placeholder="Passports Place of Issue" value="{{{$passport->place_of_issue or ''}}}" />
        </div>

        <div class="two fields">
            <div class="required field">
                <label>Passport Date Issued</label>
                @include('modules._inline_datebox', $data = ['pass_issued', (!empty($passport->date_of_issue)) ? $passport->date_of_issue : ''])
            </div>
            <div class="required field">
                <label>Passport Expiry Date</label>
                @include('modules._inline_datebox', $data = ['pass_exp', (!empty($passport->date_of_expiration)) ? $passport->date_of_expiration : ''])
            </div>
        </div>

        <div class="field">
            <input type="hidden" id="total_passport" style="display: none" name="total_passport" value="{{{count($passFiles) or '0'}}}" />
            <label id="passport">Upload Certified copy of your Passport</label>
            <div class="ui middle aligned passport selection list">
                @if(count($passFiles) > 0)
                    @foreach($passFiles as $fileItem)
                        <div class="item {{$fileItem['key']}}">
                            <div class="right floated content">
                                <div class="ui button delete green" id="button-{{$fileItem['key']}}" data-fileName="{{$fileItem['file']}}">Delete</div>
                            </div>
                            <i class="ui avatar image book icon"></i>
                            <div class="content {{$fileItem['key']}}">File Name: {{$fileItem['file']}}</div>
                        </div>
                    @endforeach
                @endif
            </div>
            <a href="#" id="add_passport">
                <i class="add upload icon"></i>Click here to upload Passport
            </a>
            <div class="ui red pass message" style="display:none">Please upload Certified copy of your Passport</div>
        </div>

        <div class="field">
            <div class="greenCardHolder" style="{{($profile->belongs_to !== 'Principal Applicant') ? 'display:none' : 'display:block'}}">
                <div class="two fields">
                    <div class="field">
                        <label>Residency Card Held</label>
                        <input type="text" name="card_no" id="card_no" placeholder="Residency Card Number" value="{{{$greenCard->document_number or ''}}}">
                    </div>
                    <div class="field">
                        <label>Residency Card Country of Issue</label>
                        @include('modules._countries_dropdown', $data = ['name' => 'card_country', 'title' => 'Residence Card Country of Issue', 'value' => (!empty($greenCard->issuing_country)) ? $greenCard->issuing_country : ''])
                    </div>
                </div>
                <div class="field">
                    <label>Residency Card Place of Issue</label>
                    <input type="text" name="card_place" placeholder="Residency Card Place of Issue" value="{{{$greenCard->place_of_issue or ''}}}" />
                </div>
                <div class="two fields">
                    <div class="field">
                        <label>Residency Card Issued Date</label>
                        @include('modules._inline_datebox', $data = ['card_issued', (!empty($greenCard->date_of_issue)) ? $greenCard->date_of_issue : ''])
                    </div>
                    <div class="field">
                        <label>Residency Card Expiry Date</label>
                        @include('modules._inline_datebox', $data = ['card_exp', (!empty($greenCard->date_of_expiration)) ? $greenCard->date_of_expiration : ''])
                    </div>
                </div>

                <div class="field">
                    <label id="residency">Upload Certified copy of your Residency Card</label>
                    <input type="hidden" id="total_residency" style="display: none" value="{{{count($resFiles) or '0'}}}" />
                    <div class="ui middle aligned residency selection list">
                        @if(count($resFiles) > 0)
                            @foreach($resFiles as $fileItem)
                                <div class="item {{$fileItem['key']}}">
                                    <div class="right floated content">
                                        <div class="ui button delete green" id="button-{{$fileItem['key']}}" data-fileName="{{$fileItem['file']}}">Delete</div>
                                    </div>
                                    <i class="ui avatar image book icon"></i>
                                    <div class="content {{$fileItem['key']}}">File Name: {{$fileItem['file']}}</div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <a href="#" id="add_residency">
                        <i class="add upload icon"></i>Click here to upload Residency Card
                    </a>
                    <div class="ui red res message" style="display:none">Please upload Certified copy of your Residency Card</div>
                </div>
            </div>
        </div>

        <div class="field">
            <div class="business_holder" style="{{($age <= 17) ? 'display:none' : 'display:block'}}">
                <div class="required field">
                    <label>Current residential address</label>
                    <div class="two fields">
                        <div class="field">
                            <input type="text" name="cur_street1" placeholder="address line 1" value="{{{$addr->street1 or ''}}}">
                        </div>
                        <div class="field">
                            <input type="text" name="cur_street2" placeholder="address line 2 (optional)" value="{{{$addr->street2 or ''}}}">
                        </div>
                    </div>

                    <div class="three fields">
                        <div class="field">
                            @include('modules._countries_dropdown', $data = ['name' => 'cur_country', 'title' => 'country', 'value' => (!empty($addr->country)) ? $addr->country : ''])
                        </div>
                        <div class="field">
                            <input type="text" name="cur_town" placeholder="town/city" value="{{{$addr->town or ''}}}">
                        </div>
                        <div class="field">
                            <input type="text" name="cur_post_code" placeholder="postal code" value="{{{$addr->postal_code or ''}}}">
                        </div>
                    </div>
                    <div class="three fields">
                        <div class="required field">
                            <label>Self Employed ?</label>
                            <div class="ui fluid search selection dropdown">
                                <input type="hidden" name="employment" id="employment" value="{{$employed}}">
                                <i class="dropdown icon"></i>
                                <div class="default text">Select Level</div>
                                    <div class="menu">
                                        <div class="item" data-value="1">Yes</div>
                                        <div class="item" data-value="0">No</div>
                                    </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label>Occupation</label>
                            <input type="text" name="occupation" placeholder="Your Occupation" value="{{{$work->primary_occupation or ''}}}">
                        </div>
                        <div class="field">
                            <label>Business or Employer’s Company Name</label>
                            <input type="text" name="business" placeholder="Name of the business" value="{{{$work->name_of_business or ''}}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="three fields">
            <div class="field">
                <button class="ui save fluid blue left labeled icon button">
                    Save
                    <i class="small upload icon"></i>
                </button>
            </div>
            <div class="field">
            </div>
            <div class="field">
                <button class="ui next fluid blue right labeled icon button">
                    Next
                    <i class="large angle right icon"></i>
                </button>
            </div>
        </div>

        {!! Form::close()!!}
    </div>

</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/file_upload.js') }}"></script>
    <script src="{{ URL::asset('js/file_manager.js') }}"></script>
    <script src="{{ URL::asset('js/important_info.js') }}"></script>
@stop
