<div class="ui middle aligned center aligned grid">
    <div class="row" style="padding-bottom:0">
        <div class="column" style="border-top: 3px solid #000;border-bottom: 1px solid #000;padding-top: 0;padding-bottom: 1px ">

            <div class="ui compact secondary pointing menu">

                <a class="item" href="/users/dashboard">
                    <i class="file text outline icon"></i>
                    Application Summary
                </a>

                @if(!empty($data))
                    <a class="item" href="{{{ $data['url'] or Request::getrequestUri() }}}">
                        <i class="edit icon"></i>
                        Application Process
                    </a>
                @endif

                <div class="ui dropdown item">
                    <i class="file pdf outline icon"></i>
                    Documents
                    <div class="menu">
                        <a class="item" href="/users/filesystem/list-documents/{{($u->profiles()->count() > 0) ? $u->profiles()->where('belongs_to', 'Principal Applicant')->first()->profile_id : ''}}">View Documents</a>
                        <a class="item" href="/users/documents/guide">Documents Guide</a>
                    </div>
                </div>

                <div class="ui dropdown item">
                    <i class="mail icon"></i>
                    Messages Inbox ({{$u->messages()->where('read', false)->get()->count()}})

                    <div class="menu">
                        <a class="item" href="/users/messaging/create">Create Message</a>
                        <a class="item" href="/users/messaging/messages">Your Messages</a>
                    </div>
                </div>

                <a class="item" href="/users/account">
                    <i class="user icon"></i>
                    Account
                </a>

            </div>

        </div>
    </div>
</div>
