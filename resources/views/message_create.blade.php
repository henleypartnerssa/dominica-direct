@extends('app_panel')

@section('title', 'Create message')

@section('content')

<h4 class="ui horizontal divider header">
    New Message
</h4>

<div class="ui large create-message form">
{!! Form::open() !!}
    <input type="hidden" name="app" id="app" value="{{{$app or '0'}}}" />
    <div class="field">
        <input type="text" name="subject" placeholder="Subject message will relate to">
    </div>
    <div class="field">
        <textarea name="message" placeholder="Insert message here..."></textarea>
    </div>
    <div class="three fields">
        <div class="field">
        </div>
        <div class="field">
        </div>
        <div class="field">
            <button class="ui blue button" style="float:right"><i class="reply icon"></i>Send Message</button>
        </div>
    </div>

{!! Form::close() !!}
</div>
@stop
