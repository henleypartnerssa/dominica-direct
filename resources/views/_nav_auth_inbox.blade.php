<div class="ui menu">

    <a class="header item" href="/users/dashboard"><i class="home icon"></i></a>

    <div class="ui dropdown item">

            <i class="payment icon"></i>
            Quote


        <div class="menu">
            <a class="item" id="a_quote">Aquire Quote</a>
            <a class="item" href="/quotations">Quote History</a>
        </div>
    </div>

    <div class="ui dropdown item">

        <i class="edit icon"></i>
        Cases

        <div class="menu">
            <a class="item" href="/users/cases/select-program">Create New Case</a>
            <a class="item" href="">List Cases</a>
        </div>
    </div>

    <a  class="item" href="">
        <i class="file pdf outline icon"></i>
        Documents
    </a>


    <a class="item" href="">
        <i class="mail icon"></i>
        Message Center
    </a>

    <div class="right menu">

        <div class="item">
            <p><i class="mail outline icon"></i> <b><a href="#">Inbox (0)</a></b></p>
        </div>

        <div class="item">
            <a href="/auth/logout" class="ui primary login button">Log-out</a>
        </div>


        <div class="item">
            <div class="ui floating dropdown labeled search icon button">
                <i class="world icon"></i>
                <span class="text">Language</span>
                <div class="menu">
                    <div class="item">Arabic</div>
                    <div class="item">Chinese</div>
                    <div class="item">Danish</div>
                    <div class="item">Dutch</div>
                    <div class="item">English</div>
                    <div class="item">French</div>
                    <div class="item">German</div>
                    <div class="item">Greek</div>
                    <div class="item">Hungarian</div>
                    <div class="item">Italian</div>
                    <div class="item">Japanese</div>
                    <div class="item">Korean</div>
                    <div class="item">Lithuanian</div>
                    <div class="item">Persian</div>
                    <div class="item">Polish</div>
                    <div class="item">Portuguese</div>
                    <div class="item">Russian</div>
                    <div class="item">Spanish</div>
                    <div class="item">Swedish</div>
                    <div class="item">Turkish</div>
                    <div class="item">Vietnamese</div>
                </div>
            </div>
        </div>

    </div>

</div>
