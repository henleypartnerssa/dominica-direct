<div class="ui menu newlands-bg b-none s-none" style="margin-bottom:0;height:50px">

    <div class="ui container">

        <div class="item topnav-left">
            <img src="{{asset('img/newlands_logo_brown.png')}}" />
            <span class="tel">+44 0 333 44-444 55</span>
        </div>

        <div class="right menu">

            <div class="item" style="color:#fff">
                <h5>Welcome &nbsp; {{ $u->name }} {{ $u->surname }} </h5>
            </div>

            <a class="item home-link" href="/users/logout">
                LOG OUT >
            </a>

        </div>
    </div>

</div>
