<!DOCTYPE html>
<html>
    <head>
        <title>Dominica Direct - @yield('title')</title>

        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ URL::asset('js/jquery-1.12.0.min.js') }}"></script>
        <script src="{{ URL::asset('semantic-ui/semantic.min.js') }}"></script>
        <script src="{{ URL::asset('js/api.js') }}"></script>
        <script src="{{ URL::asset('js/general.js') }}"></script>

    </head>
    <body>

        @include('_header')
        <div class="breaker">

        </div>
        <div class="ui container">
            @yield('content')
        </div>

        <div class="ui coupled modal">
            @include('modals._verification')
            @yield('modal');
        </div>

        @if(!Auth::check())
            @include('_footer')
        @endif

        @yield('script')

    </body>

</html>
