<!DOCTYPE html>
<html>
    <head>
        <title>Henley Direct - @yield('title')</title>

        <link href="{{ URL::asset('semantic-ui/semantic.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="{{ URL::asset('semantic-ui/semantic.min.js') }}"></script>

    </head>
    <body>
        @include('admin._top')

        <div class="header" style="width:1px;height:60px;">

        </div>

        <div class="ui main container">
            @yield('content')
        </div>

        @yield('script')

    </body>

</html>
