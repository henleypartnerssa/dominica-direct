@extends('admin.app')

@section('title', 'Dashboard')

@section('content')
    <div class="ui container">
        <h3 class="ui center aligned block header">
          All Messages
        </h3>

        <div class="ui middle aligned selection list">

            @foreach($listMessages as $message)
            <a class="item" style="text-align: left" id="{{$message['id']}}" href="/admin/messages/{{$message['id']}}">
                <div class="right floated content">
                    <small style="float:right"><b>{{$message['timestamp']}}</b></small>
                </div>
                @if($message['read'] === false)
                    <i class="ui avatar image mail icon"></i>
                @else
                    <i class="ui avatar image mail outline icon"></i>
                @endif
                <div class="content">
                    <div class="header">From: {{$message['from']}}</div>
                    <div class="description">Subject: {{$message['subject']}}</div>
                </div>
            </a>
            @endforeach
        </div>

    </div>
@stop
