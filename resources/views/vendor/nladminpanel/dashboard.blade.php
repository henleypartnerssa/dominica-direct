@extends('admin.app')

@section('title', 'Dashboard')

@section('content')
    <div class="ui container">
        <h3 class="ui center aligned block header">
          All Cases
        </h3>
        <table class="ui compact celled definition table">
          <thead>
              <tr>
                <th></th>
                <th>Client Number</th>
                <th>Client Name</th>
                <th>Current Step</th>
                <th>DD Checks Completed</th>
                <th>Retainer Received</th>
                <th>Data Reviewed</th>
                <th>Balance of Fees Paid</th>
                <th>Original Docs Received</th>
                <th>Sent to Dominica</th>
                <th>Submitted to Gov.</th>
                <th>CIU Approved</th>
                <th>Investment Compl.</th>
                <th>oath</th>
                <th>Passport Issued</th>
              </tr>
          </thead>
          <tbody>
              @foreach($apps as $app)
                <tr>
                    <td class="collapsing">
                        <a class="ui green view button" href="/admin/application/{{$app->application_id}}">View</a>
                    </td>
                    <td>{{$app->case_no}}</td>
                    <td>{{$app->profiles()->where('belongs_to', 'Principal Applicant')->first()->first_name}} {{$app->profiles()->where('belongs_to', 'Principal Applicant')->first()->last_name}}</td>
                    <td class="center aligned">{{$app->step}}</td>

                    @if($app->dd_check === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->retainer_paid === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->data_review === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->balance_paid === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->docs_received === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->sent_to_dominica === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->submitted_to_gov === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->ciu_approved === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->investment_completed === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->oath === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                    @if($app->passport_issued === true)
                        <td class="center aligned"><i class="large green checkmark icon"></i></td>
                    @else
                        <td class="center aligned error">No</td>
                    @endif

                </tr>
              @endforeach
          </tbody>
        </table>
    </div>
@stop
