<!DOCTYPE html>
<html>
    <head>
        <title>Henley Direct - Admin Login</title>

        <link href="{{ URL::asset('semantic-ui/semantic.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="{{ URL::asset('semantic-ui/semantic.min.js') }}"></script>

        <style>
            html, body {
                height: 100%;
            }
            body > .grid {
                height: 100%;
            }
        </style>
    </head>
    <body>

        <div class="ui text container">
            <div class="ui middle aligned center aligned grid" style="padding:0;margin:0;">
              <div class="column">
                <h2 class="ui image header">
                  <a href="/"><img src="img/hd_logo.jpg" class="image"></a>
                </h2>
                <div class="ui large login form">
                <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />
                  <div class="ui stacked segment">
                    <div class="field">
                      <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="username" placeholder="E-mail address">
                      </div>
                    </div>
                    <div class="field">
                      <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password">
                      </div>
                    </div>
                    <div class="fields">
                        <div class="twelve wide field">
                            <div class="ui left icon input">
                                <i class="ellipsis horizontal icon"></i>
                                <input type="number" name="otp" placeholder="OTP">
                            </div>
                        </div>
                        <div class="four wide field">
                            <button class="ui fluid large green pin button">Send Pin</button>
                        </div>
                    </div>
                    <button class="ui fluid large blue submit button">Login</button>
                  </div>
                  <div class="ui error message">
                      <div class="header"></div>
                      <p></p>
                  </div>
                </div>
            </div>
        </div>
        <script src="{{ URL::asset('js/admin/login.js') }}"></script>
    </body>

</html>
