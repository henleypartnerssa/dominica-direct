<div class="ui fixed inverted menu">
    <div class="ui container">
      <a href="/admin" class="header item">
        Henley Direct
      </a>
      <!--div class="ui simple dropdown item">
        Cases <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="#">All Cases</a>
          <a class="item" href="#">Pending Cases</a>
          <a class="item" href="#">Complete Cases</a>
        </div>
    </div-->

      <div class="right menu">
          <a class="item" href="/admin/messages">
              inbox ({{App\Message::where('sent_to', Auth::id())->where('read', false)->count()}})
          </a>
          <a class="item" href="/admin/auth/logout">
              logout
          </a>
      </div>

    </div>
  </div>
