<div class="ui visible left demo vertical inverted sidebar labeled icon menu">
  <a class="{{($data['link'] === 'data') ? 'active' : ''}} item" href="/admin/case/{{$profile->profile_id}}/{{$data['appId']}}">
    <i class="edit icon"></i>
    Applicant Data
  </a>
  <a class="{{($data['link'] === 'dep') ? 'active' : ''}} item" href="/admin/case/{{$profile->profile_id}}/{{$data['appId']}}/dependants">
    <i class="users icon"></i>
    Dependants
  </a>
  <a class="{{($data['link'] === 'doc') ? 'active' : ''}} item" href="/admin/case/{{$profile->profile_id}}/{{$data['appId']}}/documents">
    <i class="folder open outline icon"></i>
    Documents
  </a>
  <a class="item">
    <i class="edit icon"></i>
    Case Invoices
  </a>
  <a class="{{($data['link'] === 'message') ? 'active' : ''}} item" href="/admin/case/{{$profile->profile_id}}/{{$data['appId']}}/messages">
    <i class="mail outline icon"></i>
    Case Messages
  </a>
  <a class="{{($data['link'] === 'check') ? 'active' : ''}} item" href="/admin/case/{{$profile->profile_id}}/{{$data['appId']}}/approvals">
    <i class="checkmark icon"></i>
    Case Approvals
  </a>
</div>
