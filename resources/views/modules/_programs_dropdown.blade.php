<div class="ui fluid search selection dropdown">
  <input type="hidden" name="programs">
  <i class="dropdown icon"></i>
  <div class="default text">Select a Program</div>
  <div class="menu">
        <div class="item" data-value="contribution"><i class="dm flag"></i>Dominica <b>(Contribution)</b></div>
        <div class="item" data-value="real-estate"><i class="dm flag"></i>Dominica <b>(Real Estate)</b></div>
  </div>
</div>
