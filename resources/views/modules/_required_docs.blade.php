<div class="ui grid">
    <div class="row">
        <div class="eight wide column">
            <div class="ui greyborder piled segment">
                <h4 class="ui header">Example Information</h4>
                <div class="text-left content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.:</p>
                    <div class="ui list">
                        <div class="item">
                            <i class="file icon"></i>
                            <div class="content">
                              <a class="header">Example One</a>
                              <div class="description">Aliquam vulputate felis</div>
                            </div>
                        </div>
                        <div class="item">
                            <i class="file icon"></i>
                            <div class="content">
                              <a class="header">Example Two</a>
                              <div class="description">Aliquam vulputate felis</div>
                            </div>
                        </div>
                        <div class="item">
                            <i class="file icon"></i>
                            <div class="content">
                              <a class="header">Example Three</a>
                              <div class="description">Aliquam vulputate felis</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eight wide piled column">
            <div class="ui greyborder segment">
                <h4 class="ui header">Security Features</h4>
                <div class="text-left content">
                    <div class="ui list">
                        <div class="item">
                            <div class="content">
                              <div class="header">Uploading Files</div>
                              <div class="description">Each PDF file you upload will be encrypted and only accessable by the password we SMS'd via Registration process.</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                              <div class="header">Downloading Files</div>
                              <div class="description">Each PDF file you download will be encrypted and only accessable by the password we SMS'd via Registration process.</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                              <div class="header">SSL Certification</div>
                              <div class="description">This Website makes use of an SSL Certificate which establishes a secure connection between our web server and your Internet Browser thus encrypting data transfer.</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                              <div class="header">OTP Authentication</div>
                              <div class="description">One Time Pin Login Authentication are used whenenver a User whishes to logon to the system.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
