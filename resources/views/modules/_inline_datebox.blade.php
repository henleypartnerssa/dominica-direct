<div class="three fields">

    <div class="field">
        <input type="number" name="{{$data[0]}}_year" id="{{$data[0]}}_year" min="1900" max="3000" placeholder="Year (yyyy)" value="{{{(!empty($data[1])) ? Carbon\Carbon::parse($data[1])->year : '' }}}" />
    </div>
    <div class="field">
        <input type="number" name="{{$data[0]}}_month" id="{{$data[0]}}_month" min="1" max="12" placeholder="Month (mm)" value="{{{(!empty($data[1])) ? Carbon\Carbon::parse($data[1])->month : '' }}}" />
    </div>
    <div class="field">
        <input type="number" name="{{$data[0]}}_day" id="{{$data[0]}}_day" min="1" max="31" placeholder="Day (dd)" value="{{{(!empty($data[1])) ? Carbon\Carbon::parse($data[1])->day : '' }}}" />
    </div>
</div>
