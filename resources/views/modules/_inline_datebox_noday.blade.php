<div class="two fields">
    <div class="field">
        <input type="number" name="{{$data[0]}}_month" id="{{$data[0]}}_month" min="1" max="12" data-stripe="exp-month" placeholder="Month (mm)" value="{{{$data[1] or ''}}}" />
    </div>
    <div class="field">
        <input type="number" name="{{$data[0]}}_year" id="{{$data[0]}}_year" min="1900" max="3000" data-stripe="exp-year" placeholder="Year (yyyy)" value="{{{$data[2] or ''}}}" />
    </div>
</div>
