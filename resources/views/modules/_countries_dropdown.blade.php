@if(!empty($data['label']))
    <label>{{{$data['label']}}}</label>
@endif
<div class="ui fluid search {{ $data['name'] }} selection dropdown">
    <input type="hidden" name="{{ $data['name'] }}" id="{{ $data['name'] }}" value="{{ $data['value'] or '' }}">
  <i class="dropdown icon"></i>
  <div class="default text" id="default_value">{{$data['title']}}</div>
  <div class="menu">
    @foreach(App\Country::all() as $country)
        <div class="item" data-value="{{$country->name}}"><i class="{{$country->code}} flag"></i>{{$country->name}}</div>
    @endforeach
  </div>
</div>
