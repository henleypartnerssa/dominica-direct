<div class="ui fluid search cell_number selection dropdown">
    <input type="hidden" name="code" id="code">
    <i class="dropdown icon"></i>
    <div class="default text">Country Code</div>
        <div class="menu">
            @foreach($countries as $country)
                <div class="item" data-value="{{ $country->calling_code }}"><i class="{{ $country->code }} flag"></i>{{ $country->calling_code }}</div>
            @endforeach
        </div>
</div>
