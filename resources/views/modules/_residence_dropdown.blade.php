<div class="ui fluid search selection dropdown">
    <input type="hidden" name="residense">
  <i class="dropdown icon"></i>
  <div class="default text">Select Country</div>
  <div class="menu">
    @foreach(App\Country::all() as $country)
        <div class="item" data-value="{{$country->code}}"><i class="{{$country->code}} flag"></i>{{$country->name}}</div>
    @endforeach
  </div>
</div>