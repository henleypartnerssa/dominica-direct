
<?php $idx = 0; ?>
@if($proLic->count() > 0)
        <div class="field">
            <label>Please provide the details for any privileged or professional licenses that you may hold in any state, (eg. liquor, real estate, professional, financial services or gambling)</label>
    @foreach($proLic as $i => $lic)
        <?php ++$idx; ?>
            <input type="hidden" name="licence_{{$idx}}_id" value="{{{$lic->pro_licence_id or ''}}}" />
            <input type="hidden" id="licence_{{$idx}}_msg" value="{{($lic->disiplinary) ? '1' : '0'}}" />
            <div class="three fields">
                <div class="field">
                    <input type="text" name="licence_type_{{$idx}}" placeholder="Position/Designation held" value="{{{$lic->licence_type or ''}}}" />
                </div>
                <div class="field">
                    <input type="text" name="licence_no_{{$idx}}" placeholder="Licence/Registration/Practice No" value="{{{$lic->licence_no or ''}}}" />
                </div>
                <div class="field">
                    <input type="text" name="authority_{{$idx}}" placeholder="Licensing Authority" value="{{{$lic->authority or ''}}}" />
                </div>
            </div>
            <div class="inline fields">
                <div class="field">
                    <div class="ui disiplinary_{{$idx}}_no radio checkbox">
                        <input type="radio" name="disiplinary_{{$idx}}_no" />
                        <label>No</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui disiplinary_{{$idx}}_yes radio checkbox">
                        <input type="radio" name="disiplinary_{{$idx}}_yes" />
                        <label>Yes</label>
                    </div>
                </div>
                <label>Have you ever had any disiplinary action take against you in respect to the above listed licence ?</label>
            </div>
            <div class="field disiplinary_{{$idx}} hide">
                <label>Explain the nature of the action</label>
                <textarea name="disiplinary_{{$idx}}">{{{$lic->disiplinary or ''}}}</textarea>
            </div>
    @endforeach
        </div>
@else
    <div class="field">
        <label>Please provide the details for any privileged or professional licenses that you may hold in any state, (eg. liquor, real estate, professional, financial services or gambling)</label>
        <div class="three fields">
            <div class="field">
                <input type="text" name="licence_type_1" placeholder="Position/Designation held" value="" />
            </div>
            <div class="field">
                <input type="text" name="licence_no_1" placeholder="Licence/Registration/Practice No" value="" />
            </div>
            <div class="field">
                <input type="text" name="authority_1" placeholder="Licensing Authority" value="" />
            </div>
        </div>
        <div class="inline fields">
            <div class="field">
                <div class="ui disiplinary_1_no radio checkbox">
                    <input type="radio" name="disiplinary_1_no" />
                    <label>No</label>
                </div>
            </div>
            <div class="field">
                <div class="ui disiplinary_1_yes radio checkbox">
                    <input type="radio" name="disiplinary_1_yes" />
                    <label>Yes</label>
                </div>
            </div>
            <label>Have you ever had any disiplinary action take against you in respect to the above listed licence ?</label>
        </div>
        <div class="field disiplinary_1 hide">
            <label>Explain the nature of the action</label>
            <textarea name="disiplinary_1"></textarea>
        </div>
        <div class="three fields">
            <div class="field">
                <input type="text" name="licence_type_2" placeholder="Position/Designation held" value="" />
            </div>
            <div class="field">
                <input type="text" name="licence_no_2" placeholder="Licence/Registration/Practice No" value="" />
            </div>
            <div class="field">
                <input type="text" name="authority_2" placeholder="Licensing Authority" value="" />
            </div>
        </div>
    </div>
    <div class="inline fields">
        <div class="field">
            <div class="ui disiplinary_2_no radio checkbox">
                <input type="radio" name="disiplinary_2_no" />
                <label>No</label>
            </div>
        </div>
        <div class="field">
            <div class="ui disiplinary_2_yes radio checkbox">
                <input type="radio" name="disiplinary_2_yes" />
                <label>Yes</label>
            </div>
        </div>
        <label>Have you ever had any disiplinary action take against you in respect to the above listed licence ?</label>
    </div>
    <div class="field disiplinary_2 hide">
        <label>Explain the nature of the action</label>
        <textarea name="disiplinary_2"></textarea>
    </div>
@endif
