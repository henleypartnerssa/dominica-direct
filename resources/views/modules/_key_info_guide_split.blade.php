<div class="ui grid">
    <div class="row">
        <div class="eight wide column">
            <div class="ui middle aligned selection list">
                @foreach($user->profiles()->orderBy('created_at')->get() as $i => $p)
                @if ($i <= ($user->profiles()->count() / 2))
                <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/users/profile/{{$p->profile_id}}/application/important-information">
                    <div class="right floated content"><i class="large {{($p->state >= 1) ? 'green checkmark' : 'yellow warning'}} sign icon"></i></div>
                    <i class="ui avatar image user icon"></i>
                    <div class="content">
                        {{$p->first_name}} {{$p->last_name}}
                    </div>
                </a>
                @endif
                @endforeach
            </div>
        </div>
        <div class="eight wide column">
            <div class="ui middle aligned selection list">
                @foreach($user->profiles()->orderBy('created_at')->get() as $i => $p)
                @if ($i > ($user->profiles()->count() / 2))
                <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/users/profile/{{$p->profile_id}}/application/important-information">
                    <div class="right floated content"><i class="large {{($p->state >= 1) ? 'green checkmark' : 'yellow warning'}} sign icon"></i></div>
                    <i class="ui avatar image user icon"></i>
                    <div class="content">
                        {{$p->first_name}} {{$p->last_name}}
                    </div>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
