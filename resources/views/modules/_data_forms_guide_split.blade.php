<div class="ui grid">
    <div class="row">
        <div class="eight wide column">
            <div class="ui middle aligned selection list">
                @foreach($user->profiles()->orderBy('created_at')->get() as $i => $p)
                @if ($i <= ($user->profiles()->count() / 2))
                <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/users/profile/{{$p->profile_id}}/application/personal-data">
                    <div class="right floated content"><i class="large {{($p->state === 5) ? 'green checkmark' : 'yellow warning'}} sign icon"></i></div>
                    <i class="ui avatar image user icon"></i>
                    <div class="content">
                        {{$p->first_name}} {{$p->last_name}}
                    </div>
                </a>
                @endif
                @endforeach
            </div>
        </div>
        <div class="eight wide column">
            <div class="ui middle aligned selection list">
                @foreach($user->profiles()->orderBy('created_at')->get() as $i => $p)
                @if ($i > ($user->profiles()->count() / 2))
                <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/users/profile/{{$p->profile_id}}/application/personal-data">
                    <div class="right floated content"><i class="large {{($p->state === 5) ? 'green checkmark' : 'yellow warning'}} sign icon"></i></div>
                    <i class="ui avatar image user icon"></i>
                    <div class="content">
                        {{$p->first_name}} {{$p->last_name}}
                    </div>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    @if($all)
    <div class="row">
        <div class="twelve wide column"></div>
        <div class="four wide column">
            <a class="ui blue button" style="float:right" href="/users/profile/{{$profile->profile_id}}/application/upload-documents">
                Go to Step 6
            </a>
        </div>
    </div>
    @endif
</div>

<div class="breaker-60"></div>

<div class="ui white inverted pointing menu">
    @if($data['page'] === 'personal')
        <a class="item active" href="/users/profile/{{$profile->profile_id}}/application/personal-data">Personal Data</a>
    @else
        <a class="item" href="/users/profile/{{$profile->profile_id}}/application/personal-data">Personal Data</a>
    @endif

    @if($data['page'] === 'medical')
        <a class="item active" href="/users/profile/{{$profile->profile_id}}/application/medical-details">Medical Details</a>
    @elseif($profile->state < 2)
        <a class="item disabled" href="/users/profile/{{$profile->profile_id}}/application/medical-details">Medical Details</a>
    @else
        <a class="item" href="/users/profile/{{$profile->profile_id}}/application/medical-details">Medical Details</a>
    @endif

    @if($profile->belongs_to === 'Principal Applicant' or $profile->belongs_to === 'Spouse' or $profile->belongs_to === 'Father' or $profile->belongs_to === 'Mother')
        @if($data['page'] === 'family')
            <a class="item active" href="/users/profile/{{$profile->profile_id}}/application/family-members">Family Members</a>
        @elseif($profile->state < 3)
            <a class="item disabled" href="/users/profile/{{$profile->profile_id}}/application/family-members">Family Members</a>
        @else
            <a class="item" href="/users/profile/{{$profile->profile_id}}/application/family-members">Family Members</a>
        @endif
    @endif

    @if($profile->belongs_to === 'Principal Applicant' or $profile->belongs_to === 'Spouse')
        @if($data['page'] === 'business')
            <a class="item active" href="/users/profile/{{$profile->profile_id}}/application/business">Employment</a>
        @elseif($profile->state < 4)
            <a class="item disabled" href="/users/profile/{{$profile->profile_id}}/application/business">Employment</a>
        @else
            <a class="item" href="/users/profile/{{$profile->profile_id}}/application/business">Employment</a>
        @endif
    @endif

    @if($data['page'] === 'decl')
        <a class="item active" href="/users/profile/{{$profile->profile_id}}/application/declarations">Declarations</a>
    @elseif($profile->state < 5)
        <a class="item disabled" href="/users/profile/{{$profile->profile_id}}/application/declarations">Declarations</a>
    @else
        <a class="item" href="/users/profile/{{$profile->profile_id}}/application/declarations">Declarations</a>
    @endif

</div>
