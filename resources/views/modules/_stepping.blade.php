<div class="ui small ordered vertical steps">
    @foreach($steps as $step)
        <a class="{{ $step['state'] }} step" href="{{(!empty($url)) ? $url[$step['step_no']] : ''}}">
            <div class="content">
                <div class="title">{{ $step['title'] }}</div>
                <div class="description">{{ $step['description'] }}</div>
            </div>
        </a>
    @endforeach
</div>
