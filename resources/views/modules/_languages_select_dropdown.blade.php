<div class="field">
    <div class="ui fluid multiple search normal selection dropdown">
        <input type="hidden" name="languages" value="{{$profile->languages or ''}}">
        <i class="dropdown icon"></i>
        <div class="default text"></div>
            <div class="menu">
                @foreach($languages as $l)
                    <div class="item" data-value="{{ $l['language_type_id'] }}">{{ $l['type'] }}</div>
                @endforeach
            </div>
    </div>
</div>
