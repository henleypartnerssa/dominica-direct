@extends('app_panel')

@section('title', 'Documents')

@section('content')

<h2 class="heading admin">Documents</h2>

<p id="admin">Listed below are all Generated and Uploaded documents as per your Applications. You may Delete or View the documents below, However please note that in the event of Deleting a document you will be deleting the document from the respective Application as well and will be require to upload a new Document.</p>


<h4 class="ui horizontal divider header">
    Application Documents
</h4>

<div class="holder" style="text-align:left;">
    <div class="ui list">
        <div class="item" id="folder">
            <i class="folder icon"></i>
            <div class="content">
                <div class="header">Quotation</div>
                @if(empty($fileTree['quotaions']))
                    <div class="description">..</div>
                @else
                    @foreach($fileTree['quotaions'] as $file)
                        <div class="description" data-state="show"><i class="red file pdf outline icon"></i>{{$file}}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="item">
            <i class="folder icon"></i>
            <div class="content">
                <div class="header">Payments</div>
                @if(empty($fileTree['payments']))
                    <div class="description">..</div>
                @else
                    @foreach($fileTree['payments'] as $file)
                        <div class="description"><i class="red file pdf outline icon"></i>{{$file}}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="item">
            <i class="folder icon"></i>
            <div class="content">
                <div class="header">Receipts</div>
                @if(empty($fileTree['receipts']))
                    <div class="description">..</div>
                @else
                    @foreach($fileTree['receipts'] as $file)
                        <div class="description"><i class="red file pdf outline icon"></i>{{$file}}</div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="item">
            <i class="folder icon"></i>
            <div class="content">
                <div class="header">Applicants</div>
                <div class="list">
                    <div class="item">
                        <i class="folder icon"></i>
                        <div class="content">
                            <div class="header">Gershon Koks</div>
                            <div class="list">
                                <div class="item">
                                    <i class="folder icon"></i>
                                    <div class="content">
                                        <div class="header">Downloads</div>
                                        <div class="description"><i class="red file pdf outline icon"></i>AB1 - Form.pdf</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="folder icon"></i>
                                    <div class="content">
                                        <div class="header">Uploads</div>
                                        <div class="description"><i class="red file pdf outline icon"></i>AB1 - Notary Form.pdf</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="folder icon"></i>
                        <div class="content">
                            <div class="header">Courtney Koks</div>
                        </div>
                    </div>
                    <div class="item">
                        <i class="folder icon"></i>
                        <div class="content">
                            <div class="header">Kaylip Koks</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/documents.js') }}"></script>
@stop
