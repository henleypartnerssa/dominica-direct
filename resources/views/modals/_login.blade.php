<div class="ui login modal transition hidden">

    <i class="close icon"></i>

    <div class="header mod">Login Form</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <div class="ui login large form-left form">

                        <div class="ui noborder stacked segment">

                            <input type="hidden" name="_token" id="login_token" value="{{ csrf_token() }}" />

                            <div class="required field">
                                <label>Username</label>
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input type="text" name="username" id="username" placeholder="Your E-mail Address...">
                                </div>
                            </div>

                            <div class="required field">
                                <label>Password</label>
                                <div class="ui left icon input">
                                    <i class="lock icon"></i>
                                    <input type="password" id="login_password" placeholder="Your Password..." />
                                </div>
                            </div>

                            <div class="ui logon mod fluid large blue submit button">
                                Log On
                            </div>

                            <div class="ui login negative hidden message">

                                <div class="header"></div>
                                <p></p>

                            </div>

                        </div>


                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
