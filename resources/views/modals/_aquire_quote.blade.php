<div class="ui quote modal transition hidden">

    <i class="close icon"></i>

    <div class="header mod">Aquire Quote</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">


                    <div class="ui aq large form-left form">

                        <div class="ui noborder stack segment">

                            <p>Please select a Country from the dropdown list below and we will generate you a quote. Note that only the Principal Applicant (PA) information will be used.</p>

                            {!! Form::open(array('url' => 'quotations')) !!}

                            <div class="field">
                                @include('modules._programs_dropdown')
                            </div>

                            <div class="field">
                                <button class="ui quire mod fluid blue submit button">Get Quotation</button>
                            </div>

                            {!! Form::close() !!}

                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
