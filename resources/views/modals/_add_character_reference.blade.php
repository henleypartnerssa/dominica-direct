<div class="ui character_reference modal transition hidden">

    <i class="close icon"></i>

    <div class="header mod">Add Character Reference</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <input type="hidden" name="_token" id="character_reference_token" value="{{ csrf_token() }}" />

                    <div class="ui character_reference large form" style="text-align:left">

                        <input type="hidden" name="char_id" id="char_id" />

                        <div class="ui noborder stacked segment">

                            <div class="two fields">
                                <div class="required field">
                                    <label>Full Name</label>
                                    <input type="text" name="full_name" />
                                </div>
                                <div class="required field">
                                    <label>Years Known</label>
                                    <input type="number" name="years_known" />
                                </div>
                            </div>

                            <div class="three fields">
                                <div class="field">
                                    <label>Home Number</label>
                                    <input type="text" name="landline_no" />
                                </div>
                                <div class="required field">
                                    <label>Mobile Number</label>
                                    <input type="text" name="mobile_no" />
                                </div>
                                <div class="required field">
                                    <label>Email Address</label>
                                    <input type="email" name="email" />
                                </div>
                            </div>

                            <div class="three fields">
                                <div class="required field">
                                    <label>Occupation</label>
                                    <input type="text" name="occupation" />
                                </div>
                                <div class="required field">
                                    <label>Employer</label>
                                    <input type="text" name="employer" />
                                </div>
                                <div class="required field">
                                    <label>Work Phone</label>
                                    <input type="text" name="work_no" />
                                </div>
                            </div>

                            <div class="required field">
                                <label>Current Residential Address</label>
                                <div class="two fields">
                                    <div class="field">
                                        <input type="text" name="cur_street1" placeholder="address line 1" />
                                    </div>
                                    <div class="field">
                                        <input type="text" name="cur_street2" placeholder="address line 2 (optional)" />
                                    </div>
                                </div>

                                <div class="three fields">
                                    <div class="required field">
                                        @include('modules._countries_dropdown', $data = ['name'=>'cur_country', 'title' => 'Coutnry of Current Residence'])
                                    </div>
                                    <div class="field">
                                        <input type="text" name="cur_town" placeholder="town/city" />
                                    </div>
                                    <div class="field">
                                        <input type="text" name="cur_post_code" placeholder="postal code" />
                                    </div>
                                </div>

                            </div>

                            <div class="ui add_charater_reference fluid large blue inverted submit button">Add</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
