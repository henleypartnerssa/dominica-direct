<div class="ui upload_file modal transition hidden">

    <i class="close icon"></i>

    <div class="header mod">Upload File</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <div class="ui upload_file large form" style="text-align:left">

                        <div class="ui noborder stacked segment">

                            <div class="three field">
                                <div class="field">
                                    <label>Applicant Type</label>
                                </div>
                                <div class="field">
                                    <label>First Name</label>
                                </div>
                                <div class="field">
                                    <label>Surname</label>
                                </div>
                            </div>


                            <div class="ui add_address fluid large btn-color submit button" id="adding_address">Submit</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>


    </div>

</div>
