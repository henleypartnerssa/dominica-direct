<div class="ui qualification modal transition hidden" id="qualificationModal">

    <i class="close icon"></i>

    <div class="header mod">Add Qualification</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <input type="hidden" id="qualification_token" id="key_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="qual_id" id="qual_id" value="" />
                    <input type="hidden" name="qual_is_admin" id="qual_is_admin" value="0" />

                    <div class="ui qualification large form" style="text-align:left">


                        <div class="ui noborder stacked segment">

                            <div class="two fields">

                                <div class="required field">
                                    <label>From when</label>
                                    @include('modules._inline_datebox_noday', $data = ['from_qual'])
                                </div>

                                <div class="required field">
                                    <label>To when</label>
                                    @include('modules._inline_datebox_noday', $data = ['to_qual'])
                                </div>

                            </div>

                            <div class="two fields">
                                <div class="required field">
                                    <label>Name of school</label>
                                    <input type="text" name="school" id="school" placeholder="name of school">
                                </div>
                                <div class="required field">
                                    <label>Qualification obtained</label>
                                    <input type="text" name="qualification" id="qualification" placeholder="Qualification / Diploma obtained">
                                </div>
                            </div>

                            <div class="required field">
                                <label>Location</label>
                                <input type="text" name="qual_location" id="qual_location" placeholder="location of instatution">
                            </div>

                            <div class="ui add_qualification fluid large blue inverted submit button" id="adding_address">Add</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>


    </div>

</div>
