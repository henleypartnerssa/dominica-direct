<div class="ui passport modal transition hidden" id="passModal">

    <i class="close icon"></i>

    <div class="header mod">Add Passport</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <input type="hidden" name="_token" id="passport_token" value="{{ csrf_token() }}" />
                    <input type="hidden" id="pass_id" value="" />
                    <input type="hidden" id="pass_is_admin" value="0" />
                    <div id="file_inputs">
                    </div>
                    <div class="ui passport large form" style="text-align:left">

                        <div class="ui noborder stacked segment">

                            <div class="required field">
                                <label>Issuing Country</label>
                                @include('modules._countries_dropdown', $data = ['name'=> 'pass_issuing_country', 'title' => 'Issuing Country'])
                            </div>

                            <div class="required field">
                                <label>Passport Number</label>
                                <input type="text" id="pass_number" name="pass_number" autofocus/>
                            </div>

                            <div class="required field">
                                <label>Place of issue</label>
                                <input type="text" id="pass_place" name="pass_place" />
                            </div>

                            <div class="two fields">
                                <div class="required field">
                                    <label>Date of issue</label>
                                    @include('modules._inline_datebox', $data = ['pass_doi'])
                                </div>
                                <div class="required field">
                                    <label>Date of expiration</label>
                                    @include('modules._inline_datebox', $data = ['pass_dox'])
                                </div>
                            </div>

                            <div class="ui add_passport fluid large blue inverted submit button" id="adding_passport">Add</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>


    </div>

</div>
