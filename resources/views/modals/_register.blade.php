<div class="ui first coupled modal transition hidden">

    <i class="close icon"></i>

    <div class="header mod"> Registration Form</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <div class="ui register large form-left form">

                        <div class="ui noborder stacked segment">

                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

                            <div class="two fields">

                                <div class="required field">
                                  <label>First Name</label>
                                  <div class="ui left icon input">
                                      <i class="user icon"></i>
                                      {!! Form::text('name',null , ['id' => 'name']) !!}
                                  </div>
                                </div>
                                <div class="required field">
                                  <label>Last Name</label>
                                  <div class="ui left icon input">
                                      <i class="user icon"></i>
                                      {!! Form::text('lastname',null , ['id' => 'lastname']) !!}
                                  </div>
                                </div>

                            </div>

                            <div class="two fields">

                                <div class="required field">
                                  <label>Password</label>
                                  <div class="ui left icon input">
                                      <i class="lock icon"></i>
                                      <input type="password" name="register_password" id="register_password">
                                  </div>
                                </div>

                                <div class="required field">
                                  <label>Re-type Password</label>
                                  <div class="ui left icon input">
                                      <i class="lock icon"></i>
                                      <input type="password" name="retypePassword" id="retypePassword">
                                  </div>
                                </div>

                            </div>

                            <div class="two fields">

                                <div class="required field">
                                  <label>Email Address</label>
                                  <div class="ui left icon input">
                                      <i class="at icon"></i>
                                       {!! Form::email('email',null , ['id' => 'email']) !!}
                                  </div>
                                </div>

                                <div class="required field">
                                  <label>Re-type Email Address</label>
                                  <div class="ui left icon input">
                                      <i class="at icon"></i>
                                      {!! Form::email('retypeEmail',null , ['id' => 'retypeEmail']) !!}
                                  </div>
                                </div>

                            </div>

                            <div class="ui register mod fluid large blue submit button">
                                Register
                            </div>

                        </div>


                    </div>

                </div>

        </div>
    </div>

    </div>

</div>
