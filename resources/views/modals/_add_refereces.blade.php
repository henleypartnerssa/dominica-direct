<div class="ui reference modal transition hidden" id="referenceModal">

    <i class="close icon"></i>

    <div class="header mod">Add Reference</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <input type="hidden" id="reference_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="reference_id" id="reference_id" value="" />
                    <input type="hidden" name="ref_is_admin" id="ref_is_admin" value="0" />

                    <div class="ui reference large form" style="text-align:left">


                        <div class="ui noborder stacked segment">

                            <div class="two fields">

                                <div class="required field">
                                    <label>From when</label>
                                    @include('modules._inline_datebox_noday', $data = ['from_reference'])
                                </div>

                                <div class="required field">
                                    <label>To when</label>
                                    @include('modules._inline_datebox_noday', $data = ['to_reference'])
                                </div>

                            </div>

                            <div class="two fields">
                                <div class="required field">
                                    <label>Occupation</label>
                                    <input type="text" name="occupation" id="occupation" placeholder="Occupation">
                                </div>
                                <div class="required field">
                                    <label>Name of Employer</label>
                                    <input type="text" name="employer_name" id="employer_name" placeholder="Name of Employer">
                                </div>
                            </div>

                            <div class="required field">
                                <label>Location</label>
                                <input type="text" name="employer_location" id="employer_location" placeholder="location of Employer">
                            </div>

                            <div class="required field">
                                <label>Type of Business</label>
                                <input type="text" name="type_of_business" id="type_of_business" placeholder="Type of Business">
                            </div>

                            <div class="required field">
                                <label>Reason for leaving</label>
                                <textarea rows="3" name="reason_for_leaving" id="reason_for_leaving"></textarea>
                            </div>

                            <div class="ui add_reference fluid large blue inverted submit button">Add</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>


    </div>

</div>
