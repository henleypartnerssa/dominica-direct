
<div class="ui child modal transition hidden">

    <i class="close icon"></i>

    <div class="header mod">Add Child</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <input type="hidden" name="_token" id="child_token" value="{{ csrf_token() }}" />

                    <div class="ui child large form" style="text-align:left">

                        <div class="ui noborder stacked segment">

                            <input type="hidden" name="family_member_id" id="family_member_id" />

                            <div class="three fields">
                                <div class="required field">
                                    <label>Gender</label>

                                    <div class="ui fluid search gender selection dropdown">
                                        <input type="hidden" name="gender">
                                        <i class="dropdown icon"></i>
                                        <div class="default text">Gender</div>
                                            <div class="menu">
                                                <div class="item" data-value="male"><i class="man icon"></i>Male</div>
                                                <div class="item" data-value="female"><i class="woman icon"></i>Female</div>
                                            </div>
                                    </div>
                                </div>
                                <div class="required field">
                                    <label>Family Name</label>
                                    <input type="text" name="last_name" placeholder="Last name/Family name" autofocus>
                                </div>
                                <div class="required field">
                                    <label>First Names</label>
                                    <input type="text" name="first_name" placeholder="First/Given name">
                                </div>
                            </div>

                            <div class="two fields">

                                <div class="required field">
                                    <label>Date of birth</label>
                                    @include('modules._inline_datebox', $data = ['date_of_birth'])
                                </div>

                                <div class="required field">
                                    <label>Place of Birth</label>
                                    <input type="text" name="place_of_birth" placeholder="Place of Birth">
                                </div>

                            </div>

                            <div class="two fields">
                                <div class="required field">
                                    <label>Citizenship</label>
                                    @include('modules._countries_dropdown', $data = ['name'=>'citizenship', 'title' => 'Citizenship'])
                                </div>
                                <div class="required field">
                                    <label>Occupation</label>
                                    <input type="text" name="occupation" placeholder="siblings occupation" />
                                </div>
                            </div>

                            <div class="required field">
                                <label>Current Residential Address</label>
                                <div class="two fields">
                                    <div class="field">
                                        <input type="text" name="street1" placeholder="address line 1" />
                                    </div>
                                    <div class="field">
                                        <input type="text" name="street2" placeholder="address line 2 (optional)" />
                                    </div>
                                </div>

                                <div class="three fields">
                                    <div class="required field">
                                        @include('modules._countries_dropdown', $data = ['name'=>'country', 'title' => 'Coutnry of Current Residence'])
                                    </div>
                                    <div class="field">
                                        <input type="text" name="town" placeholder="town/city" />
                                    </div>
                                    <div class="field">
                                        <input type="text" name="post_code" placeholder="postal code" />
                                    </div>
                                </div>

                            </div>

                            <div class="ui add_child fluid large blue inverted submit button">Add</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
