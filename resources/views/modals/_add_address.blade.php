<div class="ui address modal transition hidden" id="addressModal">

    <i class="close icon"></i>

    <div class="header mod">Add Address</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <input type="hidden" id="addr_profile_id" style="display: none" value="{{ $profile->profile_id }}" />
                    <input type="hidden" id="addr_token" id="key_token" style="display: none" value="{{ csrf_token() }}" />
                    <input type="hidden" id="addr_type" value="ten" />
                    <input type="hidden" id="add_is_admin" value="0" />
                    <input type="hidden" id="addr_id"   value="" />

                    <div class="ui address large form" style="text-align:left">

                        <div class="ui noborder stacked segment">

                            <div class="two fields">

                                <div class="required field">
                                    <label>From when</label>
                                    @include('modules._inline_datebox_noday', $data = ['from_addr'])
                                </div>

                                <div class="required field">
                                    <label>To when</label>
                                    @include('modules._inline_datebox_noday', $data = ['to_addr'])
                                </div>

                            </div>

                            <div class="two fields">

                                <div class="required field">
                                    <label>Address line 1</label>
                                    <input type="text" name="add_addr_line_1" id="add_addr_line_1" placeholder="address line 1">
                                </div>

                                <div class="field">
                                    <label>Address line 2</label>
                                    <input type="text" name="add_addr_line_2" id="add_addr_line_2" placeholder="address line 2 (optional)">
                                </div>

                            </div>

                            <div class="three fields">

                                <div class="required field">
                                    <label>Country</label>
                                    @include('modules._countries_dropdown', $data = ['name'=> 'add_addr_country', 'title' => 'Country'])
                                </div>

                                <div class="required field">
                                    <label>Town/City</label>
                                    <input type="text" name="add_addr_town" id="add_addr_town" placeholder="town/city">
                                </div>

                                <div class="required field">
                                    <label>Postal Code</label>
                                    <input type="text" name="add_addr_postal_code" id="add_addr_postal_code" placeholder="postal code">
                                </div>

                            </div>

                            <div class="ui add_address fluid large blue inverted submit button" id="adding_address">Submit</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>


    </div>

</div>
