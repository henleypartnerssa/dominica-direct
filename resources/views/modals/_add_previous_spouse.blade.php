<div class="ui previous-spouse modal transition hidden">

    <i class="close icon"></i>

    <div class="header mod">Add Ex-Spouse</div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">
            <div class="column">

                <div class="ui container">

                    <input type="hidden" name="_token" id="previous_spouse_token" value="{{ csrf_token() }}" />

                    <div class="ui previous-spouse large form" style="text-align:left">

                        <input type="hidden" name="previous_spouse_id" id="previous_spouse_id" />

                        <div class="ui noborder stacked segment">

                            <div class="required field">
                                <label>Full Name</label>
                                <input type="text" name="full_name" />
                            </div>

                            <div class="two fields">
                                <div class="required field">
                                    <label>Date of Birth</label>
                                    @include('modules._inline_datebox', $data = ['date_of_birth'])
                                </div>
                                <div class="required field">
                                    <label>Nationality</label>
                                    @include('modules._countries_dropdown', $data = ['name'=> 'nationality', 'title' => 'Nationality'])
                                </div>
                            </div>

                            <div class="required field">
                                <label>Place of birth</label>
                                <input type="text" name="place_of_birth" />
                            </div>

                            <div class="required field">
                                <label>Divorse Date</label>
                                @include('modules._inline_datebox', $data = ['divorse_dt'])
                            </div>

                            <div class="two fields">
                                <div class="required field">
                                    <label>Marriage From date</label>
                                    @include('modules._inline_datebox', $data = ['marriage_from_dt'])
                                </div>
                                <div class="required field">
                                    <label>Marriage To date</label>
                                    @include('modules._inline_datebox', $data = ['marriage_to_dt'])
                                </div>
                            </div>

                            <div class="ui add_previous_spouse fluid large blue inverted submit button">Add</div>

                        </div>

                    </div>

                </div>

            </div>
        </div>


    </div>

</div>
