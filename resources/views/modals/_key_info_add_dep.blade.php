<div class="ui key_dep modal transition hidden">

    <i class="close icon"></i>

    <div class="header" style="text-align: center">Add Dependant</div>

    <div class="ui middle aligned center aligned grid">

        <div class="ui container">

            <input type="hidden" name="_token" id="key_token" value="{{ csrf_token() }}" />

            <div class="ui key_dep large form">

                <div class="ui noborder stacked segment">

                    <div class="three fields">

                        <div class="required field">
                            <label>Dependant Type</label>

                            <div class="ui fluid search selection dropdown">
                                <input type="hidden" name="type" id="type">
                                <i class="dropdown icon"></i>
                                <div class="default text">Dependant Types</div>
                                <div class="menu">

                                    @foreach($depTypes as $type)
                                        <div class="item" data-value="{{ $type['dependant_type_id'] }}">{{ $type['type'] }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="required field">
                            <label>Name</label>
                            <input type="text" name="name" id="name" />
                        </div>

                        <div class="required field">
                            <label>Surname</label>
                            <input type="text" name="surname" id="surname" />
                        </div>

                    </div>

                    <div class="two fields">

                        <div class="required field">
                            <label>Date of Birth</label>

                            <div class="three fields">
                                <div class="field"><input type="number" name="year" id="year" placeholder="YYYY" min="1900" max="2015" /></div>
                                <div class="field"><input type="number" name="month" id="month" placeholder="MM" min="01" max="12" /></div>
                                <div class="field"><input type="number" name="day" id="day" placeholder="DD" min="01" max="31" /></div>
                            </div>

                        </div>

                        <div class="required field">
                            <label>Gender</label>

                            <div class="ui fluid search selection dropdown">
                                <input type="hidden" name="gender" id="gender">
                                <i class="dropdown icon"></i>
                                <div class="default text">Gender</div>
                                    <div class="menu">
                                        <div class="item" data-value="male"><i class="man icon"></i>Male</div>
                                        <div class="item" data-value="female"><i class="woman icon"></i>Female</div>
                                    </div>
                            </div>

                        </div>

                    </div>

                    <div class="ui key_add_dep fluid large blue inverted submit button" id="key_add_dep">
                        Add
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
