@extends('app')

@section('title', 'FAQ')
@section('l3', 'active')

@section('header_content')
    <div class="ui text container">
        <h1 class="ui inverted header">Branding Image</h1>
    </div>
@stop

@section('content')

    <div class="ui grid">
        <div class="row devider">
            <div class="eleven wide slider column">
                <h2 class="heading">Frequently Asked Questions</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tortor massa, sodales at sagittis eget, mollis eu ipsum. Curabitur in tortor ac dolor eleifend mollis eu vitae sem. Proin scelerisque aliquam erat, in molestie dui. Aliquam eu luctus neque. Praesent scelerisque, est vitae lobortis dictum, ex purus accumsan urna, sed blandit augue diam a neque. Morbi sem orci, ultrices nec vehicula id, laoreet at arcu. Nunc rhoncus facilisis mauris vel laoreet. Proin euismod dui erat, ac consequat tellus faucibus et.</p>

                <div class="ui accordion">
                    <div class="title">
                        <i class="dropdown icon"></i>
                        How do i apply for Citizenship ?
                    </div>
                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tortor massa, sodales at sagittis eget, mollis eu ipsum. Curabitur in tortor ac dolor eleifend mollis eu vitae sem. Proin scelerisque aliquam erat, in molestie dui. Aliquam eu luctus neque. Praesent scelerisque, est vitae lobortis dictum, ex purus accumsan urna, sed blandit augue diam a neque. Morbi sem orci, ultrices nec vehicula id, laoreet at arcu. Nunc rhoncus facilisis mauris vel laoreet. Proin euismod dui erat, ac consequat tellus faucibus et.
                    </div>
                    <div class="title">
                        <i class="dropdown icon"></i>
                        Who should i contact ?
                    </div>
                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tortor massa, sodales at sagittis eget, mollis eu ipsum. Curabitur in tortor ac dolor eleifend mollis eu vitae sem. Proin scelerisque aliquam erat, in molestie dui. Aliquam eu luctus neque. Praesent scelerisque, est vitae lobortis dictum, ex purus accumsan urna, sed blandit augue diam a neque. Morbi sem orci, ultrices nec vehicula id, laoreet at arcu. Nunc rhoncus facilisis mauris vel laoreet. Proin euismod dui erat, ac consequat tellus faucibus et.
                    </div>
                    <div class="title">
                        <i class="dropdown icon"></i>
                        Best manner to transfer fees ?
                    </div>
                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tortor massa, sodales at sagittis eget, mollis eu ipsum. Curabitur in tortor ac dolor eleifend mollis eu vitae sem. Proin scelerisque aliquam erat, in molestie dui. Aliquam eu luctus neque. Praesent scelerisque, est vitae lobortis dictum, ex purus accumsan urna, sed blandit augue diam a neque. Morbi sem orci, ultrices nec vehicula id, laoreet at arcu. Nunc rhoncus facilisis mauris vel laoreet. Proin euismod dui erat, ac consequat tellus faucibus et.
                    </div>
                </div>
            </div>

            <div class="five wide column">
                @include('modules._latest_news')
            </div>

        </div>
    </div>
@stop

@section('script')
    <script src="{{ URL::asset('js/general.js') }}"></script>
@stop
