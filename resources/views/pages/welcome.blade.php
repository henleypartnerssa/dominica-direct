@extends('app')

@section('title', 'Welcome')
@section('l1', 'active')

@section('content')

<div class="ui grid">
    <div class="row">
        <div class="eight wide column" style="padding-right:2.5rem">

            <h2 class="heading welcome">Login Form</h2>

            <p>By registering with Newlands Direct, you will be able to upload documents, track your application and communicate with the Newlands processing team.</p>
            <div class="breaker-20"></div>
            <div class="ui login form">
            {!! Form::open(['url' => '/users/login', 'id' => 'login-form']) !!}
                <div class="required field">
                    <label>Username</label>
                    <input class="home-form-inputs" type="text" name="username" id="username" placeholder="your registered email address" />
                </div>

                <div class="required field">
                    <label>Password</label>
                    <input class="home-form-inputs" type="password" name="login_password" id="login_password" placeholder="your registered password" />
                </div>

                <div class="three fields">
                    <div class="field">
                        <button class="ui next login fluid newlands-bg right labeled icon button">
                            Login
                            <i class="small angle right icon"></i>
                        </button>
                    </div>
                    <div class="field" style="padding-top:8px;">
                        <a href="" style="margin-top:10px;">Forgot your password?</a>
                    </div>
                    <div class="field">

                    </div>
                </div>
            {!! Form::close() !!}
            </div>

            <div class="ui login negative message" {{{($errors->has() && $errors->all()[0] !== 'Not Registered') ? 'style=display:block' : 'style=display:none'}}}>
              <i class="close icon"></i>
              <div class="header">
                  Invalid Login Details
              </div>
              <p>Your have entered the Incorrect Username or Password. Please try again or contact support.</p>
            </div>
            <div class="ui not-validate negative message" {{{($errors->has() && $errors->all()[0] === 'Not Registered') ? 'style=display:block' : 'style=display:none'}}}>
              <i class="close icon"></i>
              <div class="header">
                  Account Not Verified
              </div>
              <p>Your account has not yet been verified or has been locked. Please check your email account Inbox/Span folders for Newlands verification email and click on the verification link to verify your account. If your account has been locked then please contact support@newlandsglobal.com.</p>
            </div>
        </div>
        <div class="eight wide column" style="border-left:1px solid #CDA349;padding-left:2.5rem">
            <h2 class="heading welcome">Registration Form</h2>

            <p>By registering with Newlands Direct, you will be able to upload documents, track your application and communicate with the Newlands processing team.</p>
            <div class="breaker-20"></div>
            <div class="ui register form">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                <div class="required field">
                    <label>First Name</label>
                    <input class="home-form-inputs" type="text" name="name" id="name" />
                </div>
                <div class="required field">
                    <label>Surname</label>
                    <input class="home-form-inputs" type="text" name="lastname" id="lastname" />
                </div>
                <div class="required field">
                    <label>Password</label>
                    <input class="home-form-inputs" type="password" name="rpassword" id="rpassword" />
                    <div class="ui top attached indicating progress" style="display:none">
                        <div class="bar"></div>
                    </div>
                    <span id="rating">rating</span>
                </div>
                <div class="required field">
                    <label>Confirm Password</label>
                    <input class="home-form-inputs" type="password" name="password_conf" id="password_conf" />
                </div>
                <div class="required field">
                    <label>Email</label>
                    <input class="home-form-inputs" type="text" name="email" id="email" />
                    <span id="dup-email">email already exists</span>
                </div>
                <div class="required field">
                    <label>Confirm Email</label>
                    <input class="home-form-inputs" type="text" name="email_conf" id="email_conf" />
                </div>

                <div class="field">
                    <div class="ui checkbox">
                      <input type="checkbox" name="tc" id="tc">
                      <label>I have read and understood the <a href="">terms and conditions</a></label>
                    </div>
                </div>

                <div class="three fields">
                    <div class="field">
                        <button class="ui register fluid newlands-bg right labeled icon button">
                            Register
                            <i class="small angle right icon"></i>
                        </button>
                    </div>
                    <div class="field">
                    </div>
                    <div class="field">

                    </div>
                </div>
            </div>
            <div class="ui regsiter warning message" id="regsiter-warning" style="display:none">
              <i class="close icon"></i>
              <div class="header">
                  Terms and Conditions
              </div>
              <p>Unfortunately we cannot Regsiter your details if you have not read and unserstood our Terms and Conditions.</p>
            </div>
            <div class="ui login negative message" id="login-warning" style="display:none">
              <i class="close icon"></i>
              <div class="header">
                  Invalid Login Details
              </div>
              <p>You have entered the Incorrect Username or Password. Please try again or contact support.</p>
            </div>
            <div class="ui mail negative message" id="mail-warning" style="display:none">
              <i class="close icon"></i>
              <div class="header">
                  Existing Email Address
              </div>
              <p>You have entered an already existing email address. Please try a diffrent email address.</p>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')
    <script src="{{ URL::asset('js/welcome.js') }}"></script>
@stop
