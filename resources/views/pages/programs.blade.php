@extends('app')

@section('title', 'Programs')
@section('l2', 'active')

@section('header_content')
    <div class="ui text container">
        <h1 class="ui inverted header">Branding Image</h1>
    </div>
@stop

@section('content')

    <div class="ui grid">
        <div class="row devider">
            <div class="eleven wide slider column">
                <h2 class="heading">Our Programs</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam egestas, est fringilla porta commodo, turpis augue blandit massa, a cursus tellus mi vel erat. Vestibulum placerat iaculis malesuada. Phasellus tempor neque non tortor imperdiet lobortis. Maecenas tortor orci, cursus vel rhoncus et, aliquet ac mauris. Proin eget metus ante. Vestibulum velit velit, condimentum quis laoreet at, vehicula eget nulla. Integer libero diam, condimentum sit amet ultricies ut, laoreet sit amet turpis. Nulla id turpis id felis mollis convallis. Fusce in felis eu dolor mattis placerat non non diam. Nunc id placerat nibh. Integer non lorem mollis, ullamcorper neque iaculis, maximus nisl. In ac sapien eget lorem pulvinar vehicula. Nam fringilla euismod tortor at dictum. Mauris lacinia, est id auctor suscipit, augue nisl accumsan massa, at fringilla ligula leo ac enim. Integer non mollis tortor, quis sodales odio. Donec eleifend, ligula dapibus pulvinar ultricies, lorem lectus varius est, vel dignissim ligula elit quis tortor.
                </p>
                <p>
                    Donec lobortis turpis metus, ac ultricies turpis dapibus et. Nam accumsan ante sed aliquam malesuada. Phasellus et turpis ac odio tempus dictum eget fermentum sem. Ut cursus faucibus pellentesque. Mauris non risus leo. Pellentesque aliquet nisi arcu, nec aliquam tellus posuere in. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris malesuada nisl ligula.
                </p>
            </div>

            <div class="five wide column">
                @include('modules._latest_news')
            </div>

        </div>
    </div>
@stop

@section('script')
    <script src="{{ URL::asset('js/general.js') }}"></script>
@stop
