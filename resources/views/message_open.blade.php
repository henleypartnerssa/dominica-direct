@extends('app_panel')

@section('title', 'Dashboard')

@section('content')

<h4 class="ui horizontal divider header">
    Message View
</h4>

<div class="ui items" style="text-align:left">
    @foreach($prevMessages as $message)
        <div class="item">
            <div class="content">
                <a class="header">Subject: {{$message->subject}}</a>
                <div class="meta">
                    <span>From: {{App\User::find($message->user_id)->name}} {{App\User::find($message->user_id)->surname}}</span>
                </div>
                <div class="description">
                    <p>{{$message->message}}</p>
                </div>
                <div class="extra">
                    {{$message->created_at}}
                </div>
            </div>
        </div>
        <h4 class="ui horizontal divider header">
            -
        </h4>
    @endforeach
</div>

<div class="ui large message form">
{!! Form::open() !!}
    <div class="field">
        <label>Reply:</label>
        <textarea name="message"></textarea>
    </div>

    <div class="three fields">
        <div class="field">
        </div>
        <div class="field">
        </div>
        <div class="field">
            <button class="ui blue button" style="float:right"><i class="reply icon"></i>Reply</button>
        </div>
    </div>
{!! Form::close() !!}
</div>
<div class="bottom"></div>
@stop

@section('script')
    <script src="{{ URL::asset('js/messages.js') }}"></script>
@stop
