<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify the Company Name .e.g. NL, HP .etc.The default
    | value is derived from the .env file.
    |
    */
    'company'           => env('COMPANY_CODE'),

    /*
    |--------------------------------------------------------------------------
    | Program Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify the program name which the Mailer should make
    | used of. The default value is derived from the .env file.
    |
    */
    'program'           => env('PROGRAM_NAME'),

    /*
    |--------------------------------------------------------------------------
    | Admin Mailing List
    |--------------------------------------------------------------------------
    |
    | Here you may specify the Admin Mailing which the Mailer use's to email
    | admin users. The default value is derived from the .env file.
    |
    */
    'admin_mail_list'   => 'gershon.koks@henleyglobal.com',

    /*
    |--------------------------------------------------------------------------
    | Signature
    |--------------------------------------------------------------------------
    |
    | This is where you specifiy the commercial Email signautre which will be
    | in each email sent to Applicant user.
    |
    */
    'signature'         => '
        <p>Kind Regards,</p>
        <p>Newlands - Dominica Direct</p>
    ',

    'system_sig'        => '
        <p>Kind Regards,</p>
        <p>Newlands - Dominica Direct</p>
    ',


];
