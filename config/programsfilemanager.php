<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Program Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify the program name which the file manager should be
    | used for. The default value is derived from the .env file.
    |
    */
    'program'           => env('PROGRAM_NAME'),

    /*
    |--------------------------------------------------------------------------
    | Program Temaplates
    |--------------------------------------------------------------------------
    |
    | Here you may specify if the library should use its internal PDF templates
    | or custom specified templates. If you wish to use custom templates
    \ then please specifify as follows: ['AB1.pdf', 'AB2.pdf'] etc.
    |
    */
    'templates'         => true,

    /*
    |--------------------------------------------------------------------------
    | File and Path creation API
    |--------------------------------------------------------------------------
    |
    | Here you may specify if the library should use the laravel API to create
    | the required folders and files. If you which to change this setting
    | then you are reqiored to specifify custom path the API should
    | create when published e.g. ['userPath' => Storage::makeDirectory('users')]
    | It is strongly remcommended that you first consult with HP South Africa
    | developement team at helpdesk-sa@henleyglobal.com before changing
    | this setting.
    |
    */
    'laravelFileApi'    => true,

    /*
    |--------------------------------------------------------------------------
    | User Profile Direcotry Identifier
    |--------------------------------------------------------------------------
    |
    | This you may specify what profile Identifier should be used. Confguration
    | options are: ID, Name.
    |
    */
    'profileIdentifier' => 'ID',

];
