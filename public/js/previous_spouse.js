$('document').ready(function() {

    deletePreviousSpouse();
    editPreviousSpouse();

    // Click Add Ex-Spouse Link
    $('#add_previous_spouse_link').on('click', function(e) {
        e.preventDefault();
        $('.ui.previous-spouse.modal').modal('show');
        $('.ui.previous-spouse.form').form('clear');
        $('.ui.add_previous_spouse.button').text('Add');
    });

    // Click Add button in Modal
    $('.ui.add_previous_spouse.button').on('click', function() {
        var val = formValidations();
        if (val) {
            var form = $('.ui.previous-spouse.form');
            form.addClass('loading');
            $.ajax({
                type    : "POST",
                url     : '/users/profile/add-previous-spouse',
                async   : true,
                data    : {
                    _token                  : $('#sibling_token').val(),
                    profile_id              : $('#profile_id').val(),
                    previous_spouse_id      : form.form('get value', 'previous_spouse_id'),
                    full_name               : form.form('get value', 'full_name'),
                    nationality             : form.form('get value', 'nationality'),
                    place_of_birth          : form.form('get value', 'place_of_birth'),
                    date_of_birth_year      : form.form('get value', 'date_of_birth_year'),
                    date_of_birth_month     : form.form('get value', 'date_of_birth_month'),
                    date_of_birth_day       : form.form('get value', 'date_of_birth_day'),
                    divorse_dt_year         : form.form('get value', 'divorse_dt_year'),
                    divorse_dt_month        : form.form('get value', 'divorse_dt_month'),
                    divorse_dt_day          : form.form('get value', 'divorse_dt_day'),
                    marriage_from_dt_year   : form.form('get value', 'marriage_from_dt_year'),
                    marriage_from_dt_month  : form.form('get value', 'marriage_from_dt_month'),
                    marriage_from_dt_day    : form.form('get value', 'marriage_from_dt_day'),
                    marriage_to_dt_year     : form.form('get value', 'marriage_to_dt_year'),
                    marriage_to_dt_month    : form.form('get value', 'marriage_to_dt_month'),
                    marriage_to_dt_day      : form.form('get value', 'marriage_to_dt_day'),
                },
                success: function(data) {
                    var json = JSON.parse(data);
                    $('.item.'+json.id).remove();
                    $('.ui.previous_spouse.selection').append(htmlDep(json.id,json.name));
                    $('.ui.previous-spouse.modal').modal('hide');
                    $('.ui.previous-spouse.form').form('clear');
                    deletePreviousSpouse();
                    editPreviousSpouse();
                }
            });
            form.removeClass('loading');
        }
    });

    function deletePreviousSpouse() {
        $('.ui.previous_spouse.delete.button').on('click', function() {
            $(this).addClass('loading');
            $(this).addClass('disabled');
            var id = $(this).attr('id');
            $.ajax({
                type    : "POST",
                url     : '/users/profile/delete-previous-spouse',
                async   : true,
                data    : {
                    _token                  : $('#sibling_token').val(),
                    previous_spouse_id      : id,
                },
                success: function(data) {
                    if (data === '1') {
                        $('.item.'+id).remove();
                    }
                }
            });
        });
    }

    function editPreviousSpouse() {
        $('.ui.previous_spouse.edit.button').on('click', function() {
            var form    = $('.ui.previous-spouse.form');
            var id      = $(this).attr('id');
            $('.ui.add_previous_spouse.button').text('Update');
            form.addClass('loading');
            $('.ui.previous-spouse.modal').modal('show');
            $.ajax({
                type    : "POST",
                url     : '/users/profile/find-previous-spouse',
                async   : true,
                data    : {
                    _token                  : $('#sibling_token').val(),
                    previous_spouse_id      : id,
                },
                success: function(data) {
                    form.removeClass('loading');
                    var json = JSON.parse(data);
                    form.form('set value', 'previous_spouse_id', json.previous_spouse_id);
                    form.form('set value', 'full_name', json.full_name);
                    form.form('set value', 'nationality', json.nationality);
                    form.form('set value', 'place_of_birth', json.place_of_birth);
                    mapDate(json.date_of_birth, 'date_of_birth', form);
                    mapDate(json.divorse_dt, 'divorse_dt', form);
                    mapDate(json.marriage_from_dt, 'marriage_from_dt', form);
                    mapDate(json.marriage_to_dt, 'marriage_to_dt', form);
                }
            });

        });
    }

    function mapDate(date, element, form) {
        var split = date.split('-');
        form.form('set value', element+'_year', split[0]);
        form.form('set value', element+'_month', split[1]);
        form.form('set value', element+'_day', split[2]);
    }

    function htmlDep(id, name) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted previous_spouse edit button" id="'+id+'">Edit</div><div class="ui red inverted previous_spouse delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image user icon"></i><div class="content">' + name + ' (Ex-Spouse) </div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function formValidations() {
        $('.ui.previous-spouse.form').form({
            fields: {
                full_name: {
                    identifier: 'full_name',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter Ex-Spouse Full Name."
                        }
                    ]
                },
                place_of_birth: {
                    identifier: 'place_of_birth',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter Ex-Spouse Place of Birth."
                        }
                    ]
                },
                nationality: {
                    identifier: 'nationality',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter Ex-Spouse Nationality."
                        }
                    ]
                },
                date_of_birth_year: {
                    identifier: 'date_of_birth_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter year."
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                date_of_birth_month: {
                    identifier: 'date_of_birth_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter month."
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                date_of_birth_day: {
                    identifier: 'date_of_birth_day',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter day."
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                divorse_dt_year: {
                    identifier: 'divorse_dt_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter year."
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                divorse_dt_month: {
                    identifier: 'divorse_dt_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter month."
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                divorse_dt_day: {
                    identifier: 'divorse_dt_day',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter day."
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                marriage_from_dt_year: {
                    identifier: 'marriage_from_dt_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter year."
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                marriage_from_dt_month: {
                    identifier: 'marriage_from_dt_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter month."
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                marriage_from_dt_day: {
                    identifier: 'marriage_from_dt_day',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter day."
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                marriage_to_dt_year: {
                    identifier: 'marriage_to_dt_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter year."
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                marriage_to_dt_month: {
                    identifier: 'marriage_to_dt_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter month."
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                marriage_to_dt_day: {
                    identifier: 'marriage_to_dt_day',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter day."
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });
        return $('.ui.previous-spouse.form').form('is valid');
    }

});
