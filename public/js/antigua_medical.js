$('document').ready(function() {

    checker('a1');
    radioCecker('a1');
    checker('a2');
    radioCecker('a2');
    checker('a3');
    radioCecker('a3');

    $('#medical-form').on('submit', function(event) {
        event.preventDefault();
        $('.ui.medical.form').addClass('loading');
        $('.ui.back.button').addClass('disabled');
        $('.ui.save-data.button').addClass('disabled');
        $('.ui.next.button').addClass('disabled');
        var val = formValidation();
        if (val) {
            $(this)[0].submit();
        } else {
            $('.ui.medical.form').removeClass('loading');
            $('.ui.back.button').removeClass('disabled');
            $('.ui.save-data.button').removeClass('disabled');
            $('.ui.next.button').removeClass('disabled');
        }
    });

    $('.ui.save-data.button').on('click', function() {
        $('.ui.medical.form').addClass('loading');
    });

    $('.ui.next.button').on('click', function() {
        $('.ui.medical.form').addClass('loading');
    });

    if ($('#a4_select').val() !== '0') {
        var arr     = $('#a4_select').val().split(",");
        var checks  = ['4a','4b','4c','4d','4e','4f','4g','4h','4i','4j','4k','4l','4m','4n','4o','4p','4q','4r','4s','4t','4u'];
        $.each(checks, function(index, value) {
            if ($.inArray(value, arr) !== -1) {
                $('.ui.'+value+'.checkbox').checkbox('check');
            }
        });
    }

    function checker(identifier) {
        $('.ui.'+identifier+'-yes.checkbox').checkbox({
            onChecked: function() {
                $('#'+identifier+'-message').show('slow');
                $('.ui.'+identifier+'-no.checkbox').checkbox('uncheck');
            },
            onUnchecked: function() {
                $('#'+identifier+'-message').hide('slow');
            }
        });
        $('.ui.'+identifier+'-no.checkbox').checkbox({
            onChecked: function() {
                $('#'+identifier+'-message').hide('slow');
                $('.ui.'+identifier+'-yes.checkbox').checkbox('uncheck');
            }
        });
    }

    function radioCecker(identifier) {
        if ($('#'+identifier).val() === '1') {
            $('.ui.'+identifier+'-yes.checkbox').checkbox('check');
            $('#'+identifier+'-message').show();
        } else if ($('#'+identifier).val() !== '1') {
            $('.ui.'+identifier+'-no.checkbox').checkbox('check');
        }
    }

    function formValidation() {
        $('.ui.medical.form').form({
            fields: {
                doctor_name: {
                    identifier  : 'doctor_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Doctors full name'
                        }
                    ]
                },
                tel: {
                    identifier  : 'tel',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Doctors telephone number'
                        }
                    ]
                },
                email: {
                    identifier  : 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Doctors email address'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter valid email address'
                        }
                    ]
                },
                doctor_street1: {
                    indentifier: 'doctor_street1',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address street'
                    }]
                },
                doctor_country: {
                    indentifier: 'doctor_country',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address country'
                    }]
                },
                doctor_town: {
                    indentifier: 'cur_town',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address town or city'
                    }]
                },
                doctor_post_code: {
                    indentifier: 'doctor_post_code',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address postal code'
                    }]
                }
            },
            inline : true,
            on     : 'blur'
        });

        return $('.ui.medical.form').form('is valid');
    }

});
