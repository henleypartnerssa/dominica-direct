$('document').ready(function() {

    $('#account').on('submit', function(event) {
        event.preventDefault();
        $(this).addClass('loading');
        var form = $('.ui.settings.form');
        var val = validateForm(form);
        if (val) {
            $(this)[0].submit();
        } else {
            $(this).removeClass('loading');
        }
    });

    function validateForm(form) {

        form.form({
            fields: {
                name: {
                    identifier: 'name',
                    rules: [
                        {
                            type    : 'empty',
                            prompt  : 'Please enter your First Name.'
                        }
                    ]
                },
                surname: {
                    identifier: 'surname',
                    rules: [
                        {
                            type    : 'empty',
                            prompt  : 'Please enter your First Name.'
                        }
                    ]
                },
                username: {
                    identifier: 'username',
                    rules: [
                        {
                            type    : 'empty',
                            prompt  : 'Please enter your First Name.'
                        },
                        {
                            type    : 'email',
                            prompt  : 'Invalid email address.'
                        }
                    ]
                },
                old_password: {
                    identifier: 'retypePassword',
                    rules: [
                        {
                            type    : 'match[password]',
                            prompt  : 'Passwords not matching.'
                        }
                    ]
                },
                doc_password: {
                    indentifier: 'doc_password',
                    rules: [
                        {
                            type: 'minLength[8]',
                            prompt: 'Password should contain more than 8 characters.'
                        }
                    ]
                },
                retype_doc_password: {
                    indentifier: 're_passsword',
                    rules: [
                        {
                            type: 'match[doc_password]',
                            prompt: 'Value does not match specified password.'
                        }
                    ]
                }
            },
            on     : 'blur',
            inline : true
        });
        return form.form('is valid');
    }

});
