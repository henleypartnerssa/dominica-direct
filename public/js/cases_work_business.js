$('document').ready(function() {

    $('#add_reference_link').on('click', function(event) {
        event.preventDefault();
        $('#reference_id').val('');
        $('.ui.reference.modal').modal('show');
    });

    deleteRef();
    editRef();
    disp('1');
    disp('2');
    checker('1');
    checker('2');

    // Add reference to DB and update Page List item
    $('.ui.add_reference.button').on('click', function() {
        var formValidated = formValidation();
        if (formValidated) {
            var formElem = $('.ui.reference.form');
            $.ajax({
                type                : 'POST',
                url                 : '/users/profile/add-employer-reference',
                async               : true,
                data                : {
                    _token              : $('#reference_token').val(),
                    profile_id          : $('#profile_id').val(),
                    ref_id              : $('#reference_id').val(),
                    from_month          : formElem.form('get value', 'from_reference_month'),
                    from_year           : formElem.form('get value', 'from_reference_year'),
                    to_month            : formElem.form('get value', 'to_reference_month'),
                    to_year             : formElem.form('get value', 'to_reference_year'),
                    occupation          : formElem.form('get value', 'occupation'),
                    employer_name       : formElem.form('get value', 'employer_name'),
                    employer_location   : formElem.form('get value', 'employer_location'),
                    type_of_business    : formElem.form('get value', 'type_of_business'),
                    reason_for_leaving  : formElem.form('get value', 'reason_for_leaving')
                },
                success: function(data, textStatus, jqXHR) {
                    var json = JSON.parse(data);

                    if (json.state === 'update') {
                        $('.item.'+json.id).remove();
                        $('.ui.references.list').append(htmlDep(json.id, json.ref));
                        $('.ui.reference.modal').modal('hide');
                        $('.ui.reference.form').form('clear');
                        editRef();
                    } else if (json.state === 'new') {
                        $('.ui.references.list').append(htmlDep(json.id, json.ref));
                        $('.ui.reference.modal').modal('hide');
                        $('.ui.reference.form').form('clear');
                        editRef();
                        deleteRef();
                    } else {
                        $('.ui.reference.modal').modal('hide');
                        $('.ui.reference.form').form('clear');
                    }
                }
            });
        }

    });

    function deleteRef() {
        $('.ui.del_reference.button').on('click', function() {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/users/profile/delete-employer-reference',
                async: true,
                data: {
                    _token  : $('#reference_token').val(),
                    id      : id,
                },
                success: function(data, textStatus, jqXHR) {
                    if (data !== '0') {
                        $('.item.'+id).remove();
                        $('.ui.reference.form').form('clear');
                    }
                }
            });
        });
    }

    function editRef() {
        $('.ui.edit_reference.button').on('click', function() {
            $('.ui.reference.modal').modal('show');
            $('.ui.reference.form').addClass('loading');

            var id = $(this).attr('id');
            var is_admin = $(this).data('admin');
            if (is_admin === 1) {
                $('#ref_is_admin').val('1');
            }

            $.ajax({
                type    : "POST",
                url     : '/users/profile/find-employer-reference',
                async   : true,
                data    : {
                    _token      : $('#reference_token').val(),
                    id          :  id
                },
                success: function(data) {

                    var json       = $.parseJSON(data);
                    var splitFrom  = json.from_date.split('/');
                    var splitTo    = json.to_date.split('/');

                    $('#reference_id').val(id);
                    $('#from_reference_month').val(splitFrom[0]);
                    $('#from_reference_year').val(splitFrom[1]);
                    $('#to_reference_month').val(splitTo[0]);
                    $('#to_reference_year').val(splitTo[1]);
                    $('#occupation').val(json.occupation);
                    $('#employer_name').val(json.employer);
                    $('#employer_location').val(json.location);
                    $('#type_of_business').val(json.type_of_business);
                    $('#reason_for_leaving').val(json.reason_for_leaving);
                    $('.ui.reference.form').removeClass('loading');
                }
            });

        });
    }

    function disp(num) {
        $('.ui.disiplinary_'+num+'_no.checkbox').checkbox();
        var msgBox = $('.field.disiplinary_'+num);
        $('.ui.disiplinary_'+num+'_no.checkbox').checkbox({
            onChecked: function() {
                msgBox.hide('slow');
                $('.ui.disiplinary_'+num+'_yes.checkbox').checkbox('uncheck');
            }
        });
        $('.ui.disiplinary_'+num+'_yes.checkbox').checkbox({
            onChecked: function() {
                msgBox.show('slow');
                $('.ui.disiplinary_'+num+'_no.checkbox').checkbox('uncheck');
            }
        });
    }

    function checker(num) {
        var val = $('#licence_'+num+'_msg').val();
        if (val === '1') {
            $('.ui.disiplinary_'+num+'_yes.checkbox').checkbox('check');
        } else {
            $('.ui.disiplinary_'+num+'_no.checkbox').checkbox('check');
        }
    }

    function htmlDep(id, name) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted edit_reference button" id="'+id+'">Edit</div><div class="ui red inverted del_reference button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image bookmark icon"></i><div class="content">' + name + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function formValidation() {

        $('.ui.reference.form').form({
            fields: {
                from_reference_month: {
                    identifier: 'from_reference_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter the month."
                        }
                    ]
                },
                from_reference_month: {
                    identifier: 'from_reference_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter year."
                        }
                    ]
                },
                to_reference_month: {
                    identifier: 'to_reference_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter the month."
                        }
                    ]
                },
                to_reference_year: {
                    identifier: 'to_reference_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter the year."
                        }
                    ]
                },
                occupation: {
                    identifier: 'occupation',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter your occupation."
                        }
                    ]
                },
                employer_name: {
                    identifier: 'employer_name',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter your previous employer name."
                        }
                    ]
                },
                employer_location: {
                    identifier: 'employer_location',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter the location."
                        }
                    ]
                },
                type_of_business: {
                    identifier: 'type_of_business',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter the type of bisuness."
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });
        return $('.ui.reference.form').form('is valid');
    }

});
