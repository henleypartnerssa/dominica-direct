$('document').ready(function() {

    var passportUpload      = new FileUploader("Passport","passport", "uploads");
    var nationalIdUpload    = new FileUploader("National ID","national_id", "uploads");
    var birthCertUpload     = new FileUploader("Birth Certificate","birth_certificate", "uploads");
    var proofResAddrUpload  = new FileUploader("Proof of Residential Address","proof_of_residential_address", "uploads");
    var marriageCertUpload  = new FileUploader("Marriage Certificate","marriage_certificate", "uploads");
    var divorseDocUpload    = new FileUploader("Divorse Document","divorse_document", "uploads");
    var letterAppUpload     = new FileUploader("Letter of Application","letter_of_application", "uploads");
    var profRefUpload       = new FileUploader("Professional Reference","professional_reference", "uploads");
    var personalRefUpload   = new FileUploader("Personal Reference","personal_reference", "uploads");
    var affFundsUpload      = new FileUploader("Affidavit attesting to Source of Funds","affidavit_funds", "uploads");
    var letterEmpUpload     = new FileUploader("Letter of Employment","letter_of_employment", "uploads");
    var letterRecUpload     = new FileUploader("Letter of Recommendation","letter_of_recommendation", "uploads");
    var policeCertUpload    = new FileUploader("Police Certificate","police_certificate", "uploads");
    var affParentUpload     = new FileUploader("Affidavit from Parent","affidavit_from_parent", "uploads");
    var banksRecommUpload   = new FileUploader("Banks Recommendations","banks_recommendations", "uploads");
    var businessPlanUpload  = new FileUploader("Business Plan","business_plan", "uploads");
    var millitaryServUpload = new FileUploader("Millitary Service Document","millitary_service", "uploads");
    var powerAttorUpload    = new FileUploader("Power of Attorney","power_of_attorney", "uploads");

    passportUpload.uploadFile();
    passportUpload.deleteFile();
    nationalIdUpload.uploadFile();
    birthCertUpload.uploadFile();
    proofResAddrUpload.uploadFile();
    marriageCertUpload.uploadFile();
    divorseDocUpload.uploadFile();
    letterAppUpload.uploadFile();
    profRefUpload.uploadFile();
    personalRefUpload.uploadFile();
    affFundsUpload.uploadFile();
    letterEmpUpload.uploadFile();
    letterRecUpload.uploadFile();
    policeCertUpload.uploadFile();
    affParentUpload.uploadFile();
    banksRecommUpload.uploadFile();
    businessPlanUpload.uploadFile();
    millitaryServUpload.uploadFile();
    powerAttorUpload.uploadFile();

});
