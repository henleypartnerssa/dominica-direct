$('document').ready(function() {
    var age             = $('#age').val();
    var ddPassUpload    = new FileUploader("Passport","passport", "uploads");
    var ddResUpload     = new FileUploader("Residency Card","residency", "uploads");
    ddPassUpload.uploadFile();
    ddPassUpload.deleteFile();
    ddResUpload.uploadFile();

    $('.ui.save.button').on('click', function() {
        $('#save').val('1');
    });

    $('.ui.next.button').on('click', function() {
        $('#save').val('0');
    });

    $('#impInfo-form').on('submit', function(event) {
        event.preventDefault();
        var val         = null;
        var passUpload  = true;
        var resUpload   = true;
        if (Number($('#total_passport').val()) <= 0) {
            passUpload = false;
            $('.ui.pass.message').show('slow');
        }
        if ($('#card_no').val() != '' && Number($('#total_residency').val()) <= 0) {
            resUpload = false;
            $('.ui.res.message').show('slow');
        }
        if (age <= 17) {
            val = formValForChild();
        } else {
            val = formValidation();
        }
        if (val && passUpload && resUpload) {
            $('.ui.important-info.form').addClass('loading');
            $('.ui.save.button').addClass('disabled');
            $('.ui.next.button').addClass('disabled');
            $(this)[0].submit();
        }
    });

    function formValidation() {
        $('.ui.important-info.form').form({
            fields: {
                surname: {
                    identifier: "surname",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Surname'
                        }
                    ]
                },
                given_names: {
                    identifier: "given_names",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Given Names'
                        }
                    ]
                },
                nationality: {
                    identifier: "nationality",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Nationality'
                        }
                    ]
                },
                gender: {
                    identifier: "gender",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Gender'
                        }
                    ]
                },
                email: {
                    identifier: "email",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Email Address'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter correct email address'
                        }
                    ]
                },
                contact_no: {
                    identifier: "contact_no",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Contact Number'
                        }
                    ]
                },
                dob_year: {
                    identifier: "dob_year",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Year of Birth'
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                dob_month: {
                    identifier: "dob_month",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Month of Birth'
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                dob_day: {
                    identifier: "dob_day",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Year of Birth'
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                pass_no: {
                    identifier: "pass_no",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Number'
                        }
                    ]
                },
                pass_country: {
                    identifier: "pass_country",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter the Passports Country of Issue'
                        }
                    ]
                },
                pass_issued_year: {
                    identifier: "pass_issued_year",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Issue Year'
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                pass_issued_month: {
                    identifier: "pass_issued_month",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Issue Month'
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                pass_issued_day: {
                    identifier: "pass_issued_day",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Issue Day'
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                pass_exp_year: {
                    identifier: "pass_exp_year",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Expiry Year'
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                pass_exp_month: {
                    identifier: "pass_exp_month",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Expiry Month'
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                pass_exp_day: {
                    identifier: "pass_exp_day",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Expiry Day'
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                cur_street1: {
                    identifier: "cur_street1",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your for the least your Main Street'
                        }
                    ]
                },
                cur_country: {
                    identifier: "cur_country",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Address Country'
                        }
                    ]
                },
                cur_town: {
                    identifier: "cur_town",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Address Town or City'
                        }
                    ]
                },
                cur_post_code: {
                    identifier: "cur_post_code",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Address Postal Code'
                        }
                    ]
                },
                employment: {
                    identifier: "employment",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select you level of employment'
                        }
                    ]
                },
                occupation: {
                    identifier: "occupation",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Occupation or Job Title'
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });
        return $('.ui.important-info.form').form('is valid');
    }

    function formValForChild() {
        $('.ui.important-info.form').form({
            fields: {
                surname: {
                    identifier: "surname",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Surname'
                        }
                    ]
                },
                given_names: {
                    identifier: "given_names",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Given Names'
                        }
                    ]
                },
                nationality: {
                    identifier: "nationality",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Nationality'
                        }
                    ]
                },
                gender: {
                    identifier: "gender",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Gender'
                        }
                    ]
                },
                dob_year: {
                    identifier: "dob_year",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Year of Birth'
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                dob_month: {
                    identifier: "dob_month",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Month of Birth'
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                dob_day: {
                    identifier: "dob_day",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Year of Birth'
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                pass_no: {
                    identifier: "pass_no",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Number'
                        }
                    ]
                },
                pass_country: {
                    identifier: "pass_country",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter the Passports Country of Issue'
                        }
                    ]
                },
                pass_issued_year: {
                    identifier: "pass_issued_year",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Issue Year'
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                pass_issued_month: {
                    identifier: "pass_issued_month",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Issue Month'
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                pass_issued_day: {
                    identifier: "pass_issued_day",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Issue Day'
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                pass_exp_year: {
                    identifier: "pass_exp_year",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Expiry Year'
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                pass_exp_month: {
                    identifier: "pass_exp_month",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Expiry Month'
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                pass_exp_day: {
                    identifier: "pass_exp_day",
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Passport Date of Expiry Day'
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });
        return $('.ui.important-info.form').form('is valid');
    }

});
