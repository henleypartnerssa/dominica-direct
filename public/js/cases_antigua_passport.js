$('document').ready(function() {

    delPass();
    editPass();

    $('#addPassport').on('click', function(event) {
        event.preventDefault();
        $('#pass_id').val('');
        $('.ui.passport.form').form('clear');
        $('.ui.passport.modal').modal('show');
    });

    $('#adding_passport').on('click', function() {
        var validation = passportModalValidation();

        if (validation) {
            $('.ui.passport.form').addClass('loading');
            $.ajax({
                type    : 'POST',
                url     : '/users/profile/add-identity-doc',
                async   : true,
                data    : {
                    _token              : $('#passport_token').val(),
                    pass_id             : $('#pass_id').val(),
                    profile_id          : $('#profile_id').val(),
                    issueing_country    : $('.ui.passport.form').form('get value', 'pass_issuing_country'),
                    document_number     : $('.ui.passport.form').form('get value', 'pass_number'),
                    place_of_issue      : $('.ui.passport.form').form('get value', 'pass_place'),
                    doi                 : normalizeDate($('.ui.passport.form'), 'doi'),
                    dox                 : normalizeDate($('.ui.passport.form'), 'dox')
                },
                success: function(data, textStatus, jqXHR) {
                    var json = JSON.parse(data);
                    if (json.flag === 'new') {
                        var admin = $('#pass_is_admin').val();
                        if (admin !== '0') {
                            $('.ui.main-passport.list').append(htmlDepAdmin(json.id, json.country));
                        } else {
                            $('.ui.main-passport.list').append(htmlDep(json.id, json.country));
                        }
                    } else {
                        $('.item.'+json.id).remove();
                        var admin = $('#pass_is_admin').val();
                        if (admin !== '0') {
                            $('.ui.main-passport.list').append(htmlDepAdmin(json.id, json.country));
                        } else {
                            $('.ui.main-passport.list').append(htmlDep(json.id, json.country));
                        }
                    }
                    $('.ui.passport.modal').modal('hide');
                    $('.ui.passport.form').form('clear');
                    $('.ui.passport.form').removeClass('loading');
                    delPass();
                    editPass();
                }
            });
        }

    });

    function delPass() {
        $('.ui.del_passport.delete.button').on('click', function() {

            var id = $(this).attr('id');

            $.ajax({
                type    : 'POST',
                url     : '/users/profile/delete-identity-doc',
                async   : true,
                data    : {
                    _token  : $('#passport_token').val(),
                    id      : id
                },
                success: function(data, textStatus, jqXHR) {
                    if (data === '1') {
                        $('.item.'+id).remove();
                    } else {
                        console.log('could not delete address');
                    }
                }
            });
        });
    }

    function editPass() {
        $('.ui.edit_passport.button').on('click', function() {
            $('.ui.passport.modal').modal('show');
            $('.ui.passport.form').addClass('loading');

            var id = $(this).attr('id');
            var is_admin = $(this).data('admin');

            if (is_admin === 'admin') {
                $('#pass_is_admin').val('1');
            }

            $.ajax({
                type    : "POST",
                url     : '/users/profile/identity-doc',
                async   : true,
                data    : {
                    _token  : $('#passport_token').val(),
                    id      :  id
                },
                success: function(data) {
                    var json        = $.parseJSON(data);
                    var splitIsDt   = json.date_of_issue.split('-');
                    var splitExDt   = json.date_of_expiration.split('-');

                    $('.ui.pass_issuing_country.dropdown').dropdown('set value', json.issuing_country);
                    $('.ui.pass_issuing_country.dropdown').dropdown('set text', json.issuing_country);

                    $('#pass_id').val(id);
                    $('#pass_number').val(json.document_number);
                    $('#pass_place').val(json.place_of_issue);
                    $('#pass_doi_year').val(splitIsDt[0]);
                    $('#pass_doi_month').val(splitIsDt[1]);
                    $('#pass_doi_day').val(splitIsDt[2]);
                    $('#pass_dox_year').val(splitExDt[0]);
                    $('#pass_dox_month').val(splitExDt[1]);
                    $('#pass_dox_day').val(splitExDt[2]);
                    $('.ui.passport.form').removeClass('loading');
                }
            });

        });
    }

    function normalizeDate(form,flag) {
        var year    = form.form('get value','pass'+'_'+flag+'_'+'year');
        var month   = form.form('get value','pass'+'_'+flag+'_'+'month');
        var day     = form.form('get value','pass'+'_'+flag+'_'+'day');
        month       = (month.length === 1) ? "0" + month : month;
        day         = (day.length === 1) ? "0" + day : day;
        return year + month + day;
    }

    function htmlDep(id, name) {
        var sect1   = '<div class="item ' +id+ '" data-id="'+id+'"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted edit_passport edit button" id="'+id+'">Edit</div><div class="ui red inverted del_passport delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image book icon"></i><div class="content">' + name + ' Passport </div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function htmlDepAdmin(id, name) {
        var sect1   = '<div class="item ' +id+ '" data-id="'+id+'"><div class="right floated content">';
        var sect2   = '<div class="ui btn-color edit_passport edit button" id="'+id+'">Edit</div></div>';
        var sect3   = '<i class="ui avatar image book icon"></i><div class="content">' + name + ' Passport </div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function passportModalValidation() {
        $('.ui.passport.form').form({
            fields: {
                pass_issuing_country: {
                    indentifier: 'pass_issuing_country',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please select a issuing Coutrny'
                    }]
                },
                pass_number: {
                    indentifier: 'pass_number',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your passport number'
                    }]
                },
                pass_place: {
                    indentifier: 'pass_place',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter the place where your passport has been issued'
                    }]
                },
                pass_doi_year: {
                    indentifier: 'pass_doi_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the year your passport was issued'
                        },
                        {
                            type: 'integer[1900..2050]',
                            prompt: 'Please enter correct Year'
                        }
                    ]
                },
                pass_doi_month: {
                    indentifier: 'pass_doi_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the month your passport was issued'
                        },
                        {
                            type: 'integer[1..12]',
                            prompt: 'Please enter correct month'
                        }
                    ]
                },
                pass_doi_day: {
                    indentifier: 'pass_doi_day',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the day your passport was issued'
                        },
                        {
                            type: 'integer[1..31]',
                            prompt: 'Please enter correct day'
                        }
                    ]
                },
                pass_dox_year: {
                    indentifier: 'pass_dox_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the year your passport expires'
                        },
                        {
                            type: 'integer[1900..2050]',
                            prompt: 'Please enter correct Year'
                        }
                    ]
                },
                pass_dox_month: {
                    indentifier: 'pass_dox_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the month your passport expires'
                        },
                        {
                            type: 'integer[1..12]',
                            prompt: 'Please enter correct month'
                        }
                    ]
                },
                pass_dox_day: {
                    indentifier: 'pass_dox_day',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the day your passport expires'
                        },
                        {
                            type: 'integer[1..31]',
                            prompt: 'Please enter correct day'
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });
        return $('.ui.passport.form').form('is valid');
    }

});
