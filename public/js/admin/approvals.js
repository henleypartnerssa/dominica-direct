$('document').ready(function() {

    var items = ['dd_check', 'data_review', 'payment_for_submission', 'received_package', 'package_dd_check', 'ciu_approval', 'payment_for_investment', 'citizenship_docs', 'visa'];

    items.forEach(function(item) {

        $('.ui.'+item+'.checkbox').checkbox({
            beforeChecked: function() {
                var obj1 = {
                    type:   item,
                    action: true,
                };
                $(this).checkbox('set disabled');
                var update = updateState(obj1);
                if (update) {
                    $(this).checkbox('set checked');
                    $(this).checkbox('set enabled');
                }
            },
            beforeUnchecked: function() {
                var obj2 = {
                    type:   item,
                    action: false,
                };
                $(this).checkbox('set disabled');
                var update = updateState(obj2);
                if (update) {
                    $(this).checkbox('set unchecked');
                    $(this).checkbox('set enabled');
                }
            }
        });

    });

    function updateState(obj) {
        $.ajax({
            type:   'POST',
            url:    '/admin/case/update-profile-state',
            data:   {
                _token  : $('#token').val(),
                type    : obj.type,
                id      : $('#id').val(),
                state   : obj.action
            },
            success: function(data) {
                if (data === '1') {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
    }

});
