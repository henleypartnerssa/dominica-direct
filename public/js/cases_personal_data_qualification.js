$('document').ready(function() {

    deleteQual();
    editQual();

    $('#add_qualification').on('click', function(event) {
        event.preventDefault();
        $('.ui.qualification.modal').modal('show');
        $('#qual_id').val('');
    });

    $('.ui.add_qualification.button').on('click', function() {

        var val = validateForm();

        if (val) {
            $('.ui.qualification.form').addClass('loading');
            $.ajax({
                type: 'POST',
                url: '/users/profile/add-qualification',
                async: true,
                data: {
                    _token      : $('#qualification_token').val(),
                    id          : $('#qual_id').val(),
                    profile_id  : $('#profile_id').val(),
                    school      : $('.ui.qualification.form').form('get value', 'school'),
                    type        : $('.ui.qualification.form').form('get value', 'qualification'),
                    from_month  : $('.ui.qualification.form').form('get value', 'from_qual_month'),
                    from_year   : $('.ui.qualification.form').form('get value', 'from_qual_year'),
                    to_month    : $('.ui.qualification.form').form('get value', 'to_qual_month'),
                    to_year     : $('.ui.qualification.form').form('get value', 'to_qual_year'),
                    location    : $('.ui.qualification.form').form('get value', 'qual_location')
                },
                success: function(data, textStatus, jqXHR) {
                    var jason = JSON.parse(data);
                    $('.item.'+jason.id).remove();
                    if($('#qual_is_admin').val() !== '0') {
                        $('#qual_list').append(htmlDepAdmin(jason.id, jason.name, jason.type));
                    } else {
                        $('#qual_list').append(htmlDep(jason.id, jason.name, jason.type));
                    }
                    $('.ui.qualification.modal').modal('hide');
                    $('.ui.qualification.form').form('clear');
                    $('.ui.qualification.form').removeClass('loading');
                    deleteQual();
                    editQual();
                }
            });
        }
    });

    function deleteQual() {
        $('.ui.qual.delete.button').on('click', function() {
           var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/users/profile/delete-qualification',
                async: true,
                data: {
                    _token  : $('#qualification_token').val(),
                    id      : id,
                },
                success: function(data, textStatus, jqXHR) {
                    if (data !== '0') {
                        $('.item.'+id).remove();
                    }
                }
            });
        });
    }

    function editQual() {
        $('.ui.qual.edit.button').on('click', function() {
            $('.ui.qualification.modal').modal('show');
            $('.ui.qualification.form').addClass('loading');

            var id = $(this).attr('id');
            var is_admin = $(this).data('admin');
            if (is_admin === 1) {
                $('#qual_is_admin').val('1');
            }

            $.ajax({
                type    : "POST",
                url     : '/users/profile/post-find-qualification',
                async   : true,
                data    : {
                    _token  : $('#qualification_token').val(),
                    id      :  id
                },
                success: function(data) {
                    var json       = $.parseJSON(data);
                    var splitFrom  = json.from_date.split('/');
                    var splitTo    = json.to_date.split('/');

                    $('#qual_id').val(id);
                    $('#from_qual_month').val(splitFrom[0]);
                    $('#from_qual_year').val(splitFrom[1]);
                    $('#to_qual_month').val(splitTo[0]);
                    $('#to_qual_year').val(splitTo[1]);
                    $('#school').val(json.qualification_name);
                    $('#qualification').val(json.qualification_type);
                    $('#qual_location').val(json.location);
                    $('.ui.qualification.form').removeClass('loading');
                }
            });

        });
    }

    function htmlDep(id, name, type) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted qual edit button" id="'+id+'">Edit</div><div class="ui red inverted qual delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image university icon"></i><div class="content">' + name + ' ( ' + type + ' )' + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function htmlDepAdmin(id, name, type) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui btn-color qual edit button" id="'+id+'">Edit</div></div>';
        var sect3   = '<i class="ui avatar image university icon"></i><div class="content">' + name + ' ( ' + type + ' )' + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function validateForm() {
        $('.ui.qualification.form').form({
            fields: {
                schoool: {
                    identifier: 'school',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter your school name'
                    }]
                },
                qualification: {
                    identifier: 'qualification',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter qualification type'
                    }]
                },
                qual_location: {
                    identifier: 'qual_location',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter school location'
                    }]
                },
                from_qual_month: {
                    identifier: 'from_qual_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the From date Month'
                        },
                        {
                            type: 'integer[1..12]',
                            prompt: 'Please enter correct month'
                        }
                    ]
                },
                from_qual_year: {
                    identifier: 'from_qual_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the From date Year'
                        },
                        {
                            type: 'integer[1900..2050]',
                            prompt: 'Please enter correct Year'
                        }
                    ]
                },
                to_qual_month: {
                    identifier: 'to_qual_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the To date Month'
                        },
                        {
                            type: 'integer[1..12]',
                            prompt: 'Please enter correct month'
                        }
                    ]
                },
                to_qual_year: {
                    identifier: 'to_qual_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the To date Year'
                        },
                        {
                            type: 'integer[1900..2050]',
                            prompt: 'Please enter correct Year'
                        }
                    ]
                }
            },
            on: 'blur',
            inline: true
        });
        return $('.ui.qualification.form').form('is valid');
    }

});
