$('document').ready(function() {

    // terms and conditions indicator
    var tc      = false;
    var mail    = false;

    /**
     * On Modal Close button click:
     * Clear Regsiter Form
     * Unbind Register Button Click Event
     * @param  {[button]} '.ui.close.button' [Modal Close Button]
     */
    $('.ui.close.button').on('click', function() {
        $('.ui.second.modal').modal('hide');
        $('.ui.register.form').form('clear');
        $('.ui.register.form').form('destroy');
        $('.ui.register.form').form('blur');
        $('.ui.progress').hide();
        $('#rating').hide();
        $('.ui.register.button').unbind('click');
    });

    /**
     * On Login Form Submit:
     * Form validation
     * Disable submit behaviour and Login Button
     * @param  {[form]} '#login-form' [Login Form]
     */
    $('#login-form').on('submit', function(event) {
        event.preventDefault();
        $(this).addClass('loading');
        $('.ui.login.button').addClass('disabled');
        $('.ui.login.button').addClass('loading');
        var val = loginFormValidation();
        if (val) {
            $(this)[0].submit();
        } else {
            $('.ui.login.button').removeClass('disabled');
            $('.ui.login.button').removeClass('loading');
        }
    });

    /**
     * On check/uncheck:
     * Set tc varable
     * @param  {[checkbox]} '.ui.checkbox' [text and conditions checkbox]
     */
    $('.ui.checkbox').checkbox({
        onChecked: function() {
            tc = true;
        },
        onUnchecked: function() {
            tc = false;
        },
    });

    /**
     * On Button click:
     * Form Validation
     * Validate if T&C are checked or not if not then alert
     * Perform AJAX request to register new User
     * @param  {[button]} '.ui.register.button' [register button]
     */
    $('.ui.register.button').on('click', function() {
        var val     = regFormValidation();
        var form    = $('.ui.register.form');
        $(this).addClass('disabled');
        $(this).addClass('loading');
        if (val === true) {

            if (mail) {
                $('#mail-warning').show("slow");
                $(this).removeClass('disabled');
                $(this).removeClass('loading');
            } else if (!tc) {
                $('#regsiter-warning').show("slow");
                $(this).removeClass('disabled');
                $(this).removeClass('loading');
            } else {
                $.ajax({
                    type:   'POST',
                    url:    '/users/register',
                    async:  true,
                    data    : {
                        _token      : form.find('#_token').val(),
                        name        : form.find('#name').val(),
                        surname     : form.find('#lastname').val(),
                        password    : form.find('#rpassword').val(),
                        email       : form.find('#email').val()
                    },
                    success : function(data) {
                        $('.ui.register.button').removeClass('disabled');
                        $('span#echoEmail').text($('.ui.register.form').find('#email').val());
                        $('.second.modal').modal('show');
                        $('.ui.register.button').removeClass('disabled');
                        $('.ui.register.button').removeClass('loading');
                    }
                });
            }
        } else {
            $(this).removeClass('disabled');
            $(this).removeClass('loading');
        }
    });

    $('#rpassword').on('input', function(e) {
        $('.ui.progress').show();
        $('#rating').show();
        passRatiing($(this).val());
    });

    $('#email').blur(function() {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        mailDupCheck();
    });

    function mailDupCheck() {
        var form = $('.ui.register.form');
        $.ajax({
            type:   'POST',
            url:    '/users/mail-check',
            async:  true,
            data    : {
                _token      : form.find('#_token').val(),
                email       : form.find('#email').val()
            },
            success : function(answer) {
                console.log(answer);
                $('#email').removeClass('loading');
                $('#email').removeClass('disabled');
                if (answer !== 'OK') {
                    $('#dup-email').show('slow');
                    mail = true;
                } else {
                    $('#dup-email').hide('slow');
                    mail = false;
                }
            }
        });
    }

    var totUpper    = 0;
    var totSpecial  = 0;
    var totnum      = 0;
    var spcial      = ['!','@','#','$','%','^','&','*','(',')','-','=','{','}','[',']',';',':','\'','\"','\\','|','`','~','<',',', '>', '.', '/', '?', '±', '§'];
    var number      = ['1','2','3','4','5','6','7','8','9'];

    /**
     * Validate if text contants any upper,special
     * or numeric charaters.
     * @param  {[string]} text [textbox value]
     */
    function checkText(text) {
        var up = 0;
        var sp = 0;
        var nm = 0;
        for (var i = 0, len = text.length; i < len; i++) {
            var txt = text[i];
            if (txt == txt.toUpperCase()) {
              ++up;
            }
            if (spcial.indexOf(txt) != -1) {
              ++sp;
            }
            if (number.indexOf(txt) != -1) {
              ++nm;
            }
        }
        totUpper    = up;
        totSpecial  = sp;
        totnum      = nm;
    }

    /**
     * Validate and Applies Password Strength
     * @param  {[string]} text [texbox text value]
     */
    function passRatiing(text) {
        checkText(text);
        var len = text.length;

        if (len == 0) {
            $('.ui.progress').hide();
            $('#rating').hide();
        }

        if (len > 0 && len <= 3) {
            $('.ui.progress').progress({
                percent : 10
            });
            $('#rating').text("very-weak");
            $('#rating').css('color', '#D95C5C');
        } else if (len < 5) {
            $('.ui.progress').progress({
                percent : 10
            });
            $('#rating').text("very-weak");
            $('#rating').css('color', '#D95C5C');
            if (totUpper == 1 || totSpecial == 1 || totnum == 1) {
                $('.ui.progress').progress({
                    percent : 30
                });
                $('#rating').text("weak");
                $('#rating').css('color', '#EFBC72');
            }
            if (totUpper > 1 || totSpecial > 1 || totnum > 1) {
                $('.ui.progress').progress({
                    percent : 50
                });
                $('#rating').text("okay");
                $('#rating').css('color', '#E6BB48');
            }
        } else if (len > 5) {
            $('.ui.progress').progress({
                percent : 50
            });
            $('#rating').text("okay");
            $('#rating').css('color', '#E6BB48');
            if (totUpper == 1 || totSpecial == 1 || totnum == 1) {
                $('.ui.progress').progress({
                    percent : 70
                });
                $('#rating').text("strong");
                $('#rating').css('color', '#B4D95C');
            }
            if (totUpper > 1 || totSpecial > 1 || totnum > 1) {
                $('.ui.progress').progress({
                    percent : 100
                });
                $('#rating').text("very-strong");
                $('#rating').css('color', '#21BA45');
            }
        }
    }

    /**
     * Validates Login Form.
     *
     * @return {bool} [Validation result]
     */
    function loginFormValidation() {

        var test = $('.ui.login.form').form({
            fields: {
                usename: {
                    identifier  : 'username',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your username'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter valid email address'
                        }
                    ]
                },
                password: {
                    identifier  : 'login_password',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your password'
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });

        return $('.ui.login.form').form('is valid');

    }

    /**
     * Validates Regsiter Form.
     *
     * @return {bool} [Validation result]
     */
    function regFormValidation() {
        var test = $('.ui.register.form').form({
            fields: {

                name: {
                    identifier  : 'name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your name'
                        }
                    ]
                },
                lastname: {
                    identifier  : 'lastname',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your Last Name'
                        }
                    ]
                },
                password: {
                    identifier  : 'rpassword',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your password'
                        }
                    ]
                },
                retypePassword: {
                    identifier  : 'password_conf',
                    rules: [
                        {
                            type   : 'match[rpassword]',
                            prompt : 'Password not mathcing'
                        }
                    ]
                },
                email: {
                    identifier  : 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your email'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter valid email address'
                        }
                    ]
                },
                retypeEmail: {
                    identifier  : 'email_conf',
                    rules: [
                        {
                            type   : 'match[email]',
                            prompt : 'Email not matching'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter valid email address'
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });

        return $('.ui.register.form').form('is valid');
    }
});
