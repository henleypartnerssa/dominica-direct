$('document').ready(function() {

    $('.ui.submit.button').on('click', function() {
        $('.ui.success.message').hide('slow');
        var uri = window.location.href;
        $.ajax({
            type: 'POST',
            url: uri,
            async: true,
            data: {
                _token      : $('#token').val(),
                courier_no  : $('#courier_no').val(),
                type        : $('#type').val()
            },
            success: function(data) {
                $('.ui.success.message').show('slow');
            }
        });
    });

});
