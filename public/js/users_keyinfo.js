$('document').ready(function() {

    $('#save_dep').on('click', function() {

        var isValidForm = mainFormValidation('key_info');

        if (isValidForm) {
            KeyInfo('create');
        }

    });

    $('#add_dep').on('click', function() {

        var isValidForm = mainFormValidation('key_info');

        if (isValidForm) {
            dropForm('key_dep');
            KeyInfo('update');
        }

    });

    $('#key_add_dep').on('click', function(event) {
        event.preventDefault();

        var isValidForm = mainFormValidation('key_dep');

        if (isValidForm) {
            newKeyDep();
        }

    });

    $('.ui.delete.button').on('click', function() {
        delKeyDep($(this).attr('id'));
    })

    function mainFormValidation(formType) {

        var form_element = $('.ui.'+formType+'.form');

        form_element.form({
            fields  : {
                name: {
                    identifier  : 'name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your name'
                        }
                    ]
                },
                surname: {
                    identifier  : 'surname',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your surname'
                        }
                    ]
                },
                year: {
                    identifier  : 'year',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your year of birth'
                        },
                        {
                            type    : 'integer[1900..2050]',
                            prompt  : 'Please enter correct year'
                        }
                    ]
                },
                month: {
                    identifier  : 'month',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your month of birth'
                        },
                        {
                            type    : 'integer[1..12]',
                            prompt  : 'Please enter correct month'
                        }
                    ]
                },
                day: {
                    identifier  : 'day',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your day of birth'
                        },
                        {
                            type    : 'integer[1..31]',
                            prompt  : 'Please enter correct day'
                        }
                    ]
                },
                gender: {
                    identifier  : 'gender',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select your Gender'
                        }
                    ]
                },
                type: {
                    identifier  : 'type',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select Dependant Type'
                        }
                    ]
                },
                relationship: {
                    identifier  : 'relationship',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select Offspring Type'
                        }
                    ]
                },
            },
            inline  : true,
            on      : 'blur'
        });

        return form_element.form('is valid');
    }

    function KeyInfo(command) {

        $.ajax({
            type:   'POST',
            url:    '/users/key-info/'+command,
            async:  true,
            data:   buildFormData('key_info'),
            success: function (data, textStatus, jqXHR) {
                if (data === '1') {
                    $('.ui.key_dep.modal').modal('show');
                } else if (data === '2') {
                    window.location.href = '/users/dashboard';
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    }

    function newKeyDep() {

        $.ajax({
            type:   'POST',
            url:    '/users/profile/add-dependant',
            async:  true,
            data:   buildFormData('key_dep'),
            success: function (data, textStatus, jqXHR) {

                if (data === '0') {
                    $('.ui.key_dep.modal').modal('hide');
                    $('.ui.negative.message').show('slow');
                } else {

                    var json = JSON.parse(data);

                    $('#dep').show();
                    $('.ui.list').append(htmlDep(json.id, json.fullname, json.type));
                    $('.ui.key_dep.modal').modal('hide');
                    dropForm('key_dep');
                    $('#'+json.id).on('click', function() {
                        delKeyDep(json.id);
                    });
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    }

    function delKeyDep(id) {

        var token = $('#key_token').val();

        $.ajax({
            type:   'POST',
            url:    '/users/profile/delete-dependant',
            async:  true,
            data:   '_token='+token+'&id='+id,
            success: function (data, textStatus, jqXHR) {
                if (data === '1') {
                    $('.item.'+id).remove();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    }

    function buildFormData(formType) {
        var form    = $('.ui.'+formType+'.form');
        var token   = $('#key_token').val();

        var data = {
            _token          : token,
            name            : form.find('#name').val(),
            type            : form.find('#type').val(),
            relationship    : form.find('#relationship').val(),
            surname         : form.find('#surname').val(),
            dob             : form.find('#year').val() + form.find('#month').val() + form.find('#day').val(),
            gender          : form.find('#gender').val()
        };

        return data;
    }

    function htmlDep(id, name, type) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui red delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image user icon"></i><div class="content">' + name + ' ' + '(' + type + ')' + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function dropForm(formType) {
        var form = $('.ui.'+formType+'.form');
        form.find('#name').val('');
        form.find('#surname').val('');
        form.find('#year').val('');
        form.find('#month').val('');
        form.find('#day').val('');
        form.find('#gender').val('');
        form.form('clear');
    }

});
