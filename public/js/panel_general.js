$('document').ready(function() {

    $('.disabled').on('click', function(e) {
        e.preventDefault();
    });

    $('.close.icon').on('click', function() {
        $(this).parent().hide('slow');
    });

    $('.ui.save-data.button').on('click', function() {
        $('#save').val('1');
    });

    $('.ui.dropdown').dropdown();
    $('.ui.accordion').accordion();
    $('.inline.icon').popup({inline: true});
    $('.ui.radio.checkbox').checkbox();

    // On Quote Create Click
    $('#a_quote').on('click', function() {
        $('.ui.quote.modal').modal('show');
    });

    $('.ui.passport.button').on('click', function() {
        $('.ui.passport.modal').modal('show');
    });

});
