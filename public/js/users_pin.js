$('document').ready(function() {

    // Declare Form
    var form = $('.ui.pin.form');
    // Declare Form Send Button
    var sendBtn     = form.find('#checkPin');
    var resendBtn   = form.find('#resendPin');

    // On Send Pin Button Click
    sendBtn.on('click', function() {

        $('#checkPin').addClass('loading');
        $('#checkPin').addClass('disabled');
        var token   = $('#_token').val();
        var num1    = $('#num1').val();
        var num2    = $('#num2').val();
        var num3    = $('#num3').val();
        var num4    = $('#num4').val();
        var number  = num1.concat(num2).concat(num3).concat(num4);
        // validate Pin via Ajax Call
        $.ajax({
            type: 'POST',
            url: '/users/mobile/pin',
            async: true,
            data: '_token='+ token +'&pin=' + Number(number),
            success: function(data, textStatus, jqXHR) {
                if (data === 'valid') {
                    window.location.href = '/users/dashboard';
                } else if (data === 'invalid') {
                    $('.ui.pin.message').show('slow');
                    $('#checkPin').removeClass("loading");
                    $('#checkPin').removeClass("disabled");
                } else {
                    window.location.href = '/users/locked-account';
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });

    });

    resendBtn.on('click', function() {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var token   = $('#_token').val();
        // validate Pin via Ajax Call
        $.ajax({
            type: 'GET',
            url: '/users/resend-pin',
            async: true,
            data: '_token='+ token,
            success: function(data, textStatus, jqXHR) {
                if (data === '1') {
                    $('#resendPin').removeClass('loading');
                    $('#resendPin').removeClass('disabled');
                    $('.ui.resend-pin.positive.message').show('slow');
                } else {
                    $('#resendPin').removeClass('loading');
                    $('#resendPin').removeClass('disabled');
                    $('.ui.resend-pin.negative.message').show('slow');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#resendPin').removeClass('loading');
                $('#resendPin').removeClass('disabled');
                $('.ui.resend-pin.negative.message').show('slow');
            }
        });

    });

});
