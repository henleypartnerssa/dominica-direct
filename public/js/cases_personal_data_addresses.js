$('document').ready(function () {

    $('#add_address').on('click', function(event) {
        event.preventDefault();

        // Reset Address ID
        $('#addr_id').val('');

        $('.ui.address.form').form('clear');
        $('.ui.address.modal').modal('show');
        $('.ui.add_addr_country.dropdown').removeClass('disabled');
    });

    deleteAdd();
    editAddress();

    $('#adding_address').on('click', function() {
        var validate = formValidation();
        if (validate) {
            $('.ui.address.form').addClass('loading');
            $.ajax({
                type: 'POST',
                url: '/users/profile/add-address',
                async: true,
                data: {
                    _token      : $('#addr_token').val(),
                    type        : $('#addr_type').val(),
                    profile_id  : $('#profile_id').val(),
                    application_id : $('#appId').val(),
                    addr_id     : $('#addr_id').val(),
                    line1       : $('.ui.address.form').form('get value', 'add_addr_line_1'),
                    line2       : $('.ui.address.form').form('get value', 'add_addr_line_2'),
                    country     : $('.ui.address.form').form('get value', 'add_addr_country'),
                    city        : $('.ui.address.form').form('get value', 'add_addr_town'),
                    code        : $('.ui.address.form').form('get value', 'add_addr_postal_code'),
                    from_month  : $('.ui.address.form').form('get value', 'from_addr_month'),
                    from_year   : $('.ui.address.form').form('get value', 'from_addr_year'),
                    to_month    : $('.ui.address.form').form('get value', 'to_addr_month'),
                    to_year     : $('.ui.address.form').form('get value', 'to_addr_year')
                },
                success: function(data, textStatus, jqXHR) {
                    var json = JSON.parse(data);
                    if (json.saved !== '0' && json.saved !== '2') {
                        var admin = $('#add_is_admin').val();
                        if (admin !== '0') {
                            $('.ui.address.selection.list').append(htmlAddrAdmin(json.id, json.addr));
                        } else {
                            $('.ui.address.selection.list').append(htmlAddr(json.id, json.addr));
                        }
                        $('.ui.address.modal').modal('hide');
                        $('.ui.address.form').form('clear');
                        $('.ui.address.form').removeClass('loading');
                        deleteAdd();
                        editAddress();
                    } else if (json.saved === '2') {
                        $('.item.'+json.id).remove();
                        if (admin !== '0') {
                            $('.ui.address.selection.list').append(htmlAddrAdmin(json.id, json.addr));
                        } else {
                            $('.ui.address.selection.list').append(htmlAddr(json.id, json.addr));
                        }
                        $('.ui.address.modal').modal('hide');
                        $('.ui.address.form').form('clear');
                        $('.ui.address.form').removeClass('loading');
                        editAddress();
                    } else {
                        $('.ui.address.modal').modal('hide');
                        $('.ui.address.form').form('clear');
                        $('.ui.address.form').removeClass('loading');
                    }
                }
            });
        }
    });

    function deleteAdd() {
        $('.ui.address.delete.button').on('click', function() {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/users/profile/delete-address',
                async: true,
                data: {
                    _token  : $('#addr_token').val(),
                    id      : id,
                },
                success: function(data, textStatus, jqXHR) {
                    if (data !== '0') {
                        $('.item.'+id).remove();
                    }
                }
            });
        });
    }

    function editAddress() {
        $('.ui.address.edit.button').on('click', function() {
            $('.ui.address.modal').modal('show');
            $('.ui.address.form').addClass('loading');

            var id = $(this).attr('id');
            var is_admin = $(this).data('admin');

            if (is_admin === 1) {
                $('#add_is_admin').val('1');
            }

            $.ajax({
                type    : "POST",
                url     : '/users/profile/find-address',
                async   : true,
                data    : {
                    _token      : $('#addr_token').val(),
                    id          :  id,
                    profile_id  : $('#addr_profile_id').val()
                },
                success: function(data) {
                    if (data !== '0') {
                        var json       = $.parseJSON(data);
                        var splitFrom  = json.pivot.from_date.split('/');
                        var splitTo    = json.pivot.to_date.split('/');

                        $('.ui.add_addr_country.dropdown').dropdown('set value', json.country);
                        $('.ui.add_addr_country.dropdown').dropdown('set text', json.country);

                        $('#addr_id').val(id);
                        $('#from_addr_month').val(splitFrom[0]);
                        $('#from_addr_year').val(splitFrom[1]);
                        $('#to_addr_month').val(splitTo[0]);
                        $('#to_addr_year').val(splitTo[1]);
                        $('#to_addr').val(json.pivot.to_date);
                        $('#add_addr_line_1').val(json.street1);
                        $('#add_addr_line_2').val(json.street2);
                        $('#add_addr_town').val(json.town);
                        $('#add_addr_postal_code').val(json.postal_code);
                        $('.ui.address.form').removeClass('loading');
                    }
                }
            });

        });
    }

    function formValidation() {
        $('.ui.address.form').form({
            fields: {
                add_addr_line_1: {
                    identifier: 'add_addr_line_1',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter the first line of your address'
                    }]
                },
                add_addr_country: {
                    identifier: 'add_addr_country',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter the address Country'
                    }]
                },
                add_addr_postal_code: {
                    identifier: 'add_addr_postal_code',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter the address postal code'
                    }]
                },
                from_addr_month: {
                    identifier: 'from_addr_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the From date Month'
                        },
                        {
                            type: 'integer[1..12]',
                            prompt: 'Please enter correct month'
                        }
                    ]
                },
                from_addr_year: {
                    identifier: 'from_addr_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the From date Year'
                        },
                        {
                            type: 'integer[1900..2050]',
                            prompt: 'Please enter correct Year'
                        }
                    ]
                },
                to_addr_month: {
                    identifier: 'to_addr_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the To date Month'
                        },
                        {
                            type: 'integer[1..12]',
                            prompt: 'Please enter correct month'
                        }
                    ]
                },
                to_addr_year: {
                    identifier: 'to_addr_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter the To date Year'
                        },
                        {
                            type: 'integer[1900..2050]',
                            prompt: 'Please enter correct Year'
                        }
                    ]
                }
            },
            on: 'blur',
            inline: true
        });
        return $('.ui.address.form').form('is valid');
    }

    function htmlAddr(id, addr) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted address edit button" id="'+id+'">Edit</div><div class="ui red inverted address delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image marker icon"></i><div class="content">' + addr + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function htmlAddrAdmin(id, addr) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted address edit button" id="'+id+'">Edit</div><div class="ui red inverted address delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image marker icon"></i><div class="content">' + addr + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }


});
