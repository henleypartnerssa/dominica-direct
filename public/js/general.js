$('document').ready(function() {

    // disable all default behaviour
    $('.disabled').on('click', function(e) {
        e.preventDefault();
    });

    $('.ui.accordion').accordion();

    $('.ui.coupled.modal').modal({
        allowMultiple: false
    });

    $('.close.icon').on('click', function() {
        $(this).parent().hide();
    });

});
