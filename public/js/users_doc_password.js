$('document').ready(function() {

    $('#doc_password').on('submit', function(event){
        event.preventDefault();
        $(this).addClass('loading');
        var val = validation();
        if (val) {
            $(this)[0].submit();
        } else {
            $(this).removeClass('loading');
        }
    });

    function validation() {
        var form = $('.ui.doc_password.form');
        form.form({
            fields: {
                password: {
                    indentifier: 'password',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter document password.'
                        },
                        {
                            type: 'minLength[8]',
                            prompt: 'Password should contain more than 8 characters.'
                        }
                    ]
                },
                re_passsword: {
                    indentifier: 're_passsword',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please re-type document password.'
                        },
                        {
                            type: 'match[password]',
                            prompt: 'Value does not match specified password.'
                        }
                    ]
                }
            },
            on: 'blur',
            inline: true
        });
        return form.form('is valid');
    }

});
