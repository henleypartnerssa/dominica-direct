$('document').ready(function() {

    FileUploader = function FileUploader(DocumentType, KeyIdentifier, Where) {
        this.docType    = DocumentType;
        this.keyID      = KeyIdentifier;
        this.where      = Where;
        this.counter    = Number($('#total_'+KeyIdentifier).val());

        this.generateFileInput = function(type) {
           ++this.counter;
           var code = this.keyID+"-"+this.counter;
           var obj = {
               element : '<input type="file" name="'+code+'" id="'+code+'">',
               code    : code
           };
           return obj;
       }

       this.addToList = function(type, obj) {
           var _this = this;
           $('#'+obj.code).change(function(){
               $('.ui.'+type+'.list').append(_this.htmlDep(type, obj.code, $('#'+obj.code)[0].files[0].name));
               _this.uploadFileEvnt(obj.code,type);
           });
       }

        // executes 1st
        this.uploadFile = function() {
            var _this = this;
            // create click event for add link
            $('#add_'+this.keyID).on('click', function(event){
                event.preventDefault();
                var obj = _this.generateFileInput(_this.keyID); // generate the input type of file for file upload window etc...
                $('#file_inputs').append(obj.element);          // append to input type to div holder
                $('#'+obj.code).trigger('click');               // trigers click event on file input type to open native file window
                _this.addToList(_this.keyID, obj);              // append to list view
            });
        }

        this.uploadFileEvnt = function(code,type) {
            var _this = this;
            $('#button-'+code).on('click', function() {
                var url = _this.lkpURI(_this.where);
                $('#button-'+code).addClass('disabled');
                $('#button-'+code).addClass('loading');
                $('#'+code).upload(url, {
                    _token      : $('#token').val(),
                    profile_id  : $('#profile_id').val()
                },
                function(success) {
                    if (success !== 'failed') {
                        var newText = "File Name: ".concat(success);
                        $('#button-'+code).addClass('delete');
                        $('#button-'+code).removeClass('blue');
                        $('#button-'+code).addClass('green');
                        $('#button-'+code).text('Delete');
                        $('#button-'+code).unbind('click');
                        $('#button-'+code).removeClass('disabled');
                        $('#button-'+code).removeClass('loading');
                        $('.content.'+code).text(newText);
                        $('#button-'+code).data('filename', success);
                        $('.ui.pass.message').hide('slow');
                        $('.ui.res.message').hide('slow');
                        _this.increment();
                        _this.deleteFile();
                    } else {
                        $('#button-'+code).addClass('error');
                        $('#button-'+code).removeClass('blue');
                        $('#button-'+code).addClass('red');
                        $('#button-'+code).text('Delete - Error File');
                        $('#button-'+code).unbind('click');
                        $('#button-'+code).removeClass('loading');
                        $('#button-'+code).removeClass('disabled');

                        $('.ui.button.error.red').on('click', function() {
                            var id = $(this).attr('id');
                            var n = id.replace("button-", "");
                            $('.item.'+n).remove();
                        });
                    }
                },
                function(prog, value) {
                });
            });
        }

        this.deleteFile = function() {
            var _this = this;
            $('.ui.button.delete').on('click', function() {
                var id          = $(this).attr('id');
                var url         = '/users/filesystem/delete-profile-uploads';
                var newId       = id.replace('button-', '');
                var fileName    = $(this).data('filename');
                _this.minus(newId);
                $.ajax({
                    type: 'POST',
                    url: url,
                    async : true,
                    data: {
                        _token      : $('#token').val(),
                        file        : fileName,
                        profile_id  : $('#profID').val(),
                        returnId    : newId
                    },
                    success: function(postData) {
                        if (postData !== 'Error') {
                            $('.item.'+postData).remove();
                            $('#'+newId).remove();
                        }
                    }
                });
            });
        }

        this.minus = function(code) {
            if (code.indexOf(this.keyID) >= 0) {
                --this.counter;
                $('#total_'+this.keyID).val(this.counter);
            }
        }

        this.increment = function() {
            $('#total_'+this.keyID).val(this.counter);
        }

        this.lkpURI = function(where) {
            var uri = null;
            switch(where) {
                case "uploads":
                    uri = "/users/filesystem/upload-profile-uploads";
                    break;
                case "uploads":
                    uri = "";
                    break;
                default:
                    break;
            }
            return uri;
        }

        this.htmlDep = function(type, code, name) {
            var sect1   = '<div class="item ' +code+ '"><div class="right floated content">';
            var sect2   = '<div class="ui blue button" id="button-'+code+'">Upload</div></div>';
            var sect3   = '<i class="ui avatar image book icon"></i><div class="content ' +code+ '">File Name: ' + name + '</div></div>';
            var html    = sect1 + sect2 + sect3;
            return html;
        }

    }

});
