$('document').ready(function() {

    var stripeResponseHandler = function(status, response) {

        if (response.error) {
            $('.ui.payment.negative.message p').text(response.error.message);
            $('.ui.payment.negative.message').show();
            $('.ui.make_payment.button').removeClass('loading disabled');
        } else {
            $('#stripe_token').val(response.id);

            $.ajax({
                type    : 'POST',
                url     : '/cases/submit-payment',
                async   : true,
                data    : {
                    _token          : $('#form_token').val(),
                    stripe_token    : $('#stripe_token').val(),
                    program         : $('#program').val(),
                    appId           : $('#appId').val()
                },
                success : function(data) {
                    $('#checkPin').removeClass('loading disabled');
                    if (data === '1') {
                        $('.ui.payment.success.message').show();
                        $('.ui.make_payment.button').removeClass('loading');
                        $('.ui.next.button').removeClass('disabled');
                    }
                },
                error: function() {
                    $('.ui.make_payment.button').removeClass('loading disabled');
                }
            });
        }
    }

    $('.ui.make_payment.submit.button').on('click', function() {
        Stripe.card.createToken($('#payment-form'), stripeResponseHandler);
        $('.ui.make_payment.button').addClass('loading disabled');
    });

});
