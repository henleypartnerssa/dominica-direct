$('document').ready(function() {

    delFamMember();
    editFamMember();

    // Click on Sibling Link
    $('#add_child_link').on('click', function(event) {
        event.preventDefault();
        $('#family_member_id').val('');
        $('.ui.child.form').form('clear');
        $('.ui.child.modal').modal('show');
    });

    // Add Sibling Modal Button Click
    $('.ui.add_child.button').on('click', function() {
        var form        = $('.ui.child.form');
        var validate    = formMemberValidation();

        if (validate) {
            form.addClass('loading');
            $.ajax({
                type    : 'POST',
                url     : '/users/profile/save-family-member',
                aysnc   : true,
                data    : {
                    _token                  : $('#child_token').val(),
                    profile_id              : $('#profile_id').val(),
                    family_member_id        : $('#family_member_id').val(),
                    first_name              : form.form('get value', 'first_name'),
                    last_name               : form.form('get value', 'last_name'),
                    date_of_birth_year      : form.form('get value', 'date_of_birth_year'),
                    date_of_birth_month     : form.form('get value', 'date_of_birth_month'),
                    date_of_birth_day       : form.form('get value', 'date_of_birth_day'),
                    place_of_birth          : form.form('get value', 'place_of_birth'),
                    citizenship             : form.form('get value', 'citizenship'),
                    occupation              : form.form('get value', 'occupation'),
                    gender                  : form.form('get value', 'gender'),
                    street1                 : form.form('get value', 'street1'),
                    street2                 : form.form('get value', 'street2'),
                    country                 : form.form('get value', 'country'),
                    town                    : form.form('get value', 'town'),
                    post_code               : form.form('get value', 'post_code'),
                    child                   : true
                },
                success: function(data, textStatus, jqXHR) {
                    var json = JSON.parse(data);
                    $('.item.'+json.id).remove();
                    $('.ui.child.list').append(htmlDep(json.id, json.name));
                    $('.ui.child.modal').modal('hide');
                    $('.ui.child.form').form('clear');
                    form.removeClass('loading');
                    delFamMember();
                    editFamMember();
                }
            });
        }

    });

    function editFamMember(type) {
        $('.ui.child.edit.button').on('click', function() {
            $('.ui.child.form').form('clear');
            $('.ui.child.modal').modal('show');
            $('.ui.child.form').addClass('loading');

            var id = $(this).attr('id');

            $.ajax({
                type    : "POST",
                url     : '/users/profile/find-family-member',
                async   : true,
                data    : {
                    _token  : $('#child_token').val(),
                    id      :  id
                },
                success: function(data) {
                    var json    = $.parseJSON(data);
                    var form    = $('.ui.child.form');
                    var split   = json.date_of_birth.split('-');
                    $('#family_member_id').val(json.family_member_id);
                    form.form('set value', 'gender', json.gender);
                    form.form('set value', 'first_name', json.first_name);
                    form.form('set value', 'last_name', json.last_name);
                    form.form('set value', 'date_of_birth_year', split[0]);
                    form.form('set value', 'date_of_birth_month', split[1]);
                    form.form('set value', 'date_of_birth_day', split[2]);
                    form.form('set value', 'place_of_birth', json.place_of_birth);
                    form.form('set value', 'citizenship', json.citizenship);
                    form.form('set value', 'occupation', json.occupation);
                    form.form('set value', 'street1', json.street1);
                    form.form('set value', 'street2', json.street2);
                    form.form('set value', 'country', json.country);
                    form.form('set value', 'town', json.town);
                    form.form('set value', 'post_code', json.postal_code);
                    $('.ui.child.form').removeClass('loading');
                }
            });
        });
    }

    function lkpDepType(id) {
        var typeStr = '';

        $.ajax({
            type    : "POST",
            url     : '/users/family/type',
            async   : false,
            data    : {
                _token      : $('#child_token').val(),
                id          :  id
            },
            success: function(data) {
                typeStr = data;
            }
        });

        return typeStr;
    }

    function delFamMember() {

        $('.ui.child.delete.button').on('click', function() {

            var id = $(this).attr('id');

            $.ajax({
                type    : 'POST',
                url     : '/users/profile/delete-family-member',
                async   : true,
                data    : {
                    _token      : $('#child_token').val(),
                    id          : id,
                    profile_id  : $('#profile_id').val()
                },
                success: function(data, textStatus, jqXHR) {
                    if (data === '1') {
                        $('.item.'+id).remove();
                    } else {
                        console.log('could not delete address');
                    }
                }
            });
        });
    }

    function htmlDep(id, name) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted child edit button" id="'+id+'">Edit</div><div class="ui red inverted child delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image user icon"></i><div class="content">' + name + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

    function formMemberValidation() {
        $('.ui.child.form').form({
            fields: {
                fist_name: {
                    identifier: 'fist_name',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter first name."
                        }
                    ]
                },
                last_name: {
                    identifier: 'last_name',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter last name."
                        }
                    ]
                },
                date_of_birth_year: {
                    identifier: 'date_of_birth_year',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter year of birth."
                        }
                    ]
                },
                date_of_birth_month: {
                    identifier: 'date_of_birth_month',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter month of birth."
                        }
                    ]
                },
                date_of_birth_day: {
                    identifier: 'date_of_birth_day',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter day of birth."
                        }
                    ]
                },
                place_of_birth: {
                    identifier: 'place_of_birth',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter place of birth."
                        }
                    ]
                },
                citizenship: {
                    identifier: 'citizenship',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter citizenship."
                        }
                    ]
                },
                street1: {
                    identifier: 'street1',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter street1."
                        }
                    ]
                },
                country : {
                    identifier: 'country',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter country."
                        }
                    ]
                },
                town : {
                    identifier: 'town',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter town."
                        }
                    ]
                },
                post_code : {
                    identifier: 'post_code',
                    rules: [
                        {
                            type: 'empty',
                            prompt: "Please enter postal code."
                        }
                    ]
                }


            },
            inline : true,
            on     : 'blur'
        });
        return $('.ui.child.form').form('is valid');
    }
});
