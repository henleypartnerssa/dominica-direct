$('document').ready(function() {

    delCharRef();
    editCharRef();

    var codes   = ["77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93"];
    var declObj = JSON.parse($('#declarations').val());

    if (declObj) {
        codes.forEach(function(c) {
            if (declObj) {
                var answer  = declObj['d'+c+'_answer'];
                if (answer === true) {
                    $('.ui.d'+c+'_yes.checkbox').checkbox('check');
                    $('.field.d'+c+'').show();
                } else {
                    $('.ui.d'+c+'_no.checkbox').checkbox('check');
                }
            }
        });
    }

    codes.forEach(function(c) {
        $('.ui.d'+c+'_yes.checkbox').checkbox({
            onChecked: function() {
                $('.ui.d'+c+'_no.checkbox').checkbox('uncheck');
                $('.field.d'+c+'').show('slow');
            }
        });
        $('.ui.d'+c+'_no.checkbox').checkbox({
            onChecked: function() {
                $('.ui.d'+c+'_yes.checkbox').checkbox('uncheck');
                $('.field.d'+c+'').hide('slow');
            }
        });
    });

    $('#add_charater_reference_link').on('click', function(e) {
        e.preventDefault();
        $('.ui.character_reference.modal').modal('show');
    });

    $('.ui.add_charater_reference.button').on('click', function() {
        var validate = formValidation();
        if (validate) {
            var form = $('.ui.character_reference.form');
            //form.addClass('loading');
            $.ajax({
                type: 'POST',
                url: '/users/profile/add-character-reference',
                async: true,
                data: {
                    _token      : $('#character_reference_token').val(),
                    profile_id  : $('#profile_id').val(),
                    char_id     : $('#char_id').val(),
                    full_name   : form.form('get value', 'full_name'),
                    years_known : form.form('get value', 'years_known'),
                    occupation  : form.form('get value', 'occupation'),
                    employer    : form.form('get value', 'employer'),
                    email       : form.form('get value', 'email'),
                    landline_no : form.form('get value', 'landline_no'),
                    mobile_no   : form.form('get value', 'mobile_no'),
                    work_no     : form.form('get value', 'work_no'),
                    cur_street1 : form.form('get value', 'cur_street1'),
                    cur_street2 : form.form('get value', 'cur_street2'),
                    cur_country : form.form('get value', 'cur_country'),
                    cur_town    : form.form('get value', 'cur_town'),
                    cur_post_code   : form.form('get value', 'cur_post_code')
                },
                success: function(data, textStatus, jqXHR) {
                    var json = JSON.parse(data);
                    $('.item.'+json.id).remove();
                    $('.ui.charator_references.selection.list').append(htmlAddr(json.id, json.name));
                    $('.ui.character_reference.modal').modal('hide');
                    form.form('clear');
                    form.removeClass('loading');
                    editCharRef();
                    delCharRef();
                }
            });
        }
    });

    function editCharRef() {
        $('.ui.charater_references.edit.button').on('click', function() {
            $('.ui.character_reference.form').form('clear');
            $('.ui.character_reference.modal').modal('show');
            $('.ui.character_reference.form').addClass('loading');

            var id = $(this).attr('id');

            $.ajax({
                type    : "POST",
                url     : '/users/profile/find-character-reference',
                async   : true,
                data    : {
                    _token  : $('#character_reference_token').val(),
                    id      :  id
                },
                success: function(data) {
                    var json    = $.parseJSON(data);
                    var form    = $('.ui.character_reference.form');
                    form.form('set value', 'char_id', json.char.character_ref_id);
                    form.form('set value', 'full_name', json.char.full_name);
                    form.form('set value', 'years_known', json.char.years_known);
                    form.form('set value', 'occupation', json.char.occupation);
                    form.form('set value', 'employer', json.char.employer);
                    form.form('set value', 'email', json.char.email);
                    form.form('set value', 'landline_no', json.char.landline_no);
                    form.form('set value', 'mobile_no', json.char.mobile_no);
                    form.form('set value', 'work_no', json.char.work_no);
                    form.form('set value', 'cur_street1', json.char.address.street1);
                    form.form('set value', 'cur_street2', json.char.address.street2);
                    form.form('set value', 'cur_country', json.char.address.country);
                    form.form('set value', 'cur_town', json.char.address.town);
                    form.form('set value', 'cur_post_code', json.char.address.postal_code);
                    $('.ui.character_reference.form').removeClass('loading');
                }
            });
        });
    }

    function delCharRef() {
        $('.ui.charater_references.delete.button').on('click', function() {
            var id = $(this).attr('id');
            $.ajax({
                type    : 'POST',
                url     : '/users/profile/delete-character-reference',
                async   : true,
                data    : {
                    _token      : $('#character_reference_token').val(),
                    id          : id,
                    profile_id  : $('#profile_id').val()
                },
                success: function(data, textStatus, jqXHR) {
                    if (data === '1') {
                        $('.item.'+id).remove();
                    } else {
                        console.log('could not delete address');
                    }
                }
            });
        });
    }

    function formValidation() {
        $('.ui.character_reference.form').form({
            fields: {
                full_name: {
                    identifier: 'full_name',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter full name'
                    }]
                },
                years_known: {
                    identifier: 'years_known',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter years known'
                    }]
                },
                occupation: {
                    identifier: 'occupation',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter occupation'
                    }]
                },
                employer: {
                    identifier: 'employer',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter employer'
                    }]
                },
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Please enter email'
                        },
                        {
                            type: 'email',
                            prompt: 'Incorrect email address'
                        }
                    ]
                },
                work_no: {
                    identifier: 'work_no',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter work number'
                    }]
                },
                mobile_no: {
                    identifier: 'mobile_no',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter mobile number'
                    }]
                },
                cur_street1: {
                    identifier: 'cur_street1',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter street'
                    }]
                },
                cur_country: {
                    identifier: 'cur_country',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter country'
                    }]
                },
                cur_town: {
                    identifier: 'cur_town',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter town'
                    }]
                },
                cur_post_code: {
                    identifier: 'cur_post_code',
                    rules: [{
                            type: 'empty',
                            prompt: 'Please enter postal code'
                    }]
                }
            },
            on: 'blur',
            inline: true
        });
        return $('.ui.character_reference.form').form('is valid');
    }

    function htmlAddr(id, name) {
        var sect1   = '<div class="item ' +id+ '"><div class="right floated content">';
        var sect2   = '<div class="ui blue inverted charater_references edit button" id="'+id+'">Edit</div><div class="ui red inverted charater_references delete button" id="'+id+'">Delete</div></div>';
        var sect3   = '<i class="ui avatar image user icon"></i><div class="content">' + name + '</div></div>';
        var html    = sect1 + sect2 + sect3;
        return html;
    }

});
