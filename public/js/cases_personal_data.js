$('document').ready(function() {

    if($('#res_counter').val() === '0') {
        $('.ui.same_add.checkbox').checkbox('check');
        $('#res_addr').hide();
    }

    if($('#military').val() !== '0') {
        $('.ui.military.checkbox').checkbox('check');
        $('.field.military').show();
    }

    if($('#offence').val() !== '0') {
        $('.ui.offence.checkbox').checkbox('check');
        $('.field.offence').show();
    }

    $('.ui.same_add.checkbox').checkbox({
        onChecked: function() {
            $('#res_addr').hide('slow');
        },
        onUnchecked: function() {
            $('#res_addr').show('slow');
        }
    });

    $('.ui.military.checkbox').checkbox({
        onChecked: function() {
            $('.field.military').show('slow');
        },
        onUnchecked: function() {
            $('.field.military').hide('slow');
        }
    });

    $('.ui.offence.checkbox').checkbox({
        onChecked: function() {
            $('.field.offence').show('slow');
        },
        onUnchecked: function() {
            $('.field.offence').hide('slow');
        }
    });

    var status = $('#marital_status').val();

    // DropDown Setting if Applicant Married or Not
    if (status === "Married" || status === "Widowed") {
            $('#marriage').fadeIn('slow');
            $('#divorce').fadeOut('slow');
        } else if (status === "Divorced") {
            $('#marriage').fadeIn('slow');
            $('#divorce').fadeIn('slow');
        } else {
            $('#marriage').fadeOut('slow');
            $('#divorce').fadeOut('slow');
        }

    // Run Validations on Form Next Submission
    $('.ui.next.button').on('click', function() {
        var mainVal = mainFormValidation();
        if (mainVal) {
            var status = $('.ui.personal-data.form').form('get value', 'marital_status');
            if (status === 'Married' || status === 'Widowed') {
                marriageInputsValidation();
            } else if (status === 'Divorced') {
                marriageInputsValidation();
                divorcedInputsValidation();
            }
            $('.ui.personal-data.form').addClass('loading');
        }
    });

    // Run Validations and set Flag on Form Next Submission
    $('.ui.save-data.button').on('click',  function() {
        $('#save').val('1');
        var mainVal = mainFormValidation();
        if (mainVal) {
            var status = $('.ui.personal-data.form').form('get value', 'marital_status');
            if (status === 'Married' || status === 'Widowed') {
                marriageInputsValidation();
            } else if (status === 'Divorced') {
                marriageInputsValidation();
                divorcedInputsValidation();
            }
            $('.ui.personal-data.form').addClass('loading');
        }
    });

    // Disply other textbox on "other" title selection
    $('#other').change(function() {
        var input = $(this).val();
        if (input === "Other") {
            $('#title_other_field').fadeIn('slow');
        } else {
            $('#title_other_field').fadeOut('slow');
        }
    });

    // Diplay marriage/disvorse textboxes derived from selection.
    $('#marital_status').change(function() {
        var input = $(this).val();

        if (input === "Married" || input === "Widowed") {
            $('#marriage').fadeIn('slow');
            $('#divorce').fadeOut('slow');
        } else if (input === "Divorced") {
            $('#marriage').fadeIn('slow');
            $('#divorce').fadeIn('slow');
        } else {
            $('#marriage').fadeOut('slow');
            $('#divorce').fadeOut('slow');
        }
    });

    // Base Form Validation
    function mainFormValidation() {
        $('.ui.personal-data.form').form({
            fields: {
                PA: {
                    indentifier: 'PA',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please select the type of applicant to complete application'
                    }]
                },
                title: {
                    indentifier: 'title',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please select a Title'
                    }]
                },
                legal_surname: {
                    indentifier: 'legal_surname',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your Legal last name'
                    }]
                },
                legal_first_name: {
                    indentifier: 'legal_first_names',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your Legal first name'
                    }]
                },
                gender: {
                    indentifier: 'gender',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your gender'
                    }]
                },
                marital_status: {
                    indentifier: 'marital_status',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your marital status'
                    }]
                },
                legal_bc_first_names: {
                    indentifier: 'legal_bc_first_names',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your Legal first name as it appears on your birth cirtificate'
                    }]
                },
                id_no: {
                    indentifier: 'id_no',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your identity number'
                    }]
                },
                id_issuing_country: {
                    indentifier: 'id_issuing_country',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your the issuing Country of your identity document'
                    }]
                },
                place_of_birth: {
                    indentifier: 'place_of_birth',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your place of birth'
                    }]
                },
                dob_year: {
                    indentifier: 'dob_year',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your year of brith'
                    }]
                },
                dob_month: {
                    indentifier: 'dob_month',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your month of birth'
                    }]
                },
                dob_day: {
                    indentifier: 'dob_day',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your day of birth'
                    }]
                },
                citizenship_at_birth: {
                    indentifier: 'citizenship_at_birth',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your Citizenship at birth'
                    }]
                },
                height: {
                    indentifier: 'height',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your height'
                    }]
                },
                eye_color: {
                    indentifier: 'eye_color',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your eye color'
                    }]
                },
                hair_color: {
                    indentifier: 'hair_color',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your hair color'
                    }]
                },
                weight: {
                    indentifier: 'weight',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your weight'
                    }]
                },
                cob: {
                    indentifier: 'cob',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please select your Country of birth'
                    }]
                },
                cab: {
                    indentifier: 'cab',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please select your Country at birth'
                    }]
                },
                permanent_telephone_no: {
                    indentifier: 'permanent_telephone_no',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your telephone number'
                    }]
                },
                mobile_telephone_no: {
                    indentifier: 'mobile_telephone_no',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your mobile number'
                    }]
                },
                personal_email_address: {
                    indentifier: 'personal_email_address',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your email address'
                    }]
                },
                cur_street1: {
                    indentifier: 'cur_street1',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address street'
                    }]
                },
                cur_country: {
                    indentifier: 'cur_country',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address country'
                    }]
                },
                cur_town: {
                    indentifier: 'cur_town',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address town or city'
                    }]
                },
                cur_post_code: {
                    indentifier: 'cur_post_code',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address postal code'
                    }]
                },
                cur_since_month: {
                    indentifier: 'cur_since_month',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address since date month'
                    }]
                },
                cur_since_year: {
                    indentifier: 'cur_since_year',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter address since date year'
                    }]
                }
            },
            on: 'blur',
            inline: true
        });
        return $('.ui.personal-data.form').form('is valid');
    }

    // Marriage Inputs Validations
    function marriageInputsValidation() {
        $('.ui.personal-data.form').form({
            fields: {
                mar_year: {
                    indentifier: 'mar_year',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter the year of your marriage.'
                    }]
                },
                mar_month: {
                    indentifier: 'mar_month',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter the month of your marriage.'
                    }]
                },
                mar_day: {
                    indentifier: 'mar_day',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter the day of your marriage.'
                    }]
                },
                marriage_place: {
                    indentifier: 'marriage_place',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your place of marriage'
                    }]
                }
            },
            on: 'blur',
            inline: true

        });
        return $('.ui.personal-data.form').form('is valid');
    }

    // Divirse Inputs validations
    function divorcedInputsValidation() {
        $('.ui.personal-data.form').form({
            fields: {
                div_year: {
                    indentifier: 'div_year',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter the year of your divorce.'
                    }]
                },
                div_month: {
                    indentifier: 'div_month',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter the month of your divorce.'
                    }]
                },
                div_day: {
                    indentifier: 'div_day',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter the day of your divorce.'
                    }]
                },
                div_place: {
                    indentifier: 'div_place',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter your place of divorce'
                    }]
                }
            },
            on: 'blur',
            inline: true
        });
        return $('.ui.personal-data.form').form('is valid');
    }

});
