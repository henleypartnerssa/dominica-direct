$('document').ready(function() {

    var d2Upload    = new FileUploader("D2 - Fingerprint & Signature","D2", "notary");
    var d3Upload    = new FileUploader("D3 - Medical Questionnaire","D3", "notary");
    var f12Upload   = new FileUploader("Form 12","F12", "notary");

    d2Upload.uploadFile();
    d2Upload.deleteFile();
    d3Upload.uploadFile();
    f12Upload.uploadFile();

});
