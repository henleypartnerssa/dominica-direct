$('document').ready(function() {

    $('.ui.cell_number.dropdown').dropdown('clear');

    var token   = $('.ui.form').find('#_token').val();

    var i = 0;

    $('#sendButton').on('click', function() {

        var code    = encodeURIComponent( '+' + $('input#code').val());
        var num     = $('input#number').val();
        var cell_no = code.concat(num);

        $('#sendButton').addClass('loading');
        $('#sendButton').addClass('disabled');

        $.ajax({

            type: 'POST',
            url: '/users/mobile/number',
            async: true,
            data: '_token='+ token +'&mobile=' + cell_no,
            success: function(data, textStatus, jqXHR) {
                if (data === '1') {
                    window.location.href = '/users/pin';
                } else if (data === '2') {
                    $('.ui.message').removeClass("hidden");
                    $('#sendButton').removeClass("loading");
                    $('#sendButton').removeClass('disabled');
                } else {
                    $('#sendButton').removeClass("loading");
                    $('#sendButton').removeClass('disabled');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('#sendButton').removeClass("loading");
                $('#sendButton').removeClass('disabled');
                console.log(errorThrown);
            }
        });

    });

});
