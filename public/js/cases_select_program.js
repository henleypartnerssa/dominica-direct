$('document').ready(function() {

    var val     = $('#has_PA').val();
    var val2    = $('#has_Spouse').val();
    var val3    = $('#has_Child').val();
    var error   = false;

    $('#select-form').submit(function(e) {
        if (error) {
            $('.ui.negative.message').show();
            e.preventDefault();
        }
    });

    $('.ui.selection.dropdown').dropdown({
        onChange: function(value, text, $choice) {
            if (value === "Principal Applicant (PA)" && val === '1') {
                $('.ui.negative.message').show();
                error = true;
            } else if (value === "Spouse of PA" && val2 === '1') {
                $('.ui.negative.message').show();
                error = true;
            }else if (value === "Dependant of PA" && val3 === '1') {
                $('.ui.negative.message').show();
                error = true;
            } else if (value === "Dependant of PA") {
                $('#dep').show();
                $('.ui.negative.message').hide();
                error = false;
            } else {
                $('.ui.negative.message').hide();
                error = false;
            }
        }
    });

});
