$('document').ready(function() {
    var fds = $('#father_deseased').val();
    var mds = $('#mother_deseased').val();

    // On Page Load validate if father not deceased then show address
    if (fds === 'No') {
        $('#father_res_addr').fadeIn('slow');
    }
    // On Page Load validate if father not deceased then show address
    if(mds === 'No') {
        $('#mother_res_addr').fadeIn('slow');
    }

    // On father deceased dropdown value change
    $('#father_deseased').change(function(e) {
        var input = $(this).val();
        if (input === 'No') {
            $('#father_res_addr').fadeIn('slow');
        } else {
            $('#father_res_addr').fadeOut('slow');
        }

    });

    // On father deceased dropdown value change
    $('#mother_deseased').change(function(e) {
        var input = $(this).val();
        if (input === 'No') {
            $('#mother_res_addr').fadeIn('slow');
        } else {
            $('#mother_res_addr').fadeOut('slow');
        }
    });

    $('#family-member-form').on('submit', function(event) {
        event.preventDefault();
        $(this).addClass('loading');
        var val = mainFormValidation();
        if (val) {
            $(this)[0].submit();
        } else {
            $(this).removeClass('loading');
        }
    });

    function mainFormValidation() {
        var form = $('.ui.family-member.form');
        form.form({
            fields: {
                father_fam_name: {
                    identifier  : 'father_fam_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter family name'
                        }
                    ]
                },
                father_first_name: {
                    identifier  : 'father_first_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter first names'
                        }
                    ]
                },
                father_dob_year: {
                    identifier  : 'father_dob_year',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter year'
                        }
                    ]
                },
                father_dob_month: {
                    identifier  : 'father_dob_month',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter month'
                        }
                    ]
                },
                father_dob_day: {
                    identifier  : 'father_dob_day',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter day'
                        }
                    ]
                },
                father_pob: {
                    identifier  : 'father_pob',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter palce of birth'
                        }
                    ]
                },
                father_citizenship: {
                    identifier  : 'father_citizenship',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter citizenship'
                        }
                    ]
                },
                father_occupation: {
                    identifier  : 'father_occupation',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter occupation'
                        }
                    ]
                },
                mother_fam_name: {
                    identifier  : 'mother_fam_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter family name'
                        }
                    ]
                },
                mother_first_name: {
                    identifier  : 'mother_first_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter first names'
                        }
                    ]
                },
                mother_dob_year: {
                    identifier  : 'mother_dob_year',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter year'
                        }
                    ]
                },
                mother_dob_month: {
                    identifier  : 'mother_dob_month',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter month'
                        }
                    ]
                },
                mother_dob_day: {
                    identifier  : 'mother_dob_day',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter day'
                        }
                    ]
                },
                mother_pob: {
                    identifier  : 'mother_pob',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter palce of birth'
                        }
                    ]
                },
                mother_citizenship: {
                    identifier  : 'mother_citizenship',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter citizenship'
                        }
                    ]
                },
                mother_occupation: {
                    identifier  : 'mother_occupation',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter occupation'
                        }
                    ]
                }
            },
            inline : true,
            on     : 'blur'
        });
        return form.form('is valid');
    }

});
